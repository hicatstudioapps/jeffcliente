package es.coiasoft.mrjeff;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import es.coiasoft.mrjeff.HomeActivity;
import es.coiasoft.mrjeff.MenuActivity;
import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.ValidateVersion;


public class CallActivity extends MenuActivity {

    protected Activity instance;
    protected TextView textCart;
    protected ImageView btnCart;
    protected LinearLayout btnCall;
    protected LinearLayout buttonWhatsapp;
    protected LinearLayout btnEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.callme;
        finishActivity = Boolean.FALSE;
        super.onCreate(savedInstanceState);

    }

    public void initActivity(){
        finishActivity = Boolean.FALSE;
        instance = this;
        textCart = (TextView)findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView)findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);
        btnCall = (LinearLayout)findViewById(R.id.buttonCall);
        btnEmail = (LinearLayout)findViewById(R.id.buttonSendMail);
        buttonWhatsapp = (LinearLayout)findViewById(R.id.buttonWhatsapp);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCall();
            }
        });
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSendMail();
            }
        });
        buttonWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWhatsappConversation();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        initActivity();
    }
    
    @Override
    protected void setContentView(){
        setContentView(R.layout.activity_call);
    }


    protected int posMenu(){
        return 4;
    }

    @Override
    public void onBackPressed() {
        finishActivity = Boolean.TRUE;
        Intent show = new Intent(this, HomeActivity.class);
        startActivity(show);
    }


    public void btnCall(){
        finishActivity = Boolean.FALSE;
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(MrJeffConstant.CALL.phoneMrJeff));
        startActivity(callIntent);
    }


    public void btnSendMail(){
        finishActivity = Boolean.FALSE;
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{MrJeffConstant.CALL.emailMrJeff});
        i.putExtra(Intent.EXTRA_SUBJECT, instance.getApplicationContext().getResources().getString(R.string.emailsubject));
        i.putExtra(Intent.EXTRA_TEXT, instance.getApplicationContext().getResources().getString(R.string.emailbody));
        try {
            startActivity(Intent.createChooser(i, instance.getApplicationContext().getResources().getString(R.string.emailsend)));
        } catch (android.content.ActivityNotFoundException ex) {
            new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.emailerror).setMessage(R.string.emailerrorBody).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    setResult(RESULT_OK);
                }
            }).show();
        }
    }

    public void openWhatsappConversation(){
        finishActivity = Boolean.FALSE;
        Uri uri = Uri.parse("smsto:" + "+34644446163");
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.putExtra("sms_body", "Hola hola");
        i.setPackage("com.whatsapp");
        startActivity(i);
    }

    protected String getNameScreen(){
        return MrJeffConstant.LOCALYTICS.SCREEN_CALL;
    }

}
