package es.coiasoft.mrjeff;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.localytics.android.Localytics;

import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.Utils.DialogAssessment;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.ValidateVersion;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import io.fabric.sdk.android.Fabric;

/**
 * Created by linovm on 10/10/15.
 */
public abstract class ConnectionActivity extends AppCompatActivity {

    protected Boolean finishActivity = Boolean.TRUE;

    public int getNoLoad() {
        return noLoad;
    }

    public int noLoad=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        finishActivity = Boolean.FALSE;
        super.onCreate(savedInstanceState);
        //ExceptionHandler.register(this,"Error jeff","leoperezortiz@gmail.com");
        Fabric.with(this, new Answers());
        Fabric.with(this, new Crashlytics());
        setContentViewActivity();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeConnection = connectivityManager.getActiveNetworkInfo();
        boolean isOnline = (activeConnection != null) && activeConnection.isConnected();
        if(!isOnline){
            noLoad=-1;
            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorConectionTitle).setMessage(R.string.modalErrorConectionSubTitle).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    setResult(RESULT_OK);
                    finish();
                }
            }).show();
        }

        if(!CustomerStorage.getInstance(getApplicationContext()).isLogged()){
            try {
                LoginManager.getInstance().logOut();
            }catch (Exception e){

            }
        }


    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    protected abstract void setContentViewActivity();

    @Override
    protected void onPause() {
        super.onPause();
        if(finishActivity) {
            AppEventsLogger.deactivateApp(this);
            finish();
        }
    }

    @Override
    protected void onResume() {
            super.onResume();
            AppEventsLogger.activateApp(this);
            Localytics.tagScreen(getNameScreen());
            ValidateVersion validate = new ValidateVersion();
            validate.sendConsult(this);
    }


    @Override
    public void startActivity (Intent intent){
       // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
        super.startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(finishActivity) {
            Runtime.getRuntime().gc();
            System.gc();
        }
    }

    public Boolean getFinishActivity() {
        return finishActivity;
    }

    protected abstract String getNameScreen();

    public void setFinishActivity(Boolean finishActivity) {
        this.finishActivity = finishActivity;
    }
}
