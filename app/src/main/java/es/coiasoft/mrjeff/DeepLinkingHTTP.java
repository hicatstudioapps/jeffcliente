package es.coiasoft.mrjeff;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.orders.response.Line_items;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.orders.send.Order;
import es.coiasoft.mrjeff.domain.orders.send.Product;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.worker.FindOrderWorker;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.util.FindStateOrder;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;

/**
 * Created by Paulino on 23/05/2016.
 */
public class DeepLinkingHTTP extends MenuActivity implements Observer {

    private Handler handler = new Handler();
    private ProgressDialog progressDialog;
    protected TextView textCart;
    protected ImageView btnCart;
    protected Activity instance;
    BranchUniversalObject o=null;
    String canonicalURL="";
    @Override
    public void onStart() {
        super.onStart();

        Branch branch = Branch.getInstance();
        branch.initSession(new Branch.BranchUniversalReferralInitListener() {
            @Override
            public void onInitFinished(BranchUniversalObject branchUniversalObject, LinkProperties linkProperties, BranchError error) {
                if (error == null) {
                    if(branchUniversalObject!=null){
                        canonicalURL=branchUniversalObject.getCanonicalUrl();
                        if(TextUtils.isEmpty(canonicalURL)) {
                            if (branchUniversalObject.getMetadata().get("$type").equals("orderID")) {
                                canonicalURL = branchUniversalObject.getMetadata().get("$baseURL") + branchUniversalObject.getMetadata().get("orderID");
                                loadPage();
                            } else if (branchUniversalObject.getMetadata().get("$type").equals("coupon")) {
                                Intent other = new Intent(DeepLinkingHTTP.this, DeepLinkingMJeffSchema.class);
                                other.putExtra("url", branchUniversalObject.getMetadata().get("$baseURL") + branchUniversalObject.getMetadata().get("couponID"));
                                startActivity(other);
                                finish();
                            } else if (branchUniversalObject.getMetadata().get("$type").equals("cart-coupon")) {
                                Intent other = new Intent(DeepLinkingHTTP.this, DeepLinkingMJeffSchema.class);
                                other.putExtra("url", "mrjeff://edit-order/"+ branchUniversalObject.getMetadata().get("orderID") + "/" + branchUniversalObject.getMetadata().get("couponID"));
                                startActivity(other);
                                finish();
                            } else if (branchUniversalObject.getMetadata().get("$type").equals("rate")) {
                                Intent other = new Intent(DeepLinkingHTTP.this, DeepLinkingMJeffSchema.class);
                                other.putExtra("url", branchUniversalObject.getMetadata().get("$baseURL") + "/" + branchUniversalObject.getMetadata().get("orderID"));
                                startActivity(other);
                                finish();
                            } else if (branchUniversalObject.getMetadata().get("$type").equals("edit-order")) {
                                Intent other = new Intent(DeepLinkingHTTP.this, DeepLinkingMJeffSchema.class);
                                other.putExtra("url", "mrjeff://edit-order/" + branchUniversalObject.getMetadata().get("orderID"));
                                startActivity(other);
                                finish();
                            }
                        }
                        else
                        loadPage();
                    }
                    else
                       // canonicalURL= referringParams.getString("$canonical_url");
                        loadPage();

                    Log.i("BranchConfig", "deep link data: " + linkProperties.toString());
                }
                else
                    loadPage();
            }

        }, this.getIntent().getData(), this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.empty;
        super.onCreate(savedInstanceState);
        textCart = (TextView) findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView) findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);
        progressDialog = ProgressDialogMrJeff.createAndStart(this);
        this.instance = this;
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_deep_linking);
    }

    @Override
    protected int posMenu() {
        return -1;
    }

    @Override
    protected String getNameScreen() {
        return MrJeffConstant.LOCALYTICS.SCREEN_DEEP_LINKING;
    }

    private void loadPage() {
        //agregue
        if(CustomerStorage.getInstance(null).getCustomer()==null)
            finish();
        //
        Intent intent = getIntent();
        if (intent != null && intent.getData() != null) {
            String url = TextUtils.isEmpty(canonicalURL) ? intent.getData().toString(): canonicalURL;
            if (url != null && url.contains(MrJeffConstant.DEEP_LINKING.PATTER_PRODUCT)) {
                launchProduct(url);
            } else if (url != null && url.contains(MrJeffConstant.DEEP_LINKING.PATTER_ORDER)) {
                launchOrder(url);
            } else if (url != null && url.contains(MrJeffConstant.DEEP_LINKING.PATTERN_SUSCRIPCION)) {
                launchSuscripcion(url);
            } else {
                launchDefault();
            }

        } else {
            launchDefault();
        }
    }


    private void launchDefault() {
        if (CustomerStorage.getInstance(this).isLogged()) {
            Intent myIntent = new Intent(this, HomeActivity.class);
            startActivityForResult(myIntent, 0);
        } else {
            Intent myIntent = new Intent(this, LoginNewAccountActivity.class);
            myIntent.putExtra(MrJeffConstant.LOGIN_EXTRA.LOGIN_ATTR, MrJeffConstant.LOGIN_EXTRA.HOME_LOGIN);
            startActivityForResult(myIntent, 0);
        }
    }

    private void launchSuscripcion(String url) {
        Intent show = new Intent(this, NoLavoActivity.class);
        show.putExtra(MrJeffConstant.INTENT_ATTR.IDPRODUCT, ProductStorage.getInstance(instance).getCategory("nolavo").get(0).getId());
        show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 2);
        startActivity(show);
    }

    private void launchProduct(String url) {
        if(url.contains("?branch")){
            url= url.substring(0,url.indexOf("?"));
        }
        Integer idProduct = ProductStorage.getInstance(this).findProductByUrl(url);
        if(idProduct != null) {
            Intent show = null;
            if(isNoLavo(idProduct)) {
                show = new Intent(this, NoLavoActivity.class);
            } else {
                show = new Intent(this, OneProductActivity.class);
            }
            show.putExtra(MrJeffConstant.INTENT_ATTR.IDPRODUCT, idProduct);
            show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 2);
            startActivity(show);
        }else {
            Intent myIntent = new Intent(this, Error404Activity.class);
            startActivity(myIntent);
        }
    }

    private boolean isNoLavo(Integer idProduct){
        boolean result = false;
        Products product = ProductStorage.getInstance(this).getProduct(idProduct);
        if(product != null && product.getTags() != null && !product.getTags().isEmpty()){
            for(String tag: product.getTags()){
                result = result || (tag != null && tag.trim().equalsIgnoreCase("nolavo"));
            }
        }

        return result;
    }

    private void launchOrder(String url) {
        String idOrderUrl = url.substring(url.indexOf(MrJeffConstant.DEEP_LINKING.PATTER_ORDER) + MrJeffConstant.DEEP_LINKING.PATTER_ORDER.length());
        idOrderUrl= idOrderUrl.replaceAll("/", "");
        if(idOrderUrl.contains("?")){
            idOrderUrl= idOrderUrl.substring(0,idOrderUrl.indexOf("?"));
        }
        try {
            Integer idOrder = Integer.valueOf(idOrderUrl);
            FindOrderWorker worker = new FindOrderWorker(idOrder);
            worker.addObserver(this);
            Thread thread = new Thread(worker);
            thread.start();
        } catch (Exception e) {

        }
    }

    @Override
    public void update(Observable observable,final  Object data) {
        if (observable instanceof FindOrderWorker) {
            if (data != null &&
                    data instanceof Orders) {
                Thread threadNotiy = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Integer idCustomerOrder = ((Orders) data).getCustomer_id();
                                Integer idCustomer = CustomerStorage.getInstance(null).getCustomer().getId();
                                if (idCustomer.equals(idCustomerOrder)) {
                                    //Pertenece al mismo usuario
                                    if ("pending".equalsIgnoreCase(((Orders) data).getStatus())) {
                                        //Esta pendiente
                                        if (allProductInApp((Orders) data)) {
                                            showOrder((Orders) data);
                                        } else {
                                            Intent myIntent = new Intent(instance, Error404Activity.class);
                                            startActivity(myIntent);
                                        }
                                    } else {
                                        initActivityDetail((Orders) data);
                                    }
                                } else {
                                    Intent myIntent = new Intent(instance, ErrorUserActivity.class);
                                    startActivity(myIntent);
                                }
                            }
                        });
                    }
                });

                threadNotiy.start();
            } else {
                Thread threadNotiy = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Intent myIntent = new Intent(instance, Error404Activity.class);
                                startActivity(myIntent);
                            }
                        });
                    }
                });
                threadNotiy.start();
            }
        }
    }

    private void showOrder(Orders data) {
        Order order = new Order(data);
        OrderStorage.getInstance(this).setOrderSend(order);
        es.coiasoft.mrjeff.domain.orders.response.Order orderResponse = new es.coiasoft.mrjeff.domain.orders.response.Order(data);
        OrderStorage.getInstance(this).setOrder(orderResponse);
        OrderStorage.getInstance(this).saveJson();
        setProduct(data);
        Intent show = new Intent(this, OrderAddressActivity.class);
        show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, false);
        startActivity(show);

    }

    private void setProduct(Orders data) {
        OrderProductsStorage.getInstance(this).clear();
        if (data != null && data.getLine_items() != null &&
                !data.getLine_items().isEmpty()) {
            for (Line_items item : data.getLine_items()) {
                OrderProductsStorage.getInstance(this).addProduct(item.getProduct_id(), item.getQuantity());
            }
        }

        OrderProductsStorage.getInstance(this).saveJson();
    }

    private boolean allProductInApp(Orders order) {
        boolean result = true;
        if (order != null && order.getLine_items() != null &&
                !order.getLine_items().isEmpty()) {
            for (Line_items item : order.getLine_items()) {
                result = result && ProductStorage.getInstance(this).getProduct(item.getProduct_id()) != null;
            }
        } else {
            result = false;
        }

        return result;
    }

    private void initActivityDetail(Orders data) {
        EnumStateOrder state = FindStateOrder.getStateOrder(data);
        Intent myIntent = null;
        switch (state) {
            case FINALLY:
            case DELIVERY:
                myIntent = new Intent(this, MyOrderActivity.class);
                break;
            case CONFIRM:
            case WASHING:
            case PICKUP:
                myIntent = new Intent(this, MyEditOrderActivity.class);
                break;

        }
        Gson gson = new Gson();
        String json = gson.toJson(data, Orders.class);
        myIntent.putExtra(MrJeffConstant.INTENT_ATTR.ORDER, json);
        startActivityForResult(myIntent, 0);
    }

    @Override
    public void startActivity(Intent intent){
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        super.startActivity(intent);
    }
}
