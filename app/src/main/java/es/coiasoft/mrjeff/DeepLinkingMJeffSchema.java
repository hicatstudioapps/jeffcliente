package es.coiasoft.mrjeff;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.orders.response.Line_items;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.orders.send.Order;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.worker.FindCouponWorker;
import es.coiasoft.mrjeff.manager.worker.FindOrderWorker;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.util.FindStateOrder;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/**
 * Created by Paulino on 23/05/2016.
 */
public class DeepLinkingMJeffSchema extends MenuActivity implements Observer {

    private Handler handler = new Handler();
    private ProgressDialog progressDialog;
    protected TextView textCart;
    protected ImageView btnCart;
    protected DeepLinkingMJeffSchema instance;
    protected String coupon;
    protected Boolean haveOrder = Boolean.FALSE;
    protected Orders order;
    private GoogleApiClient client;
    String url;

    @Override
    public void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
//
//        Branch branch = Branch.getInstance();
//        if(branch != null && this.getIntent() != null && this.getIntent().getData() != null) {
//            branch.initSession(new Branch.BranchReferralInitListener() {
//                @Override
//                public void onInitFinished(JSONObject referringParams, BranchError error) {
//                    try {
//                        url="mrjeff://"+referringParams.getString("$deeplink_path");
//                        loadPage();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }, this.getIntent().getData(), this);
//        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.empty;
        super.onCreate(savedInstanceState);
        textCart = (TextView) findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView) findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);
        progressDialog = ProgressDialogMrJeff.createAndStart(this);
        this.instance = this;
        loadPage();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_deep_linking);
    }

    @Override
    protected int posMenu() {
        return -1;
    }

    @Override
    protected String getNameScreen() {
        return MrJeffConstant.LOCALYTICS.SCREEN_DEEP_LINKING;
    }

    private void loadPage() {
        //agregue
        if (CustomerStorage.getInstance(null).getCustomer() == null)
            finish();
        //
        Intent intent = getIntent();
        if (intent != null) {
            String url = intent.getStringExtra("url");
            if (TextUtils.isEmpty(url))
                url = intent.getData().toString();
            if (url != null && url.contains(MrJeffConstant.DEEP_LINKING.PATTER_COUPON)) {//CUPON
                haveOrder = Boolean.FALSE;
                launchCoupon(url);
            } else if (url != null && url.contains(MrJeffConstant.DEEP_LINKING.PATTER_COUPON_ORDER)) {//cupon sobre orden
                haveOrder = Boolean.TRUE;
                launchOrder(url, true);
            } else if (url != null && url.contains(MrJeffConstant.DEEP_LINKING.PATTER_TIMETABLE)) {
                String[] part = url.split("/");
                try {
                    if (part.length==4) {
                        launchOrder(url, false);
                    } else {
                        int number= Integer.parseInt(part[part.length-1]);
                        launchOrder(url, true);
                    }
                } catch (Exception ex) {
                    haveOrder = Boolean.TRUE;
                    launchOrder(url, true);
                }
            } else if (url != null && url.contains(MrJeffConstant.DEEP_LINKING.PATTER_RATE)) {
                launchRate(url);
            } else {
                launchDefault();
            }
        } else {
            launchDefault();
        }
    }

    private void launchRate(String url) {
        String idOrder = url.trim().substring(MrJeffConstant.DEEP_LINKING.PATTER_COUPON.length());
        try {
            if (idOrder != null) {
                Integer id = Integer.valueOf(idOrder);
                Intent myIntent = new Intent(this, MyWashActivity.class);
                myIntent.putExtra(MyWashActivity.ORDER_ATTR,id);
                startActivity(myIntent);
            } else {
                Intent myIntent = new Intent(this, MyWashActivity.class);
                startActivity(myIntent);
            }
        }catch (Exception e){
            Intent myIntent = new Intent(this, MyWashActivity.class);
            startActivity(myIntent);
        }

    }

    private void launchEditOrder() {
        Intent myIntent = new Intent(this, MyWashActivity.class);
        startActivityForResult(myIntent, 0);
    }

    private void launchDefault() {
        if (CustomerStorage.getInstance(this).isLogged()) {
            Intent myIntent = new Intent(this, HomeActivity.class);
            startActivityForResult(myIntent, 0);
        } else {
            Intent myIntent = new Intent(this, LoginNewAccountActivity.class);
            myIntent.putExtra(MrJeffConstant.LOGIN_EXTRA.LOGIN_ATTR, MrJeffConstant.LOGIN_EXTRA.HOME_LOGIN);
            startActivityForResult(myIntent, 0);
        }
    }

    private void launchCoupon(String url) {
        //String baseUrl=url.substring(0,url.indexOf("?"));
        String couponCode = url.trim().substring(MrJeffConstant.DEEP_LINKING.PATTER_COUPON.length(),url.length());
        if (couponCode != null) {
            this.coupon = couponCode;
            FindCouponWorker worker = new FindCouponWorker(couponCode);
            worker.addObserver(this);
            Thread thread = new Thread(worker);
            thread.start();
        } else {
            Intent myIntent = new Intent(this, Error404Activity.class);
            startActivity(myIntent);
        }
    }

    private void launchOrder(String url,boolean cupon) {
//        String urlIds = url.substring(MrJeffConstant.DEEP_LINKING.PATTER_COUPON_ORDER.length());
        String[] ids = url.split("/");
        String orderId;
        if(cupon){
        this.coupon= ids[ids.length-1];
        orderId= ids[ids.length-2];}
        else{
            orderId= ids[ids.length-1];
        }
        try {
            Integer idOrder = Integer.valueOf(orderId);
            FindOrderWorker worker = new FindOrderWorker(idOrder);
            worker.addObserver(this);
            Thread thread = new Thread(worker);
            thread.start();
        } catch (Exception e) {
            Intent myIntent = new Intent(this, Error404Activity.class);
            startActivity(myIntent);
        }
    }


    @Override
    public void update(Observable observable, final Object data) {
        if (observable instanceof FindOrderWorker) {
            if (data != null &&
                    data instanceof Orders) {
                Thread threadNotiy = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Integer idCustomerOrder = ((Orders) data).getCustomer_id();
                                Integer idCustomer = CustomerStorage.getInstance(null).getCustomer().getId();
                                if (idCustomer.equals(idCustomerOrder)) {
                                    if ("pending".equalsIgnoreCase(((Orders) data).getStatus())) {
                                        if (allProductInApp((Orders) data)) {
                                            if(TextUtils.isEmpty(coupon))
                                                showOrder((Orders)data);
                                            else {
                                            order = (Orders) data;
                                            FindCouponWorker worker = new FindCouponWorker(coupon);
                                            worker.addObserver(instance);
                                            Thread thread = new Thread(worker);
                                            thread.start();}
                                        } else {
                                            Intent myIntent = new Intent(instance, Error404Activity.class);
                                            startActivity(myIntent);
                                        }
                                    } else {
                                        initActivityDetail((Orders) data);
                                    }
                                } else {
                                    Intent myIntent = new Intent(instance, ErrorUserActivity.class);
                                    startActivity(myIntent);
                                }
                            }
                        });
                    }
                });

                threadNotiy.start();
            } else {
                Thread threadNotiy = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Intent myIntent = new Intent(instance, Error404Activity.class);
                                startActivity(myIntent);
                            }
                        });
                    }
                });
                threadNotiy.start();
            }
        } else if (observable instanceof FindCouponWorker) {
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (data instanceof Boolean && (Boolean) data) {
                                CustomerStorage.getInstance(getApplicationContext()).setCoupon(coupon);
                                if (haveOrder) {
                                    showOrder(order);
                                } else {
                                    Intent myIntent = new Intent(instance, HomeActivity.class);
                                    myIntent.putExtra(MrJeffConstant.INTENT_ATTR.HOME_MESSAGE, coupon);
                                    startActivityForResult(myIntent, 0);
                                }
                            } else {
                                Intent myIntent = new Intent(instance, Error404Activity.class);
                                startActivity(myIntent);
                            }
                        }
                    });
                }
            });

            threadNotiy.start();
        }
    }

    private void showOrder(Orders data) {
        Order order = new Order(data);
        OrderStorage.getInstance(this).setOrderSend(order);
        es.coiasoft.mrjeff.domain.orders.response.Order orderResponse = new es.coiasoft.mrjeff.domain.orders.response.Order(data);
        OrderStorage.getInstance(this).setOrder(orderResponse);
        OrderStorage.getInstance(this).saveJson();
        setProduct(data);
        Intent show = new Intent(this, OrderAddressActivity.class);
        show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, false);
        show.putExtra(MrJeffConstant.INTENT_ATTR.HOME_MESSAGE, coupon);
        startActivity(show);
    }

    private void setProduct(Orders data) {
        OrderProductsStorage.getInstance(this).clear();
        if (data != null && data.getLine_items() != null &&
                !data.getLine_items().isEmpty()) {
            for (Line_items item : data.getLine_items()) {
                OrderProductsStorage.getInstance(this).addProduct(item.getProduct_id(), item.getQuantity());
            }
        }

        OrderProductsStorage.getInstance(this).saveJson();
    }

    private boolean allProductInApp(Orders order) {
        boolean result = true;
        if (order != null && order.getLine_items() != null &&
                !order.getLine_items().isEmpty()) {
            for (Line_items item : order.getLine_items()) {
                result = result && ProductStorage.getInstance(this).getProduct(item.getProduct_id()) != null;
            }
        } else {
            result = false;
        }

        return result;
    }

    private void initActivityDetail(Orders data) {
        EnumStateOrder state = FindStateOrder.getStateOrder(data);
        Intent myIntent = null;
        switch (state) {
            case FINALLY:
            case DELIVERY:
                myIntent = new Intent(this, MyOrderActivity.class);
                break;
            case CONFIRM:
            case WASHING:
            case PICKUP:
                myIntent = new Intent(this, MyEditOrderActivity.class);
                break;

        }
        Gson gson = new Gson();
        String json = gson.toJson(data, Orders.class);
        myIntent.putExtra(MrJeffConstant.INTENT_ATTR.ORDER, json);
        startActivityForResult(myIntent, 0);
    }

    @Override
    public void startActivity(Intent intent) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        super.startActivity(intent);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DeepLinkingMJeffSchema Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://cupon/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://es.coiasoft.mrjeff/mrjeff/cupon/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
