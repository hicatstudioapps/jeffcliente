package es.coiasoft.mrjeff;

import android.app.Activity;
import android.os.Bundle;

import es.coiasoft.mrjeff.Utils.DialogAssessment;

/**
 * Created by Paulino on 04/02/2016.
 */
public class DialogRateActivity extends Activity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DialogAssessment.showDialog(this);
    }
}
