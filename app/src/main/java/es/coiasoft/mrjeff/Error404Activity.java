package es.coiasoft.mrjeff;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.orders.response.Line_items;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.orders.send.Order;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.worker.FindOrderWorker;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.util.FindStateOrder;

/**
 * Created by Paulino on 23/05/2016.
 */
public class Error404Activity extends MenuActivity{

    private Handler handler = new Handler();
    private ProgressDialog progressDialog;
    protected TextView textCart;
    protected ImageView btnCart;
    protected LinearLayout btnReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.titleerror;
        super.onCreate(savedInstanceState);
        textCart = (TextView) findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView) findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);
        btnReturn = (LinearLayout) findViewById(R.id.btnReturn);
        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent show = new Intent(getApplicationContext(), HomeActivity.class);
                show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 2);
                startActivity(show);
            }
        });
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_404);
    }

    @Override
    protected int posMenu() {
        return -1;
    }

    @Override
    protected String getNameScreen() {
        return MrJeffConstant.LOCALYTICS.SCREEN_DEEP_LINKING;
    }


    @Override
    public void onBackPressed() {
        finishActivity = Boolean.TRUE;
        Intent show = new Intent(this, HomeActivity.class);
        show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 2);
        startActivity(show);
    }

}
