package es.coiasoft.mrjeff;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;

/**
 * Created by Paulino on 23/05/2016.
 */
public class ErrorUserActivity extends MenuActivity{

    private Handler handler = new Handler();
    private ProgressDialog progressDialog;
    protected TextView textCart;
    protected ImageView btnCart;
    protected LinearLayout btnReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.titleerror;
        super.onCreate(savedInstanceState);
        textCart = (TextView) findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView) findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);
        btnReturn = (LinearLayout) findViewById(R.id.btnReturn);
        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomerStorage.getInstance(null).loginOut();
                Intent show = new Intent(getApplicationContext(), LoginNewAccountActivity.class);
                show.putExtra(MrJeffConstant.LOGIN_EXTRA.LOGIN_ATTR, MrJeffConstant.LOGIN_EXTRA.HOME_LOGIN);
                startActivityForResult(show, 0);
            }
        });
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_error_order_user);
    }

    @Override
    protected int posMenu() {
        return -1;
    }

    @Override
    protected String getNameScreen() {
        return MrJeffConstant.LOCALYTICS.SCREEN_DEEP_LINKING;
    }


    @Override
    public void onBackPressed() {
        finishActivity = Boolean.TRUE;
        Intent show = new Intent(this, HomeActivity.class);
        show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 2);
        startActivity(show);
    }

}
