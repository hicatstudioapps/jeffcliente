package es.coiasoft.mrjeff;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.widget.ShareDialog;
import com.localytics.android.Localytics;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.ApiServiceGenerator;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalytisEvenMrJeff;
import es.coiasoft.mrjeff.domain.orders.Coupon;
import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.manager.JeffApi;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;
import es.coiasoft.mrjeff.manager.worker.DeleteProductWorker;
import es.coiasoft.mrjeff.manager.worker.FindOrdersCustomerWorker;
import es.coiasoft.mrjeff.manager.worker.ReferralWorker;
import es.coiasoft.mrjeff.manager.worker.UpdateCacheOrderWorker;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import okhttp3.ResponseBody;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import twitter4j.TwitterFactory;

/**
 * Created by linovm on 15/11/15.
 */
public class FinallyCartActivity extends MenuActivity implements Observer{


    protected Handler handler = new Handler();
    protected int wizard;
    protected FinallyCartActivity instance;
    private ConfigRepository dataSource;
    protected TextView textCart;
    protected ImageView btnCart;
    private LinearLayout panelReferral;
    private MrJeffTextView text_code_coupon;
    private MrJeffTextView codecoupon;
    private MrJeffTextView text_code_coupon_shared;
    private ImageView btn_copy;
    private MrJeffTextView titleReferral;
    private MrJeffTextView textReferrall;
    private ImageView btnfacebook;
    private ImageView btntwitter;
    private ImageView btninstagram;
    private ImageView btnmail;
    private ImageView btntwhatsapp;
    private LinearLayout couponCode;
    private LinearLayout couponCodeShared;
    private LinearLayout panelPresent;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    private TwitterFactory tf;
    private boolean isSuscription;
    protected ProgressDialog progressDialog;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.confirm;
        super.onCreate(savedInstanceState);
        if(!FacebookSdk.isInitialized()){
            FacebookSdk.sdkInitialize(getApplicationContext());
        }


        initActivityComponents();

    }

    private void initActivityComponents(){
        finishActivity = Boolean.FALSE;
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        try {
            isSuscription = getIntent().getExtras().getBoolean(MrJeffConstant.INTENT_ATTR.SUSCRIPTION);
        }catch (Exception e){
            isSuscription = false;
        }
        finishActivity=Boolean.FALSE;
        instance = this;
        dataSource = new ConfigRepository(instance);
        dataSource.updateRegister(ConfigRepository.IDORDER, "");
        this.wizard = 0;
        textCart = (TextView) findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        numpage.setVisibility(View.GONE);
        btnCart = (ImageView) findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);
        this.panelReferral = (LinearLayout)findViewById(R.id.referral);
        this.text_code_coupon = (MrJeffTextView) findViewById(R.id.text_code_coupon);
        this.text_code_coupon_shared = (MrJeffTextView) findViewById(R.id.text_code_coupon_shared);
        this.btn_copy = (ImageView) findViewById(R.id.btn_copy);
        this.titleReferral = (MrJeffTextView) findViewById(R.id.titleReferral);
        this.textReferrall = (MrJeffTextView) findViewById(R.id.textReferrall);
        this.btnfacebook = (ImageView) findViewById(R.id.btnfacebook);
        this.btntwitter = (ImageView) findViewById(R.id.btntwitter);
        this.btninstagram = (ImageView) findViewById(R.id.btninstagram);
        this.btnmail = (ImageView) findViewById(R.id.btnmail);
        this.btntwhatsapp = (ImageView) findViewById(R.id.btntwhatsapp);
        this.couponCode = (LinearLayout) findViewById(R.id.couponCode);
        this.couponCodeShared = (LinearLayout) findViewById(R.id.couponCodeShared);
        this.panelPresent = (LinearLayout) findViewById(R.id.panelPresent);
        this.codecoupon = (MrJeffTextView) findViewById(R.id.codecoupon);


        enventLocalytics();
        enventLocalyticsLTV();
        Localytics.incrementProfileAttribute(MrJeffConstant.LOCALYTICS.POSTAL_CODE, 1, Localytics.ProfileScope.APPLICATION);

        initActivity();
        initButtonsShared();
        progressDialog = ProgressDialogMrJeff.createAndStart(FinallyCartActivity.this,R.string.finalcard);
        OrderProductsStorage.getInstance(getApplicationContext()).clear();

        UpdateCacheOrderWorker worker = new UpdateCacheOrderWorker(this);
        Thread thread = new Thread(worker);
        thread.start();
    }

    @Override
    protected void setContentView() {
        setContentViewActivity();
    }

    @Override
    protected int posMenu() {
        return -1;
    }

    @Override
    protected void setContentViewActivity() {
        setContentView(R.layout.activity_finally_cart_referral);
    }


    private void initButtonsShared(){
        intiButtonClickBoard();
        btnFacebookInit();
        btnTwitterInit();
        btnInstagram();
        btnSendMail();
        btnWhatsapp();
    }

    private void intiButtonClickBoard(){
        btn_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                String code = text_code_coupon.getText().toString();
                ClipData clip = ClipData.newPlainText("MrJeff", code);
                clipboard.setPrimaryClip(clip);
            }
        });
    }


    private void btnFacebookInit(){
        btnfacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(instance.getApplicationContext());
                event.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.COUPON, text_code_coupon.getText().toString());
                event.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.METHOD, "Facebook");
                event.send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.SHARED_REFERRAL);
                sharedImageInPackage("com.facebook.katana", "Facebook");
            }
        });
    }

    private void btnTwitterInit(){
        btntwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(instance.getApplicationContext());
                event.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.COUPON, text_code_coupon.getText().toString());
                event.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.METHOD, "Twitter");
                event.send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.SHARED_REFERRAL);
                sharedImageInPackage("com.twitter.android", "Twitter");
            }
        });
    }

    public void btnSendMail(){
        btnmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(instance.getApplicationContext());
                event.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.COUPON, text_code_coupon.getText().toString());
                event.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.METHOD, "Email");
                event.send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.SHARED_REFERRAL);

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/html");
                i.putExtra(Intent.EXTRA_SUBJECT, instance.getResources().getString(R.string.couponsharedSubject));
                i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                i.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                i.putExtra(Intent.EXTRA_STREAM, insertFromFile(R.raw.mail,text_code_coupon.getText().toString()));

                try {
                    i.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(insertTextFromFile(R.raw.body,text_code_coupon.getText().toString())));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                i.setType("text/html");
                try {
                    startActivity(Intent.createChooser(i, instance.getApplicationContext().getResources().getString(R.string.emailsend)));
                } catch (android.content.ActivityNotFoundException ex) {
                    new android.app.AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.emailerror).setMessage(R.string.emailerrorBody).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            setResult(RESULT_OK);
                        }
                    }).show();
                }
            }
        });

    }


    public void btnWhatsapp(){
        btntwhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(instance.getApplicationContext());
                event.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.COUPON, text_code_coupon.getText().toString());
                event.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.METHOD, "WhatsApp");
                event.send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.SHARED_REFERRAL);

            }
        });
    }

    public void btnInstagram(){
        btninstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(instance.getApplicationContext());
                event.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.COUPON, text_code_coupon.getText().toString());
                event.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.METHOD, "Instagram");
                event.send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.SHARED_REFERRAL);
                sharedImageInPackage("com.instagram.android", "Instagram");
            }
        });
    }

    public void sharedImageInPackage(String packageName,String name){
        PackageManager pm = getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("image/*");
            String text = instance.getResources().getString(R.string.couponsharedText, text_code_coupon.getText());
            Uri path = getImage();
            waIntent.putExtra(Intent.EXTRA_STREAM, path);
            if(appInstalledOrNot(packageName, name)) {
                PackageInfo info = pm.getPackageInfo(packageName, PackageManager.GET_META_DATA);
                waIntent.setPackage(packageName);
            }
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, instance.getResources().getString(R.string.couponshared)));
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(instance, name + " " + instance.getResources().getString(R.string.couponsharednotinstalled), Toast.LENGTH_SHORT).show();
        }
    }

    public Uri getImage(){
        Uri path = Uri.parse("android.resource://es.coiasoft.mrjeff/" + R.drawable.present_coupon);
        String nameFile = "mrjeff";

        try {

            String pathDirectoryImage = Environment.getExternalStorageDirectory().toString();
            File fileImage = new File(pathDirectoryImage, nameFile+".jpg");
            if(!fileImage.exists()) {

                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.present_coupon);

                int height = bitmap.getHeight();
                int width = bitmap.getWidth();

                couponCodeShared.setDrawingCacheEnabled(true);
                couponCodeShared.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                couponCodeShared.layout(0, 0, couponCodeShared.getMeasuredWidth(), couponCodeShared.getMeasuredHeight());
                couponCodeShared.buildDrawingCache(true);
                Bitmap text = Bitmap.createBitmap(couponCodeShared.getDrawingCache());
                couponCodeShared.setDrawingCacheEnabled(false);

                Bitmap cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas comboImage = new Canvas(cs);
                comboImage.drawBitmap(bitmap, 0, 0, null);

                if (text != null) {
                    int widthText = text.getWidth();
                    int heightText = text.getHeight();

                    int x = ((width - widthText) / 2) + (widthText/4);
                    int y = ((height - heightText) / 3) - heightText + (heightText / 8);
                    comboImage.drawBitmap(text, x, y, null);

                }

                String pathString = saveImage(cs, nameFile);
            }

            String pathDirectorySave = Environment.getExternalStorageDirectory().toString();
            File file = new File(pathDirectorySave, nameFile+".jpg");
            path = Uri.fromFile(file);

        }catch (Exception e){
            Log.e("IMAGEMRJEFF","Create Image", e);
        }

        return path;
    }

    private String saveImage(Bitmap bmp,String name){
        String result = null;
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut = null;
        File file = null;
        try {
            file = new File(path, name+".jpg");
            fOut = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fOut); // bmp is your Bitmap instance
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fOut != null) {
                    fOut.flush();
                    fOut.close();
                }
                if(file != null){
                    result = file.getAbsolutePath();
                    MediaStore.Images.Media.insertImage(getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    private boolean appInstalledOrNot(String uri,String name) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
            Toast.makeText(instance, name + " not Installed", Toast.LENGTH_SHORT).show();

        }
        return app_installed;
    }

    private void initActivity(){
        ReferralWorker worker = new ReferralWorker();
        worker.addObserver(this);
        Thread thread = new Thread(worker);
        thread.start();
    }

    private void finishOpenActivity(){
        deleteProductCoupon();
        //showMessage();
        CustomerStorage.getInstance(getApplicationContext()).setCoupon(null);
    }

    private void enventLocalytics(){
        if(!isSuscription) {
            new LocalytisEvenMrJeff(getApplicationContext(),Boolean.TRUE,MrJeffConstant.LOCALYTICS_EVENTS.NAME.FINAL,Boolean.TRUE);
        } else {
            LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(getApplicationContext(),Boolean.TRUE,null,Boolean.FALSE);
            event.send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.FINAL_SUSCRIPTION);
        }
    }
    private void enventLocalyticsLTV(){
        if(!isSuscription) {
            Integer paid = 0;
            try {
                Float total = Float.valueOf(OrderStorage.getInstance(getApplicationContext()).getOrder().getTotal());
                paid = total.intValue() * 100;
            } catch (Exception e) {
                Log.e("FinallyCartActivity", "Convert total in Integer");
            }

            new LocalytisEvenMrJeff(getApplicationContext(), Boolean.TRUE, MrJeffConstant.LOCALYTICS_EVENTS.NAME.ITEM_PURCHASED, Boolean.TRUE, paid);
        }
    }


    private void deleteProductCoupon(){
       if(OrderStorage.getInstance(getApplicationContext()).getProductDelete() != null){
           ApiServiceGenerator.createService(JeffApi.class).deleteProductOrOrder(String.valueOf(OrderStorage.getInstance(getApplicationContext()).getProductDelete()),
                   "product")
                   .subscribeOn(Schedulers.io())
                   .subscribe(new Action1<ResponseBody>() {
                       @Override
                       public void call(ResponseBody responseBody) {

                       }
                   });
//           DeleteProductWorker worker = new DeleteProductWorker(OrderStorage.getInstance(getApplicationContext()).getProductDelete());
//           Thread thread = new Thread(worker);
//           thread.start();
       }
    }

    private void showMessage(){
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.calendarregister).setMessage(R.string.calendarregistertext).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(instance.getApplicationContext());
                                event.send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.ADD_DATE_CALENDAR);
                                setCalendar();
                            }
                        }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).show();
                    }
                });
            }
        });
        threadUpdate.start();
    }

    private void cleanOrders(){
        OrderStorage.getInstance(getApplicationContext()).setProductDelete(null);
        OrderProductsStorage.getInstance(getApplicationContext()).saveJson();
        if(OrderStorage.getInstance(getApplicationContext()).getRemoveOk()) {
            OrderStorage.getInstance(getApplicationContext()).setOrder(new Order());
            OrderStorage.getInstance(getApplicationContext()).setOrderSend(new es.coiasoft.mrjeff.domain.orders.send.Order());
            OrderStorage.getInstance(getApplicationContext()).saveJson();
        } else {
            OrderStorage.getInstance(getApplicationContext()).setRemoveOk(true);
        }
    }




    @Override
    public void onBackPressed() {
        Intent show = new Intent(this, HomeActivity.class);
        startActivity(show);
    }

    @Override
    public void onDestroy(){
        cleanOrders();
        super.onDestroy();
    }


    private void setCalendar() {
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            wizard=1;
                            Intent calIntent = new Intent(Intent.ACTION_EDIT);
                            calIntent.setType("vnd.android.cursor.item/event");
                            calIntent.putExtra(CalendarContract.Events.TITLE, "Mr. Jeff");
                            calIntent.putExtra(CalendarContract.Events.EVENT_LOCATION, OrderStorage.getInstance(getApplicationContext()).getOrder().getBilling_address().getAddress_1() +
                                    ", "+OrderStorage.getInstance(getApplicationContext()).getOrder().getBilling_address().getCity());
                            calIntent.putExtra(CalendarContract.Events.DESCRIPTION, getApplicationContext().getString(R.string.calendarDescriptionBill));


                            String date = OrderStorage.getInstance(getApplicationContext()).getOrderSend().getOrder_meta().getMyfield2()+" "+
                                    OrderStorage.getInstance(getApplicationContext()).getOrderSend().getOrder_meta().getMyfield3().split(" ")[0].trim();
                            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                            Date dateFormat = format.parse(date);
                            calIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
                            calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,dateFormat.getTime());
                            calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,dateFormat.getTime());
                            startActivity(calIntent);
                        }catch (Exception e){
                            e.printStackTrace();
                            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                    "mailto","leoperezortiz@gmail.com", null));
                            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "error calendario");
                            emailIntent.putExtra(Intent.EXTRA_TEXT, e.toString());
                            startActivity(Intent.createChooser(emailIntent,"Send Error"));
                        }
                    }
                });
            }
        });
        threadUpdate.start();
    }

    private void setCalendarShipping() {
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            wizard=2;
                            Intent calIntent = new Intent(Intent.ACTION_EDIT);
                            calIntent.setType("vnd.android.cursor.item/event");
                            calIntent.putExtra(CalendarContract.Events.TITLE, "Mr. Jeff");
                            calIntent.putExtra(CalendarContract.Events.EVENT_LOCATION,OrderStorage.getInstance(getApplicationContext()).getOrder().getShipping_address().getAddress_1() +
                                    ", "+OrderStorage.getInstance(getApplicationContext()).getOrder().getShipping_address().getCity());
                            calIntent.putExtra(CalendarContract.Events.DESCRIPTION, getApplicationContext().getString(R.string.calendarDescriptionShip));

                            String date = OrderStorage.getInstance(getApplicationContext()).getOrderSend().getOrder_meta().getMyfield4()+" "+
                                    OrderStorage.getInstance(getApplicationContext()).getOrderSend().getOrder_meta().getMyfield5().split(" ")[0].trim();
                            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                            Date dateFormat = format.parse(date);
                            calIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
                            calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,dateFormat.getTime());
                            calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,dateFormat.getTime());
                            startActivity(calIntent);
                        }catch (Exception e){
                            e.printStackTrace();
                            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                    "mailto","leoperezortiz@gmail.com", null));
                            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "error calendario");
                            emailIntent.putExtra(Intent.EXTRA_TEXT, e.toString());
                            startActivity(Intent.createChooser(emailIntent,"Send Error"));
                        }
                    }
                });
            }
        });
        threadUpdate.start();

    }


    @Override
    public void onResume(){
        super.onResume();
       switch (wizard){
            case 1: setCalendarShipping();
                break;
            case 2:
                break;
            default:
                break;
        }
    }

    private void correctCoupon(final Coupon coupon){
        final String code = coupon.getCode();
        final String description = coupon.getDescription();
        final String discount = coupon.getAmount() + (coupon.getType() != null && coupon.getType().equals("percent")?"%":"€");
        Thread threadNotiy = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        codecoupon.setText(code);
                        text_code_coupon.setText(code);
                        text_code_coupon_shared.setText(code);
                        titleReferral.setText(description);
                        textReferrall.setText(instance.getResources().getString(R.string.couponreferraltextfinally,discount,discount));

                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        finishOpenActivity();
                    }
                });
            }
        });

        threadNotiy.start();

    }

    private void inCorrectCoupon(){
        Thread threadNotiy = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        btnfacebook.setVisibility(View.GONE);
                        btntwitter.setVisibility(View.GONE);
                        btninstagram.setVisibility(View.GONE);
                        btnmail.setVisibility(View.GONE);
                        btntwhatsapp.setVisibility(View.GONE);
                        couponCode.setVisibility(View.GONE);
                        panelPresent.setVisibility(View.GONE);

                        titleReferral.setText(R.string.complete);
                        textReferrall.setText(R.string.completeCalendar);


                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                    }
                });
            }
        });

        threadNotiy.start();

    }

    protected String getNameScreen(){
        return MrJeffConstant.LOCALYTICS.SCREEN_FINALLY;
    }

    @Override
    public void update(Observable observable, Object data) {
        if(observable instanceof  ReferralWorker &&
                data!=null && data instanceof Coupon){
                correctCoupon((Coupon) data);
        } else {
            inCorrectCoupon();
        }
    }

    public Uri insertFromFile(int resourceId,String coupon) {
        String pathDirectoryImage = Environment.getExternalStorageDirectory().toString();
        File fileEmail = new File(pathDirectoryImage, "referral_coupon.html");
       try {
           if (!fileEmail.exists()) {
               InputStream insertsStream = getResources().openRawResource(resourceId);
               BufferedReader insertReader = new BufferedReader(new InputStreamReader(insertsStream));
               FileOutputStream fOut = new FileOutputStream(fileEmail);
               while (insertReader.ready()) {
                   fOut.write(insertReader.readLine().replaceAll("##COUPON##",coupon).getBytes());
               }
               insertReader.close();
               fOut.close();
           }
       }catch (Exception e){

       }

        String pathDirectorySave = Environment.getExternalStorageDirectory().toString();
        File file = new File(pathDirectorySave, "referral_coupon.html");
        return Uri.fromFile(file);
    }

    public String insertTextFromFile(int resourceId,String coupon) throws IOException {
        StringBuilder builder = new StringBuilder();
        InputStream insertsStream = getResources().openRawResource(resourceId);
        BufferedReader insertReader = new BufferedReader(new InputStreamReader(insertsStream));
        while (insertReader.ready()) {
            builder.append(insertReader.readLine());
        }
        insertReader.close();
        return builder.toString().replace("##COUPON##",coupon);
    }

}
