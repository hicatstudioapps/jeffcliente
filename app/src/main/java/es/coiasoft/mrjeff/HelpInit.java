package es.coiasoft.mrjeff;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.ValidateVersion;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.PostalCodeStorage;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;
import es.coiasoft.mrjeff.manager.worker.FindCPWorker;
import es.coiasoft.mrjeff.manager.worker.RegisterCPandEmailWorker;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;


public class HelpInit extends ConnectionActivity{

    public static final String EXTRA_HELP="HELP";

    final Handler handler = new Handler();
    private static final String FRAGMENT_HELP_CP="cp";
    private static final String FRAGMENT_HELP_EMAIL="email";
    private static final String FRAGMENT_HELP_THKANS="email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void setContentViewActivity() {
        setContentView(R.layout.activity_help);
    }

    @Override
    public void onResume(){
        super.onResume();
        FragmentPostalCodeView fragment = FragmentPostalCodeView.newInstance(null);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentLayoutHelp, fragment, FRAGMENT_HELP_CP);
        ft.commitAllowingStateLoss();;
    }

    public void goToEmail(String postalCode) {
        if(!isFinishing()) {
            Bundle params = new Bundle();
            params.putString(FragmentEmailView.POSTAL_CODE_PARAMETER, postalCode);
            FragmentEmailView fragment = FragmentEmailView.newInstance(params);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinrigth, R.anim.fadeoutleft, R.anim.fadeinleft, R.anim.fadeoutrigth);
            ft.replace(R.id.fragmentLayoutHelp, fragment, FRAGMENT_HELP_EMAIL);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }



    public void goToCP() {
        if(!isFinishing()) {
            Bundle params = new Bundle();
            FragmentPostalCodeView fragment = FragmentPostalCodeView.newInstance(null);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinleft, R.anim.fadeoutrigth, R.anim.fadeinrigth, R.anim.fadeoutleft);
            ft.replace(R.id.fragmentLayoutHelp, fragment, FRAGMENT_HELP_CP);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }

    public void goToThanks() {
        if(!isFinishing()) {
            FragmentFinallyView fragment = FragmentFinallyView.newInstance(null);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinrigth, R.anim.fadeoutleft, R.anim.fadeinleft, R.anim.fadeoutrigth);
            ft.replace(R.id.fragmentLayoutHelp, fragment, FRAGMENT_HELP_THKANS);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }

    protected String getNameScreen(){
        return MrJeffConstant.LOCALYTICS.SCREEN_POSTAL_CODE;
    }

    @Override
    public void startActivity (Intent intent){
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        super.startActivity(intent);
    }


    public static class FragmentEmailView extends Fragment implements Observer{


        public static final String POSTAL_CODE_PARAMETER = "postalcode";
        private Handler handler = new Handler();
        private EditText textEmail;
        private LinearLayout panelwizardemail;
        private TextView errorMail;
        private ImageView atbtn;
        private FragmentEmailView instance;
        private LinearLayout buttonsarrow;
        private String cp;

        public static FragmentEmailView newInstance(Bundle options) {
            FragmentEmailView var1 = new FragmentEmailView();
            var1.setArguments(options);
            return var1;
        }

        public FragmentEmailView() {
            super();
            this.instance = this;
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle arguments = getArguments();
            if(arguments != null && arguments.containsKey(POSTAL_CODE_PARAMETER)) {
                cp = arguments.getString(POSTAL_CODE_PARAMETER);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            View v =  inflater.inflate(R.layout.fragment_help_email, container, false);
            initComponents(v);
            initEvent();
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        @Override
        public void update(Observable observable, Object data) {
            if(observable instanceof RegisterCPandEmailWorker){
                unblockedPanelMail();
            }
        }


        protected void initComponents(View v){
            panelwizardemail = (LinearLayout) v.findViewById(R.id.panelwizarmail);
            errorMail = (TextView) v.findViewById(R.id.errorMessagemail);
            textEmail = (EditText) v.findViewById(R.id.attext);
            atbtn = (ImageView) v.findViewById(R.id.atbtn);
            buttonsarrow = (LinearLayout) v.findViewById(R.id.panelArrow);
        }



        private void initEvent(){
            atbtn.setOnClickListener(getClickListenerSendail());
            textEmail.setOnEditorActionListener(getClickListenerForAT());
            buttonsarrow.setOnClickListener(getClickListenerArrow());
        }

        private View.OnClickListener getClickListenerArrow(){
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((HelpInit)getActivity()).goToCP();
                }
            };
        }

        private TextView.OnEditorActionListener getClickListenerForAT(){
            return new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        registerEmailAndCp();
                    }
                    return false;
                }
            };
        }

        private View.OnClickListener getClickListenerSendail(){
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    registerEmailAndCp();
                }
            };
        }

        private void unblockedPanelMail() {

            Runnable blockRunnable = new Runnable() {
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            ((HelpInit)getActivity()).goToThanks();
                            atbtn.setImageResource(R.drawable.atbck);
                        }
                    });
                }
            };

            Thread lunch = new Thread(blockRunnable);
            lunch.start();
        }


        private void registerEmailAndCp(){
            final Handler handler = new Handler();

            Runnable blockRunnable = new Runnable() {
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                            inputMethodManager.hideSoftInputFromWindow(textEmail.getWindowToken(), 0);
                            errorMail.setVisibility(View.INVISIBLE);
                            String email = textEmail.getText().toString();
                            if(isValidEmail(email)) {
                                atbtn.setImageResource(R.drawable.okcp);
                                new LocalyticsEventActionMrJeff(getActivity().getApplicationContext(),MrJeffConstant.LOCALYTICS_EVENTS.ACTION.HELPEMAIL,Boolean.FALSE,email);
                                textEmail.setEnabled(Boolean.FALSE);
                                RegisterCPandEmailWorker registerCPandEmailWorker = new RegisterCPandEmailWorker(cp, email);
                                registerCPandEmailWorker.addObserver(instance);
                                Thread threadCPEmail = new Thread(registerCPandEmailWorker);
                                threadCPEmail.start();

                            } else{
                                atbtn.setImageResource(R.drawable.delcp);
                                errorMail.setVisibility(View.VISIBLE);

                            }
                        }
                    });
                }
            };

            Thread lunch = new Thread(blockRunnable);
            lunch.start();

        }

        public final static boolean isValidEmail(CharSequence target) {
            if (TextUtils.isEmpty(target)) {
                return false;
            } else {
                return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
            }
        }

    }

    public static class FragmentFinallyView extends Fragment{


        private Handler handler = new Handler();
        private FragmentFinallyView instance;
        private LinearLayout buttonsarrowFinaly;

        public static FragmentFinallyView newInstance(Bundle options) {
            FragmentFinallyView var1 = new FragmentFinallyView();
            var1.setArguments(options);
            return var1;
        }

        public FragmentFinallyView() {

            super();
            this.instance = this;
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            View v =  inflater.inflate(R.layout.fragment_help_thanks, container, false);
            initComponents(v);
            initEvent();
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        protected void initComponents(View v){
            buttonsarrowFinaly = (LinearLayout) v.findViewById(R.id.panelArrowFinally);
        }



        private void initEvent(){
            buttonsarrowFinaly.setOnClickListener(getClickListenerArrow());
        }

        private View.OnClickListener getClickListenerArrow(){
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ((HelpInit)getActivity()).goToCP();
                }
            };
        }

    }

    public static class FragmentPostalCodeView extends Fragment implements Observer{


        private Handler handler = new Handler();
        private EditText textPostalCode;
        private ImageView btnMap;
        private LinearLayout panelwizardcp;
        private TextView errorCP;
        private FragmentPostalCodeView instance;
        private ConfigRepository dataSource;
        private ProgressDialogMrJeff progressDialogMrJeff;

        public static FragmentPostalCodeView newInstance(Bundle options) {
            FragmentPostalCodeView var1 = new FragmentPostalCodeView();
            var1.setArguments(options);
            return var1;
        }

        public FragmentPostalCodeView() {

            super();
            this.instance = this;
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            View v =  inflater.inflate(R.layout.fragment_help_postal_code, container, false);
            initComponents(v);
            initEvent();
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        @Override
        public void update(Observable observable, Object data) {
            if(observable instanceof FindCPWorker){
                unblockedPanel();
            }
        }


        protected void initComponents(View v){
            dataSource = new ConfigRepository(getActivity());
            panelwizardcp = (LinearLayout) v.findViewById(R.id.panelwizarcp);
            errorCP = (TextView) v.findViewById(R.id.errorMessageCP);
            textPostalCode = (EditText) v.findViewById(R.id.cptext);
            btnMap = (ImageView) v.findViewById(R.id.map);
        }



        private void initEvent(){
            btnMap.setOnClickListener(getClickListenerMap());
            textPostalCode.setOnEditorActionListener(getClickListenerForCP());
            textPostalCode.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if(KeyEvent.ACTION_UP == event.getAction()) {
                        btnMap.setImageResource(R.drawable.codigo);
                        errorCP.setVisibility(View.GONE);
                    }
                    return false;
                }
            });
        }

        private View.OnClickListener getClickListenerMap(){
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    eventFormCP(v);
                }
            };
        }

        private TextView.OnEditorActionListener getClickListenerForCP(){
            return new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEND) {
                        eventFormCP(v);
                    }
                    return false;
                }
            };
        }

        private void eventFormCP(final View v){
            InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(textPostalCode.getWindowToken(), 0);
            findCP(v);
        }

        private void findCP (final View v){
            final Handler handler = new Handler();
            PostalCodeStorage.getInstance(getActivity().getApplicationContext());
            Runnable blockRunnable = new Runnable() {
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            final String country = "Espanya";
                            final String cp = textPostalCode.getText().toString();
                            errorCP.setVisibility(View.INVISIBLE);
                            if (cp != null && cp.length() == 5) {
                                new LocalyticsEventActionMrJeff(getActivity().getApplicationContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.HELPCP,Boolean.FALSE,cp);
                                FindCPWorker cpWorker = new FindCPWorker();
                                cpWorker.addObserver(instance);
                                Thread threadCPs = new Thread(cpWorker);
                                threadCPs.start();
                            } else {
                                btnMap.setImageResource(R.drawable.delcp);
                                errorCP.setVisibility(View.VISIBLE);
                            }

                        }
                    });
                }
            };

            Thread lunch = new Thread(blockRunnable);
            lunch.start();
        }

        private void unblockedPanel() {

            Runnable blockRunnable = new Runnable() {
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(progressDialogMrJeff != null && progressDialogMrJeff.isShowing()){
                                progressDialogMrJeff.hide();
                            }
                            final String country = "Espanya";
                            final String cp = textPostalCode.getText().toString();
                            Boolean find = PostalCodeStorage.getInstance(textPostalCode.getContext()).find(country, cp);
                            textPostalCode.setEnabled(Boolean.TRUE);
                            btnMap.setEnabled(Boolean.TRUE);
                            if (find) {
                                btnMap.setImageResource(R.drawable.okcp);
                                progressDialogMrJeff = ProgressDialogMrJeff.createAndStart(instance.getActivity());
                                textPostalCode.setText("");
                                String city = PostalCodeStorage.getInstance(getActivity().getApplicationContext()).getCity(cp);
                                panelwizardcp.setVisibility(View.GONE);
                                dataSource.updateRegister(ConfigRepository.NEW_USER, "1");
                                dataSource.updateRegister(ConfigRepository.POSTALCODE, cp);
                                dataSource.updateRegister(ConfigRepository.CITY, city);
                                if(CustomerStorage.getInstance(getActivity()).isLogged()) {
                                    Intent myIntent = new Intent(textPostalCode.getContext(), HomeActivity.class);
                                    startActivityForResult(myIntent, 0);
                                } else {
                                    new LocalyticsEventActionMrJeff(getActivity().getApplicationContext(),MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTHELP,Boolean.FALSE,"Correcto");
                                    Intent myIntent = new Intent(textPostalCode.getContext(), LoginNewAccountActivity.class);
                                    myIntent.putExtra(MrJeffConstant.LOGIN_EXTRA.LOGIN_ATTR,MrJeffConstant.LOGIN_EXTRA.HOME_LOGIN);
                                    startActivity(myIntent);
                                }
                                ((HelpInit)getActivity()).goToEmail(cp);
                                btnMap.setImageResource(R.drawable.codigo);
                            } else {
                                btnMap.setImageResource(R.drawable.delcp);
                                panelwizardcp.setVisibility(View.GONE);
                                textPostalCode.setText("");
                                ((HelpInit)getActivity()).goToEmail(cp);
                                btnMap.setImageResource(R.drawable.codigo);
                            }
                        }
                    });
                }
            };

            Thread lunch = new Thread(blockRunnable);
            lunch.start();
        }
    }



}

