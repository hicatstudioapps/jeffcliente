package es.coiasoft.mrjeff;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.localytics.android.Localytics;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import es.coiasoft.mrjeff.Utils.ApiServiceGenerator;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.Utils.event.LocalytisEvenMrJeff;
import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.StatusOrder;
import es.coiasoft.mrjeff.domain.SuscriptionInfo;
import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.orders.send.Line_items;
import es.coiasoft.mrjeff.domain.orders.send.OrderMeta;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.MonthStorage;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.OrdersCustomerStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.domain.storage.SuscriptionCustomerStorage;
import es.coiasoft.mrjeff.manager.JeffApi;
import es.coiasoft.mrjeff.manager.service.SearchOrderService;
import es.coiasoft.mrjeff.manager.service.SearchStatusService;
import es.coiasoft.mrjeff.manager.worker.FindOrdersCustomerWorker;
import es.coiasoft.mrjeff.manager.worker.FindProductsWorker;
import es.coiasoft.mrjeff.manager.worker.GetSuscriptionWorker;
import es.coiasoft.mrjeff.manager.worker.SendOrderWorker;
import es.coiasoft.mrjeff.view.adapter.HolderRecyclerHome;
import es.coiasoft.mrjeff.view.adapter.HolderRecyclerHomeSuscription;
import es.coiasoft.mrjeff.view.adapter.RecyclerAdapterMrJeff;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.components.RateControllerDash;
import es.coiasoft.mrjeff.view.components.RecyclerViewMrJeff;
import es.coiasoft.mrjeff.view.components.ViewPagerMrJeff;
import es.coiasoft.mrjeff.view.listener.ProductAddRemoveListener;
import retrofit2.Call;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class HomeActivity extends MenuActivity implements Observer, ProductAddRemoveListener{

    public static final String PARAM_KEY = "keyproduct";
    final Handler handler = new Handler();
    private static final String KEY_OFFER="zpacks";
    private static final String KEY_LAV="zlavanderia";
    private static final String KEY_NO_LAVO="newsuscriptions";
    private static final String KEY_TIN="ztintoreria";
    private static final String KEY_HOME="zcasa";
    protected ImageView linkNoLavo;
    protected LinearLayout btnFootOfertas;
    protected LinearLayout btnFootLav;
    protected LinearLayout btnFootTin;
    protected LinearLayout btnFoototros;
    protected TextView textCart;
    protected ImageView btnCart;
    private ProgressDialog progressDialog;
    protected int pos = 2;
    int noLoad=1;

    private ViewPagerMrJeff pager;
    private TabCollectionPagerAdapter tabCollectionPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.app_name;
        super.onCreate(savedInstanceState);
        readParams();
        if(MrJeffApplication.page != -1)
            pos= MrJeffApplication.page;
        initComponents();
        initEvent();
        disabledAllPanel();

        switch (pos){
            case 0:
                changePanel(btnFootOfertas,R.drawable.iconofferselect,R.color.blueMrJeff);
                break;
            case 1:
                changePanel(btnFootLav,R.drawable.iconlavselect,R.color.blueMrJeff);
                break;
            case 2:
                linkNoLavo.setImageResource(R.drawable.iconnolavoselect);
                break;
            case 3:
                changePanel(btnFootTin,R.drawable.icontinselect,R.color.blueMrJeff);
                break;
            case 4:
                changePanel(btnFoototros,R.drawable.iconhomeselect,R.color.blueMrJeff);
                break;
            default:
                linkNoLavo.setImageResource(R.drawable.iconnolavoselect);
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putInt("xx",22);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onResume(){
       super.onResume();
        Order o=OrderStorage.getInstance(null).getOrder();
        if(o!= null)
            Toast.makeText(this,"ID_PEDIDO "+o.getOrder_number(),Toast.LENGTH_LONG).show();
    }

    private void readParams(){
        try {
            pos = getIntent().getExtras().getInt(MrJeffConstant.INTENT_ATTR.POSFRAGMENT);
        }catch (Exception e){
            if(MrJeffApplication.page != -1)
                pos= MrJeffApplication.page;
            else
                pos=2;
        }

        try {
            String coupon = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.HOME_MESSAGE);
            if(coupon != null && !coupon.trim().toLowerCase().equals("null")) {
                new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Theme_AppCompat_Light)).setTitle(R.string.empty).setMessage(getResources().getString(R.string.couponapply, coupon)).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }).show();
            }
        }catch (Exception e){
        }
    }
    private void initComponents(){

        if(CustomerStorage.getInstance(this).isLogged()) {
            Crashlytics.setUserIdentifier(CustomerStorage.getInstance(this).getCustomer().getId().toString());
            Crashlytics.setUserEmail(CustomerStorage.getInstance(this).getCustomer().getEmail());
            Crashlytics.setUserName(CustomerStorage.getInstance(this).getCustomer().getFirst_name());
        }

        textCart = (TextView)findViewById(R.id.numberCartHeader);
        textCart.setText(OrderProductsStorage.getInstance(getApplicationContext()).getNumTotal().toString());
        btnCart = (ImageView)findViewById(R.id.btnCart);
        if(OrderProductsStorage.getInstance(getApplicationContext()).getNumTotal().intValue() > 0){
            btnCart.setImageResource(R.drawable.basket);
        } else {
            btnCart.setImageResource(R.drawable.basketempty);
        }
        linkNoLavo = (ImageView) findViewById(R.id.nolavo);
        btnFootOfertas = (LinearLayout) findViewById(R.id.btnFootOfertas);
        btnFootLav = (LinearLayout) findViewById(R.id.btnFootLav);
        btnFootTin = (LinearLayout) findViewById(R.id.btnFootTin);
        btnFoototros = (LinearLayout) findViewById(R.id.btnFoototros);


        pager = (ViewPagerMrJeff) findViewById(R.id.pager);
        pager.setMaxPage(5);
        tabCollectionPagerAdapter = new TabCollectionPagerAdapter(getSupportFragmentManager(), this);

        pager.setAdapter(tabCollectionPagerAdapter);
        pager.setOffscreenPageLimit(1);
        pager.setCurrentItem(pos);
        pager.setPosition(pos);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pos = position;
                MrJeffApplication.page= pos;
                pager.setPosition(position);
                disabledAllPanel();
                switch (position){
                    case 0:
                        changePanel(btnFootOfertas,R.drawable.iconofferselect,R.color.blueMrJeff);
                        break;
                    case 1:
                        changePanel(btnFootLav,R.drawable.iconlavselect,R.color.blueMrJeff);
                        break;
                    case 2:
                        linkNoLavo.setImageResource(R.drawable.iconnolavoselect);
                        break;
                    case 3:
                        changePanel(btnFootTin,R.drawable.icontinselect,R.color.blueMrJeff);
                        break;
                    case 4:
                        changePanel(btnFoototros,R.drawable.iconhomeselect,R.color.blueMrJeff);
                        break;
                    default:
                        linkNoLavo.setImageResource(R.drawable.iconnolavoselect);
                        break;
                }

                if(pos != 2){
                    textheader.setText(R.string.app_name);
                } else {
                    if(SuscriptionCustomerStorage.getInstance(HomeActivity.this).isSuscription()){
                        textheader.setText(R.string.dastitle);
                    } else {
                        textheader.setText(R.string.suscriptions);
                    }

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



    }

    private void initEvent(){
        btnFootOfertas.setOnClickListener(getOnClickListener(0,R.drawable.iconofferselect));
        btnFootLav.setOnClickListener(getOnClickListener(1,R.drawable.iconlavselect));
        linkNoLavo.setOnClickListener(getOnClickNoLavoListener(2));
        btnFootTin.setOnClickListener(getOnClickListener(3,R.drawable.icontinselect));
        btnFoototros.setOnClickListener(getOnClickListener(4,R.drawable.iconhomeselect));
    }

    private View.OnClickListener getOnClickListener(final int position, final int icon){
        return new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(pos != position){
                    disabledAllPanel();
                    changePanel((LinearLayout) v,icon,R.color.blueMrJeff);
                    //movePanel(position);
                    pager.setCurrentItem(position);
                }

            }
        };
    }

    private View.OnClickListener getOnClickNoLavoListener(final int position){
        return new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(pos != position){
                    disabledAllPanel();
                    linkNoLavo.setImageResource(R.drawable.iconnolavoselect);
                    //movePanel(position);
                    pager.setCurrentItem(position);
                }

            }
        };
    }

    private Fragment getFragment(int position,Bundle params){
        if(position == 2){
            if(SuscriptionCustomerStorage.getInstance(this).isSuscription()){
                return FragmentSuscription.newInstance(params);
            } else {
                return FragmentListNewSuscriptionHome.newInstance(params);
            }
        }else{
            return FragmentListProductHome.newInstance(params);
        }
    }


    private String getlabel(int position){
        switch (position){
            case 0:
                return KEY_OFFER;
            case 1:
                return KEY_LAV;
            case 2:
                return KEY_NO_LAVO;
            case 3:
                return KEY_TIN;
            case 4:
                return KEY_HOME;
            default:
                return KEY_NO_LAVO;
        }
    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected String getNameScreen(){
        return MrJeffConstant.LOCALYTICS.SCREEN_PRODUCTS_PACKS;
    }


    @Override
    protected void setContentView(){
        setContentView(R.layout.activity_home);
    }

    protected void disabledAllPanel(){
        changePanel(btnFootOfertas,R.drawable.iconoffer,R.color.grey80);
        changePanel(btnFootLav,R.drawable.iconlav,R.color.grey80);
        changePanel(btnFootTin,R.drawable.icontin,R.color.grey80);
        changePanel(btnFoototros,R.drawable.iconhome,R.color.grey80);
        disabledNoLavo();
    }

    protected void changePanel(LinearLayout panelButton, int icon,int color){
        for (int i = 0; i < panelButton.getChildCount(); i++) {
            View view = panelButton.getChildAt(i);
            if (view instanceof ImageView) {
                ((ImageView) view).setImageResource(icon);
            } else if (view instanceof TextView) {
                ((TextView) view).setTextColor(getApplicationContext().getResources().getColor(color));
            }
        }

    }

    protected void disabledNoLavo(){
        linkNoLavo.setImageResource(R.drawable.iconnolavo);
    }

    @Override
    protected int posMenu() {
        return 0;
    }


    @Override
    protected void createOrder(){
        super.createOrder();
        removeTaxMin();
        es.coiasoft.mrjeff.domain.orders.send.Order  order = getOrder();
        progressDialog = ProgressDialogMrJeff.createAndStart(this,R.string.initcard);
        SendOrderWorker worker = new SendOrderWorker(order);
        worker.addObserver(this);
        Thread threadOrder = new Thread(worker);
        threadOrder.start();
//        Intent show = new Intent(this, OrderAddressActivity.class);
//        show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, false);
//        startActivity(show);
    }


    protected void removeTaxMin(){
        if(OrderStorage.getInstance(getApplicationContext()).getProductMinOrder() != null){
            Line_items itemRemove = null;
            for(Line_items item: OrderStorage.getInstance(getApplicationContext()).getOrderSend().getLine_items()){
                if(item.getProduct_id().intValue() == OrderStorage.getInstance(getApplicationContext()).getProductMinOrder().intValue()){
                    itemRemove = item;
                    break;
                }
            }
            if(itemRemove != null) {
                OrderStorage.getInstance(getApplicationContext()).getOrderSend().getLine_items().remove(itemRemove);
            }
        }
    }

    private es.coiasoft.mrjeff.domain.orders.send.Order getOrder(){

        es.coiasoft.mrjeff.domain.orders.send.Order result = OrderStorage.getInstance(getApplicationContext()).getOrderSend();
        if(result == null){
            result = new es.coiasoft.mrjeff.domain.orders.send.Order();
        }
        result.setLine_items(new ArrayList<Line_items>());
        result.setOrder_meta(new OrderMeta());
        MrJeffApplication.isSubscription=false;
        MrJeffApplication.subType="";
        if(OrderProductsStorage.getInstance(getApplicationContext()).getNumTotal().intValue() > 0){
            Map<Integer,Integer> products = OrderProductsStorage.getInstance(getApplicationContext()).getProducts();

            if(products != null && !products.isEmpty()){
                Set<Integer> keys = products.keySet();
                for(Integer key:keys){
                    if(products.get(key) != null && products.get(key).intValue() > 0) {
                        Line_items item = new Line_items();
                        item.setProduct_id(key);
                        item.setQuantity(products.get(key));
                        result.getLine_items().add(item);
                        Log.d("Order p", "getOrder: ");
                    }
                }
            }
        }

        OrderStorage.getInstance(getApplicationContext()).setOrderSend(result);
        OrderStorage.getInstance(getApplicationContext()).saveJson();
        return result;
    }

    @Override
    public void update(Observable observable, Object data) {
        if(observable instanceof  SendOrderWorker){
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(progressDialog != null && progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                            if(OrderStorage.getInstance(getApplicationContext()).getError()){

                                new AlertDialog.Builder(new ContextThemeWrapper(HomeActivity.this, R.style.Theme_AppCompat_Light)).setTitle(R.string.errrorConnection).setMessage(R.string.errrorConnectiontext).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        setResult(RESULT_OK);
                                    }
                                }).show();
                            } else {
                                new LocalytisEvenMrJeff(getApplicationContext(),Boolean.TRUE, MrJeffConstant.LOCALYTICS_EVENTS.NAME.NEW_CART,Boolean.TRUE);
                                Intent show = new Intent(progressDialog.getContext(), OrderAddressActivity.class);
                                show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, false);
                                startActivity(show);
                            }
                        }
                    });
                }
            });
            threadNotiy.start();
        }

    }

    @Override
    public void startActivity (Intent intent){
        //getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        super.startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void addProduct(Integer idProduct) {
        String valueNumberCart = textCart.getText().toString();
        Log.d("ProductoID",idProduct+"");
        Integer numCart = 0;
        try {
            numCart = Integer.valueOf(valueNumberCart) + 1;
        } catch (Exception e) {
            numCart = 0;
        }

        btnCart.setImageResource(R.drawable.basket);
        textCart.setText(numCart.toString());
        OrderProductsStorage.getInstance(getApplicationContext()).addProductElement(idProduct);
        new LocalyticsEventActionMrJeff(this, MrJeffConstant.LOCALYTICS_EVENTS.ACTION.HOMEADD,Boolean.FALSE,idProduct != null ? idProduct.toString():"");
    }

    @Override
    public void removeProduct(Integer idProduct) {
        String valueNumberCart = textCart.getText().toString();
        Integer numCart = 0;
        try {
            numCart = Integer.valueOf(valueNumberCart) - 1;
        } catch (Exception e) {
            numCart = 0;
        }
        numCart = numCart.compareTo(Integer.valueOf(0)) <= 0 ? 0 : numCart;
        textCart.setText(numCart.toString());
        if(numCart == 0){
            btnCart.setImageResource(R.drawable.basketempty);
        }
        new LocalyticsEventActionMrJeff(this, MrJeffConstant.LOCALYTICS_EVENTS.ACTION.HOMEREMOVE, Boolean.FALSE, idProduct != null ? idProduct.toString() : "");
        OrderProductsStorage.getInstance(getApplicationContext()).removeProductElement(idProduct);
    }


    public static class FragmentListProductHome extends Fragment implements Observer{

        protected View v;
        private Handler handler = new Handler();
        private FragmentListProductHome instance;
        private RecyclerView listView;
        protected TextView textCart;
        protected ImageView btnCart;
        private String key;
        private int pos=2;
        private ProgressDialogMrJeff progressDialog;

        public static FragmentListProductHome newInstance(Bundle options) {
            FragmentListProductHome var1 = new FragmentListProductHome();
            var1.setArguments(options);
            return var1;
        }

        public FragmentListProductHome() {
            super();
            this.instance = this;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle arguments = getArguments();
            if(arguments != null && arguments.containsKey(HomeActivity.PARAM_KEY)) {
                key = arguments.getString(HomeActivity.PARAM_KEY);
            }

            if(arguments != null && arguments.containsKey(MrJeffConstant.INTENT_ATTR.POSFRAGMENT)){
                pos = arguments.getInt(MrJeffConstant.INTENT_ATTR.POSFRAGMENT);
            }
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v =  inflater.inflate(R.layout.fragment_list_product_home, container, false);
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
            initComponents(v);
            initEvent();
            searchKey();
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        protected void initComponents(View v){
            textCart = (TextView)getActivity().findViewById(R.id.numberCartHeader);
            btnCart = (ImageView)getActivity().findViewById(R.id.btnCart);
            textCart.setText(OrderProductsStorage.getInstance(getActivity()).getNumTotal().toString());
            textCart.setVisibility(View.VISIBLE);
            btnCart.setVisibility(View.VISIBLE);
            listView = (RecyclerView) v.findViewById(R.id.listProductsHome);
        }



        private void initEvent(){

        }


        private void searchKey(){
            if(key != null) {
                final ArrayList<Products> products = ProductStorage.getInstance(getActivity()).getCategory(key);

                if (products != null && !products.isEmpty()) {
                    LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
                    mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    listView.setLayoutManager(mLinearLayoutManager);
                    listView.setAdapter(new RecyclerAdapterMrJeff<HolderRecyclerHome,Products>(getActivity(),products,R.layout.producthome) {
                        @Override
                        public HolderRecyclerHome getInstanceHolder(View v) {
                            HolderRecyclerHome holder = new HolderRecyclerHome(v,(HomeActivity)getActivity());
                            changeHeightListProduct(holder,products.size());
                            return holder;
                        }
                    });
                } else {
                    if(progressDialog == null) {
                        progressDialog = ProgressDialogMrJeff.createAndStart(getActivity(), R.string.searchmywash);
                    }else {
                        progressDialog.show();
                    }
                    FindProductsWorker worker = FindProductsWorker.getInstance(listView.getContext());
                    worker.addObserver(this);
                    Thread thread = new Thread(worker);
                    thread.start();

                }
            }
        }

        protected void changeHeightListProduct(final HolderRecyclerHome h, int size){
            int aux = 4;
            if(size < 4){
                aux = size;
            }
            try {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) h.getPanelproduct().getLayoutParams();
                if (params == null) {
                    params = new RelativeLayout.LayoutParams(listView.getWidth(), listView.getHeight() / aux);
                } else {
                    params.height = Integer.valueOf(listView.getHeight() / aux);
                }


                h.getPanelproduct().setLayoutParams(params);
                h.getPanelproduct().requestLayout();

            }catch (Exception e){
                try {
                    AbsListView.LayoutParams params = new AbsListView.LayoutParams(listView.getWidth(), listView.getHeight() / aux);
                    h.getPanelproduct().setLayoutParams(params);
                    h.getPanelproduct().requestLayout();
                }catch (Exception e1){
                    Log.e("ERROR_LIST","Error al generar lista",e1);
                }
            }
        }

        @Override
        public void update(Observable observable, Object data) {
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    if(progressDialog != null && progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            searchKey();
                        }
                    });
                }
            });

            threadNotiy.start();
        }

    }


    public static class FragmentListNewSuscriptionHome extends Fragment implements Observer{

        protected View v;
        private FragmentListNewSuscriptionHome instance;
        private RecyclerViewMrJeff listView;
        protected TextView textCart;
        protected ImageView btnCart;
        private String key;
        private Handler handler = new Handler();
        private int pos=2;
        private ProgressDialogMrJeff progressDialog;

        public static FragmentListNewSuscriptionHome newInstance(Bundle options) {
            FragmentListNewSuscriptionHome var1 = new FragmentListNewSuscriptionHome();
            var1.setArguments(options);
            return var1;
        }

        public FragmentListNewSuscriptionHome() {
            super();
            this.instance = this;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle arguments = getArguments();
            if(arguments != null && arguments.containsKey(HomeActivity.PARAM_KEY)) {
                key = arguments.getString(HomeActivity.PARAM_KEY);
            }

            if(arguments != null && arguments.containsKey(MrJeffConstant.INTENT_ATTR.POSFRAGMENT)){
                pos = arguments.getInt(MrJeffConstant.INTENT_ATTR.POSFRAGMENT);
            }
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v =  inflater.inflate(R.layout.fragment_list_product_home, container, false);
            initComponents(v);
            initEvent();
            searchKey();
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        protected void initComponents(View v){
            textCart = (TextView)getActivity().findViewById(R.id.numberCartHeader);
            btnCart = (ImageView)getActivity().findViewById(R.id.btnCart);
            textCart.setText(OrderProductsStorage.getInstance(getActivity()).getNumTotal().toString());

            textCart.setVisibility(View.VISIBLE);
            btnCart.setVisibility(View.VISIBLE);
            listView = (RecyclerViewMrJeff) v.findViewById(R.id.listProductsHome);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
            mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            listView.setLayoutManager(mLinearLayoutManager);
        }



        private void initEvent(){

        }


        private void searchKey(){
            if(key != null) {
                final ArrayList<Products> products = ProductStorage.getInstance(getActivity()).getCategory(key);

                if (products != null && !products.isEmpty()) {
                    listView.setAdapter(new RecyclerAdapterMrJeff<HolderRecyclerHomeSuscription,Products>(getActivity(),products,R.layout.sucriptionhome) {
                        @Override
                        public HolderRecyclerHomeSuscription getInstanceHolder(View v) {
                            HolderRecyclerHomeSuscription holder =  new HolderRecyclerHomeSuscription(v);
                            changeHeightListProduct(holder,products.size());
                            return holder;
                        }
                    });
                } else {
                    if(progressDialog == null) {
                        progressDialog = ProgressDialogMrJeff.createAndStart(getActivity(), R.string.searchmywash);
                    }else {
                        progressDialog.show();
                    }
                    FindProductsWorker worker = FindProductsWorker.getInstance(listView.getContext());
                    worker.addObserver(this);
                    Thread thread = new Thread(worker);
                    thread.start();

                }
            }
        }

        protected void changeHeightListProduct(final HolderRecyclerHomeSuscription h, int size){
            int aux = 4;
            if(size < 4){
                aux = size;
            }
            try {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) h.getPanelproduct().getLayoutParams();
                if (params == null) {
                    params = new RelativeLayout.LayoutParams(listView.getWidth(), listView.getHeight() / aux);
                } else {
                    params.height = Integer.valueOf(listView.getHeight() / aux);
                }


                h.getPanelproduct().setLayoutParams(params);
                h.getPanelproduct().requestLayout();

            }catch (Exception e){
                try {
                    AbsListView.LayoutParams params = new AbsListView.LayoutParams(listView.getWidth(), listView.getHeight() / aux);
                    h.getPanelproduct().setLayoutParams(params);
                    h.getPanelproduct().requestLayout();
                }catch (Exception e1){
                    Log.e("ERROR_LIST","Error al generar lista",e1);
                }
            }
        }


        @Override
        public void update(Observable observable, Object data) {
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    if(progressDialog != null && progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            searchKey();
                        }
                    });
                }
            });

            threadNotiy.start();
        }
    }


    public static class FragmentSuscription extends Fragment implements Animation.AnimationListener, Observer{

        protected View v;
        public static final String KEY_NOLOVA_ATTR= "Claim";
        private Handler handler = new Handler();
        private FragmentSuscription instance;
        protected TextView textCart;
        protected ImageView btnCart;
        private String key;
        private Integer idProduct;
        private int pos=2;
        private ImageView imagestate;
        private MrJeffTextView datesus;
        private MrJeffTextView hoursus;
        private MrJeffTextView buttonrate;
        private MrJeffTextView buttonrated;
        private RelativeLayout buttoncall;
        private MrJeffTextView labeltype;
        private MrJeffTextView numorder;
        private MrJeffTextView numtotal;
        private MrJeffTextView dateend;
        private MrJeffTextView how;
        private ImageView imagecamisas;
        private ImageView imagecolada;
        private MrJeffTextView coladanumber;
        private MrJeffTextView camisasnumber;
        private RelativeLayout panelqueda;
        private LinearLayout panelrest;
        private LinearLayout panelentramite;
        private RelativeLayout panelinfo;
        private MrJeffTextView texthelp;
        private RelativeLayout panelinfo2;
        private MrJeffTextView texthelp2;
        protected ProgressDialog progressDialog;
        protected boolean show = false;
        private Animation bottomUp;
        private Animation bottomDown;
        private Orders order;
        protected RateControllerDash rateController;
        protected SwipeRefreshLayout swipe;
        int times=0;

        public static FragmentSuscription newInstance(Bundle options) {
            FragmentSuscription var1 = new FragmentSuscription();
            var1.setArguments(options);
            return var1;
        }

        public FragmentSuscription() {
            super();
            this.instance = this;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle arguments = getArguments();
            if(arguments != null && arguments.containsKey(HomeActivity.PARAM_KEY)) {
                key = arguments.getString(HomeActivity.PARAM_KEY);
            }

            if(arguments != null && arguments.containsKey(MrJeffConstant.INTENT_ATTR.POSFRAGMENT)){
                pos = arguments.getInt(MrJeffConstant.INTENT_ATTR.POSFRAGMENT);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v =  inflater.inflate(R.layout.fragment_suscription, container, false);
            initComponents(v);
            loadAllSuscription();
            swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
//                    GetSuscriptionWorker worker = new GetSuscriptionWorker(CustomerStorage.getInstance(getActivity()).getCustomer().getId().toString(),getActivity());
//                    worker.addObserver(FragmentSuscription.this);
//                    Thread thread = new Thread(worker);
//                    thread.start();
                    loadAllSuscription();
                }
            });
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        protected void initComponents(View v){
            rateController = new RateControllerDash(v,this);
            textCart = (TextView)getActivity().findViewById(R.id.numberCartHeader);
            btnCart = (ImageView)getActivity().findViewById(R.id.btnCart);
            textCart.setText(OrderProductsStorage.getInstance(getActivity()).getNumTotal().toString());
//            textCart.setVisibility(View.GONE);
//            btnCart.setVisibility(View.GONE);

            swipe = (SwipeRefreshLayout)v.findViewById(R.id.swipe);
            imagestate=(ImageView) v.findViewById(R.id.imagestate);
            datesus = (MrJeffTextView)v.findViewById(R.id.datesus);
            hoursus = (MrJeffTextView)v.findViewById(R.id.hoursus);
            buttonrate = (MrJeffTextView)v.findViewById(R.id.buttonrate);
            buttonrated = (MrJeffTextView)v.findViewById(R.id.buttonrated);
            buttoncall = (RelativeLayout)v.findViewById(R.id.buttoncall);
            numorder = (MrJeffTextView) v.findViewById(R.id.numorder);
            numtotal = (MrJeffTextView) v.findViewById(R.id.numtotal);
            dateend = (MrJeffTextView) v.findViewById(R.id.dateend);
            imagecamisas = (ImageView)v.findViewById(R.id.imagecamisas);
            imagecolada = (ImageView)v.findViewById(R.id.imagecolada);
            coladanumber = (MrJeffTextView) v.findViewById(R.id.coladanumber);
            camisasnumber = (MrJeffTextView) v.findViewById(R.id.camisasnumber);
            panelqueda = (RelativeLayout)v.findViewById(R.id.panelqueda);
            panelrest = (LinearLayout)v.findViewById(R.id.panelrest);
            panelentramite = (LinearLayout)v.findViewById(R.id.panelentramite);
            labeltype = (MrJeffTextView) v.findViewById(R.id.labeltype);
            texthelp = (MrJeffTextView) v.findViewById(R.id.texthelp);
            panelinfo = (RelativeLayout) v.findViewById(R.id.panelinfo);
            texthelp2 = (MrJeffTextView) v.findViewById(R.id.texthelp2);
            panelinfo2 = (RelativeLayout) v.findViewById(R.id.panelinfo2);
            bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.bottom_up);
            bottomUp.setAnimationListener(this);
            bottomDown= AnimationUtils.loadAnimation(getActivity(), R.anim.bottom_down);
            bottomDown.setAnimationListener(this);
            how= (MrJeffTextView) v.findViewById(R.id.how);
        }

        private void initData(){

            SuscriptionInfo info = SuscriptionCustomerStorage.getInstance(getActivity()).getOrder();
            if(info != null) {
                Integer numOrders = Integer.valueOf(info.getActualOrders());
                Integer numTotal = Integer.valueOf(info.getMaxOrders());
                numorder.setText(Integer.valueOf(numTotal - numOrders).toString());
                numtotal.setText(info.getMaxOrders());
//                if(progressDialog == null) {
//                    progressDialog = ProgressDialogMrJeff.createAndStart(getActivity(), R.string.searchmywash);
//                }else if (!progressDialog.isShowing()){
//                    progressDialog.show();
//                }
//                times++;
//                FindOrdersCustomerWorker worker = new FindOrdersCustomerWorker(CustomerStorage.getInstance(getActivity()).getCustomer().getId().toString(), true, getActivity(),Integer.parseInt(info.getOrderId()));
//                worker.addObserver(this);
//                Thread threadProduct = new Thread(worker);
//                threadProduct.start();
            } else {
                numorder.setText("0");
                numtotal.setText("0");
            }

            Integer colada = 0;
            Integer maxColada = 55;
            try {
                maxColada = Integer.valueOf(info.getMaxKilos());
                colada= Integer.valueOf(info.getActualkilos());
            }catch (Exception e){

            }
            paintImagePercent(maxColada,colada,imagecolada);
            colada = maxColada - colada;
            coladanumber.setText(colada+"Kg");

            Integer camisas = 0;
            Integer maxCamisas = 55;
            try {
                maxCamisas = Integer.valueOf(info.getMaxShirts());
                camisas= Integer.valueOf(info.getActualShirts());
            }catch (Exception e){

            }
            paintImagePercent(maxCamisas,camisas,imagecamisas);
            camisas = maxCamisas - camisas;
            camisasnumber.setText(camisas.toString());


            try {
                SimpleDateFormat formatRequest = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                SimpleDateFormat formatPaint = new SimpleDateFormat("dd/MM/yyyy");
                dateend.setText(formatPaint.format(formatRequest.parse(info.getEndDate())));

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(formatRequest.parse(info.getEndDate()));
                String text = texthelp.getText().toString();
                StringBuilder builderDate = new StringBuilder();
                builderDate.append(text);
                builderDate.append(" ");
                builderDate.append(calendar.get(Calendar.DAY_OF_MONTH));
                builderDate.append(" ");
                builderDate.append(getActivity().getResources().getString(MonthStorage.months.get(calendar.get(Calendar.MONTH)+1)));
                texthelp.setText(builderDate.toString());
            }catch (Exception e){
                dateend.setText("");
            }

            if(Integer.valueOf(1).equals(info.getOrdervisitstext())){
                labeltype.setText(labeltype.getContext().getString(R.string.das31));
            } else {
                labeltype.setText(getActivity().getString(R.string.das32));
            }

            order = OrdersCustomerStorage.getInstance(getActivity()).getOrder(Integer.valueOf(info.getOrderId()));
            if(order != null){
                paintOrder(order);
            } else {
            }


            if(SuscriptionCustomerStorage.getInstance(getActivity()).isNeccesaryLoad() && CustomerStorage.getInstance(getActivity()).isLogged() ) {
//                if(progressDialog == null) {
//                    progressDialog = ProgressDialogMrJeff.createAndStart(getActivity(), R.string.searchmywash);
//                }else {
//                    progressDialog.show();
//                }
//                GetSuscriptionWorker worker = new GetSuscriptionWorker(CustomerStorage.getInstance(getActivity()).getCustomer().getId().toString(), getActivity());
//                worker.addObserver(FragmentSuscription.this);
//                Thread thread = new Thread(worker);
//                thread.start();
            }else {

            }
        }

        void loadAllSuscription() {
            if(((HomeActivity)getActivity()).getNoLoad()==-1)
                return;
            if ( CustomerStorage.getInstance(getActivity()).isLogged()) {
                if (progressDialog == null) {
                    progressDialog = ProgressDialogMrJeff.createAndStart(getActivity(), R.string.searchmywash);
                } else {
                    progressDialog.show();
                }
                rx.Observable.create(new rx.Observable.OnSubscribe<Boolean>() {
                    @Override
                    public void call(Subscriber<? super Boolean> subscriber) {
                        //cargamos los datos de la suscripcion
                        JeffApi jeffApi = ApiServiceGenerator.createService(JeffApi.class);
                        Call<SuscriptionInfo> suscriptionInfoCall = jeffApi.getSuscriptionInfo(CustomerStorage.getInstance(getActivity()).getCustomer().getId().toString());
                        try {
                            SuscriptionInfo info = suscriptionInfoCall.execute().body();
                            if (info != null) {
                                SuscriptionCustomerStorage.getInstance(getActivity()).setIddCustomer(CustomerStorage.getInstance(getActivity()).getCustomer().getId().toString());
                                SuscriptionCustomerStorage.getInstance(getActivity()).addInfo(info);
                                SuscriptionCustomerStorage.getInstance(getActivity()).saveJson();
                                //cargamos orden
                                SearchOrderService serviceOrder = new SearchOrderService();
                                Orders orders = serviceOrder.searchOrder(Integer.valueOf(info.getOrderId()));
                                if (orders != null) {
                                    SearchStatusService searchStatusService = new SearchStatusService();
                                    Map<String, StatusOrder> status = searchStatusService.searchStatus(info.getOrderId().toString());

                                    if (status != null && !status.isEmpty()) {
                                        orders.setStateEnum(getState(status.get(orders.getId().toString())));
                                    }
                                    OrdersCustomerStorage.getInstance(getActivity()).addOrder(orders);
                                    OrdersCustomerStorage.getInstance(getActivity()).saveJson();
                                }
                                subscriber.onNext(true);
                                subscriber.onCompleted();
                            } else {
                                subscriber.onNext(false);
                                subscriber.onCompleted();
                            }
                        } catch (IOException e) {
                            subscriber.onError(e);
                            e.printStackTrace();

                        }
                    }
                }).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<Boolean>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                swipe.setRefreshing(false);
                                showError();
                            }

                            @Override
                            public void onNext(Boolean aBoolean) {
                                progressDialog.dismiss();
                                if (aBoolean){
                                    initEvent();
                                    initData();
                                }
                                else showError();
                            }
                        });
            }
        }

        private void showError() {
            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorConectionTitle).setMessage(R.string.modalErrorConectionSubTitle).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    loadAllSuscription();
                }
            }).show();
        }

        private EnumStateOrder getState(StatusOrder statusOrder){
            EnumStateOrder result = null;

            if(statusOrder != null && statusOrder.getIdstatus() >= 0){
                switch (statusOrder.getIdstatus()){
                    case 0:
                        result = EnumStateOrder.CONFIRM;
                        break;
                    case 1:
                        result = EnumStateOrder.PICKUP;
                        break;
                    case 2:
                        result = EnumStateOrder.WASHING;
                        break;
                    case 3:
                        result = EnumStateOrder.DELIVERY;
                        break;
                    case 4:
                        result = EnumStateOrder.FINALLY;
                        break;
                    default:
                        result = EnumStateOrder.CONFIRM;
                        break;
                }
            } else {
                result = EnumStateOrder.CONFIRM;
            }
            return result;
        }

        private void paintImagePercent(Integer total, Integer use, ImageView image){
            int percent = (use * 100) / total;
            percent  = 100 - percent;

            if(percent <20){
                image.setImageResource(R.drawable.circledas0);
            } else if(percent <40){
                image.setImageResource(R.drawable.circledas20);
            }else if(percent <60){
                image.setImageResource(R.drawable.circledas40);
            }else if(percent <80){
                image.setImageResource(R.drawable.circledas80);
            }else {
                image.setImageResource(R.drawable.circledas100);
            }
        }

        private void paintOrder(Orders order){

            if(order != null){
                if(order.getStateEnum() == null){
                    order.setStateEnum(EnumStateOrder.CONFIRM);
                }
                Log.d("fecha recogida", order.getOrder_meta().getMyfield2()+"*"+order.getOrder_meta().getMyfield3());
                Log.d("fecha entrega", order.getOrder_meta().getMyfield4()+"*"+order.getOrder_meta().getMyfield5());
                switch (order.getStateEnum()){
                    case CONFIRM:
                        Log.d("paintOrder:", "CONFIRM");
                        imagestate.setImageResource(R.drawable.dasenproceso);
                        String date= order.getOrder_meta().getMyfield2()+" "+ order.getOrder_meta().getMyfield3().split(" - ")[1];
                        DateTime dateTime= DateTimeFormat.forPattern("dd-MM-yyyy HH:mm").parseDateTime(date);
                        if(new DateTime().isAfter(dateTime)){
                            paintDate(order.getOrder_meta().getMyfield4(),order.getOrder_meta().getMyfield5());
                            order.setStateEnum(EnumStateOrder.WASHING);
                            OrdersCustomerStorage ordersCustomerStorage= OrdersCustomerStorage.getInstance(null);
                            ordersCustomerStorage.addOrder(order);
                            ordersCustomerStorage.saveJson();
                        }
                            else
                            paintDate(order.getOrder_meta().getMyfield2(),order.getOrder_meta().getMyfield3());
                         break;
                    case PICKUP:
                        Log.d("paintOrder:", "PICKUP");
                        imagestate.setImageResource(R.drawable.dasrecogida);
                        date= order.getOrder_meta().getMyfield2()+" "+ order.getOrder_meta().getMyfield3().split(" - ")[1];
                        dateTime= DateTimeFormat.forPattern("dd-MM-yyyy HH:mm").parseDateTime(date);
                        if(new DateTime().isAfter(dateTime)){
                            paintDate(order.getOrder_meta().getMyfield4(),order.getOrder_meta().getMyfield5());
                            order.setStateEnum(EnumStateOrder.WASHING);
                            OrdersCustomerStorage.getInstance(null).addOrder(order);
                            OrdersCustomerStorage.getInstance(null).saveJson();
                        }
                        else
                            paintDate(order.getOrder_meta().getMyfield2(),order.getOrder_meta().getMyfield3());
                        break;
                    case WASHING:
                        Log.d("paintOrder:", "WASHING");
                        imagestate.setImageResource(R.drawable.daslavando);
                        paintDate(order.getOrder_meta().getMyfield4(),order.getOrder_meta().getMyfield5());
                        break;
                    case DELIVERY:
                    case FINALLY:
                        paintDate(order.getOrder_meta().getMyfield4(),order.getOrder_meta().getMyfield5());
                        imagestate.setImageResource(R.drawable.dasentregado);
                        if(order.isHaveRate()){
                            buttonrated.setVisibility(View.VISIBLE);
                        } else {
                            buttonrate.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }else {
                datesus.setText("");
                hoursus.setText("");
            }


        }

        private void paintDate(String date, String hour){
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            Calendar calendar = Calendar.getInstance();
            try {
                calendar.setTime(format.parse(date));

                try {
                    StringBuilder builderDate = new StringBuilder();
                    builderDate.append(calendar.get(Calendar.DAY_OF_MONTH));
                    builderDate.append(" ");
                    builderDate.append(getActivity().getResources().getString(MonthStorage.months.get(calendar.get(Calendar.MONTH)+1)));
                    builderDate.append(" ");
                    builderDate.append(calendar.get(Calendar.YEAR));
                    datesus.setText(builderDate.toString());
                }catch (Exception e){
                    datesus.setText("");
                    hoursus.setText("");
                }

                String hourInit = hour.split(":")[0];
                String hourPaint = Integer.valueOf(hourInit)+"h - "+(Integer.valueOf(hourInit)+1)+"h";
                hoursus.setText(hourPaint);
            } catch (Exception   e) {
                datesus.setText("");
                hoursus.setText("");
            }
        }



        private void initEvent(){


            panelinfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(show){
                        hiddenPanel();
                    }
                }
            });

            panelinfo2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(show){
                        hiddenPanel2();
                    }
                }
            });



            panelqueda.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!show){
                        showPanel();
                    }
                }
            });

            how.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!show){
                        showPanel2();
                    }
                }
            });

            buttoncall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnCall();
                }
            });

            buttonrate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(order != null && order.getId() != null){
                        openRate(order.getId().toString());
                    }
                }
            });

            panelentramite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), OrderAddressDashboardPresentActivity.class);
                    intent.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT,0);
                    intent.putExtra(MrJeffConstant.INTENT_ATTR.DEFAULT,false);
                    intent.putExtra(MrJeffConstant.INTENT_ATTR.ISEDIT,false);
                    intent.putExtra(MrJeffConstant.INTENT_ATTR.EDITABLE,SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getOrdersinteraction().intValue() != 0);
                    intent.putExtra(MrJeffConstant.INTENT_ATTR.ORDERID,Integer.valueOf(SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getOrderId()));
                    getActivity().startActivity(intent);
                }
            });

            panelrest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getOrdersinteraction().intValue() != 0) {
                        Intent intent = new Intent(getActivity(), OrderAddressDashboardPresentActivity.class);
                        intent.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 0);
                        intent.putExtra(MrJeffConstant.INTENT_ATTR.DEFAULT, true);
                        intent.putExtra(MrJeffConstant.INTENT_ATTR.ISEDIT,false);
                        intent.putExtra(MrJeffConstant.INTENT_ATTR.EDITABLE, SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getOrdersinteraction().intValue() != 0);
                        intent.putExtra(MrJeffConstant.INTENT_ATTR.ORDERID, Integer.valueOf(SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getOrderId()));
                        getActivity().startActivity(intent);
                    }
                }
            });

        }

        public void btnCall(){
            new LocalyticsEventActionMrJeff(getActivity(),MrJeffConstant.LOCALYTICS_EVENTS.ACTION.SUSCRIPTIONCALL,Boolean.TRUE,"Correcto");
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse(MrJeffConstant.CALL.phoneMrJeff));
            startActivity(callIntent);
        }

        private void  showPanel(){
            show = true;
            panelinfo.startAnimation(bottomUp);
            panelinfo.setVisibility(View.VISIBLE);
        }


        private void  hiddenPanel(){
            show = false;
            panelinfo.startAnimation(bottomDown);
            panelinfo.setVisibility(View.GONE);
        }

        private void  showPanel2(){
            show = true;
            panelinfo2.startAnimation(bottomUp);
            panelinfo2.setVisibility(View.VISIBLE);
        }


        private void  hiddenPanel2(){
            show = false;
            panelinfo2.startAnimation(bottomDown);
            panelinfo2.setVisibility(View.GONE);
        }




        @Override
        public void update(final Observable observable, Object data) {
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(observable instanceof GetSuscriptionWorker){
                                swipe.setRefreshing(false);
                                times++;
                                initData();
                            } else {
                                Log.d("times", times+ " run: ");
                                if (progressDialog != null && progressDialog.isShowing() && times>2) {
                                    progressDialog.dismiss();
                                    times=0;
                                }
                                order = OrdersCustomerStorage.getInstance(getActivity()).getOrder(Integer.valueOf(SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getOrderId()));
                                if (order != null) {
                                    paintOrder(order);
                                }
                            }
                        }
                    });
                }
            });

            threadNotiy.start();

        }



        @Override
        public void onAnimationStart(Animation animation) {
            if(!show){
                panelinfo.setBackgroundResource(android.R.color.transparent);
                panelinfo2.setBackgroundResource(android.R.color.transparent);
            }
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            if(show){
                panelinfo.setBackgroundResource(R.color.colorSecondaryTrans);
                panelinfo2.setBackgroundResource(R.color.colorSecondaryTrans);
            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }

        public void setValoration(){
            buttonrated.setVisibility(View.VISIBLE);
            buttonrate.setVisibility(View.GONE);
        }

        public void openRate(String idOrder){
            rateController.showPanel(idOrder);
        }
    }


    public class TabCollectionPagerAdapter extends FragmentStatePagerAdapter {
        private HomeActivity context;
        private int count = 5;


        public TabCollectionPagerAdapter(FragmentManager fm, HomeActivity context) {
            super(fm);
            this.context = context;
        }

        @Override
        public Fragment getItem(int i) {

            Bundle params = new Bundle();
            params.putString(PARAM_KEY, getlabel(i));
            params.putInt(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, i);
            //pos = i;
            return getFragment(i, params);
        }

        @Override
        public int getCount() {
            return count;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "OBJECT " + position;
        }
    }


}


