package es.coiasoft.mrjeff;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.localytics.android.Localytics;

import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.manager.worker.CustomerWorker;
import es.coiasoft.mrjeff.manager.worker.LoginFacebookWorker;
import es.coiasoft.mrjeff.manager.worker.LoginGPlusWorker;
import es.coiasoft.mrjeff.manager.worker.LoginWorker;
import es.coiasoft.mrjeff.manager.worker.UpdateCacheOrderWorker;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.listener.FocusChangeNewUserEditTextListener;

/**
 * Created by linovm on 3/12/15.
 */
public class LoginNewAccountActivity extends ConnectionActivity implements GoogleApiClient.OnConnectionFailedListener {


    protected LoginNewAccountActivity instance;
    protected GoogleApiClient mGoogleApiClient;
    protected GoogleSignInOptions gso;
    protected FragmentLoginView fragmentLoginView;
    protected boolean blockBack = true;
    private static final String FRAGMENT_OPTIONS="options";
    private static final String FRAGMENT_LOGIN_FORM="form";
    private static final String FRAGMENT_NEW_USER="newuser";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        finishActivity = Boolean.FALSE;
        FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().requestProfile().requestId()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        instance = this;
        try {
            LoginManager.getInstance().logOut();
        }catch (Exception e){

        }

        fragmentLoginView = FragmentLoginView.newInstance(null);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentLayoutLogin, fragmentLoginView, FRAGMENT_OPTIONS);
        ft.commit();


    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void setContentViewActivity() {
        setContentView(R.layout.activity_login);
    }

    public void goToLoginForm() {
        Bundle params = new Bundle();
        FragmentLoginFormView fragment = FragmentLoginFormView.newInstance(params);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.fadeinrigth, R.anim.fadeoutleft, R.anim.fadeinleft, R.anim.fadeoutrigth);
        ft.replace(R.id.fragmentLayoutLogin, fragment, FRAGMENT_LOGIN_FORM);
        ft.addToBackStack(null);
        ft.commit();
        blockBack=false;
    }

    public void goToRegister() {
        Bundle params = new Bundle();
        FragmentNewUserView fragment = FragmentNewUserView.newInstance(params);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.fadeinrigth, R.anim.fadeoutleft, R.anim.fadeinleft, R.anim.fadeoutrigth);
        ft.replace(R.id.fragmentLayoutLogin, fragment, FRAGMENT_NEW_USER);
        ft.addToBackStack(null);
        ft.commit();
        blockBack=false;
    }

    public void gotoBackLoginForm() {
        Bundle params = new Bundle();
        FragmentLoginFormView fragment = FragmentLoginFormView.newInstance(params);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.fadeinleft, R.anim.fadeoutrigth, R.anim.fadeinrigth, R.anim.fadeoutleft);
        ft.replace(R.id.fragmentLayoutLogin, fragment, FRAGMENT_LOGIN_FORM);
        ft.addToBackStack(null);
        ft.commit();
        blockBack=false;
    }


    public void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, MrJeffConstant.ACTIVITY_RESULT.GOOGLE_SIG_IN);
    }




    @Override
    public void onBackPressed() {
      if(!blockBack){
          super.onBackPressed();
      }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(fragmentLoginView != null ){
            fragmentLoginView.finishResultActivity(requestCode,resultCode,data);
        }


    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    protected String getNameScreen(){
        return MrJeffConstant.LOCALYTICS.SCREEN_LOGIN;
    }


    public static class FragmentLoginFormView extends Fragment implements Observer{


        protected View v;
        private Handler handler = new Handler();
        private FragmentLoginFormView instance;
        private ProgressDialogMrJeff progressDialogMrJeff;
        protected EditText login_email;
        protected EditText login_password;
        protected LinearLayout windowlogin;
        protected LinearLayout btnlogin;
        protected LinearLayout btloginregister;

        public ProgressDialogMrJeff getProgressDialogMrJeff() {
            return progressDialogMrJeff;
        }

        public void setProgressDialogMrJeff(ProgressDialogMrJeff progressDialogMrJeff) {
            this.progressDialogMrJeff = progressDialogMrJeff;
        }


        public static FragmentLoginFormView newInstance(Bundle options) {
            FragmentLoginFormView var1 = new FragmentLoginFormView();
            var1.setArguments(options);
            return var1;
        }

        public FragmentLoginFormView() {
            super();
            this.instance = this;
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v = inflater.inflate(R.layout.loginuser_layout, container, false);
            initComponents(v);
            initEvent();
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        protected void initComponents(View v) {
            windowlogin = (LinearLayout) v.findViewById(R.id.wdlogin);
            login_email = (EditText) v.findViewById(R.id.user_email_login);
            login_password = (EditText) v.findViewById(R.id.user_pass_login);
            btnlogin = (LinearLayout) v.findViewById(R.id.btnlogin);
            btloginregister = (LinearLayout) v.findViewById(R.id.btloginregister);
        }


        private void initEvent() {
            btnlogin.setOnClickListener(getOnClickListenerLogin());

            btloginregister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LoginNewAccountActivity)getActivity()).goToRegister();
                }
            });

            login_password.setOnEditorActionListener(getClickListenerForATLogin());
        }

        private TextView.OnEditorActionListener getClickListenerForATLogin() {
            return new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        senLoginOder();
                    }
                    return false;
                }
            };
        }

        private View.OnClickListener getOnClickListenerLogin() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(login_email.getWindowToken(), 0);
                    inputMethodManager.hideSoftInputFromWindow(login_password.getWindowToken(), 0);
                    senLoginOder();

                }
            };
        }


        private void senLoginOder() {
            boolean haveError = !FocusChangeNewUserEditTextListener.validRequired(login_email.getText().toString());
            haveError = haveError || !FocusChangeNewUserEditTextListener.validRequiredMin(6, login_password.getText().toString());


            if (haveError) {
                new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getActivity().setResult(Activity.RESULT_OK);
                    }
                }).show();

            } else {
                loginUser();
            }
        }


        private void loginUser(){
            progressDialogMrJeff = ProgressDialogMrJeff.createAndStart(getActivity(),R.string.loginloading);
            String email = login_email.getText().toString();
            String password = login_password.getText().toString();
            LoginWorker worker = new LoginWorker(email,password);
            worker.addObserver(this);
            Thread thread = new Thread(worker);
            thread.start();
        }


        @Override
        public void update(final Observable observable, Object data) {
            if(observable instanceof  LoginWorker){
                if(data instanceof Integer &&
                        ((Integer)data).intValue() == LoginWorker.RESULT_SUCCESS){
                    Thread threadUpdate = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialogMrJeff = ProgressDialogMrJeff.createAndStart(getActivity(),R.string.logininfo);
                                    UpdateCacheOrderWorker worker = new UpdateCacheOrderWorker(getActivity());
                                    worker.addObserver(FragmentLoginFormView.this);
                                    Thread thread = new Thread(worker);
                                    thread.start();
                                    if(getProgressDialogMrJeff() != null &&
                                            getProgressDialogMrJeff().isShowing()) {
                                        getProgressDialogMrJeff().dismiss();
                                    }
                                    Localytics.setCustomerFirstName(CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name());
                                    Localytics.setCustomerLastName(CustomerStorage.getInstance(getActivity()).getCustomer().getLast_name());
                                    Localytics.setCustomerEmail(CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
                                    Crashlytics.setUserIdentifier(CustomerStorage.getInstance(getActivity()).getCustomer().getId().toString());
                                    Crashlytics.setUserEmail(CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
                                    Crashlytics.setUserName(CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name());


                                    Map<String, String> values = new HashMap<String, String>();
                                    values.put("Email", CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
                                    Localytics.tagEvent("Se ha autenticado", values);
                                    Localytics.tagEvent("Unificador. Se ha autenticado", values);
                                }
                            });
                        }
                    });
                    threadUpdate.start();


                }else {
                    Thread threadUpdate = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if(getProgressDialogMrJeff() != null &&
                                            getProgressDialogMrJeff().isShowing()) {
                                        getProgressDialogMrJeff().dismiss();
                                    }
                                    new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.login_modal_error).setMessage(R.string.login_modal_error_text).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            getActivity().setResult(Activity.RESULT_OK);
                                        }
                                    }).show();
                                }
                            });
                        }
                    });
                    threadUpdate.start();
                }
            } else if(observable instanceof  UpdateCacheOrderWorker){
                if(getProgressDialogMrJeff() != null &&
                        getProgressDialogMrJeff().isShowing()) {
                    getProgressDialogMrJeff().dismiss();
                }
                Intent show = new Intent(getActivity(), HomeActivity.class);
                startActivity(show);
            }

        }



    }


    public static class FragmentLoginView extends Fragment implements Observer{


        protected View v;
        private Handler handler = new Handler();
        private ProgressDialogMrJeff progressDialogMrJeff;
        protected LinearLayout windowoptions;
        protected LinearLayout btnWDLogin;
        protected LoginButton loginFaceButton;
        protected Bundle bFacebookData;
        protected LinearLayout buttonlogingoogle;
        protected FragmentLoginView instance;
        protected CallbackManager callbackManager;

        public static FragmentLoginView newInstance(Bundle options) {
            FragmentLoginView var1 = new FragmentLoginView();
            var1.setArguments(options);
            return var1;
        }

        public FragmentLoginView() {
            super();
            this.instance = this;
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v =  inflater.inflate(R.layout.optionslogin_layout, container, false);
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
            initComponents(v);
            initEvent();
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        public ProgressDialogMrJeff getProgressDialogMrJeff() {
            return progressDialogMrJeff;
        }

        public void setProgressDialogMrJeff(ProgressDialogMrJeff progressDialogMrJeff) {
            this.progressDialogMrJeff = progressDialogMrJeff;
        }

        @Override
        public void update(final Observable observable, Object data) {
            if(observable instanceof LoginFacebookWorker ||
                    observable instanceof LoginGPlusWorker){
                if(data instanceof Integer &&
                        ((Integer)data).intValue() == LoginWorker.RESULT_SUCCESS){
                    Thread threadUpdate = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialogMrJeff = ProgressDialogMrJeff.createAndStart(getActivity(),R.string.logininfo);
                                    UpdateCacheOrderWorker worker = new UpdateCacheOrderWorker(getActivity());
                                    worker.addObserver(FragmentLoginView.this);
                                    Thread thread = new Thread(worker);
                                    thread.start();
                                    if(getProgressDialogMrJeff() != null &&
                                            getProgressDialogMrJeff().isShowing()) {
                                        getProgressDialogMrJeff().dismiss();
                                    }
                                    Localytics.setCustomerFirstName(CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name());
                                    Localytics.setCustomerLastName(CustomerStorage.getInstance(getActivity()).getCustomer().getLast_name());
                                    Localytics.setCustomerEmail(CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());

                                    Crashlytics.setUserIdentifier(CustomerStorage.getInstance(getActivity()).getCustomer().getId().toString());
                                    Crashlytics.setUserEmail(CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
                                    Crashlytics.setUserName(CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name());

                                    Map<String, String> values = new HashMap<String, String>();
                                    values.put("Email", CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
                                    if(observable instanceof LoginGPlusWorker) {
                                        Localytics.tagEvent("Se ha autenticado con facebook", values);
                                    } else {
                                        Localytics.tagEvent("Se ha autenticado con google plus", values);
                                    }
                                    Localytics.tagEvent("Unificador. Se ha autenticado", values);
                                }
                            });
                        }
                    });
                    threadUpdate.start();


                }else {
                    Thread threadUpdate = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if(getProgressDialogMrJeff() != null &&
                                            getProgressDialogMrJeff().isShowing()) {
                                        getProgressDialogMrJeff().hide();
                                    }
                                    new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.login_modal_error).setMessage(R.string.login_modal_error_text).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            getActivity().setResult(Activity.RESULT_OK);
                                        }
                                    }).show();
                                }
                            });
                        }
                    });
                    threadUpdate.start();
                }
            } else if(observable instanceof  UpdateCacheOrderWorker){
                if(getProgressDialogMrJeff() != null &&
                        getProgressDialogMrJeff().isShowing()) {
                    getProgressDialogMrJeff().dismiss();
                }
                Intent show = new Intent(btnWDLogin.getContext(), HomeActivity.class);
                startActivity(show);
            }
        }


        protected void initComponents(View v){
            callbackManager = CallbackManager.Factory.create();
            windowoptions = (LinearLayout)v.findViewById(R.id.menuoptions);
            btnWDLogin = (LinearLayout)v.findViewById(R.id.buttonlogin);

            loginFaceButton = (LoginButton) v.findViewById(R.id.login_button_facebook);
            float fbIconScale = 1.45F;
            Drawable drawable = getResources().getDrawable(
                    com.facebook.R.drawable.com_facebook_button_icon);
            drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * fbIconScale),
                    (int) (drawable.getIntrinsicHeight() * fbIconScale));
            loginFaceButton.setCompoundDrawables(drawable, null, null, null);
            loginFaceButton.setCompoundDrawablePadding(getResources().
                    getDimensionPixelSize(R.dimen.fb_margin_override_textpadding));
            loginFaceButton.setPadding(
                    loginFaceButton.getResources().getDimensionPixelSize(
                            R.dimen.fb_margin_override_lr),
                    loginFaceButton.getResources().getDimensionPixelSize(
                            R.dimen.fb_margin_override_top),
                    0,
                    loginFaceButton.getResources().getDimensionPixelSize(
                            R.dimen.fb_margin_override_bottom));
            loginFaceButton.setReadPermissions("user_friends", "email", "user_about_me", "user_location");

            buttonlogingoogle= (LinearLayout) v.findViewById(R.id.buttonlogingoogle);


            btnWDLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LoginNewAccountActivity)getActivity()).goToLoginForm();
                }
            });
        }



        private void initEvent(){
            loginFaceButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    progressDialogMrJeff = ProgressDialogMrJeff.createAndStart(getActivity(),R.string.accountface);
                    GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.i("LoginActivity", response.toString());
                            // Get facebook data from login
                            bFacebookData = getFacebookData(object);
                            if (bFacebookData != null) {
                                LoginFacebookWorker worker = new LoginFacebookWorker(bFacebookData);
                                worker.addObserver(instance);
                                Thread thread = new Thread(worker);
                                thread.start();
                            } else {
                                new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage("Error").setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        getActivity().setResult(Activity.RESULT_OK);
                                    }
                                }).show();
                            }

                        }
                    });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id, first_name, last_name, email,gender, location"); // Parámetros que pedimos a facebook
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                @Override
                public void onCancel() {
                    new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage("Cancelado").setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            getActivity().setResult(Activity.RESULT_OK);
                        }
                    }).show();
                }

                @Override
                public void onError(FacebookException exception) {
                    new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage("Error").setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            getActivity().setResult(Activity.RESULT_OK);
                        }
                    }).show();
                }
            });


            buttonlogingoogle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LoginNewAccountActivity)getActivity()).signIn();
                }
            });
        }


        private Bundle getFacebookData(JSONObject object) {

            try {
                Bundle bundle = new Bundle();
                String id = object.getString("id");

                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
                Log.i("profile_pic", profile_pic + "");
                bundle.putString("profile_pic", profile_pic.toString());

                bundle.putString("idFacebook", id);
                if (object.has("first_name"))
                    bundle.putString("first_name", object.getString("first_name"));
                if (object.has("last_name"))
                    bundle.putString("last_name", object.getString("last_name"));
                if (object.has("email"))
                    bundle.putString("email", object.getString("email"));
                if (object.has("gender"))
                    bundle.putString("gender", object.getString("gender"));
                if (object.has("birthday"))
                    bundle.putString("birthday", object.getString("birthday"));
                if (object.has("location"))
                    bundle.putString("location", object.getJSONObject("location").getString("name"));

                return bundle;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }

        }

        public void finishResultActivity(int requestCode, int resultCode, Intent data){
            if(callbackManager != null) {
                callbackManager.onActivityResult(requestCode, resultCode, data);
                if (requestCode == MrJeffConstant.ACTIVITY_RESULT.GOOGLE_SIG_IN) {
                    GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                    handleSignInResult(result);
                }
            }
        }


        private void handleSignInResult(GoogleSignInResult result) {
            if (result.isSuccess()) {
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct = result.getSignInAccount();
                final Bundle bundle = new Bundle();

                if(acct.getPhotoUrl() != null){
                    bundle.putString("profile_pic",acct.getPhotoUrl().toString());
                }


                bundle.putString("id", acct.getId());
                if (acct.getDisplayName() != null)
                    bundle.putString("first_name", acct.getDisplayName());
                bundle.putString("last_name", "");
                if (acct.getEmail() != null)
                    bundle.putString("email", acct.getEmail());


                progressDialogMrJeff = ProgressDialogMrJeff.createAndStart(getActivity());
                LoginGPlusWorker worker = new LoginGPlusWorker(bundle);
                worker.addObserver(instance);
                Thread thread = new Thread(worker);
                thread.start();

            } else {
                new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalgplusrror).setMessage(R.string.modalgpluserrortext).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
            }
        }

    }

    public static class FragmentNewUserView extends Fragment implements Observer{


        protected View v;
        private Handler handler = new Handler();
        private FragmentNewUserView instance;
        protected LinearLayout windownewuser;
        private ProgressDialogMrJeff progressDialogMrJeff;
        protected LinearLayout btnnewuser;
        protected LinearLayout btloginlogin;
        protected EditText newuser_email;
        protected EditText newuser_firstname;
        protected EditText newuser_password;
        protected EditText newuser_repassword;

        public ProgressDialogMrJeff getProgressDialogMrJeff() {
            return progressDialogMrJeff;
        }

        public void setProgressDialogMrJeff(ProgressDialogMrJeff progressDialogMrJeff) {
            this.progressDialogMrJeff = progressDialogMrJeff;
        }

        public static FragmentNewUserView newInstance(Bundle options) {
            FragmentNewUserView var1 = new FragmentNewUserView();
            var1.setArguments(options);
            return var1;
        }

        public FragmentNewUserView() {

            super();
            this.instance = this;
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v = inflater.inflate(R.layout.newuser_layout, container, false);
            initComponents(v);
            initEvent();
            return v;
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        protected void initComponents(View v) {
            windownewuser = (LinearLayout)v.findViewById(R.id.wdnewuser);
            newuser_email = (EditText)v.findViewById(R.id.user_email);
            newuser_firstname = (EditText)v.findViewById(R.id.user_firstname);
            newuser_password = (EditText)v.findViewById(R.id.user_pass);
            newuser_repassword = (EditText)v.findViewById(R.id.user_repass);
            btnnewuser = (LinearLayout)v.findViewById(R.id.btnnewuser);
            btloginlogin = (LinearLayout)v.findViewById(R.id.btloginlogin);
        }


        private void initEvent() {
            newuser_email.setOnFocusChangeListener(new FocusChangeNewUserEditTextListener(newuser_email, null));
            newuser_firstname.setOnFocusChangeListener(new FocusChangeNewUserEditTextListener(newuser_firstname,null));
            newuser_password.setOnFocusChangeListener(new FocusChangeNewUserEditTextListener(newuser_password, null));
            newuser_repassword.setOnFocusChangeListener(new FocusChangeNewUserEditTextListener(newuser_repassword, newuser_password));

            btnnewuser.setOnClickListener(getOnClickListenerNewUser());
            btloginlogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((LoginNewAccountActivity)getActivity()).gotoBackLoginForm();
                }
            });

            newuser_repassword.setOnEditorActionListener(getClickListenerForAT());
        }

        private View.OnClickListener getOnClickListenerNewUser(){
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                    createNewUser();

                }
            };
        }

        private TextView.OnEditorActionListener getClickListenerForAT(){
            return new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        createNewUser();
                    }
                    return false;
                }
            };
        }

        private void createNewUser(){
            boolean haveError =  !FocusChangeNewUserEditTextListener.isValidEmail(newuser_email.getText().toString());
            haveError = haveError || !FocusChangeNewUserEditTextListener.validRequired(newuser_firstname.getText().toString());
            haveError = haveError || !FocusChangeNewUserEditTextListener.validRequiredMin(6, newuser_password.getText().toString());
            haveError = haveError || !FocusChangeNewUserEditTextListener.isEqual(newuser_password.getText().toString(), newuser_repassword.getText().toString());


            if(haveError){
                new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getActivity().setResult(Activity.RESULT_OK);
                    }
                }).show();

            } else {
                createUser();
            }
        }

        private void createUser(){
            progressDialogMrJeff = ProgressDialogMrJeff.createAndStart(getActivity(),R.string.registerloading);
            Customer customer = new Customer();
            customer.setUsername(newuser_firstname.getText().toString().trim());
            customer.setFirst_name(newuser_firstname.getText().toString());
            customer.setLast_name(" ");
            customer.setEmail(newuser_email.getText().toString());
            customer.setPassword(newuser_password.getText().toString());
            CustomerWorker worker = new CustomerWorker(customer);
            worker.addObserver(this);
            Thread thread = new Thread(worker);
            thread.start();

        }

        @Override
        public void update(final Observable observable, Object data) {
            if(observable instanceof CustomerWorker){
                if(data instanceof Integer &&
                        ((Integer)data).intValue() == CustomerWorker.RESULT_SUCCESS){

                    Thread threadUpdate = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if(getProgressDialogMrJeff() != null &&
                                            getProgressDialogMrJeff().isShowing()) {
                                        getProgressDialogMrJeff().dismiss();
                                    }
                                    Map<String, String> values = new HashMap<String, String>();
                                    values.put("Email", newuser_email.getText().toString());
                                    Localytics.tagEvent("Se ha registrado", values);
                                    newuser_email.setText("");
                                    newuser_firstname.setText("");
                                    newuser_password.setText("");
                                    newuser_repassword.setText("");
                                    new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.newuser_correct).setMessage(R.string.newuser_correct_subtitle).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            ((LoginNewAccountActivity)getActivity()).gotoBackLoginForm();
                                        }
                                    }).show();
                                }
                            });
                        }
                    });
                    threadUpdate.start();

                } else {
                    Thread threadUpdate = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if(getProgressDialogMrJeff() != null &&
                                            getProgressDialogMrJeff().isShowing()) {
                                        getProgressDialogMrJeff().dismiss();
                                    }
                                    new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.newuser_incorrect).setMessage(R.string.newuser_incorrect_subtitle).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            getActivity().setResult(Activity.RESULT_OK);
                                        }
                                    }).show();
                                }
                            });
                        }
                    });
                    threadUpdate.start();
                }
            }

        }




    }
}
