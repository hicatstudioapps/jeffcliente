package es.coiasoft.mrjeff;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.appsflyer.AppsFlyerLib;
import com.crashlytics.android.Crashlytics;
import com.localytics.android.Localytics;
import com.localytics.android.PushNotificationOptions;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalytisEvenMrJeff;
import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.PostalCodeStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;
import es.coiasoft.mrjeff.manager.worker.DiskLruImageCache;
import es.coiasoft.mrjeff.manager.worker.FindCPWorker;
import es.coiasoft.mrjeff.manager.worker.SendMailAndPhoneWorker;
import es.coiasoft.mrjeff.receiver.AlarmOrderActive;
import es.coiasoft.mrjeff.receiver.AlarmRate;
import es.coiasoft.mrjeff.receiver.AlarmUpdate;
import io.fabric.sdk.android.Fabric;


public class MainActivity extends FragmentActivity {


    private ConfigRepository dataSource;
    private Handler handler;
    private Activity instance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        configLocalytics();
        ExceptionHandler.register(this,"Error jeff cliente"," info@mrjeffapp.com");
        init();
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setAlarms();

       /* DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        switch(metrics.densityDpi){
            case DisplayMetrics.DENSITY_LOW:
                Log.d("SIZE_WINDOW", "El tamaño es DENSITY_LOW " + DisplayMetrics.DENSITY_LOW);
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                Log.d("SIZE_WINDOW", "El tamaño es DENSITY_MEDIUM " + DisplayMetrics.DENSITY_MEDIUM);
                break;
            case DisplayMetrics.DENSITY_HIGH:
                Log.d("SIZE_WINDOW", "El tamaño es DENSITY_HIGH " + DisplayMetrics.DENSITY_HIGH);
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                Log.d("SIZE_WINDOW", "El tamaño es DENSITY_XHIGH " + DisplayMetrics.DENSITY_XHIGH);
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                Log.d("SIZE_WINDOW", "El tamaño es DENSITY_XXHIGH " + DisplayMetrics.DENSITY_XXHIGH);
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
                Log.d("SIZE_WINDOW", "El tamaño es DENSITY_XXXHIGH " + DisplayMetrics.DENSITY_XXXHIGH);
                break;
        } */

    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }


    private void init(){
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "es.coiasoft.mrjeff", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                Log.d("KeyHash:", md.digest().toString());

            }

        }catch (Exception e){

        }
        //setContentView(R.layout.activity_main);
        handler = new Handler();
        instance = this;
        initCacheImages();
        if(PostalCodeStorage.getInstance(getApplicationContext()).count() == 0){
            FindCPWorker worker = new FindCPWorker();
            Thread threadOrder =new Thread(worker);
            threadOrder.start();
        }

        dataSource = new ConfigRepository(instance);
        String value = dataSource.getValueConfig(ConfigRepository.NEW_USER);
        String valueIdCart =  dataSource.getValueConfig(ConfigRepository.IDORDER);
        Boolean isNew = value == null || value.equals("0");
        Boolean haveOrder = valueIdCart != null && !valueIdCart.equals("0") && valueIdCart.length() > 0;
        AppsFlyerLib.setAppsFlyerKey("tEgWda6bWT6HnK9hekRutC");
        AppsFlyerLib.sendTracking(getApplicationContext());
        /*if(haveOrder){
            DeleteOrderWorker worker =new DeleteOrderWorker(valueIdCart);
            Thread thread = new Thread(worker);
            thread.start();
            dataSource.updateRegister(ConfigRepository.IDORDER,"");
        }*/

        if(haveOrder && OrderProductsStorage.getInstance(getApplicationContext()).getNumTotal() != null &&
                OrderProductsStorage.getInstance(getApplicationContext()).getNumTotal().intValue() > 0){
            if(ProductStorage.getInstance(getApplicationContext()).getProduct(OrderProductsStorage.getInstance(getApplicationContext()).getProducts().keySet().iterator().next()) == null){
                OrderProductsStorage.getInstance(getApplicationContext()).clear();
                OrderStorage.getInstance(getApplicationContext()).setOrderSend(new es.coiasoft.mrjeff.domain.orders.send.Order());
                OrderStorage.getInstance(getApplicationContext()).setOrder(new Order());
                OrderStorage.getInstance(getApplicationContext()).saveJson();
                dataSource.updateRegister(ConfigRepository.IDORDER,"");
            }

        }

        if(isNew){
            try {
                LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(getApplicationContext());
                event.send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.NEW_USER);
            }catch (Exception e){

            }
            sendNumberAndMail();
            Intent myIntent = new Intent(instance, VideopInit.class);
            startActivityForResult(myIntent, 0);
        } else {
            String link= null;
            try {
                Bundle bundle = getIntent().getExtras();
                link = bundle.getString("deeplink");

            }catch (Exception e){

            }

            if(link != null  && !link.isEmpty() && !link.trim().equalsIgnoreCase("null")){
                Log.d("GCM-MRJEFF","MainActivity - Deeplink - " + link);
                if(link.startsWith("http")){
                    Intent myIntent = new Intent(instance, DeepLinkingHTTP.class);
                    myIntent.setData(Uri.parse(link));
                    startActivityForResult(myIntent, 0);
                }else {
                    Intent myIntent = new Intent(instance, DeepLinkingMJeffSchema.class);
                    myIntent.setData(Uri.parse(link));
                    startActivityForResult(myIntent, 0);
                }
            } else if(CustomerStorage.getInstance(this).isLogged()) {
                Log.d("GCM-MRJEFF","MainActivity - Deeplink - No Link");
                Intent myIntent = new Intent(instance, HomeActivity.class);
                startActivityForResult(myIntent, 0);
            } else {
                Intent myIntent = new Intent(instance, LoginNewAccountActivity.class);
                Log.d("GCM-MRJEFF","MainActivity - Deeplink - No Link");
                myIntent.putExtra(MrJeffConstant.LOGIN_EXTRA.LOGIN_ATTR,MrJeffConstant.LOGIN_EXTRA.HOME_LOGIN);
                startActivityForResult(myIntent, 0);
            }
        }

        String valueContact =  dataSource.getValueConfig(ConfigRepository.CONTACT);
        Boolean createContact = valueContact == null || valueContact.equals("0");

        if(createContact) {
            createContact();
            dataSource.updateRegister(ConfigRepository.CONTACT,"1");
        }
    }

    public void configLocalytics(){
        Localytics.registerPush("382305254846");
        Localytics.setCustomerId(getBuild());
    }

    private String getBuild(){
            final String deviceId = ((TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
            if (deviceId != null) {
                return deviceId;
            } else {
                return android.os.Build.SERIAL;
            }
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        setIntent(intent);
    }


    private void initCacheImages(){
        final Context context = this;
        Runnable initCacheImages = new Runnable() {
            @Override
            public void run() {
                DiskLruImageCache.getInstance(getApplicationContext());
            }
        };

        (new Thread(initCacheImages)).start();
    }

    private void sendNumberAndMail(){
        String mail = getEmail(this);
        String phone = numberPhone();
        SendMailAndPhoneWorker worker = new SendMailAndPhoneWorker(phone,mail);
        Thread threadOrder = new Thread(worker);
        threadOrder.start();
    }


    private String numberPhone(){
        String mPhoneNumber = "0000000000";
        return mPhoneNumber;
    }


    private String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);

        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }

    private Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    private void setAlarms(){
        setAlarmUpdate();
        setAlarmOrderActive();
        //setAlarmRate();
    }

    private void setAlarmUpdate(){
        Long timeInterval = 3600000L;
        //Long timeInterval = 30000L;
        Intent intentBroad = new Intent(getApplicationContext(), AlarmUpdate.class);
        PendingIntent intent = PendingIntent.getBroadcast(getApplicationContext(),0,intentBroad,0);
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), timeInterval, intent);
    }

    private void setAlarmOrderActive(){
        //Long timeInterval = 3600000L;
        Long timeInterval = 120000L;
        Intent intentBroad = new Intent(getApplicationContext(), AlarmOrderActive.class);
        PendingIntent intent = PendingIntent.getBroadcast(getApplicationContext(), 1, intentBroad, 0);
        Calendar cal = Calendar.getInstance();
        /* cal.set(Calendar.HOUR_OF_DAY,15);
        cal.set(Calendar.MINUTE,0);*/
        long date = cal.getTimeInMillis() + timeInterval;
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,cal.getTimeInMillis(),timeInterval,intent);
    }

    private void setAlarmRate(){
        Long timeInterval = 60000L;
        Intent intentBroad = new Intent(getApplicationContext(), AlarmRate.class);
        PendingIntent intent = PendingIntent.getBroadcast(getApplicationContext(),2,intentBroad,0);
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        // Getting current time and add the seconds in it
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 59);
        Long timeInit = 86400000L * 3L;
        timeInit = cal.getTimeInMillis() + timeInit;
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, timeInit, timeInterval, intent);
    }


    private void createContact(){
        String DisplayName = "MrJeffApp";
        String MobileNumber = "+34644446163";
        String emailID = "hola@mrjeffapp.com";

        ArrayList<ContentProviderOperation> ops = new ArrayList < ContentProviderOperation > ();

        ops.add(ContentProviderOperation.newInsert(
                ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

        if (DisplayName != null) {
            ops.add(ContentProviderOperation.newInsert(
                    ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(
                            ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                            DisplayName).build());
        }

        if (MobileNumber != null) {
            ops.add(ContentProviderOperation.
                    newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, MobileNumber)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());
        }


        if (emailID != null) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Email.DATA, emailID)
                    .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                    .build());
        }

        try {
            getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            Log.d("CONTACT", "Error al crear",e);
            Toast.makeText(instance, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


}
