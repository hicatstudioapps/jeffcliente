package es.coiasoft.mrjeff;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import java.io.InputStream;
import java.util.ArrayList;


import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.conn.scheme.Scheme;
import cz.msebera.android.httpclient.conn.ssl.SSLSocketFactory;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.domain.ItemList;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;

/**
 * Created by linovm on 21/10/15.
 */
public abstract class MenuActivity extends ConnectionActivity {

    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    protected ArrayList<ItemList> items;
    protected TextView cardActivos;
    protected TextView numberorders;
    protected TextView cartHeader;
    private RelativeLayout btnCartPanel;
    protected LinearLayout buttonLoginNewAccount;
    protected LinearLayout buttonLoginOut;
    protected int titleWindowId = -1;
    protected Handler handlerMenu = new Handler();
    //protected LoginDialog loginDialog;
    protected GoogleSignInOptions gso;
    protected MrJeffTextView numpage;
    protected TextView textheader;
    protected NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        setToolbar();
        initComponentsMenu();
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(getOnMenuClickListener(this));
            if(posMenu() >= 0 && posMenu() < navigationView.getMenu().size()) {
                navigationView.getMenu().getItem(posMenu()).setChecked(true);
            }
        }

        if (savedInstanceState == null) {
            // Seleccionar item
        }

    }


    protected void initComponentsMenu(){
        textheader = (TextView)toolbar.findViewById(R.id.titleHeader);
        if(titleWindowId != -1){
            textheader.setText(titleWindowId);
        }
        numpage = (MrJeffTextView)toolbar.findViewById(R.id.numpage);
        btnCartPanel = (RelativeLayout)toolbar.findViewById(R.id.panelbuttoncart);
        cartHeader = (TextView)toolbar.findViewById(R.id.numberCartHeader);
        eventComponentsHeader();
        initBySessionUser();
        validLogin();



    }

    protected void eventComponentsHeader(){
        btnCartPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(OrderProductsStorage.getInstance(getApplicationContext()).getNumTotal().intValue() >0) {
                    new LocalyticsEventActionMrJeff(MenuActivity.this, MrJeffConstant.LOCALYTICS_EVENTS.ACTION.HOMECART,Boolean.FALSE,"Correcto");
                    createOrder();
                } else {
                    new LocalyticsEventActionMrJeff(MenuActivity.this, MrJeffConstant.LOCALYTICS_EVENTS.ACTION.HOMECART,Boolean.FALSE,"No Correcto. Carrito vacio");
                    new AlertDialog.Builder(new ContextThemeWrapper(MenuActivity.this, R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorCartNumProduct).setMessage(R.string.modalErrorCartNumProductText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            setResult(RESULT_OK);
                        }
                    }).show();
                }
            }
        });
    }

    private void validLogin(){
        if(!CustomerStorage.getInstance(getApplicationContext()).isLogged()) {
            Intent myIntent = new Intent(this, LoginNewAccountActivity.class);
            myIntent.putExtra(MrJeffConstant.LOGIN_EXTRA.LOGIN_ATTR,MrJeffConstant.LOGIN_EXTRA.HOME_LOGIN);
            startActivityForResult(myIntent, 0);
        }
    }
    protected abstract void setContentView();

    protected abstract int posMenu();

    protected void setContentViewActivity(){
        setContentView();
    }

    protected void createOrder(){
        OrderStorage.getInstance(this).setProductMinOrder(null);

    }



    private void setToolbar() {
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            ab.setHomeAsUpIndicator(R.drawable.buttonmenularge);
            ab.setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
            return true;
        }

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
     return  super.onOptionsItemSelected(item);
    }


    private NavigationView.OnNavigationItemSelectedListener getOnMenuClickListener(final AppCompatActivity activity){
        return new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_productos:
                       if(posMenu() != 0) {
                            Intent show = new Intent(activity, HomeActivity.class);
                            startActivity(show);
                        }
                        return true;
                    case R.id.myaccount:
                        if(posMenu() != 1) {
                            Intent show = new Intent(activity, ProfileActivity.class);
                            startActivity(show);
                        }
                        return true;
                    case R.id.mywash:
                        if(posMenu() != 2) {
                            Intent show = new Intent(activity, MyWashActivity.class);
                            startActivity(show);
                        }
                        return true;
                    case R.id.prices:
                        if(posMenu() != 3) {
                            Intent show = new Intent(activity, PriceActivity.class);
                            startActivity(show);
                        }
                        return true;
                    case R.id.callme:
                        if(posMenu() != 4) {
                            Intent show = new Intent(activity, CallActivity.class);
                            startActivity(show);
                        }
                        return true;
                    case R.id.nav_log_out:
                        CustomerStorage.getInstance(getApplicationContext()).loginOut();
                        Intent myIntent = new Intent(activity, HomeActivity.class);
                        startActivityForResult(myIntent, 0);
                        return true;
                }
                return false;
            }
        };

    }

    protected void initBySessionUser(){
        if(CustomerStorage.getInstance(getApplicationContext()).isLogged()){
            setUserData();
        }
    }

    public void setUserData(){
        View panelUser = navigationView.getHeaderView(0);
        panelUser.setVisibility(View.VISIBLE);
        TextView nameUser = (TextView)panelUser.findViewById(R.id.usernamepanel);
        nameUser.setText(CustomerStorage.getInstance(getApplicationContext()).getCustomer().getFirst_name() + " " + CustomerStorage.getInstance(getApplicationContext()).getCustomer().getLast_name());
        if(CustomerStorage.getInstance(getApplicationContext()).getCustomer() == null ||
                CustomerStorage.getInstance(getApplicationContext()).getCustomer().getAvatar_url() == null ||
                CustomerStorage.getInstance(getApplicationContext()).getCustomer().getAvatar_url().isEmpty() ||
                CustomerStorage.getInstance(getApplicationContext()).getCustomer().getAvatar_url().indexOf("secure.gravatar.com")>0) {
            ImageView image = (ImageView) panelUser.findViewById(R.id.imageuser);
            image.setImageResource(R.drawable.app_prefil);
        } else {
            printImageUser(CustomerStorage.getInstance(getApplicationContext()).getCustomer().getAvatar_url());
        }

    }

    public void printImageUser(final String url){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                final Bitmap bitmap  = BitmapFactory.decodeStream(getImage(url));
                handlerMenu.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            View panelUser = navigationView.getHeaderView(0);
                            ImageView image = (ImageView) panelUser.findViewById(R.id.imageuser);
                            image.setImageBitmap(bitmap);
                        }catch (Exception e){
                            Log.e("Error user image",url, e);
                        }
                    }
                });
            }
        });

        thread.start();

    }

    private InputStream getImage(String url){
        InputStream inputStream = null;
        try {
            SSLSocketFactory sf = SSLSocketFactory.getSocketFactory();
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            Scheme sch = new Scheme("https", sf, 443);
            HttpClient httpClient = new DefaultHttpClient();
            httpClient.getConnectionManager().getSchemeRegistry().register(sch);
            HttpGet httpGet = new HttpGet(url);

            HttpResponse response = httpClient.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();
            } else {
                Log.d("JSON", "Failed to download file");
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", "error recibiendo imagen",e);
        }
        return inputStream;
    }

}
