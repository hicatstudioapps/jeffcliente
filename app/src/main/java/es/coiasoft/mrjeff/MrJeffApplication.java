package es.coiasoft.mrjeff;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.localytics.android.AnalyticsListener;
import com.localytics.android.Localytics;
import com.localytics.android.LocalyticsActivityLifecycleCallbacks;
import com.localytics.android.PushNotificationOptions;

import java.util.ArrayList;
import java.util.Map;

import io.branch.referral.Branch;
import io.branch.referral.BranchApp;
import io.fabric.sdk.android.Fabric;

/**
 * Created by linovm on 9/12/15.
 */
public class MrJeffApplication extends MultiDexApplication implements Application.ActivityLifecycleCallbacks {

    private ArrayList<ActivityLifecycleCallbacks> callback = new ArrayList<ActivityLifecycleCallbacks>();
    public static boolean isPaying=false;
    public static es.coiasoft.mrjeff.domain.orders.send.Order orderSend;
    public static boolean isSubscription=false;
    public static String subType;
    public static int page=-1;
    public static String entregaReference="";
    @Override
    public void onCreate()
    {
        super.onCreate();
        Branch.getAutoInstance(this);
        registerActivityLifecycleCallbacks(new LocalyticsActivityLifecycleCallbacks(this));
        registerActivityLifecycleCallbacks(this);
        PushNotificationOptions.Builder builderNotification = new PushNotificationOptions.Builder();
        builderNotification.setSmallIcon(R.drawable.ic_stat_notification);
        builderNotification.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        Intent intent = new Intent(this,MainActivity.class);
        builderNotification.setLaunchIntent(intent);

        if(!Fabric.isInitialized()) {
            Fabric.with(this, new Answers(),new Crashlytics());
        }

        Fresco.initialize(this);
        Localytics.setPushNotificationOptions(builderNotification.build());
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static long isActivityVisibleTime() {
        return currentMinutes;
    }

    public static void activityResumed() {
        Log.d("ACTIVEVIEW","Se activa la vista");
        if(activityVisible){
            currentMinutes = System.currentTimeMillis();
        }
        activityVisible = true;
    }

    public static void activityPaused() {
        Log.d("ACTIVEVIEW","Se para la vista");
        activityVisible = false;
        openDialog = false;
    }

    public static void canOpenDialog() {
        openDialog = true;
    }

    public static boolean isCanOpen(){
        return openDialog;
    }


    private static boolean activityVisible;
    private static long currentMinutes = -1L;
    private static boolean openDialog = false;


    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        activityResumed();
    }

    @Override
    public void onActivityStarted(Activity activity) {
        activityResumed();
    }

    @Override
    public void onActivityResumed(Activity activity) {
        activityResumed();
    }

    @Override
    public void onActivityPaused(Activity activity) {
        activityPaused();
    }

    @Override
    public void onActivityStopped(Activity activity) {
        activityPaused();
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        activityPaused();
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        activityPaused();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
