package es.coiasoft.mrjeff;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.FloatProperty;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.ApiServiceGenerator;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.domain.Attributes;
import es.coiasoft.mrjeff.domain.DatetimeMrJeff;
import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.ResultDatetimeMrJeff;
import es.coiasoft.mrjeff.domain.orders.response.Line_items;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.storage.DateTimeTableStorage;
import es.coiasoft.mrjeff.domain.storage.MonthStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.JeffApi;
import es.coiasoft.mrjeff.manager.worker.SendOrderWorker;
import es.coiasoft.mrjeff.manager.worker.UpdateDateOrderWorker;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.components.SelectDatePanel;
import es.coiasoft.mrjeff.view.components.util.SelectPanelListener;
import retrofit2.Call;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class MyEditOrderActivity extends MenuActivity implements Observer, SelectPanelListener{

    protected Activity instance;
    private MrJeffTextView hourPickUp;
    private MrJeffTextView dayPickUp;
    private MrJeffTextView hourDelivery;
    private MrJeffTextView dayDelivery;
    private MrJeffTextView titlewindow;
    private MrJeffTextView subtitlewindow;
    private LinearLayout buttonsave;
    private LinearLayout buttonWhatsapp;
    private LinearLayout buttonCall;
    protected TextView textCart;
    protected ImageView btnCart;
    private LinearLayout datehourpickup;
    private LinearLayout datehourdelivery;
    private RelativeLayout parent;
    protected ProgressDialogMrJeff progressDialog;
    private SelectDatePanel selectDatePanel;
    private  EnumStateOrder state;
    private es.coiasoft.mrjeff.domain.orders.send.Order orderSend;


    protected  Orders order;
    final Handler handler = new Handler();
    private boolean isPickupEnable=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.myordertitle;
        super.onCreate(savedInstanceState);
    }


    private void initActivity(){
        instance = this;
        hourPickUp = (MrJeffTextView) findViewById(R.id.hourpickup);
        dayPickUp = (MrJeffTextView) findViewById(R.id.datepickup);
        hourDelivery = (MrJeffTextView) findViewById(R.id.hourdelivery);
        dayDelivery = (MrJeffTextView) findViewById(R.id.datedelivery);
        datehourpickup = (LinearLayout) findViewById(R.id.datehourpickup);
        datehourdelivery = (LinearLayout) findViewById(R.id.datehourdelivery);
        parent = (RelativeLayout) findViewById(R.id.parentEditOrder);
        titlewindow = (MrJeffTextView) findViewById(R.id.titlewindow);
        subtitlewindow = (MrJeffTextView) findViewById(R.id.subtitlewindow);
        buttonsave = (LinearLayout) findViewById(R.id.buttonsave);
        buttonWhatsapp = (LinearLayout) findViewById(R.id.buttonWhatsapp);
        buttonCall = (LinearLayout) findViewById(R.id.buttonCall);
        textCart = (TextView) findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView) findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);


        selectDatePanel = new SelectDatePanel(parent, this,true);

        String orderJson = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.ORDER);

        try {
            Gson gson = new Gson();
            order =  gson.fromJson(orderJson,Orders.class);
            if (order != null) {
                putData();

            }
        }catch (Exception e){
            Log.e("MyOrderActivity","Recoger orden",e);
        }
        // e aqui cargamos los time tables, para poder validad contra los orderClosing
        String pickUpDate= order.getOrder_meta().getMyfield2();
        String pickUpHour= order.getOrder_meta().getMyfield3().split("-")[0].trim();
        DateTime pickUpOnlydate= DateTimeFormat.forPattern("dd-MM-yyyy HH:mm").parseDateTime(pickUpDate+" "+pickUpHour);

        String dropDate= order.getOrder_meta().getMyfield4();
        String dropHour= order.getOrder_meta().getMyfield5().split("-")[0].trim();
        DateTime dropOnlydate= DateTimeFormat.forPattern("dd-MM-yyyy HH:mm").parseDateTime(dropDate+" "+dropHour);
        DateTime today= new DateTime();
        if(today.isAfter(pickUpOnlydate) && today.isAfter(dropOnlydate)){
            isPickupEnable=false;
            isDropEnable=false;
            setPickUpListener();
            setDropListener();
        }else
        loadCP();
        state = order.getStateEnum();

//        if((state == null || (state != null && state.equals(EnumStateOrder.CONFIRM ))) && !isBlocked(order.getBilling_address().getAddress_2(), order.getOrder_meta() != null?order.getOrder_meta().getMyfield2():null, order.getOrder_meta()!= null?order.getOrder_meta().getMyfield3():null)) {
//            datehourpickup.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    String dateHour = order.getBilling_address().getAddress_2();
//                    String date = null;
//                    String hour = null;
//                    if (dateHour != null && dateHour.split(" ").length == 2) {
//                        String[] dateSplit = dateHour.split(" ");
//                        date = dateSplit[0];
//                        hour = dateSplit[1];
//                    } else if(order.getOrder_meta()!= null &&
//                            order.getOrder_meta().getMyfield2() != null &&
//                            order.getOrder_meta().getMyfield3() != null){
//                        String[] dateOrder = order.getOrder_meta().getMyfield2().split("-");
//                        date = dateOrder[2] + "-" + dateOrder[1] + "-" + dateOrder[0];
//                        hour =  order.getOrder_meta().getMyfield3().replace(" - ","");
//                    }
//                    selectDatePanel.showPanelSelectDate(instance.getResources().getString(R.string.pickup), date, hour, order.getBilling_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT, null,null);
//                }
//            });
//        } else {
//            datehourpickup.setBackgroundResource(R.color.grey20);
////            datehourpickup.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View v) {
////                    new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.titlechangetimetable).setMessage(message).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
////                        public void onClick(DialogInterface dialog, int id) {
////
////                        }
////                    }).show();
////                }
////            });
//        }
//
//        if((state == null || (state != null && ( state.equals(EnumStateOrder.CONFIRM ) ||
//                state.equals(EnumStateOrder.PICKUP ) ||
//                state.equals(EnumStateOrder.WASHING )))) && !isBlocked(order.getShipping_address().getAddress_2(), order.getOrder_meta() != null?order.getOrder_meta().getMyfield4():null, order.getOrder_meta()!= null?order.getOrder_meta().getMyfield5():null)) {
//            datehourdelivery.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    String dateHour = order.getShipping_address().getAddress_2();
//                    String date = null;
//                    String hour = null;
//                    Calendar calendar = Calendar.getInstance();
//                    if (dateHour != null && dateHour.split(" ").length == 2) {
//                        String[] dateSplit = dateHour.split(" ");
//                        date = dateSplit[0];
//                        hour = dateSplit[1];
//                    } else if(order.getOrder_meta()!= null &&
//                            order.getOrder_meta().getMyfield4() != null &&
//                            order.getOrder_meta().getMyfield5() != null){
//                        String[] dateOrder = order.getOrder_meta().getMyfield4().split("-");
//                        date = dateOrder[2] + "-" + dateOrder[1] + "-" + dateOrder[0];
//                        hour =  order.getOrder_meta().getMyfield5().replace(" - ","");
//                    }
//                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//                    try {
//                        calendar.setTime(format.parse(date));
//                    }catch (Exception e){
//
//                    }
//                    selectDatePanel.showPanelSelectDate(instance.getResources().getString(R.string.delivery), date, hour, order.getShipping_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND, order.getBilling_address().getAddress_2(),getMinDays(calendar));
//                }
//            });
//        } else {
//            datehourdelivery.setBackgroundResource(R.color.grey20);
//        }
    }

    private void loadCP() {
        progressDialog= ProgressDialogMrJeff.createAndStart(this, R.string.loadtimetable);
        //cargamos los timetables
        rx.Observable.create(new rx.Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                //cargamos recogida
                Call<ResultDatetimeMrJeff> call= ApiServiceGenerator.createService(JeffApi.class).getTimeTable(order.getShipping_address().getPostcode());
                ResultDatetimeMrJeff result= null;
                try {
                    result = call.execute().body();
                } catch (IOException e) {
                    subscriber.onError(e);
                    subscriber.unsubscribe();
                    e.printStackTrace();
                }
                if (result != null && result.getTimetable() != null && !result.getTimetable().isEmpty()) {
                    for (DatetimeMrJeff day : result.getTimetable()) {
                        if (day != null) {
                            DateTimeTableStorage.getInstance().addDate(order.getShipping_address().getPostcode(), day.getDay(), day);
                        }
                    }
                } else {
                    subscriber.onNext(false);
                    subscriber.onCompleted();
                }
                //cargamos entrega
                try {
                    call= ApiServiceGenerator.createService(JeffApi.class).getTimeTable(order.getShipping_address().getPostcode());
                    result = call.execute().body();
                } catch (IOException e) {
                    subscriber.onError(e);
                    e.printStackTrace();
                    subscriber.unsubscribe();
                }
                if (result != null && result.getTimetable() != null && !result.getTimetable().isEmpty()) {
                    for (DatetimeMrJeff day : result.getTimetable()) {
                        if (day != null) {
                            DateTimeTableStorage.getInstance().addDate(order.getBilling_address().getPostcode(), day.getDay(), day);
                        }
                    }
                } else {
                    subscriber.onNext(false);
                    subscriber.onCompleted();
                }
                subscriber.onNext(true);
                subscriber.onCompleted();
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                        new MaterialDialog.Builder(MyEditOrderActivity.this)
                                .content("Ha ocurrido un error cargando el servicio de horarios. Las fechas de recogida y entrega no podrán" +
                                        " ser editadas")
                                .positiveText("Volver a Intentar")
                                .negativeText(R.string.ok)
                                .onPositive((dialog, which) -> {loadCP();dialog.dismiss();})
                                .onNegative((dialog, which) -> dialog.dismiss()).show();
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if(!aBoolean){
                            progressDialog.dismiss();
                        new MaterialDialog.Builder(MyEditOrderActivity.this)
                                .content("Ha ocurrido un error cargando el servicio de horarios. Las fechas de recogida y entrega no podrán" +
                                        " ser editadas")
                                .positiveText("Volver a Intentar")
                                .negativeText(R.string.ok)
                                .onPositive((dialog, which) -> {loadCP();dialog.dismiss();})
                                .onNegative((dialog, which) -> dialog.dismiss()).show();}
                        else {
                            getReferenceDropDay(getPickUpDay(getDatePickUp()));
                            enablePickup();
                        }
                    }
                });
    }

    private void enablePickup() {
        //validamos la edicion de la fecha de recogida
        String pickUpDate= order.getOrder_meta().getMyfield2();
        String pickupDateForQueryTimeTable= DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(pickUpDate).toString("yyyy-MM-dd");
        String pickUpHour= order.getOrder_meta().getMyfield3().split("-")[0].trim();
        if(DateTimeTableStorage.getInstance().getDateTimeTable()
                .get(order.getShipping_address().getPostcode()).get(pickupDateForQueryTimeTable)==null){
            isPickupEnable=false;
            setPickUpListener();
            enableDelivery();
            return;
        }
        String pickUpOrderClosing= DateTimeTableStorage.getInstance().getDateTimeTable()
                .get(order.getShipping_address().getPostcode()).get(pickupDateForQueryTimeTable).getOrdersClosing();
        DateTime pickUpOnlydate= DateTimeFormat.forPattern("dd-MM-yyyy HH:mm").parseDateTime(pickUpDate+" "+pickUpHour);
        DateTime today= new DateTime();
        Period period= new Period(today,pickUpOnlydate,PeriodType.yearMonthDayTime().withYearsRemoved().withMonthsRemoved());
        Log.d("diferencia de horas",period.getHours()+ "");
        if(today.getMonthOfYear() < pickUpOnlydate.getMonthOfYear())
            isPickupEnable=true;
        else if(today.getMonthOfYear() > pickUpOnlydate.getMonthOfYear())
            isPickupEnable=false;
        else if(today.getDayOfMonth() < pickUpOnlydate.getDayOfMonth()){
            Log.d("hoy menor que recogida",period.getHours()+ "");
            isPickupEnable=true;
        }else if(today.getDayOfMonth() > pickUpOnlydate.getDayOfMonth()){
            isPickupEnable=false;
            Log.d("hoy mayor que recogida",period.getMinutes()+ "");
        }else if (period.getDays()==0){
            Log.d("hoy igual que recogida",period.getHours()+ "");
            if(pickUpOrderClosing.split(";").length==1){
                //con una solo order closing, la hora actual debe ser inferior a la del order closing
                int orderClosing1= Integer.parseInt(pickUpOrderClosing.split(";")[0].split(":")[0]);
                int hourToday= today.getHourOfDay();
                if(hourToday < orderClosing1){
                    isPickupEnable=true;
                }else
                    isPickupEnable=false;
            }else{
                Log.d("Dos OrderClosing", "---");
                int orderClosing1= Integer.parseInt(pickUpOrderClosing.split(";")[0].split(":")[0]);
                int orderClosing2= Integer.parseInt(pickUpOrderClosing.split(";")[1].split(":")[0]);
                int hourToday= today.getHourOfDay();
                //vemos en que rango esta la hora actual con el orderClosing
                if(hourToday < orderClosing1){
                    Log.d("ahora menor que  OrdC1", "---");
                    if(Integer.parseInt(pickUpHour.split(":")[0].trim()) > orderClosing1){
                        isPickupEnable=true;
                    }
                    else isPickupEnable=false;
                }else {
                    if(Integer.parseInt(pickUpHour.split(":")[0].trim())> orderClosing2){
                        isPickupEnable=true;
                    }
                    else isPickupEnable=false;
                }

            }
        }
        setPickUpListener();
        enableDelivery();
    }

    boolean isDropEnable=false;
    private void enableDelivery() {
        //validamos la edicion de la fecha de recogida
        String dropDate= order.getOrder_meta().getMyfield4();
        String dropDateForQueryTimeTable= DateTimeFormat.forPattern("dd-MM-yyyy").parseDateTime(dropDate).toString("yyyy-MM-dd");
        String dropHour= order.getOrder_meta().getMyfield5().split("-")[0].trim();
        String dropOrderClosing= DateTimeTableStorage.getInstance().getDateTimeTable()
                .get(order.getBilling_address().getPostcode()).get(dropDateForQueryTimeTable).getOrdersClosing();
        DateTime dropOnlydate= DateTimeFormat.forPattern("dd-MM-yyyy HH:mm").parseDateTime(dropDate+" "+dropHour);
        DateTime today= new DateTime();
        Period period= new Period(today,dropOnlydate,PeriodType.yearMonthDayTime().withYearsRemoved().withMonthsRemoved());
        Log.d("diferencia de horas",period.getHours()+ "");
        if(today.getMonthOfYear() < dropOnlydate.getMonthOfYear())
            isPickupEnable=true;
        else if(today.getMonthOfYear() > dropOnlydate.getMonthOfYear())
            isPickupEnable=false;
        else if(today.getDayOfMonth() < dropOnlydate.getDayOfMonth()){
            Log.d("hoy menor que recogida",period.getHours()+ "");
            isDropEnable=true;
        }else if(today.getDayOfMonth() > dropOnlydate.getDayOfMonth()){
            isDropEnable=false;
            Log.d("hoy mayor que recogida",period.getMinutes()+ "");
        }else if (period.getDays()==0){
            Log.d("hoy igual que recogida",period.getHours()+ "");
            if(dropOrderClosing.split(";").length==1){
                //con una solo order closing, la hora actual debe ser inferior a la del order closing
                int orderClosing1= Integer.parseInt(dropOrderClosing.split(";")[0].split(":")[0]);
                int hourToday= today.getHourOfDay();
                if(hourToday < orderClosing1){
                    if(Integer.parseInt(dropHour.split(":")[0].trim()) >= orderClosing1)
                        isDropEnable=true;
                    else
                        isDropEnable=false;
                }else
                    isDropEnable=false;
            }else{
                Log.d("Dos OrderClosing", "---");
                int orderClosing1= Integer.parseInt(dropOrderClosing.split(";")[0].split(":")[0]);
                int orderClosing2= Integer.parseInt(dropOrderClosing.split(";")[1].split(":")[0]);
                int hourToday= today.getHourOfDay();
                //vemos en que rango esta la hora actual con el orderClosing
                if(hourToday < orderClosing1){
                    Log.d("ahora menor que  OrdC1", "---");
                    if(Integer.parseInt(dropHour.split(":")[0].trim()) > orderClosing1){
                        isDropEnable=true;
                    }
                    else isPickupEnable=false;
                }else {
                    if(Integer.parseInt(dropHour.split(":")[0].trim())> orderClosing2){
                        isDropEnable=true;
                    }
                    else isDropEnable=false;
                }

            }
        }
        setDropListener();
        progressDialog.dismiss();
    }

    private void setDropListener() {
        if(isPickupEnable || (state == null || (state != null && ( state.equals(EnumStateOrder.CONFIRM ) ||
                state.equals(EnumStateOrder.PICKUP ) ||
                state.equals(EnumStateOrder.WASHING )))) && isDropEnable) {
            datehourdelivery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String dateHour = order.getShipping_address().getAddress_2();
                    String date = null;
                    String hour = null;
                    Calendar calendar = Calendar.getInstance();
                    if (dateHour != null && dateHour.split(" ").length == 2) {
                        String[] dateSplit = dateHour.split(" ");
                        date = dateSplit[0];
                        hour = dateSplit[1];
                    } else if(order.getOrder_meta()!= null &&
                            order.getOrder_meta().getMyfield4() != null &&
                            order.getOrder_meta().getMyfield5() != null){
                        String[] dateOrder = order.getOrder_meta().getMyfield4().split("-");
                        date = dateOrder[2] + "-" + dateOrder[1] + "-" + dateOrder[0];
                        hour =  order.getOrder_meta().getMyfield5().replace(" - ","");
                    }
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        calendar.setTime(format.parse(date));
                    }catch (Exception e){

                    }
                    selectDatePanel.showPanelSelectDate(instance.getResources().getString(R.string.delivery), date, hour, order.getShipping_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND, order.getBilling_address().getAddress_2(),getMinDays(calendar));
                }
            });
        } else {
            datehourdelivery.setOnClickListener(null);
            datehourdelivery.setBackgroundResource(R.color.grey20);
//            new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.titlechangetimetable)
//                    .setMessage("No se puede cambiar la fecha de entrega de pedido.").setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                        }
//                    }).show();
        }
    }

    private void setPickUpListener() {
        if ((state == null || (state != null && state.equals(EnumStateOrder.CONFIRM))) && isPickupEnable) {
            datehourpickup.setOnClickListener(v -> {
                String dateHour = order.getBilling_address().getAddress_2();
                String date = null;
                String hour = null;
                if (dateHour != null && dateHour.split(" ").length == 2) {
                    String[] dateSplit = dateHour.split(" ");
                    date = dateSplit[0];
                    hour = dateSplit[1];
                } else if (order.getOrder_meta() != null &&
                        order.getOrder_meta().getMyfield2() != null &&
                        order.getOrder_meta().getMyfield3() != null) {
                    String[] dateOrder = order.getOrder_meta().getMyfield2().split("-");
                    date = dateOrder[2] + "-" + dateOrder[1] + "-" + dateOrder[0];
                    hour = order.getOrder_meta().getMyfield3().replace(" - ", "");
                }
                selectDatePanel.showPanelSelectDate(instance.getResources().getString(R.string.pickup), date, hour, order.getBilling_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT, null, null);
            });
        } else {
            datehourpickup.setOnClickListener(null);
            datehourpickup.setBackgroundResource(R.color.grey20);
//            new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.titlechangetimetable)
//                    .setMessage("No puede cambiar la fecha de recogida de pedido.").setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                }
//            }).show();
        }
    }

    String message="";
    private boolean isBlocked(String address2, String fieldDate, String fieldHour){
        boolean result = false;
        String dateHour = address2;
        String date = null;
        String hour = null;
        DateTime jodaTime= new DateTime();
        try {
            if (dateHour != null && dateHour.split(" ").length == 2) {
                String[] dateSplit = dateHour.split(" ");
                date = dateSplit[0];
                hour = dateSplit[1].split("-")[0];
            } else if (fieldDate != null &&
                    fieldHour != null) {
                String[] dateOrder = fieldDate.split("-");
                date = dateOrder[2] + "-" + dateOrder[1] + "-" + dateOrder[0];
                hour = fieldHour.replace(" - ", "-").split("-")[0];
            }

            String year = date.split("-")[0];
            String month = date.split("-")[1];
            String day = date.split("-")[2];
            String h = hour.split(":")[0];
            String m = hour.split(":")[1];
//            Calendar dateOrder = Calendar.getInstance();
//            dateOrder.set(Calendar.YEAR,Integer.valueOf(year));
//            dateOrder.set(Calendar.MONTH,Integer.valueOf(month));
//            dateOrder.set(Calendar.DAY_OF_MONTH,Integer.valueOf(day));
//            dateOrder.set(Calendar.HOUR_OF_DAY,Integer.valueOf(h));
//            dateOrder.set(Calendar.MINUTE,Integer.valueOf(m));
            jodaTime= new DateTime();
            jodaTime=jodaTime.year().setCopy(Integer.valueOf(year));
            jodaTime=jodaTime.monthOfYear().setCopy(Integer.valueOf(month));
            jodaTime=jodaTime.dayOfMonth().setCopy(Integer.valueOf(day));
            jodaTime=jodaTime.hourOfDay().setCopy(Integer.valueOf(h));
            jodaTime=jodaTime.minuteOfHour().setCopy(Integer.valueOf(m));
            Calendar today = Calendar.getInstance();

//            result = (dateOrder.getTimeInMillis() - today.getTimeInMillis()) <  MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_TWO_HOUR;

            Period period= new Period(new DateTime(),jodaTime, PeriodType.yearMonthDay().withYearsRemoved().withMonthsRemoved());
            if(period.getDays()> 0 && period.getDays()<=1)
                message= getString(R.string.textchangetimetable);
            if(period.getDays() < 0)
                message= getString(R.string.mas_24);
            if(period.getDays()>0)
                result=false;
            else
                result =true;

        }catch (Exception e){
            e.printStackTrace();
        }


        return result;
    }

    private void putData() {
        setEvenCall();
        if(order.getOrder_meta() != null &&
                order.getOrder_meta().getSubscription_number() != null){
            titlewindow.setText(getResources().getString(R.string.editsuscriptiontitle));
            setDateSuscription();
        } else {
            titlewindow.setText(getResources().getString(R.string.editordertitle));
             if(order.getLine_items() != null &&
                    !order.getLine_items().isEmpty()){
                int pos = 0;
                String builder= "";
                for(Line_items item: order.getLine_items()){
                    if(Float.parseFloat(item.getPrice()) > 0){
                    if(!builder.equals(""))
                        builder+=" ,";
                    builder+= item.getName();
                    }
                }
                subtitlewindow.setText(builder.toString());
            }
        }
        Calendar calendar = putHoursPickUp();
        putHoursDelivery(calendar,false);
     //   getReferenceDropDay(getPickUpDay(getDatePickUp()));
    }

    public Calendar getPickUpDay(String datee){
        String[] parts = datee.split(" ");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(parts[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        hourPickUp.setText(parts[1]);
        String[] hour= parts[1].split("-");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour[0].trim().split(":")[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(hour[0].trim().split(":")[1]));
        return calendar;
    }

    private void setDateSuscription(){
        if(order.getCreated_at() != null){
            String date = order.getCreated_at().indexOf("T") > 0 ? order.getCreated_at().substring(0,order.getCreated_at().indexOf("T")):null;

            if(date != null){
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    Date dateFormated = format.parse(date);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(dateFormated);
                    StringBuilder builderDate = new StringBuilder();
                    builderDate.append(getResources().getString(R.string.editsuscriptionsubtitle));
                    builderDate.append(" ");
                    builderDate.append(setDay(calendar).toLowerCase());
                    builderDate.append(" ");
                    builderDate.append(dateFormated.getDate());
                    builderDate.append(" ");
                    builderDate.append(getResources().getString(MonthStorage.months.get(dateFormated.getMonth()+1)));
                    subtitlewindow.setText(builderDate.toString());
                }catch (Exception e){

                }
            }
        }
    }


    public void btnCall(){
        finishActivity = Boolean.FALSE;
        new LocalyticsEventActionMrJeff(getApplicationContext(),MrJeffConstant.LOCALYTICS_EVENTS.ACTION.SUSCRIPTIONCALL,Boolean.TRUE,"Correcto");
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(MrJeffConstant.CALL.phoneMrJeff));
        startActivity(callIntent);
    }

    protected void setEvenCall()
{
        buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCall();
            }
        });
        buttonWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWhatsappConversation();
            }
        });

        buttonsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
    }

    public void openWhatsappConversation(){
        finishActivity = Boolean.FALSE;
        new LocalyticsEventActionMrJeff(getApplicationContext(),MrJeffConstant.LOCALYTICS_EVENTS.ACTION.SUSCRIPTIONWHATSAPP,Boolean.TRUE,"Correcto");
        Uri uri = Uri.parse("smsto:" + "+34644446163");
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.putExtra("sms_body", "Hola hola");
        i.setPackage("com.whatsapp");
        startActivity(i);
    }



    public Calendar putHoursPickUp() {
        Calendar calendar = null;
        try {
            String datePickUp  = getDatePickUp();
            if(datePickUp != null) {
                calendar = paintHourDatePickUp(datePickUp);
            }
        } catch (Exception e) {
            dayPickUp.setText("");
            hourPickUp.setText("");
        }

        return calendar;
    }

    private String getDatePickUp(){
        String result = null;
        try {
            if (order.getOrder_meta() != null &&
                    order.getOrder_meta().getMyfield2() != null &&
                    order.getOrder_meta().getMyfield3() != null) {
                result = getForMeta(order.getOrder_meta().getMyfield2(), order.getOrder_meta().getMyfield3());
            }else if (order.getBilling_address().getAddress_2() != null &&
                    order.getBilling_address().getAddress_2().length() > 16) {
                result = getForAddress2(order.getBilling_address().getAddress_2());
            }
        }catch (Exception e){
                Log.e("ERROR ", "Parse date in order edit");
        }

        return result;
    }


    private String getForAddress2(String address){
        String addressComplete = address.replace(" - ", "-");
        String[] splitDateComplete = addressComplete.split(" ");
        String date = splitDateComplete[0].replace("/","-");
        String [] dateSplit = date.split("-");

        if(dateSplit[0].length() == 2) {
            return dateSplit[2]+"-"+dateSplit[1]+"-"+dateSplit[0]+" "+splitDateComplete[1];
        } else {
            return dateSplit[0]+"-"+dateSplit[1]+"-"+dateSplit[2]+" "+splitDateComplete[1];
        }

    }

    private String getForMeta(String date, String hour){
        String[] dateSplit = date.split("-");
        return dateSplit[2]+"-"+dateSplit[1]+"-"+dateSplit[0]+" "+hour.replace(" ","");
    }




    public Calendar paintHourDatePickUp(String datePaint) throws ParseException {
        String[] parts = datePaint.split(" ");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse(parts[0]);
        hourPickUp.setText(parts[1]);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        long time = calendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
        if (Calendar.getInstance().get(Calendar.YEAR) == calendar.get(Calendar.YEAR) &&
                Calendar.getInstance().get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {
            dayPickUp.setText(getResources().getString(R.string.today));
        } else if (time >= 0 && time <= MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY) {
            dayPickUp.setText(getResources().getString(R.string.tomorrow));
        } else if (time >= 0 && time <= (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * 5)) {
            setDay(calendar, dayPickUp);
        } else {
            dayPickUp.setText(parts[0]);
        }

        order.getBilling_address().setAddress_2(datePaint);

        return calendar;
    }

    private String getDateDelivery(){
        String result = null;
        try {
            if (order.getOrder_meta() != null &&
                    order.getOrder_meta().getMyfield4() != null &&
                    order.getOrder_meta().getMyfield5() != null) {
                result = getForMeta(order.getOrder_meta().getMyfield4(), order.getOrder_meta().getMyfield5());
            } else if (order.getShipping_address().getAddress_2() != null &&
                    order.getShipping_address().getAddress_2().length() > 16) {
                result = getForAddress2(order.getShipping_address().getAddress_2());
            }
        }catch (Exception e){
            Log.e("ERROR ", "Parse date in order edit");
        }

        return result;
    }

    public void putHoursDelivery(Calendar calendar, boolean reload) {
        try {
            String datePickUp = getDateDelivery();
            if(datePickUp==null){
                dayDelivery.setText("");
                hourDelivery.setText("");return;
            }
            if(datePickUp == null || reload) {
                calendar.setTimeInMillis(calendar.getTimeInMillis() + (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * getMinDays(calendar)));
                datePickUp = DateTimeTableStorage.getInstance().getSecondDateHour(order.getShipping_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND, calendar);
            }
            paintHourDateDelivery(datePickUp);

        } catch (Exception e) {
            dayDelivery.setText("");
            hourDelivery.setText("");
        }
    }

    public void getReferenceDropDay(Calendar calendar) {
        try {
            String datePickUp = getDateDelivery();
                calendar.setTimeInMillis(calendar.getTimeInMillis() + (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * getMinDays(calendar)));
                datePickUp = DateTimeTableStorage.getInstance().getSecondDateHour(order.getShipping_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND, calendar);
            //MrJeffApplication.entregaReference=datePickUp;
            } catch (Exception e) {
            dayDelivery.setText("");
            hourDelivery.setText("");
        }
    }

    private Integer getMinDays(Calendar referenceDate) {
        long time = referenceDate.getTimeInMillis();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(calendar.getTime());
        Integer result = DateTimeTableStorage.getInstance().getMinDays(order.getShipping_address().getPostcode(),date);
        try {
            if (order != null && order.getLine_items() != null) {
                for (es.coiasoft.mrjeff.domain.orders.response.Line_items item : order.getLine_items()) {
                    Integer idProduct = item.getProduct_id();
                    Products product = ProductStorage.getInstance(this).getProduct(idProduct);
                    if (product.getAttributes() != null) {
                        for (Attributes attr : product.getAttributes()) {
                            if (attr.getName().trim().toLowerCase().equals(MrJeffConstant.ORDER_DAYS_SEND.ATTR_DAYS.trim().toLowerCase())) {
                                try {
                                    Integer days = Integer.valueOf(attr.getOptions().get(0));
                                    if (result.compareTo(days) < 0) {
                                        result = days;
                                    }
                                    break;
                                }catch (Exception e){

                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
        return result;
    }

    public Calendar paintHourDateDelivery(String datePaint) throws ParseException {
        String[] parts = datePaint.split(" ");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse(parts[0]);
        hourDelivery.setText(parts[1]);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        long time = calendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
        if (Calendar.getInstance().get(Calendar.YEAR) == calendar.get(Calendar.YEAR) &&
                Calendar.getInstance().get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {
            dayDelivery.setText(getResources().getString(R.string.today));
        } else if (time >= 0 && time <= MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY) {
            dayDelivery.setText(getResources().getString(R.string.tomorrow));
        } else if (time >= 0 && time <= (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * 5)) {
            setDay(calendar, dayDelivery);
        } else {
            dayDelivery.setText(parts[0]);
        }
        order.getShipping_address().setAddress_2(datePaint);

        return calendar;
    }

    private void setDay(Calendar calendar, MrJeffTextView textView) {
        textView.setText(setDay(calendar));
    }

    private String setDay(Calendar calendar) {
        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                return getResources().getString(R.string.monday);
            case Calendar.TUESDAY:
                return getResources().getString(R.string.tuesday);
            case Calendar.WEDNESDAY:
                return getResources().getString(R.string.wednesday);
            case Calendar.THURSDAY:
                return getResources().getString(R.string.thursday);
            case Calendar.FRIDAY:
                return getResources().getString(R.string.friday);
            case Calendar.SATURDAY:
                return getResources().getString(R.string.saturday);
            case Calendar.SUNDAY:
                return getResources().getString(R.string.sunday);
            default:
                return "";
        }
    }
    @Override
    public void onResume(){
        super.onResume();
        initActivity();
    }

    @Override
    protected void setContentView(){
        setContentView(R.layout.activity_myeditorder);
    }

    protected int posMenu(){
        return 2;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    protected String getNameScreen(){
        return MrJeffConstant.LOCALYTICS.SCREEN_MY_ONE_WASH;
    }


    @Override
    public void update(Observable observable, Object data) {
        if(observable instanceof  SendOrderWorker){
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(OrderStorage.getInstance(getApplicationContext()).getError()){
                                if(progressDialog != null && progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }
                                new AlertDialog.Builder(new ContextThemeWrapper(MyEditOrderActivity.this, R.style.Theme_AppCompat_Light)).setTitle(R.string.errrorConnection).setMessage(R.string.errrorConnectiontext).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        setResult(RESULT_OK);
                                    }
                                }).show();
                            } else {

                                if(progressDialog != null && progressDialog.isShowing()){
                                    progressDialog.dismiss();
                                }
                                UpdateDateOrderWorker worker = new UpdateDateOrderWorker(orderSend,order.getId());
                                Thread thread = new Thread(worker);
                                thread.start();

                                Intent show = new Intent(instance, MyWashActivity.class);
                                startActivity(show);
                            }
                        }
                    });
                }
            });
            threadNotiy.start();
        }

    }

    private void save() {
        Boolean correct = true;
//        if(state != null && state.equals(EnumStateOrder.CONFIRM )) {
//            correct = DateTimeTableStorage.getInstance().isValidDateRestrictedHour(order.getBilling_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT, order.getBilling_address().getAddress_2().split(" ")[0], order.getBilling_address().getAddress_2().split(" ")[1]);
//            if (!correct) {
//                new LocalyticsEventActionMrJeff(this, MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "No Valido");
//                showErrorOpenDate(R.string.date_disabled, R.string.error_date_bill);
//                return;
//            }
//        }

//        correct = DateTimeTableStorage.getInstance().isValidDateRestrictedHour(order.getShipping_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND, order.getShipping_address().getAddress_2().split(" ")[0], order.getShipping_address().getAddress_2().split(" ")[1]);
//        if (!correct) {
//            new LocalyticsEventActionMrJeff(this, MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "No Valido");
//            showErrorOpenDate(R.string.date_disabled, R.string.error_date_ship);
//            return;
//        }

        if (correct) {
            new LocalyticsEventActionMrJeff(this, MrJeffConstant.LOCALYTICS_EVENTS.ACTION.UPDATE_ORDER, false, "", order.getBilling_address().getAddress_2(),order.getShipping_address().getAddress_2());
            orderSend = new es.coiasoft.mrjeff.domain.orders.send.Order();
            orderSend.setBilling_address(new es.coiasoft.mrjeff.domain.orders.send.Billing_address(this.order.getBilling_address()));
            orderSend.setShipping_address(new es.coiasoft.mrjeff.domain.orders.send.Shipping_address(this.order.getShipping_address()));

            progressDialog = ProgressDialogMrJeff.createAndStart(this, R.string.updateaddress);

            SendOrderWorker worker = new SendOrderWorker(orderSend, Boolean.TRUE,Boolean.TRUE,this.order.getId());
            worker.addObserver(this);
            Thread threadOrder = new Thread(worker);
            threadOrder.start();
        }

    }

    private void showErrorOpenDate(int title, int text) {
        new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Theme_AppCompat_Light)).setTitle(title).setMessage(text).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        }).show();
    }

    public void notifyChanges(){
        buttonsave.setVisibility(View.VISIBLE);
    }
}


