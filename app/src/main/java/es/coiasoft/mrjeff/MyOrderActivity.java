package es.coiasoft.mrjeff;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.ValidateVersion;
import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.orders.response.Billing_address;
import es.coiasoft.mrjeff.domain.orders.response.Line_items;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.orders.response.Shipping_address;
import es.coiasoft.mrjeff.domain.storage.MonthStorage;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.worker.SendOrderWorker;
import es.coiasoft.mrjeff.view.adapter.ListAdapterProductMyOrder;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;


public class MyOrderActivity extends MenuActivity implements Observer{

    protected Activity instance;
    protected TextView textCart;
    protected ImageView btnCart;

    protected TextView textMyOrderNum;
    protected TextView textMyOrderDate;
    protected TextView textMyOrderTotal;
    protected ListView listProducts;
    protected LinearLayout btnReplyOrder;


    protected TextView myorderbaddressname;
    protected TextView myorderbaddressphone;
    protected TextView myorderbaddressemail;
    protected TextView myorderbaddressaddress;
    protected TextView myorderbaddresspc;
    protected TextView myorderbaddressdate;


    protected TextView myordersaddressname;
    protected TextView myordersaddressaddress;
    protected TextView myordersaddresspc;
    protected TextView myordersaddressdate;
    protected TextView myorderbaddresshour;
    protected TextView myordersaddresshour;

    protected ProgressDialogMrJeff progressDialog;


    protected  Orders order;
    final Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.myordertitle;
        super.onCreate(savedInstanceState);
    }


    private void initActivity(){
        instance = this;
        textCart = (TextView)findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView)findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);

        textMyOrderNum = (TextView)findViewById(R.id.myordernpedido);
        textMyOrderDate = (TextView)findViewById(R.id.myorderdate);
        textMyOrderTotal = (TextView)findViewById(R.id.myordertotal);
        btnReplyOrder = (LinearLayout)findViewById(R.id.btnretryOrder);

        myorderbaddressname = (TextView)findViewById(R.id.myorderbaddressname);
        myorderbaddressphone = (TextView)findViewById(R.id.myorderbaddressphone);
        myorderbaddressemail = (TextView)findViewById(R.id.myorderbaddressemail);
        myorderbaddressaddress = (TextView)findViewById(R.id.myorderbaddressaddress);
        myorderbaddresspc = (TextView)findViewById(R.id.myorderbaddresspc);
        myorderbaddressdate = (TextView)findViewById(R.id.myorderbaddressdate);
        myorderbaddresshour = (TextView)findViewById(R.id.myorderbaddresshour);

        myordersaddressname = (TextView)findViewById(R.id.myordersaddressname);
        myordersaddressaddress = (TextView)findViewById(R.id.myordersaddressaddress);
        myordersaddresspc = (TextView)findViewById(R.id.myordersaddresspc);
        myordersaddressdate = (TextView)findViewById(R.id.myordersaddressdate);
        myordersaddresshour = (TextView)findViewById(R.id.myorderaddresshour);

        listProducts = (ListView) findViewById(R.id.listProducts);

        String orderJson = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.ORDER);

        try {
            Gson gson = new Gson();
            order =  gson.fromJson(orderJson,Orders.class);
            if (order != null) {
                initComponents((Orders) order);

            }
        }catch (Exception e){
            Log.e("MyOrderActivity","Recoger orden",e);
        }

    }


    private void initComponents(Orders orders){

        textMyOrderNum.setText(orders.getId().toString());

        if(orders.getCreated_at() != null){
            String date = orders.getCreated_at().indexOf("T") > 0 ? orders.getCreated_at().substring(0,orders.getCreated_at().indexOf("T")):null;

            if(date != null){
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date dateFormated = format.parse(date);
                    StringBuilder builderDate = new StringBuilder();
                    builderDate.append(dateFormated.getDate());
                    builderDate.append(" ");
                    builderDate.append(textMyOrderDate.getContext().getResources().getString(MonthStorage.months.get(dateFormated.getMonth()+1)));
                    builderDate.append(" ");
                    builderDate.append(dateFormated.getYear() + 1900);
                    textMyOrderDate.setText(builderDate.toString());
                }catch (Exception e){

                }
            }
        }

        if(orders.getTotal() != null && !orders.getTotal().isEmpty()){
            textMyOrderTotal.setText(orders.getTotal() + "€");
        }

        if(orders.getLine_items() != null &&
                !orders.getLine_items().isEmpty()){
            listProducts.setAdapter(new ListAdapterProductMyOrder(listProducts.getContext(), R.layout.product_myorder, orders.getLine_items(), listProducts));
        }


        if(orders.getBilling_address() != null){
            initAddressBilling(orders);
        }

        if(orders.getShipping_address() != null){
            initAddressShipping(orders);
        }

        if(order != null && order.getOrder_meta() != null && order.getOrder_meta().getSubscription_number() != null){
            btnReplyOrder.setVisibility(View.GONE);
        } else {
            if (order.getLine_items() != null && !order.getLine_items().isEmpty()) {
                btnReplyOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        copyProduct();
                        createCopyOrder();
                    }
                });
            } else {
                btnReplyOrder.setVisibility(View.GONE);
            }
        }


    }

    private void initAddressBilling(Orders orders){
        Billing_address address = orders.getBilling_address();
        myorderbaddressname.setText((address.getFirst_name() != null? address.getFirst_name() + " ":"") +(address.getLast_name() != null? address.getLast_name() + " ":""));
        myorderbaddressphone.setText((address.getPhone() != null? address.getPhone() + " ":""));
        myorderbaddressemail.setText((address.getEmail() != null? address.getEmail() + " ":""));
        StringBuilder builderAddress = new StringBuilder("");
        builderAddress.append(address.getAddress_1() != null ? address.getAddress_1() + ", " : "");
        builderAddress.append(address.getCity() != null ? address.getCity() :  "");
        myorderbaddressaddress.setText(builderAddress.toString());
        myorderbaddresspc.setText((address.getPostcode() != null? address.getPostcode() + " ":""));
        myorderbaddressdate.setText((getDateBillingAddress(orders)!= null? getDateBillingAddress(orders) + " ":""));
        myorderbaddresshour.setText((getHourBillingAddress(orders)!= null? getHourBillingAddress(orders) + " ":""));
    }


    private String getDateBillingAddress(Orders orders){
        String result = null;

        if(orders != null && orders.getOrder_meta() != null && orders.getOrder_meta().getMyfield2() != null){
            result = getDateMeta(orders.getOrder_meta().getMyfield2());
        } else if(orders.getBilling_address() != null&& orders.getBilling_address().getAddress_2() != null){
            result = getDateAddress2(orders.getBilling_address().getAddress_2());
        }

        return result;

    }

    private String getDateShippingddress(Orders orders){
        String result = null;

        if(orders != null && orders.getOrder_meta() != null && orders.getOrder_meta().getMyfield4() != null){
            result = getDateMeta(orders.getOrder_meta().getMyfield4());
        } else if(orders.getShipping_address() != null && orders.getShipping_address().getAddress_2() != null){
            result = getDateAddress2(orders.getShipping_address().getAddress_2());
        }

        return result;

    }

    private String getHourBillingAddress(Orders orders){
        String result = null;

        if(orders != null && orders.getOrder_meta() != null && orders.getOrder_meta().getMyfield3() != null && !orders.getOrder_meta().getMyfield3().isEmpty()){
            result = orders.getOrder_meta().getMyfield3().substring(0,orders.getOrder_meta().getMyfield3().indexOf("-")>0?orders.getOrder_meta().getMyfield3().indexOf("-"):orders.getOrder_meta().getMyfield3().length()).trim();
        } else if(orders.getBilling_address() != null && orders.getBilling_address().getAddress_2() != null){
            result = getHourAddress2(orders.getBilling_address().getAddress_2());
        }

        return result;

    }

    private String getHourShippingAddress(Orders orders){
        String result = null;

        if(orders != null && orders.getOrder_meta() != null && orders.getOrder_meta().getMyfield5() != null){
            result = orders.getOrder_meta().getMyfield5().substring(0,orders.getOrder_meta().getMyfield5().indexOf("-")>0?orders.getOrder_meta().getMyfield5().indexOf("-"):orders.getOrder_meta().getMyfield5().length()).trim();
        } else if(orders.getShipping_address() != null && orders.getShipping_address().getAddress_2() != null){
            result = getHourAddress2(orders.getBilling_address().getAddress_2());
        }

        return result;

    }

    private String getDateMeta(String dateMeta){
        String result = "";
        SimpleDateFormat formatSource = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatTarget = new SimpleDateFormat("dd-MM-yyyy");
        String datePickUp = dateMeta;
        try {
            SimpleDateFormat format = null;
            if(datePickUp.split("-")[0].length()==4) {
                format = formatSource;
            } else {
                format = formatTarget;
            }
            Date date = format.parse(datePickUp);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            result += calendar.get(Calendar.DAY_OF_MONTH) + " ";
            result += getResources().getString(MonthStorage.months.get(calendar.get(Calendar.MONTH) + 1))+ " ";
            result += calendar.get(Calendar.YEAR);
        }catch (ParseException e){
            try {
                Date date = formatTarget.parse(datePickUp);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                result += calendar.get(Calendar.DAY_OF_MONTH) + " ";
                result += getResources().getString(MonthStorage.months.get(calendar.get(Calendar.MONTH) + 1))+ " ";
                result += calendar.get(Calendar.YEAR);
            }catch (Exception e1){

            }
        }
        return result;
    }


    private String getDateAddress2(String address2){
        String result = "";
        String aux = convertAddress2(address2);
        SimpleDateFormat formatSource = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        SimpleDateFormat formatTarget = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        String datePickUp = aux.replace(" - ","-");
        try {
            if(datePickUp.split("-")[0].length()==4) {
                Date date = formatSource.parse(datePickUp);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                result += calendar.get(Calendar.DAY_OF_MONTH) + " ";
                result += getResources().getString(MonthStorage.months.get(calendar.get(Calendar.MONTH) + 1)) + " ";
                result += calendar.get(Calendar.YEAR);
            } else {
                Date date = formatTarget.parse(datePickUp);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                result += calendar.get(Calendar.DAY_OF_MONTH) + " ";
                result += getResources().getString(MonthStorage.months.get(calendar.get(Calendar.MONTH) + 1))+ " ";
                result += calendar.get(Calendar.YEAR);
            }
        }catch (ParseException e){
            try {
                Date date = formatTarget.parse(datePickUp);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                result += calendar.get(Calendar.DAY_OF_MONTH) + " ";
                result += getResources().getString(MonthStorage.months.get(calendar.get(Calendar.MONTH) + 1))+ " ";
                result += calendar.get(Calendar.YEAR);
            }catch (Exception e1){

            }
        }
        return result;
    }

    private String getHourAddress2(String address2){
        String result = "";
        String aux = convertAddress2(address2);
        SimpleDateFormat formatSource = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        SimpleDateFormat formatTarget = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        String datePickUp = aux.replace(" - ","-");
        try {

            if(datePickUp.split("-")[0].length()==4) {
                Date date = formatSource.parse(datePickUp);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                result += calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE);
            } else {
                Date date = formatTarget.parse(datePickUp);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                result += calendar.get(Calendar.HOUR_OF_DAY) + ":"+calendar.get(Calendar.MINUTE);
            }
        }catch (ParseException e){
            try {
                Date date = formatTarget.parse(datePickUp);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                result += calendar.get(Calendar.HOUR_OF_DAY) + ":"+calendar.get(Calendar.MINUTE);
            }catch (Exception e1){

            }
        }
        return result;
    }

    private static String convertAddress2(String address2){
        String result = "";
        String [] splitDate = address2.split(" ");
        result = splitDate[0].replaceAll("/","-") + " " + splitDate[1].substring(0,splitDate[1].indexOf("-")>0?splitDate[1].indexOf("-"):splitDate[1].length()).trim();
        return result;
    }

    private void initAddressShipping(Orders orders){
        Shipping_address address = orders.getShipping_address();
        myordersaddressname.setText((address.getFirst_name() != null? address.getFirst_name() + " ":"") +(address.getLast_name() != null? address.getLast_name() + " ":""));
        if(myordersaddressname.getText() == null || myordersaddressname.getText().length() < 4){
            myordersaddressname.setText(myorderbaddressname.getText());
        }
        StringBuilder builderAddress = new StringBuilder("");
        builderAddress.append(address.getAddress_1() != null ? address.getAddress_1() + ", " : "");
        builderAddress.append(address.getCity() != null ? address.getCity() :  "");
        myordersaddressaddress.setText(builderAddress.toString());
        myordersaddresspc.setText((address.getPostcode() != null? address.getPostcode() + " ":""));
        myordersaddressdate.setText((address.getAddress_2() != null ? address.getAddress_2() + " " : ""));

        myordersaddressdate.setText((getDateShippingddress(orders)!= null? getDateShippingddress(orders) + " ":""));
        myordersaddresshour.setText((getHourShippingAddress(orders)!= null? getHourShippingAddress(orders) + " ":""));
    }

    @Override
    public void onResume(){
        super.onResume();
        initActivity();
    }

    @Override
    protected void setContentView(){
        setContentView(R.layout.activity_myorder);
    }

    protected int posMenu(){
        return 3;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    protected String getNameScreen(){
        return MrJeffConstant.LOCALYTICS.SCREEN_MY_ONE_WASH;
    }



    private void copyProduct(){
        OrderProductsStorage.getInstance(getApplicationContext()).clear();
        if(order.getLine_items() != null){
            for(Line_items item: order.getLine_items()){
                if(ProductStorage.getInstance(getApplicationContext()).getProduct(item.getProduct_id()) != null) {
                    OrderProductsStorage.getInstance(getApplicationContext()).addProduct(item.getProduct_id(),item.getQuantity());
                }
            }
        }
    }

    protected void createCopyOrder(){
        super.createOrder();
        es.coiasoft.mrjeff.domain.orders.send.Order  order =  getOrder();
        progressDialog = ProgressDialogMrJeff.createAndStart(this,R.string.initcard);
        SendOrderWorker worker = new SendOrderWorker(order);
        worker.addObserver(this);
        Thread threadOrder = new Thread(worker);
        threadOrder.start();
    }

    private es.coiasoft.mrjeff.domain.orders.send.Order getOrder(){

        es.coiasoft.mrjeff.domain.orders.send.Order result = OrderStorage.getInstance(getApplicationContext()).getOrderSend();
        if(result == null){
            result = new es.coiasoft.mrjeff.domain.orders.send.Order();
        }
        result.setLine_items(new ArrayList<es.coiasoft.mrjeff.domain.orders.send.Line_items>());

        if(OrderProductsStorage.getInstance(getApplicationContext()).getNumTotal().intValue() > 0){
            Map<Integer,Integer> products = OrderProductsStorage.getInstance(getApplicationContext()).getProducts();

            if(products != null && !products.isEmpty()){
                Set<Integer> keys = products.keySet();
                for(Integer key:keys){
                    if(products.get(key) != null && products.get(key).intValue() > 0) {
                        es.coiasoft.mrjeff.domain.orders.send.Line_items item = new es.coiasoft.mrjeff.domain.orders.send.Line_items();
                        item.setProduct_id(key);
                        item.setQuantity(products.get(key));
                        result.getLine_items().add(item);
                    }
                }
            }
        }

        OrderStorage.getInstance(getApplicationContext()).setOrderSend(result);
        OrderStorage.getInstance(getApplicationContext()).saveJson();
        return result;
    }

    @Override
    public void update(Observable observable, Object data) {
        if(observable instanceof  SendOrderWorker){
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(progressDialog != null && progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                            if(OrderStorage.getInstance(getApplicationContext()).getError()){

                                new AlertDialog.Builder(new ContextThemeWrapper(MyOrderActivity.this, R.style.Theme_AppCompat_Light)).setTitle(R.string.errrorConnection).setMessage(R.string.errrorConnectiontext).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        setResult(RESULT_OK);
                                    }
                                }).show();
                            } else {
                                Intent show = new Intent(btnReplyOrder.getContext(), OrderAddressActivity.class);
                                show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, false);
                                startActivity(show);
                            }
                        }
                    });
                }
            });
            threadNotiy.start();
        }

    }
}


