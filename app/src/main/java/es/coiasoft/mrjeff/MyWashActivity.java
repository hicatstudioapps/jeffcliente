package es.coiasoft.mrjeff;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.ValidateVersion;
import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.GroupStateOrder;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrdersCustomerStorage;
import es.coiasoft.mrjeff.manager.model.comparator.GroupStateOrderComparator;
import es.coiasoft.mrjeff.manager.worker.FindOrdersCustomerWorker;
import es.coiasoft.mrjeff.view.adapter.HolderRecylerStateOrder;
import es.coiasoft.mrjeff.view.adapter.RecyclerAdapterMrJeff;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.components.RateController;
import es.coiasoft.mrjeff.view.components.RecyclerViewMrJeff;
import es.coiasoft.mrjeff.view.listener.ScrollViewListenerMrJeff;


public class MyWashActivity extends MenuActivity implements Observer, ScrollViewListenerMrJeff {

    public static final String ORDER_ATTR = "ATTR";

    protected Activity instance;
    protected TextView textCart;
    protected ImageView btnCart;
    protected RecyclerViewMrJeff listOrdersByMonth;
    protected LinearLayout notwahser;
    protected LinearLayout panelTitle;
    protected TextView titlesection;
    protected Handler handler = new Handler();
    protected ProgressDialog progressDialog;
    protected RateController rateController;
    protected LinearLayoutManager mLinearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.mywash;
        super.onCreate(savedInstanceState);
        initActivity();
    }

    private void initActivity(){
        instance = this;
        textCart = (TextView) findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView) findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);
        panelTitle = (LinearLayout) findViewById(R.id.panelTitle);
        titlesection = (TextView) findViewById(R.id.titlesection);
        listOrdersByMonth = (RecyclerViewMrJeff) findViewById(R.id.listOrdersByMonth);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listOrdersByMonth.setLayoutManager(mLinearLayoutManager);
        notwahser = (LinearLayout) findViewById(R.id.notwahser);
//        if (CustomerStorage.getInstance(getApplicationContext()).isLogged() &&
//                CustomerStorage.getInstance(getApplicationContext()).getCustomer().getId() != null) {
            if(progressDialog == null) {
                progressDialog = ProgressDialogMrJeff.createAndStart(MyWashActivity.this, R.string.searchmywash);
            }else {
                progressDialog.show();
            }
            FindOrdersCustomerWorker worker = new FindOrdersCustomerWorker(CustomerStorage.getInstance(getApplicationContext()).getCustomer().getId().toString(),this,-1);
            worker.addObserver(this);
            Thread thread = new Thread(worker);
            thread.start();
//        }
        rateController = new RateController(getWindow(),this);
    }

    @Override
    public void onResume() {
        super.onResume();
//        //initActivity();
//        if (CustomerStorage.getInstance(getApplicationContext()).isLogged() &&
//                CustomerStorage.getInstance(getApplicationContext()).getCustomer().getId() != null) {
//            if(progressDialog == null) {
//                progressDialog = ProgressDialogMrJeff.createAndStart(MyWashActivity.this, R.string.searchmywash);
//            }else {
//                progressDialog.show();
//            }
//            FindOrdersCustomerWorker worker = new FindOrdersCustomerWorker(CustomerStorage.getInstance(getApplicationContext()).getCustomer().getId().toString(),this);
////            FindOrdersCustomerWorker worker = new FindOrdersCustomerWorker("1592",this);
//            worker.addObserver(this);
//            Thread thread = new Thread(worker);
//            thread.start();
//        }
//
//
//
//
//
//        try {
//            int id = getIntent().getIntExtra(ORDER_ATTR, -1);
//            if(id > 0){
//                openRate(String.valueOf(id));
//            }
//        }catch (Exception e){
//
//        }

    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_mywash);
    }

    protected int posMenu() {
        return 2;
    }

    @Override
    public void onBackPressed() {
        finishActivity = Boolean.TRUE;
        Intent show = new Intent(this, HomeActivity.class);
        startActivity(show);
    }

    @Override
    public void update(Observable observable, final Object data) {
        Thread threadNotiy = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        if (data != null || data instanceof List) {
                            ArrayList<GroupStateOrder> months = getorder(data);
                            if (months != null && !months.isEmpty()) {
                                panelTitle.setVisibility(View.VISIBLE);
                                listOrdersByMonth.setVisibility(View.VISIBLE);
                                listOrdersByMonth.setAdapter(new RecyclerAdapterMrJeff<HolderRecylerStateOrder,GroupStateOrder>(MyWashActivity.this,months, R.layout.myordersmonth) {
                                    @Override
                                    public HolderRecylerStateOrder getInstanceHolder(View v) {
                                        return new HolderRecylerStateOrder(v,MyWashActivity.this);
                                    }

                                });
                            } else {
                                panelTitle.setVisibility(View.GONE);
                                listOrdersByMonth.setVisibility(View.GONE);
                                notwahser.setVisibility(View.VISIBLE);
                            }
                        } else {
                            notwahser.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });

        threadNotiy.start();
    }


    private ArrayList<GroupStateOrder> getorder(Object data) {
        Map<EnumStateOrder, GroupStateOrder> states = new HashMap<EnumStateOrder, GroupStateOrder>();

        if (data instanceof List) {
            for (Object d : (List) data) {
                if (d instanceof Orders && !"pending".equalsIgnoreCase(((Orders) d).getStatus())
                        && !"cancelled".equalsIgnoreCase(((Orders) d).getStatus())
                && (((Orders) d).getOrder_meta() == null ||
                        ((Orders) d).getOrder_meta().getIssuscription() == null ||
                        !((Orders) d).getOrder_meta().getIssuscription().equals("1"))) {
                    EnumStateOrder state = ((Orders) d).getStateEnum();
                    if(state == null){
                        state = EnumStateOrder.CONFIRM;
                    }
                    GroupStateOrder groupStateOrder = states.get(state);
                    if (groupStateOrder == null) {
                        groupStateOrder = new GroupStateOrder();
                        groupStateOrder.setState(state);

                        groupStateOrder.setOrders(new ArrayList<Orders>());
                    }

                    groupStateOrder.getOrders().add((Orders) d);

                    states.put(state, groupStateOrder);
                }
            }
        }

        ArrayList<GroupStateOrder> result = new ArrayList<GroupStateOrder>(states.values());
        Collections.sort(result, new GroupStateOrderComparator());
        return result;
    }


    protected String getNameScreen() {
        return MrJeffConstant.LOCALYTICS.SCREEN_MY_WASH;
    }

    public void openRate(String idOrder){
        rateController.showPanel(idOrder);
    }

    public void updateOrders(String idOrder){
        Integer id = Integer.valueOf(idOrder);
        Orders order = OrdersCustomerStorage.getInstance(this).getOrder(id);
        if(order != null){
            order.setHaveRate(true);
            OrdersCustomerStorage.getInstance(this).saveJson();
        }

        if(progressDialog == null) {
            progressDialog = ProgressDialogMrJeff.createAndStart(MyWashActivity.this, R.string.searchmywash);
        }else {
            progressDialog.show();
        }
        FindOrdersCustomerWorker worker = new FindOrdersCustomerWorker(CustomerStorage.getInstance(getApplicationContext()).getCustomer().getId().toString(),this,-1);
        worker.addObserver(this);
        Thread thread = new Thread(worker);
        thread.start();


    }

    @Override
    public void onScrollChangedCoiasoft(int x, int y, int oldx, int oldy) {
        int childVisible =  mLinearLayoutManager.getChildCount();
        int childTotal = mLinearLayoutManager.getItemCount();
        int visibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
        if(childVisible > 0 && mLinearLayoutManager.getChildCount() > 0) {
            View c = mLinearLayoutManager.getChildAt(0);
            if(c != null) {
                TextView text = (TextView) c.findViewById(R.id.titlemontorders);
                titlesection.setText(text.getText());
            }
        }
    }
}
