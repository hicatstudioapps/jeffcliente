package es.coiasoft.mrjeff;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.Utils.event.LocalytisEvenMrJeff;
import es.coiasoft.mrjeff.domain.Faq;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.orders.send.OrderMeta;
import es.coiasoft.mrjeff.domain.storage.FaqsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.worker.SearchFaqsWorker;
import es.coiasoft.mrjeff.manager.worker.SendOrderWorker;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.util.LoadImage;


public class NoLavoActivity extends MenuActivity implements Observer{

    protected Activity instance;
    protected Handler handler = new Handler();

    protected MrJeffTextView textPrice;
    protected SimpleDraweeView imageProduct;
    protected MrJeffTextView descriptionProduct1;
    protected MrJeffTextView descriptionProduct2;
    protected MrJeffTextView descriptionProduct3;
    protected MrJeffTextView descriptionProduct4;
    protected LinearLayout panelFaqs;
    protected LinearLayout btnCall;
    protected LinearLayout buttonWhatsapp;
    protected LinearLayout faqs;
    protected LinearLayout panelPrice;
    protected MrJeffTextView titleProduct;
    protected MrJeffTextView title;
    protected TextView textCart;
    protected TextView  textPrice2;
    protected ImageView btnCart;
    protected TextView number;
    protected RelativeLayout panelSuscription;
    protected int posParent;
    private ProgressDialog progressDialog;


    protected Products products;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        finishActivity = Boolean.FALSE;
        titleWindowId = R.string.empty;
        super.onCreate(savedInstanceState);
        instance = this;
        findProduct();
        if(products != null && products.getId() != null) {
            initComponents();
            List<Faq> faqs = FaqsStorage.getInstance(this).findProduct(products.getId().toString());
            if(faqs != null && !faqs.isEmpty()){
                putListFaqs(faqs);
            } else {
                SearchFaqsWorker worker = new SearchFaqsWorker(products.getId().toString(),this);
                worker.addObserver(this);
                Thread thread = new Thread(worker);
                thread.start();
            }
            LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(getApplicationContext());
            event.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.TYPE_SUSCRIPTION, products.getId().toString());
            event.send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.ENTRA_SUSCRIPTION);
        }

        try {
            posParent = getIntent().getExtras().getInt(MrJeffConstant.INTENT_ATTR.POSFRAGMENT);
        }catch (Exception e){
            posParent = 2;
        }
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_no_lavo);
    }

    @Override
    protected int posMenu() {
        return -1;
    }

    protected void findProduct(){
        Integer idProduct = getIntent().getExtras().getInt(MrJeffConstant.INTENT_ATTR.IDPRODUCT);
        products = ProductStorage.getInstance(getApplicationContext()).getProduct(idProduct);
    }

    protected void initComponents(){
        defineComponents();
        setEvenCall();
        setComponents();

    }

    protected void setComponents(){
        textCart.setText(OrderProductsStorage.getInstance(getApplicationContext()).getNumTotal().toString());
        if(OrderProductsStorage.getInstance(getApplicationContext()).getNumTotal().intValue() > 0){
            btnCart.setImageResource(R.drawable.basket);
        } else {
            btnCart.setImageResource(R.drawable.basketempty);
        }
        textPrice.setText(products.getPrice().substring(0,products.getPrice().indexOf(".")));
        textPrice2.setText(getResources().getString(R.string.month));
        TextView textheader = (TextView)findViewById(R.id.titleHeader);
        if(textheader != null && products !=null && products.getTitle() != null) {
            textheader.setText(products.getTitle());
            title.setText(products.getTitle());
        }

        ArrayList<String> attr = products.getAttributeByKey(MrJeffConstant.PRODUCT_ATTR.CLAIM);
        if(attr != null && attr.size()>=1){
            titleProduct.setText(attr.get(0));
        } else {
            titleProduct.setVisibility(View.GONE);
        }
        if(attr != null && attr.size()>=2){
            descriptionProduct1.setText(attr.get(1));
        } else {
            descriptionProduct1.setVisibility(View.GONE);
        }
        if(attr != null && attr.size()>=3){
            descriptionProduct2.setText(attr.get(2));
        } else {
            descriptionProduct2.setVisibility(View.GONE);
        }
        if(attr != null && attr.size()>=4){
            descriptionProduct3.setText(attr.get(3));
        } else {
            descriptionProduct3.setVisibility(View.GONE);
        }
        if(attr != null && attr.size()>=5){
            descriptionProduct4.setText(attr.get(4));
        } else {
            descriptionProduct4.setVisibility(View.GONE);
        }

        if(attr != null && attr.size()>=7){
            textheader.setText(attr.get(6));
        } else {
            if(attr != null && attr.size()>=6){
                textheader.setText(attr.get(5));
            } else {
                textheader.setText(R.string.suscriptions);
            }
        }


        new LoadImage(products.getUrlImage(),imageProduct,this);
    }



    protected void defineComponents(){
        btnCall = (LinearLayout)findViewById(R.id.buttonCall);
        buttonWhatsapp = (LinearLayout)findViewById(R.id.buttonWhatsapp);
        textPrice = (MrJeffTextView)findViewById(R.id.textPrice);
        textPrice2 = (MrJeffTextView)findViewById(R.id.textPrice2);
        imageProduct = (SimpleDraweeView)findViewById(R.id.imageProduct);
        descriptionProduct1 = (MrJeffTextView)findViewById(R.id.descriptionProductLine1);
        descriptionProduct2 = (MrJeffTextView)findViewById(R.id.descriptionProductLine2);
        descriptionProduct3 = (MrJeffTextView)findViewById(R.id.descriptionProductLine3);
        descriptionProduct4 = (MrJeffTextView)findViewById(R.id.descriptionProductLine4);
        titleProduct = (MrJeffTextView)findViewById(R.id.titleProduct);
        panelFaqs = (LinearLayout)findViewById(R.id.panelFaqs);
        textCart = (TextView)findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView)findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);
        faqs = (LinearLayout)findViewById(R.id.faqs);
        panelPrice = (LinearLayout)findViewById(R.id.panelPrice);
        number = (TextView)findViewById(R.id.number);
        panelSuscription = (RelativeLayout) findViewById(R.id.buttonSuscription);
        title = (MrJeffTextView)findViewById(R.id.title);
    }


    protected void setEvenCall(){
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCall();
            }
        });
        buttonWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWhatsappConversation();
            }
        });
        panelSuscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createOrder();
            }
        });
    }


    @Override
    public void onBackPressed() {
        finishActivity = Boolean.TRUE;
        Intent show = new Intent(this, HomeActivity.class);
        show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, posParent >= 0?posParent:2);
        startActivity(show);
    }


    public void btnCall(){
        finishActivity = Boolean.FALSE;
        new LocalyticsEventActionMrJeff(getApplicationContext(),MrJeffConstant.LOCALYTICS_EVENTS.ACTION.SUSCRIPTIONCALL,Boolean.TRUE,"Correcto");
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(MrJeffConstant.CALL.phoneMrJeff));
        startActivity(callIntent);
    }

    public void createOrder(){
        super.createOrder();
        es.coiasoft.mrjeff.domain.orders.send.Order  order = getOrder();
        progressDialog = ProgressDialogMrJeff.createAndStart(this,R.string.initcard);
        SendOrderWorker worker = new SendOrderWorker(order);
        worker.addObserver(this);
        Thread threadOrder = new Thread(worker);
        threadOrder.start();
    }

    public void openWhatsappConversation(){
        finishActivity = Boolean.FALSE;
        new LocalyticsEventActionMrJeff(getApplicationContext(),MrJeffConstant.LOCALYTICS_EVENTS.ACTION.SUSCRIPTIONWHATSAPP,Boolean.TRUE,"Correcto");
        Uri uri = Uri.parse("smsto:" + "+34644446163");
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.putExtra("sms_body", "Hola hola");
        i.setPackage("com.whatsapp");
        startActivity(i);
    }

    protected String getNameScreen(){
        return MrJeffConstant.LOCALYTICS.SCREEN_ONE_PRODUCT + (products != null && products.getId() != null ? products.getId() : "");
    }


    private es.coiasoft.mrjeff.domain.orders.send.Order getOrder(){

        es.coiasoft.mrjeff.domain.orders.send.Order result = OrderStorage.getInstance(getApplicationContext()).getOrderSend();
        OrderStorage.getInstance(getApplicationContext()).setOrder(new Order());
        if(result == null){
            result = new es.coiasoft.mrjeff.domain.orders.send.Order();
        }
        result.setLine_items(new ArrayList<es.coiasoft.mrjeff.domain.orders.send.Line_items>());
        //Validar si tenia mas productos
        OrderProductsStorage.getInstance(getApplicationContext()).clear();
        es.coiasoft.mrjeff.domain.orders.send.Line_items item = new es.coiasoft.mrjeff.domain.orders.send.Line_items();
        item.setProduct_id(products.getId());
        item.setQuantity(1);
        result.getLine_items().add(item);

        result.setOrder_meta(new OrderMeta());
        result.getOrder_meta().setIssuscription("1");
        MrJeffApplication.isSubscription=true;
        String type = null;
        ArrayList<String> typeSuscription = products.getAttributeByKey(MrJeffConstant.PRODUCT_ATTR.SUSCRIPTION_TYPE);
        if(typeSuscription!=null && !typeSuscription.isEmpty()){
            type = typeSuscription.get(0);
        }
        result.getOrder_meta().setTypesuscription(type);
        MrJeffApplication.subType=type;
        OrderStorage.getInstance(getApplicationContext()).setOrderSend(result);
        OrderStorage.getInstance(getApplicationContext()).saveJson();
        return result;
    }

    private void putFaqs(Object data){
        if(data != null &&
                data instanceof  List
        && !((List)data).isEmpty()){

            for(Object item: (List)data) {
                if(item != null && item instanceof Faq) {
                    LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = mInflater.inflate(R.layout.faq, null);
                    LinearLayout panel = (LinearLayout) view.findViewById(R.id.paneFaq);
                    MrJeffTextView question = (MrJeffTextView) view.findViewById(R.id.question);
                    question.setText(((Faq)item).getQuestion());
                    final MrJeffTextView answerd = (MrJeffTextView) view.findViewById(R.id.answerd);
                    answerd.setText(((Faq)item).getAnswer());
                    panel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(answerd.getVisibility() == View.GONE){
                                answerd.setVisibility(View.VISIBLE);
                            } else {
                                answerd.setVisibility(View.GONE);
                            }
                        }
                    });
                    faqs.addView(view);
                }
            }

            panelFaqs.setVisibility(View.VISIBLE);

        }

    }

    private void putListFaqs(List<Faq> data){
        if(data != null && !data.isEmpty()){
            for(Faq item: data) {
                LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = mInflater.inflate(R.layout.faq, null);
                LinearLayout panel = (LinearLayout) view.findViewById(R.id.paneFaq);
                MrJeffTextView question = (MrJeffTextView) view.findViewById(R.id.question);
                question.setText(item.getQuestion());
                final MrJeffTextView answerd = (MrJeffTextView) view.findViewById(R.id.answerd);
                answerd.setText(item.getAnswer());
                panel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(answerd.getVisibility() == View.GONE){
                            answerd.setVisibility(View.VISIBLE);
                        } else {
                            answerd.setVisibility(View.GONE);
                        }
                    }
                });
                faqs.addView(view);
            }
            panelFaqs.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void update(Observable observable,final Object data) {
        if(observable instanceof SearchFaqsWorker ){
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            putFaqs(data);
                        }
                    });
                }
            });
            threadNotiy.start();
        } else if(observable instanceof  SendOrderWorker){
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(progressDialog != null && progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                            if(OrderStorage.getInstance(getApplicationContext()).getError()){
                                new AlertDialog.Builder(new ContextThemeWrapper(NoLavoActivity.this, R.style.Theme_AppCompat_Light)).setTitle(R.string.errrorConnection).setMessage(R.string.errrorConnectiontext).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        setResult(RESULT_OK);
                                    }
                                }).show();
                            } else {
                                String type = null;
                                ArrayList<String> typeSuscription = products.getAttributeByKey(MrJeffConstant.PRODUCT_ATTR.SUSCRIPTION_TYPE);
                                if(typeSuscription!=null && !typeSuscription.isEmpty()){
                                    type = typeSuscription.get(0);
                                }
                                new LocalytisEvenMrJeff(getApplicationContext(),Boolean.TRUE, MrJeffConstant.LOCALYTICS_EVENTS.NAME.NEW_CART,Boolean.TRUE);
                                Intent show = null;
                                if(type == null || !type.equals(MrJeffConstant.INTENT_ATTR.TYPE_SUS_RES)) {
                                    show = new Intent(progressDialog.getContext(), OrderAddressActivity.class);
                                }else {
                                    show = new Intent(progressDialog.getContext(), ResidenciaActivity.class);
                                }
                                show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, true);
                                show.putExtra(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION,type);
                                startActivity(show);
                            }
                        }
                    });
                }
            });
            threadNotiy.start();
        }
    }
}


