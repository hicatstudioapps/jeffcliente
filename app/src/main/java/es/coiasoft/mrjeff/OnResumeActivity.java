package es.coiasoft.mrjeff;

import android.os.Bundle;

import es.coiasoft.mrjeff.manager.model.ConfigRepository;

/**
 * Created by linovm on 5/11/15.
 */
public abstract class OnResumeActivity extends MenuActivity {

    private static final long MAX_TIME = 30000;

    private ConfigRepository dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataSource = new ConfigRepository(this);
    }

    @Override
    public void onStart(){
        super.onStart();
        //long timeNow = Calendar.getInstance().getTimeInMillis();
        //long timeClose = Long.valueOf(dataSource.getValueConfig(ConfigRepository.ONRESUME));
        //dataSource.updateRegister(ConfigRepository.ONRESUME,"0");
        //if(timeClose != 0l && (timeNow - timeClose) > MAX_TIME){
        //    Intent myIntent = new Intent(this, HomeActivity.class);
        //    startActivityForResult(myIntent, 0);
        // }
    }

    @Override
    public void onStop()
    {
        super.onStop();
      // dataSource.updateRegister(ConfigRepository.ONRESUME, String.valueOf(Calendar.getInstance().getTimeInMillis()));

    }
}
