package es.coiasoft.mrjeff;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.Utils.event.LocalytisEvenMrJeff;
import es.coiasoft.mrjeff.domain.Faq;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.storage.FaqsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.worker.SearchFaqsWorker;
import es.coiasoft.mrjeff.manager.worker.SendOrderWorker;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.util.LoadImage;


public class OneProductActivity extends MenuActivity implements Observer{

    protected Activity instance;
    protected Handler handler = new Handler();

    protected MrJeffTextView textPrice;
    protected SimpleDraweeView imageProduct;
    protected MrJeffTextView descriptionProduct;
    protected MrJeffTextView titleProduct;
    protected LinearLayout panelFaqs;
    protected LinearLayout btnCall;
    protected LinearLayout buttonWhatsapp;
    protected LinearLayout faqs;
    protected LinearLayout panelPrice;
    protected TextView textCart;
    protected ImageView btnCart;
    protected ImageView buttominus;
    protected ImageView buttonplus;
    protected TextView number;
    protected int posParent;
    private ProgressDialog progressDialog;


    protected Products products;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        finishActivity = Boolean.FALSE;
        titleWindowId = R.string.empty;
        super.onCreate(savedInstanceState);
        instance = this;
        findProduct();
        if(products != null && products.getId() != null) {
            initComponents();
            List<Faq> faqs = FaqsStorage.getInstance(this).findProduct(products.getId().toString());
            if(faqs != null && !faqs.isEmpty()){
                putListFaqs(faqs);
            } else {
                SearchFaqsWorker worker = new SearchFaqsWorker(products.getId().toString(),this);
                worker.addObserver(this);
                Thread thread = new Thread(worker);
                thread.start();
            }
            LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(getApplicationContext());
            event.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.TYPE_SUSCRIPTION, products.getId().toString());
            event.send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.ENTRA_SUSCRIPTION);
        }

        try {
            posParent = getIntent().getExtras().getInt(MrJeffConstant.INTENT_ATTR.POSFRAGMENT);
        }catch (Exception e){
            posParent = 2;
        }



    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_one_product);
    }

    @Override
    protected int posMenu() {
        return -1;
    }

    protected void findProduct(){
        Integer idProduct = getIntent().getExtras().getInt(MrJeffConstant.INTENT_ATTR.IDPRODUCT);
        products = ProductStorage.getInstance(getApplicationContext()).getProduct(idProduct);
    }

    protected void initComponents(){
        defineComponents();
        setEvenCall();
        setComponents();

    }

    protected void setComponents(){
        textCart.setText(OrderProductsStorage.getInstance(getApplicationContext()).getNumTotal().toString());
        if(OrderProductsStorage.getInstance(getApplicationContext()).getNumTotal().intValue() > 0){
            btnCart.setImageResource(R.drawable.basket);
        } else {
            btnCart.setImageResource(R.drawable.basketempty);
        }
        textPrice.setText(products != null && products.getPrice() != null ? products.getPrice() + "€" : "");
        final Integer id = products.getId();
        Integer num = OrderProductsStorage.getInstance(getBaseContext()).countProduct(id);
        if (num.compareTo(Integer.valueOf(0)) > 0) {
            number.setText(num.toString());
            buttominus.setVisibility(View.VISIBLE);
        } else {
            buttominus.setVisibility(View.GONE);
        }
        descriptionProduct.setText(Html.fromHtml(products.getShort_description()));
        TextView textheader = (TextView)findViewById(R.id.titleHeader);
        if(textheader != null && products !=null && products.getTitle() != null) {
            textheader.setText(products.getTitle());
            titleProduct.setText(products.getTitle());
        }
        new LoadImage(products.getUrlImage(),imageProduct,this);
    }



    protected void defineComponents(){
        btnCall = (LinearLayout)findViewById(R.id.buttonCall);
        buttonWhatsapp = (LinearLayout)findViewById(R.id.buttonWhatsapp);
        textPrice = (MrJeffTextView)findViewById(R.id.textPrice);
        imageProduct = (SimpleDraweeView)findViewById(R.id.imageProduct);
        descriptionProduct = (MrJeffTextView)findViewById(R.id.descriptionProduct);
        titleProduct = (MrJeffTextView)findViewById(R.id.titleProduct);
        panelFaqs = (LinearLayout)findViewById(R.id.panelFaqs);
        textCart = (TextView)findViewById(R.id.numberCartHeader);
        btnCart = (ImageView)findViewById(R.id.btnCart);
        faqs = (LinearLayout)findViewById(R.id.faqs);
        panelPrice = (LinearLayout)findViewById(R.id.panelPrice);
        buttominus = (ImageView)findViewById(R.id.buttominus);
        buttonplus = (ImageView)findViewById(R.id.buttonplus);
        number = (TextView)findViewById(R.id.number);

    }


    protected void setEvenCall(){
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCall();
            }
        });
        buttonWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWhatsappConversation();
            }
        });
        panelPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAdd();
            }
        });
        buttonplus.setOnClickListener(getOnClickListenerPlus());
        buttominus.setOnClickListener(getOnClickListenerMinus());
    }


    @Override
    public void onBackPressed() {
        finishActivity = Boolean.TRUE;
        Intent show = new Intent(this, HomeActivity.class);
        show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, posParent >= 0?posParent:2);
        startActivity(show);
    }


    public void btnCall(){
        finishActivity = Boolean.FALSE;
        new LocalyticsEventActionMrJeff(getApplicationContext(),MrJeffConstant.LOCALYTICS_EVENTS.ACTION.SUSCRIPTIONCALL,Boolean.TRUE,"Correcto");
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(MrJeffConstant.CALL.phoneMrJeff));
        startActivity(callIntent);
    }

    public void openWhatsappConversation(){
        finishActivity = Boolean.FALSE;
        new LocalyticsEventActionMrJeff(getApplicationContext(),MrJeffConstant.LOCALYTICS_EVENTS.ACTION.SUSCRIPTIONWHATSAPP,Boolean.TRUE,"Correcto");
        Uri uri = Uri.parse("smsto:" + "+34644446163");
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.putExtra("sms_body", "Hola hola");
        i.setPackage("com.whatsapp");
        startActivity(i);
    }

    protected String getNameScreen(){
        return MrJeffConstant.LOCALYTICS.SCREEN_ONE_PRODUCT + (products != null && products.getId() != null ? products.getId() : "");
    }

    private void onClickAdd(){
        OrderProductsStorage.getInstance(getApplicationContext()).addProductElement(products.getId());
        Intent show = new Intent(OneProductActivity.this, HomeActivity.class);
        show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, false);
        startActivity(show);

    }

    private void putFaqs(Object data){
        if(data != null &&
                data instanceof  List
                && !((List)data).isEmpty()){

            for(Object item: (List)data) {
                if(item != null && item instanceof Faq) {
                    LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view = mInflater.inflate(R.layout.faq, null);
                    LinearLayout panel = (LinearLayout) view.findViewById(R.id.paneFaq);
                    MrJeffTextView question = (MrJeffTextView) view.findViewById(R.id.question);
                    question.setText(((Faq)item).getQuestion());
                    final MrJeffTextView answerd = (MrJeffTextView) view.findViewById(R.id.answerd);
                    answerd.setText(((Faq)item).getAnswer());
                    panel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(answerd.getVisibility() == View.GONE){
                                answerd.setVisibility(View.VISIBLE);
                            } else {
                                answerd.setVisibility(View.GONE);
                            }
                        }
                    });
                    faqs.addView(view);
                }
            }

            panelFaqs.setVisibility(View.VISIBLE);

        }

    }

    private void putListFaqs(List<Faq> data){
        if(data != null && !data.isEmpty()){
            for(Faq item: data) {
                LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = mInflater.inflate(R.layout.faq, null);
                LinearLayout panel = (LinearLayout) view.findViewById(R.id.paneFaq);
                MrJeffTextView question = (MrJeffTextView) view.findViewById(R.id.question);
                question.setText(item.getQuestion());
                final MrJeffTextView answerd = (MrJeffTextView) view.findViewById(R.id.answerd);
                answerd.setText(item.getAnswer());
                panel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(answerd.getVisibility() == View.GONE){
                            answerd.setVisibility(View.VISIBLE);
                        } else {
                            answerd.setVisibility(View.GONE);
                        }
                    }
                });
                faqs.addView(view);
            }
            panelFaqs.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void update(Observable observable,final Object data) {
        if(observable instanceof SearchFaqsWorker ){
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            putFaqs(data);
                        }
                    });
                }
            });
            threadNotiy.start();
        } else if(observable instanceof  SendOrderWorker){
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(progressDialog != null && progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                            if(OrderStorage.getInstance(getApplicationContext()).getError()){

                                new android.support.v7.app.AlertDialog.Builder(new ContextThemeWrapper(OneProductActivity.this, R.style.Theme_AppCompat_Light)).setTitle(R.string.errrorConnection).setMessage(R.string.errrorConnectiontext).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        setResult(RESULT_OK);
                                    }
                                }).show();
                            } else {
                                new LocalytisEvenMrJeff(getApplicationContext(),Boolean.TRUE, MrJeffConstant.LOCALYTICS_EVENTS.NAME.NEW_CART,Boolean.TRUE);
                                Intent show = new Intent(progressDialog.getContext(), OrderAddressActivity.class);
                                show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, false);
                                startActivity(show);
                            }
                        }
                    });
                }
            });
            threadNotiy.start();
        }
    }

    @Override
    protected void createOrder(){
        removeTaxMin();
        es.coiasoft.mrjeff.domain.orders.send.Order  order = getOrder();
        progressDialog = ProgressDialogMrJeff.createAndStart(this,R.string.initcard);
        SendOrderWorker worker = new SendOrderWorker(order);
        worker.addObserver(this);
        Thread threadOrder = new Thread(worker);
        threadOrder.start();
    }

    protected void removeTaxMin(){
        if(OrderStorage.getInstance(getApplicationContext()).getProductMinOrder() != null){
            es.coiasoft.mrjeff.domain.orders.send.Line_items itemRemove = null;
            for(es.coiasoft.mrjeff.domain.orders.send.Line_items item: OrderStorage.getInstance(getApplicationContext()).getOrderSend().getLine_items()){
                if(item.getProduct_id().intValue() == OrderStorage.getInstance(getApplicationContext()).getProductMinOrder().intValue()){
                    itemRemove = item;
                    break;
                }
            }
            if(itemRemove != null) {
                OrderStorage.getInstance(getApplicationContext()).getOrderSend().getLine_items().remove(itemRemove);
            }
        }
    }

    private es.coiasoft.mrjeff.domain.orders.send.Order getOrder(){

        es.coiasoft.mrjeff.domain.orders.send.Order result = OrderStorage.getInstance(getApplicationContext()).getOrderSend();
        if(result == null){
            result = new es.coiasoft.mrjeff.domain.orders.send.Order();
        }
        result.setLine_items(new ArrayList<es.coiasoft.mrjeff.domain.orders.send.Line_items>());

        if(OrderProductsStorage.getInstance(getApplicationContext()).getNumTotal().intValue() > 0){
            Map<Integer,Integer> products = OrderProductsStorage.getInstance(getApplicationContext()).getProducts();

            if(products != null && !products.isEmpty()){
                Set<Integer> keys = products.keySet();
                for(Integer key:keys){
                    if(products.get(key) != null && products.get(key).intValue() > 0) {
                        es.coiasoft.mrjeff.domain.orders.send.Line_items item = new es.coiasoft.mrjeff.domain.orders.send.Line_items();
                        item.setProduct_id(key);
                        item.setQuantity(products.get(key));
                        result.getLine_items().add(item);
                    }
                }
            }
        }

        OrderStorage.getInstance(getApplicationContext()).setOrderSend(result);
        OrderStorage.getInstance(getApplicationContext()).saveJson();

        return result;
    }

    private View.OnClickListener getOnClickListenerPlus(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valueNumber = number.getText().toString();
                String valueNumberCart = textCart.getText().toString();
                Integer num = 0;
                Integer numCart = 0;
                try {
                    num = Integer.valueOf(valueNumber) + 1;
                    numCart = Integer.valueOf(valueNumberCart) + 1;
                } catch (Exception e) {
                    num = 0;
                    numCart = 0;
                }

                btnCart.setImageResource(R.drawable.basket);
                number.setText(num.toString());
                textCart.setText(numCart.toString());
                buttominus.setVisibility(View.VISIBLE);
                OrderProductsStorage.getInstance(getApplicationContext()).addProductElement(products.getId());
                new LocalyticsEventActionMrJeff(getApplicationContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.HOMEADD,Boolean.FALSE,products.getId()!= null ? products.getId().toString():"");
            }
        };

    }

    private View.OnClickListener getOnClickListenerMinus(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valueNumber =number.getText().toString();
                String valueNumberCart = textCart.getText().toString();
                Integer num = 0;
                Integer numCart = 0;
                try {
                    num = Integer.valueOf(valueNumber) - 1;
                    numCart = Integer.valueOf(valueNumberCart) - 1;
                } catch (Exception e) {
                    num = 0;
                    numCart = 0;
                }
                num = num.compareTo(Integer.valueOf(0)) <= 0 ? 0 : num;
                numCart = numCart.compareTo(Integer.valueOf(0)) <= 0 ? 0 : numCart;
                number.setText(num.toString());
                textCart.setText(numCart.toString());
                if(numCart == 0){
                    btnCart.setImageResource(R.drawable.basketempty);
                }
                if (num.intValue() <= 0) {
                    buttominus.setVisibility(View.INVISIBLE);
                }
                new LocalyticsEventActionMrJeff(getApplicationContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.HOMEREMOVE, Boolean.FALSE,products.getId() != null ? products.getId().toString() : "");
                OrderProductsStorage.getInstance(getApplicationContext()).removeProductElement(products.getId());
            }
        };

    }
}


