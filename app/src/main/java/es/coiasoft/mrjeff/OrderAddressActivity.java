package es.coiasoft.mrjeff;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.AlteredCharSequence;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.Utils.event.LocalytisEvenMrJeff;
import es.coiasoft.mrjeff.domain.Attributes;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.domain.orders.send.Billing_address;
import es.coiasoft.mrjeff.domain.orders.send.Shipping_address;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.DateTimeTableStorage;
import es.coiasoft.mrjeff.domain.storage.LocationStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.PostalCodeStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.worker.SearchTimeTableWorker;
import es.coiasoft.mrjeff.manager.worker.SendOrderWorker;
import es.coiasoft.mrjeff.manager.worker.SendOrderWorkerMin;
import es.coiasoft.mrjeff.view.adapter.PlaceArrayAdapter;
import es.coiasoft.mrjeff.view.adapter.domain.PlaceAutoComplete;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.components.SelectDatePanel;
import es.coiasoft.mrjeff.view.components.util.SelectPanelListener;
import es.coiasoft.mrjeff.view.fragments.SupporMapFragmentDragable;
import es.coiasoft.mrjeff.view.listener.ListenerMap;
import es.coiasoft.mrjeff.view.listener.OrderAdressFragmentListener;
import es.coiasoft.mrjeff.view.validator.FormValidatorMrJeff;

/**
 * Created by Paulino on 09/03/2016.
 */
public class OrderAddressActivity extends MenuActivity implements OrderAdressFragmentListener{

    private static final String FRAGMENT_ADDRESS_SHOW="show";
    private static final String FRAGMENT_ADDRESS_SERACH_SHOW="searchaddress";
    private static final String FRAGMENT_ADDRESS_TIME_SHOW="dateaddress";
    private static final String FRAGMENT_ADDRESS_TIME_SHOW_HOUR="dateaddresshour";
    protected TextView textCart;
    protected ImageView btnCart;
    private boolean isSuscription = false;
    private boolean zalando = false;
    private String typeSuscription = null;
    private int posFragment=0;
    public static LatLng toPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.addresstitle;
        super.onCreate(savedInstanceState);

        try {
            zalando = getIntent().getExtras().getBoolean(MrJeffConstant.INTENT_ATTR.ZALANDO);
            //agregado
            zalando=false;
        }catch (Exception e){
            zalando = false;
        }
        if(!MrJeffApplication.isSubscription) {
            try {
                isSuscription = getIntent().getExtras().getBoolean(MrJeffConstant.INTENT_ATTR.SUSCRIPTION);
            } catch (Exception e) {
                isSuscription = false;
            }

            if (isSuscription) {
                try {
                    typeSuscription = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION);
                } catch (Exception e) {
                    typeSuscription = null;
                }
            }
        }else{
            isSuscription=true;
            typeSuscription=MrJeffApplication.subType;
        }

        textCart = (TextView)findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView)findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);
        numpage.setVisibility(View.VISIBLE);
        Log.d("ORDERADDRESS","Entra en onCreate");
        try {

            Log.d("ORDERADDRESS","Intenta recoger la posicion");
            posFragment = getIntent().getExtras().getInt(MrJeffConstant.INTENT_ATTR.POSFRAGMENT);
            Log.d("ORDERADDRESS","La recoge y es " + posFragment);
        }catch (Exception e){
            posFragment = 0;
            Log.d("ORDERADDRESS","Fallo y se puso el valor" + posFragment);
        }
        getFragment();



        try {
            String coupon = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.HOME_MESSAGE);
            if(coupon != null && !coupon.trim().toLowerCase().equals("null")) {
                new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Theme_AppCompat_Light)).setTitle(R.string.empty).setMessage(getResources().getString(R.string.couponapply, coupon)).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }).show();
            }
        }catch (Exception e){
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isSuscription=false;
    }

    @Override
    public void onResume(){
        super.onResume();
        Order o=OrderStorage.getInstance(getApplicationContext()).getOrder();
//        if(o!= null)
//            Toast.makeText(this,"ID_PEDIDO "+o.getOrder_number(),Toast.LENGTH_LONG).show();
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_order_address);
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationStorage.getInstance().initLocation(locationManager);
    }

    private void getFragment(){
        Log.d("ORDERADDRESS","Entra en getfragment " + posFragment);
        switch (posFragment) {
            case 0:
                Log.d("ORDERADDRESS","Abre mapa");
                openFragmentMap();
                break;
            case 1:
                Log.d("ORDERADDRESS","Abre busqueda");
                onClickSearchAddressBilling();
                break;
            case 2:
                Log.d("ORDERADDRESS","Abre horario");
                openFragmentHour(posFragment);
                break;
            case 3:
                Log.d("ORDERADDRESS","Abre horario");
                openFragmentHour(posFragment);
                break;
            default:
                Log.d("ORDERADDRESS","Abre mapa default");
                openFragmentMap();
                break;
        }
    }

    private void openFragmentMap(){
        try {
            if(!isFinishing()) {
                Bundle params = new Bundle();
                params.putBoolean(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
                params.putBoolean(MrJeffConstant.INTENT_ATTR.ZALANDO, zalando);
                FragmentOrderAddress fragment = FragmentOrderAddress.newInstance(params);
                fragment.setListener(this);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_ADDRESS_SHOW);
                ft.commitAllowingStateLoss();
            }
        }catch (Exception e){
            Log.e("ORDERADDRESS","Error al iniciar");
            throw e;
        }
    }

    private void openFragmentHour(int posFragment){
        try {
            if(!isFinishing()) {
                Bundle params = new Bundle();
                params.putBoolean(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
                params.putBoolean(MrJeffConstant.INTENT_ATTR.ZALANDO, zalando);
                if(typeSuscription != null){
                    params.putString(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION, typeSuscription);
                }
                Fragment fragment = null;
                if(posFragment == 2 && !isSuscription) {
                    fragment = FragmentOrderHourAddress.newInstance(params);
                    ((FragmentOrderHourAddress)fragment).setListener(this);
                } else {
                    fragment = FragmentOrderHourAddressSuscription.newInstance(params);
                    ((FragmentOrderHourAddressSuscription)fragment).setListener(this);
                }
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_ADDRESS_TIME_SHOW_HOUR);
                ft.commitAllowingStateLoss();
            }
        }catch (Exception e){
            Log.e("ORDERADDRESS","Error al iniciar");
            throw e;
        }
    }


    @Override
    public void onBackPressed() {
        Intent show = null;
        switch (posFragment) {
            case 0:
                if(zalando) {
                    show = new Intent(this, ZalandoActivity.class);
                } else {
                    show = new Intent(this, HomeActivity.class);
                }
                show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
                show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 0);
                startActivity(show);
                break;
            case 1:
                onClickOkAddress();
                break;
            case 2:
                onClickOkAddress();
                break;
            case 3:
                onClickOkAddress();
                break;
            default:
                if(zalando) {
                    show = new Intent(this, ZalandoActivity.class);
                } else {
                    show = new Intent(this, HomeActivity.class);
                }
                show = new Intent(this, HomeActivity.class);
                show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
                show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 2);
                startActivity(show);
                break;
        }
    }

    @Override
    protected int posMenu() {
        return -1;
    }

    @Override
    protected String getNameScreen() {
        return MrJeffConstant.LOCALYTICS.SCREEN_ORDER_ADDRES;
    }

    @Override
    public void onClickSearchAddressBilling() {
        if(!isFinishing()) {
            posFragment = 1;
            Bundle params = new Bundle();
            params.putBoolean(FragmentOrderSearchAddress.PARAM_TYPE_ADDRESS, false);
            params.putBoolean(MrJeffConstant.INTENT_ATTR.ZALANDO, zalando);
            FragmentOrderSearchAddress fragment = FragmentOrderSearchAddress.newInstance(params);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinrigth, R.anim.fadeoutleft, R.anim.fadeinleft, R.anim.fadeoutrigth);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_ADDRESS_SERACH_SHOW);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }

    @Override
    public void onClickSearchAddressShipping() {
        if(!isFinishing()) {
            posFragment = 1;
            Bundle params = new Bundle();
            params.putBoolean(FragmentOrderSearchAddress.PARAM_TYPE_ADDRESS, true);
            params.putBoolean(MrJeffConstant.INTENT_ATTR.ZALANDO, zalando);
            FragmentOrderSearchAddress fragment = FragmentOrderSearchAddress.newInstance(params);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinrigth, R.anim.fadeoutleft, R.anim.fadeinleft, R.anim.fadeoutrigth);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_ADDRESS_SERACH_SHOW);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }



    @Override
    public void onClickOkAddress() {
        if(!isFinishing()) {
            posFragment = 0;
            Bundle params = new Bundle();
            params.putBoolean(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
            params.putBoolean(MrJeffConstant.INTENT_ATTR.ZALANDO, zalando);
            FragmentOrderAddress fragment = FragmentOrderAddress.newInstance(params);


            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinleft, R.anim.fadeoutrigth, R.anim.fadeinrigth, R.anim.fadeoutleft);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_ADDRESS_SHOW);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
            disenabledButtonLocation();
        }
    }


    @Override
    public void onClickCancelChangeAddress() {
        if(!isFinishing()) {
            posFragment = 0;
            Bundle params = new Bundle();
            params.putBoolean(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
            params.putBoolean(MrJeffConstant.INTENT_ATTR.ZALANDO, zalando);
            FragmentOrderAddress fragment = FragmentOrderAddress.newInstance(params);
            fragment.setListener(this);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinleft, R.anim.fadeoutrigth, R.anim.fadeinrigth, R.anim.fadeoutleft);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_ADDRESS_SHOW);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
            disenabledButtonLocation();
        }
    }

    @Override
    public void onClickGoTohour() {
        if(!isFinishing()) {
            posFragment = 2;
            Bundle params = new Bundle();
            params.putBoolean(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
            params.putBoolean(MrJeffConstant.INTENT_ATTR.ZALANDO, zalando);
            if(typeSuscription != null){
                params.putString(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION, typeSuscription);
            }


            Fragment fragment = null;
            if(!isSuscription()) {
                fragment = FragmentOrderHourAddress.newInstance(params);
                ((FragmentOrderHourAddress)fragment).setListener(this);
            } else {
                fragment = FragmentOrderHourAddressSuscription.newInstance(params);
                ((FragmentOrderHourAddressSuscription)fragment).setListener(this);

            }
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinrigth, R.anim.fadeoutleft, R.anim.fadeinleft, R.anim.fadeoutrigth);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_ADDRESS_TIME_SHOW_HOUR);
            ft.addToBackStack(FRAGMENT_ADDRESS_SHOW);
            ft.commitAllowingStateLoss();
            disenabledButtonLocation();
        }
    }

    @Override
    public void setTitlePageOne() {
        textheader.setText(getResources().getString(R.string.addresstitle));
        numpage.setText(getResources().getString(R.string.onefromtwo));
    }

    @Override
    public void setTitlePageTwo() {
        textheader.setText(getResources().getString(R.string.hour));
        numpage.setText(getResources().getString(R.string.twofromtwo));
    }


    public void disenabledButtonLocation(){
        ImageView imageViewLocation = (ImageView) findViewById(R.id.btnLocation);
        imageViewLocation.setVisibility(View.GONE);
    }

    public boolean isSuscription() {
        return isSuscription;
    }


    public static class FragmentOrderAddress extends Fragment implements OnMapReadyCallback, Observer, ListenerMap {
        public static final int ERROR_ADDRESS =-1;
        public static final int ERROR_CP =-2;

        private OrderAdressFragmentListener listener;
        private GoogleMap mMap;
        protected View v;

        private MrJeffTextView textBilling;
        private MrJeffTextView textShipping;
        private EditText textNumberBilling;
        private EditText textNumberShipping;
        private EditText textFloorBilling;
        private EditText textFloorShipping;
        private MrJeffTextView textCityBill;
        private MrJeffTextView textCPBill;
        private MrJeffTextView textCityShipp;
        private MrJeffTextView textCPShipp;
        private MrJeffTextView titledelivery;
        private MrJeffTextView titlepickup;
        private LinearLayout panelShippingAddres;
        private LinearLayout btnShameAddress;
        private LinearLayout btnOtherAddress;
        private RelativeLayout buttonhour;
        private ImageView imageBtnOtherAddress;
        private ImageView imageBtnShameAddress;
        private ProgressDialog progressDialog;
        private ImageView pointmap, pointmapbig, geomrjeff;
        private Handler handler = new Handler();
        private Boolean shameAddress = Boolean.TRUE;
        private boolean isSuscription;
        private boolean zalando = false;
        private EditText textComments;
        private ScrollView scroll;
        private SupporMapFragmentDragable mapFragment;


        public static FragmentOrderAddress newInstance(Bundle options) {
            FragmentOrderAddress var1 = new FragmentOrderAddress();
            var1.setArguments(options);
            return var1;
        }

        public FragmentOrderAddress() {
            super();

        }


        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            if (activity instanceof OrderAddressActivity) {
                this.listener = (OrderAddressActivity) activity;
            }
        }
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle arguments = getArguments();
            if (arguments != null) {
                isSuscription = arguments.getBoolean(MrJeffConstant.INTENT_ATTR.SUSCRIPTION);
                try {
                    zalando = arguments.getBoolean(MrJeffConstant.INTENT_ATTR.ZALANDO);
                }catch (Exception e){

                }
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v = inflater.inflate(R.layout.fragment_order_address, container, false);
            initComponents(v);
            initMap();
            initEvent();
            if (getActivity() instanceof OrderAddressActivity) {
                ((OrderAddressActivity) getActivity()).disenabledButtonLocation();
            }
            return v;
        }

        @Override
        public void onLowMemory() {
            super.onLowMemory();
            System.gc();
        }

        protected void initMap() {

            Runnable runMap = new Runnable() {
                @Override
                public void run() {
                    final int ZOOM = 100;

                    if (getActivity() != null &&  GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()) == ConnectionResult.SUCCESS ) {
                        System.gc();
                        if(mMap != null) {
                            mMap.clear();
                        }
                    }

                    if(getActivity() != null) {
                        mapFragment = SupporMapFragmentDragable.newInstanceDrawable(null);
                        mapFragment.setListenerMap(FragmentOrderAddress.this);
                        getChildFragmentManager().beginTransaction().add(R.id.contentmap, mapFragment).commit();
                        mapFragment.getMapAsync(FragmentOrderAddress.this);
                    }
                }
            };

            Handler handlerMap = new Handler();
            handlerMap.post(runMap);

        }


        @Override
        public void setOnDragStarted() {
            pointmap.setVisibility(View.GONE);
            pointmapbig.setVisibility(View.VISIBLE);
        }

        @Override
        public void setOnDragEnd(GoogleMap gMap) {
            pointmap.setVisibility(View.VISIBLE);
            pointmapbig.setVisibility(View.GONE);
            putAddress(gMap.getCameraPosition().target.latitude, gMap.getCameraPosition().target.longitude);
        }


        protected void initComponents(View v) {
            textBilling = (MrJeffTextView) v.findViewById(R.id.textAddressBill);
            textShipping = (MrJeffTextView) v.findViewById(R.id.textAddressShipping);
            textNumberBilling = (EditText) v.findViewById(R.id.textAddressBillNumber);
            textNumberShipping = (EditText) v.findViewById(R.id.textAddressShipingNumber);
            textFloorBilling = (EditText) v.findViewById(R.id.textAddressBillDoor);
            textFloorShipping = (EditText) v.findViewById(R.id.textAddressShippingDoor);
            textCityBill = (MrJeffTextView) v.findViewById(R.id.textCityBill);
            textCPBill = (MrJeffTextView) v.findViewById(R.id.textCPBill);
            textCityShipp = (MrJeffTextView) v.findViewById(R.id.textCityShipp);
            textCPShipp = (MrJeffTextView) v.findViewById(R.id.textCPShipp);
            panelShippingAddres = (LinearLayout) v.findViewById(R.id.panelShippingAddress);
            btnShameAddress = (LinearLayout) v.findViewById(R.id.btnShameAddress);
            btnOtherAddress = (LinearLayout) v.findViewById(R.id.btnOtherAddress);
            imageBtnOtherAddress = (ImageView) v.findViewById(R.id.imageBtnOtherAddress);
            imageBtnShameAddress = (ImageView) v.findViewById(R.id.imageBtnShameAddress);
            pointmap = (ImageView) v.findViewById(R.id.pointmap);
            pointmapbig = (ImageView) v.findViewById(R.id.pointmapbig);
            geomrjeff = (ImageView) v.findViewById(R.id.geomrjeff);
            buttonhour = (RelativeLayout) v.findViewById(R.id.buttonhour);
            textComments = (EditText) v.findViewById(R.id.textComments);
            scroll = (ScrollView) v.findViewById(R.id.scrollorderaddress);
            titledelivery = (MrJeffTextView) v.findViewById(R.id.titledelivery);
            titlepickup = (MrJeffTextView) v.findViewById(R.id.titlepickup);
            textComments.setText(OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getNote() != null?OrderStorage.getInstance(getActivity()).getOrderSend().getNote() :"");

            if(isSuscription){
                btnShameAddress.setVisibility(View.GONE);
                btnOtherAddress.setVisibility(View.GONE);
                titledelivery.setVisibility(View.GONE);
                titlepickup.setText(getResources().getString(R.string.addresstitle));
            }
        }

        private void initEvent() {

            textBilling.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.SEARCHBILLING, false,"","Valido");
                        listener.onClickSearchAddressBilling();
                    }
                }
            });

            textShipping.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.SEARCHSHIPPING, false,"","Valido");
                        listener.onClickSearchAddressShipping();
                    }
                }
            });

            btnShameAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.SAMEADDRESS, false,"Misma","Valido");
                    panelShippingAddres.setVisibility(View.GONE);
                    imageBtnOtherAddress.setImageResource(R.drawable.check_empty);
                    imageBtnShameAddress.setImageResource(R.drawable.check);
                    shameAddress = Boolean.TRUE;
                }
            });

            btnOtherAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.SAMEADDRESS, false,"Otra","Valido");
                    panelShippingAddres.setVisibility(View.VISIBLE);
                    imageBtnOtherAddress.setImageResource(R.drawable.check);
                    imageBtnShameAddress.setImageResource(R.drawable.check_empty);
                    shameAddress = Boolean.FALSE;
                    scroll.requestLayout();
                    scroll.post(new Runnable() {

                        @Override
                        public void run() {
                            scroll.fullScroll(ScrollView.FOCUS_DOWN);
                        }
                    });
                }
            });

            geomrjeff.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMessageMyLocation();
                }
            });

            buttonhour.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    listener.onClickGoTohour();
                     if(TextUtils.isEmpty(textNumberBilling.getText().toString().trim())){
                         new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.date_disabled).setMessage(R.string.stree_number).setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                             public void onClick(DialogInterface dialog, int id) {
                                 dialog.dismiss();
                             }
                         })
                                 .setPositiveButton(R.string.continue1, new DialogInterface.OnClickListener() {
                                     @Override
                                     public void onClick(DialogInterface dialogInterface, int i) {
                                         dialogInterface.dismiss();
                                         textNumberBilling.setText("S/N");
                                         save();

                                     }
                                 }).show();
                     }else
                    save();
                }
            });
        }


        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            if(listener != null){
                listener.setTitlePageOne();
            }
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
            if(listener != null){
                listener.setTitlePageOne();
            }
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()) == ConnectionResult.SUCCESS) {
                System.gc();
                if(mapFragment != null )
                    mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        googleMap.clear();
                    }
                });
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
//            loadCP(null);
            mMap = googleMap;
            mapFragment.setMap(mMap);
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            LatLng location = null;
            if (LocationStorage.getInstance().getLastLocation() != null) {
                location = new LatLng(LocationStorage.getInstance().getLastLocation().getLatitude(), LocationStorage.getInstance().getLastLocation().getLongitude());
            } else {
                location = new LatLng(40.417160, -3.703561);
            }
            MarkerOptions options = new MarkerOptions();
            options.position(location);
            options.draggable(false);
            //options.icon(BitmapDescriptorFactory.fromResource(R.drawable.mrjeff_geolocalization));
            options.alpha(0f);
            mMap.addMarker(options);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 16.0f));

            if (OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null
                    && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1() != null
                    && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode() != null
                    && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getCity() != null) {
                if (searchAddress(withoutFloor(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1()) + ", " + OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode() + ", " + OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getCity(), true, textBilling)) {
                    String[] part = OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1().split(",");
                    textBilling.setText(part.length > 0 ? part[0] : OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1());
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.ADDRESSBILING, false, textBilling.getText().toString(),"Valido");
                    textNumberBilling.setText(part.length > 1 ? part[1] : "");
                    if (part.length > 2) {
                        String others = "";
                        for (int i = 2; i < part.length; i++) {
                            others = (i > 2 ? ", " : "") + part[i];
                        }
                        textFloorBilling.setText(others);
                    }
                    textCPBill.setText(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode());
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CPBILLING, false, "",OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode());
                    loadCP(textCPBill);
                    textCityBill.setText(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getCity());
                }
            } else if (CustomerStorage.getInstance(getActivity()).isLogged()
                    && CustomerStorage.getInstance(getActivity()).getCustomer() != null
                    && CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address() != null
                    && CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getAddress_1() != null) {
                if (searchAddress(withoutFloor(CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getAddress_1()) + ", " + CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPostcode() + ", " + CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getCity(), true, textBilling)) {
                    setBillingAddress();

                    String[] part = CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getAddress_1().split(",");
                    textBilling.setText(part.length > 0 ? part[0] : OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1());
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.ADDRESSBILING, false, textBilling.getText().toString(),"Valido");
                    textNumberBilling.setText(part.length > 1 ? part[1] : "");
                    if (part.length > 2) {
                        String others = "";
                        for (int i = 2; i < part.length; i++) {
                            others = (i > 2 ? ", " : "") + part[i];
                        }
                        textFloorBilling.setText(others);
                    }
                    textCPBill.setText(CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPostcode());
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CPBILLING, false, "",CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPostcode());
                    loadCP(textCPBill);
                    textCityBill.setText(CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getCity());
                }

            }

            if (differentAddressShipping() && !isSuscription) {
                imageBtnOtherAddress.setImageResource(R.drawable.check);
                imageBtnShameAddress.setImageResource(R.drawable.check_empty);
                shameAddress = Boolean.FALSE;
                panelShippingAddres.setVisibility(View.VISIBLE);
                if (searchAddress(withoutFloor(OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_1()) + ", " + OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode() + ", " + OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getCity(), false, textShipping)) {
                    String[] part = OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_1().split(",");
                    textShipping.setText(part.length > 0 ? part[0] : OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_1());
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.ADDRESSSHIPPING, false, textShipping.getText().toString(),"Valido");
                    textNumberShipping.setText(part.length > 1 ? part[1] : "");
                    if (part.length > 2) {
                        String others = "";
                        for (int i = 2; i < part.length; i++) {
                            others = (i > 2 ? ", " : "") + part[i];
                        }
                        textFloorShipping.setText(others);
                    }
                    textCPShipp.setText(OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode());
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CPSHIPPING, false, textCPShipp.getText().toString(),"Valido");
                    loadCP(textCPShipp);
                    textCityShipp.setText(OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getCity());
                }
            }
            if(OrderAddressActivity.toPosition!=null){
                location=OrderAddressActivity.toPosition;
                options = new MarkerOptions();
                options.position(location);
                options.draggable(false);
                //options.icon(BitmapDescriptorFactory.fromResource(R.drawable.mrjeff_geolocalization));
                options.alpha(0f);
                mMap.addMarker(options);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 16.0f));
            }
        }

        private void setBillingAddress() {
            Billing_address result = new Billing_address();
            boolean has = OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1() != null && FormValidatorMrJeff.validAddress(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1());
            result.setAddress_1(has ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1() : CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getAddress_1());
            result.setAddress_2(OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2() : null);
            has = OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode() != null && FormValidatorMrJeff.isValidPostalCode(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode());
            result.setPostcode(has ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode() : CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPostcode());
            has = OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getCity() != null;
            result.setCity(has ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getCity() : CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getCity());
            result.setEmail(CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
            result.setFirst_name(CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name());
            result.setLast_name(CustomerStorage.getInstance(getActivity()).getCustomer().getLast_name());

            OrderStorage.getInstance(getActivity()).getOrderSend().setBilling_address(result);
            OrderStorage.getInstance(getActivity()).saveJson();
        }

        private void loadCP(MrJeffTextView cp){
            String cpp="28048";
            if(cp==null || TextUtils.isEmpty(cp.getText()))
                cpp="28048";
            else
            cpp=cp.getText().toString();
            Log.d("Postal Code", cpp);
            if (!DateTimeTableStorage.getInstance().isLoadCp(cpp)) {
                SearchTimeTableWorker worker = new SearchTimeTableWorker(cpp);
                Thread thread = new Thread(worker);
                thread.start();
            }
        }

        private void setBillingAddressTextView() {
            Billing_address result = new Billing_address();
            boolean has = textBilling.getText() != null && !textBilling.getText().toString().isEmpty();
            boolean hasNumber = textNumberBilling.getText() != null && !textNumberBilling.getText().toString().isEmpty();
            boolean hasOther = textFloorBilling.getText() != null && !textFloorBilling.getText().toString().isEmpty();
            result.setAddress_1(has ? textBilling.getText().toString() + (hasNumber ? ", " + textNumberBilling.getText().toString() + (hasOther ? " , " + textFloorBilling.getText().toString() : "") : "") : "");
            has = OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode() != null && FormValidatorMrJeff.isValidPostalCode(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode());
            result.setPostcode(textCPBill.getText().toString());
            result.setCity(textCityBill.getText().toString());
            result.setEmail(CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
            result.setFirst_name(CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name());
            result.setLast_name(CustomerStorage.getInstance(getActivity()).getCustomer().getLast_name());
            result.setPhone(OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPhone() : CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address() != null ? CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPhone():null);

            OrderStorage.getInstance(getActivity()).getOrderSend().setBilling_address(result);
            OrderStorage.getInstance(getActivity()).saveJson();
        }

        private void setShippingAddressTextView() {
            Shipping_address result = new Shipping_address();
            boolean has = textShipping.getText() != null && !textShipping.getText().toString().isEmpty();
            boolean hasNumber = textNumberShipping.getText() != null && !textNumberShipping.getText().toString().isEmpty();
            boolean hasOther = textFloorShipping.getText() != null && !textFloorShipping.getText().toString().isEmpty();
            result.setAddress_1(has ? textShipping.getText().toString() + (hasNumber ? ", " + textNumberShipping.getText().toString() + (hasOther ? " , " + textFloorShipping.getText().toString() : "") : "") : "");
            has = OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode() != null && FormValidatorMrJeff.isValidPostalCode(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode());
            result.setPostcode(textCPShipp.getText().toString());
            result.setCity(textCityShipp.getText().toString());

            OrderStorage.getInstance(getActivity()).getOrderSend().setShipping_address(result);
            OrderStorage.getInstance(getActivity()).saveJson();
        }

        private boolean differentAddressShipping() {
            boolean result = false;
            String nameBilling = null;
            String nameShipping = null;
            if (OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null
                    && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1() != null
                    && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode() != null
                    && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getCity() != null) {
                nameBilling = OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1() + ", " + OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode() + ", " + OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getCity();
            }

            if (OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address() != null
                    && OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_1() != null
                    && OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode() != null
                    && OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getCity() != null) {
                nameShipping = OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_1() + ", " + OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode() + ", " + OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getCity();
            }

            if (nameShipping != null && nameBilling != null) {
                result = !nameShipping.equals(nameBilling);
            } else if (nameShipping != null && nameBilling == null) {
                result = true;
            }

            return result;
        }

        public String withoutFloor(String addressName){
            String address = "";
            if(addressName != null){
                String [] split = addressName.split(",");
                if(split.length >=2) {
                    address= split[0] + "," + split[1];
                } else{
                    address = addressName;
                }
            }
            return  address;
        }

        public boolean searchAddress(String addressName, boolean clear, TextView textView) {
            boolean result = false;
            Geocoder geoCoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> address = geoCoder.getFromLocationName(addressName, 1);

                if (address.size() > 0) {
                    result = true;
                    Double lat = (double) (address.get(0).getLatitude());
                    Double lon = (double) (address.get(0).getLongitude());

                    final LatLng user = new LatLng(lat, lon);
                    mMap.clear();
                    MarkerOptions options = new MarkerOptions();
                    options.position(user);
                    options.draggable(true);
                    options.alpha(0f);
                    //options.icon(BitmapDescriptorFactory.fromResource(R.drawable.mrjeff_geolocalization));
                    Marker hamburg = mMap.addMarker(options);
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(user));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);

                }


            } catch (Exception e) {

            }

            return result;
        }

        @Override
        public void onDestroyView() {
            if (mMap != null) {
                Runnable runMap = new Runnable() {
                    @Override
                    public void run() {
                        getFragmentManager()
                                .beginTransaction()
                                .remove(mapFragment)
                                .commit();
                    }
                };

            }
            super.onDestroyView();
        }

        private void putAddress(Double latitude, Double longitude) {
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> list = geocoder.getFromLocation(latitude, longitude, 1);
                if (!list.isEmpty()) {
                    String addressName = list.get(0).getAddressLine(0);
                    String postalCode = list.get(0).getPostalCode();
                    String city = list.get(0).getLocality();

                    String[] part = addressName.split(",");
                    textBilling.setText(part.length > 0 ? part[0] : OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1());
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.ADDRESSBILING, false, "","Valido");
                    textNumberBilling.setText(part.length > 1 ? part[1] : "");
                    textCPBill.setText(postalCode);
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CPBILLING, false, "","Valido");
                    Log.d("Postal code", textCPBill.getText().toString());
                    loadCP(textCPBill);
                    textCityBill.setText(city);

                }
            } catch (Exception e) {

            }
        }

        @Override
        public void update(final Observable observable, Object data) {
            Thread threadUpdate = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            if (observable instanceof LocationStorage) {
                                findLocation(true);
                            }
                        }
                    });
                }
            });

            threadUpdate.start();

        }

        public void setListener(OrderAdressFragmentListener listener) {
            this.listener = listener;
        }

        private void showErrorOpenDate(int title, int text) {
            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(title).setMessage(text).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            }).show();
        }

        private void save() {

            try {
                InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(textNumberBilling.getWindowToken(), 0);
            }catch (Exception e){

            }
//
            Boolean isCorrect = Boolean.TRUE;
            try {
                isCorrect = FormValidatorMrJeff.validAddress(textBilling.getText().toString() + ", " + textNumberBilling.getText().toString() + (textFloorBilling.getText() != null && !textFloorBilling.getText().toString().isEmpty() ? "," + textFloorBilling.getText().toString() : ""));

                if (!isCorrect) {
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSBILLING, false,"","No Valido");
                    showErrorOpenDate(R.string.date_disabled, R.string.error_address_bill_not_valid);
                    return;
                }

                isCorrect = textNumberBilling.getText() != null && textNumberBilling.getText().length() >0 ;
                if (!isCorrect) {
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSBILLING, false,"","No Valido");
                    showErrorOpenDate(R.string.date_disabled, R.string.error_address_bill_not_valid);
                    return;
                }

                isCorrect = searchAddressValid(textBilling.getText().toString() + ", " + textNumberBilling.getText().toString() + "," + textCPBill.getText().toString() + "," + textCityBill.getText().toString());

                if (!isCorrect) {
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSBILLING, false,"","No Valido");
                    return;
                } else {
                    setBillingAddressTextView();
                }

                if (panelShippingAddres.getVisibility() == View.GONE) {
                    Shipping_address shipping_address = new Shipping_address();
                    shipping_address.setAddress_1(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1());
                    shipping_address.setCity(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getCity());
                    shipping_address.setPostcode(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode());
                    shipping_address.setFirst_name(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getFirst_name());
                    shipping_address.setLast_name(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getLast_name());

                    OrderStorage.getInstance(getActivity()).getOrderSend().setShipping_address(shipping_address);
                } else {
                    isCorrect = FormValidatorMrJeff.validAddress(textShipping.getText().toString() + ", " + textNumberShipping.getText().toString() + (textFloorShipping.getText() != null && !textFloorShipping.getText().toString().isEmpty() ? "," + textFloorShipping.getText().toString() : ""));
                    if (!isCorrect) {
                        new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSBILLING, false,"","No Valido");
                        showErrorOpenDate(R.string.addressNotValid, R.string.error_address_ship_not_valid);
                        return;
                    }

                    isCorrect = textNumberShipping.getText() != null && textNumberShipping.getText().length() >0 ;
                    if (!isCorrect) {
                        new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSBILLING, false,"","No Valido");
                        showErrorOpenDate(R.string.addressNotValid, R.string.error_address_bill_not_valid);
                        return;
                    }

                    isCorrect = searchAddressValid(textShipping.getText().toString() + ", " + textNumberShipping.getText().toString() + (textFloorShipping.getText() != null && !textFloorShipping.getText().toString().isEmpty() ? "," + textFloorShipping.getText().toString() : "") + "," + textCPShipp.getText().toString() + "," + textCityShipp.getText().toString());

                    if (!isCorrect) {
                        new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSBILLING, false,"","No Valido");
                        return;
                    } else {
                        setShippingAddressTextView();
                    }

                    isCorrect = isTheSameCity();

                    if (!isCorrect) {
                        showErrorOpenDate(R.string.addressNotValid, R.string.addressNotValidCpText);
                        new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSBILLING, false,"","No Valido");
                        return;
                    }

                }



                if (isCorrect) {
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSBILLING, false,"","Valido");
                    OrderStorage.getInstance(getActivity()).getOrderSend().setNote(textComments.getText() != null ? textComments.getText() .toString():"");
                    listener.onClickGoTohour();
                }

            }catch (Exception e){
                e.printStackTrace();
                Log.e("ADDRESS", "Error al validar dirección", e);
                showErrorOpenDate(R.string.error, R.string.error_address_bill_not_valid);
            }

        }

        private boolean isTheSameCity(){
            String city1 =  PostalCodeStorage.getInstance(getActivity()).getCity(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode());
            String city2 =  PostalCodeStorage.getInstance(getActivity()).getCity(OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode());
            return city1.trim().equalsIgnoreCase(city2.trim());

        }

        public boolean searchAddressValid(String addressName) {
            boolean result = false;
            Geocoder geoCoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> address = geoCoder.getFromLocationName(addressName, 1);

                if (address.size() > 0) {
                    Double lat = (double) (address.get(0).getLatitude());
                    Double lon = (double) (address.get(0).getLongitude());

                    result = putAddressValid(lat,lon);

                }


            } catch (Exception e) {

            }

            return result;
        }


        private boolean putAddressValid(Double latitude, Double longitude) {
            boolean resultBoolean = false;
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> list = geocoder.getFromLocation(latitude, longitude, 1);
                if (!list.isEmpty()) {
                    String addressName = list.get(0).getAddressLine(0);
                    String postalCode = list.get(0).getPostalCode();
                    String city = list.get(0).getLocality();

                    String address = "";

                    String[] parts = addressName.split(",");
                    for (String part : parts) {
                        address += !part.contains(postalCode) && !part.contains(city) ? (address.length() > 0 ? ", " : "") + part : "";
                    }

                    int result = isValid(addressName, postalCode, city);
                    if (result > 0) {
                        resultBoolean = true;
                    } else if (result == ERROR_CP) {
                        new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.addressNotValid).setMessage(R.string.addressNotValidText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }).show();
                    } else {
                        new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).show();
                    }

                }
            } catch (Exception e) {

            }

            return resultBoolean;
        }

        private int isValid(String address, String cp, String city){
            if(!FormValidatorMrJeff.validAddress(address)){
                return ERROR_ADDRESS;
            }else if(!FormValidatorMrJeff.isValidPostalCode(cp)){
                return ERROR_CP;
            }

            return 1;
        }


        protected void questionEnableGPS() {
            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.location_gps_disabled).setMessage(R.string.location_gps_disabled_text).setNeutralButton(R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(gpsOptionsIntent);
                }
            }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            }).show();


        }


        protected void findLocation(boolean showMessage) {
            if (LocationStorage.getInstance().getInit()) {
                if (LocationStorage.getInstance().validStorage()) {
                    getAddress();
                } else if (LocationStorage.getInstance().getLocationActive()) {
                    LocationStorage.getInstance().addObserver(this);
                    progressDialog = ProgressDialogMrJeff.createAndStart(getActivity(),R.string.searchlocation);
                } else {
                    if (showMessage) {
                        questionEnableGPS();
                    }
                }

            } else {
                if (LocationStorage.getInstance().getLocationActive()) {
                    try {
                        if (LocationStorage.getInstance().validStorage()) {
                            getAddress();
                        } else if (LocationStorage.getInstance().getLocationActive()) {
                            LocationStorage.getInstance().addObserver(this);
                            progressDialog = new ProgressDialog(getActivity());
                            progressDialog.setMessage(getActivity().getString(R.string.location_search));
                            progressDialog.setCanceledOnTouchOutside(false);
                            progressDialog.show();
                        }
                    } catch (Exception e) {
                        Log.e("LOCATION", "Error al reactivar GPS");
                    }
                } else {
                    if (showMessage) {
                        questionEnableGPS();
                    }
                }
            }
        }

        protected void getAddress() {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            LocationStorage.getInstance().deleteObserver(this);
            if (LocationStorage.getInstance().getLastLocation() != null &&
                    LocationStorage.getInstance().validStorage() &&
                    LocationStorage.getInstance().getLastLocation().getLatitude() != 0.0 && LocationStorage.getInstance().getLastLocation().getLongitude() != 0.0) {

                Double lat = LocationStorage.getInstance().getLastLocation().getLatitude();
                Double lon = LocationStorage.getInstance().getLastLocation().getLongitude();

                final LatLng user = new LatLng(lat, lon);
                mMap.clear();
                MarkerOptions options = new MarkerOptions();
                options.position(user);
                options.draggable(true);
                options.alpha(0f);
                //options.icon(BitmapDescriptorFactory.fromResource(R.drawable.mrjeff_geolocalization));
                Marker hamburg = mMap.addMarker(options);
                mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(user));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);
                putAddress(LocationStorage.getInstance().getLastLocation().getLatitude(), LocationStorage.getInstance().getLastLocation().getLongitude());
            }
        }

        public void showMessageMyLocation() {
            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setMessage(R.string.address_question).setNeutralButton(R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    findLocation(true);
                }
            }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            }).show();
        }
    }

    public static class FragmentOrderHourAddress extends Fragment implements Observer, SelectPanelListener {


        private OrderAdressFragmentListener listener;
        protected View v;
        private EditText name;
        private EditText email;
        private EditText phone;
        private MrJeffTextView hourPickUp;
        private MrJeffTextView dayPickUp;
        private MrJeffTextView hourDelivery;
        private MrJeffTextView dayDelivery;
        private LinearLayout datehourpickup;
        private LinearLayout datehourdelivery;
        private Handler handler = new Handler();
        private FragmentOrderHourAddress instance;
        private ProgressDialog progressDialog;
        private RelativeLayout buttonpay;
        private String typeSuscription = null;
        private Boolean isSubscription = false;
        private Boolean zalando = false;
        private SelectDatePanel selectDatePanel;

        public static FragmentOrderHourAddress newInstance(Bundle options) {
            FragmentOrderHourAddress var1 = new FragmentOrderHourAddress();
            var1.setArguments(options);
            return var1;
        }

        public FragmentOrderHourAddress() {

            super();
            this.instance = this;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            if (this.listener instanceof OrderAdressFragmentListener) {
                this.listener = (OrderAdressFragmentListener) activity;
            }
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle arguments = getArguments();
            if (arguments != null) {
                isSubscription = arguments.getBoolean(MrJeffConstant.INTENT_ATTR.SUSCRIPTION);
                if(isSubscription){
                    try {
                        typeSuscription = arguments.getString(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION);
                    }catch (Exception e){

                    }
                }
                try {
                    zalando = arguments.getBoolean(MrJeffConstant.INTENT_ATTR.ZALANDO);
                }catch (Exception e){

                }
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v = inflater.inflate(R.layout.fragment_order_address_hour, container, false);
            initComponents(v);
            initEvent();
            initData();
            return v;
        }


        protected void initComponents(View v) {
            name = (EditText) v.findViewById(R.id.name);
            email = (EditText) v.findViewById(R.id.email);
            phone = (EditText) v.findViewById(R.id.phone);
            hourPickUp = (MrJeffTextView) v.findViewById(R.id.hourpickup);
            dayPickUp = (MrJeffTextView) v.findViewById(R.id.datepickup);
            hourDelivery = (MrJeffTextView) v.findViewById(R.id.hourdelivery);
            dayDelivery = (MrJeffTextView) v.findViewById(R.id.datedelivery);
            datehourpickup = (LinearLayout) v.findViewById(R.id.datehourpickup);
            datehourdelivery = (LinearLayout) v.findViewById(R.id.datehourdelivery);
            buttonpay = (RelativeLayout) v.findViewById(R.id.buttonpay);
            selectDatePanel = new SelectDatePanel(v, this);
        }

        //click abrir horas de recogida
        private void initEvent() {
            datehourpickup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String dateHour = OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2();
                    String date = null;
                    String hour = null;
                    if (dateHour != null && dateHour.split(" ").length == 2) {
                        String[] dateSplit = dateHour.split(" ");
                        date = dateSplit[0];
                        hour = dateSplit[1];
                    }
                    selectDatePanel.showPanelSelectDate(getActivity().getResources().getString(R.string.pickup), date, hour, OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT, null,null);
                }
            });
//click abrir horas de entrega
            datehourdelivery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String dateHour = OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2();
                    String date = null;
                    String hour = null;
                    Calendar calendar = Calendar.getInstance();
                    if (dateHour != null && dateHour.split(" ").length == 2) {
                        String[] dateSplit = dateHour.split(" ");
                        date = dateSplit[0];
                        hour = dateSplit[1];
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            calendar.setTime(format.parse(date));
                        }catch (Exception e){

                        }
                    }
                    selectDatePanel.showPanelSelectDate(getActivity().getResources().getString(R.string.delivery), date, hour, OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND, OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2(),getMinDays(calendar));
                }
            });

            name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        new LocalyticsEventActionMrJeff(v.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NAMEBILLING, false, name.getText() != null ? name.getText().toString() : "", FormValidatorMrJeff.validRequiredMin(3, name.getText() != null ? name.getText().toString() : "") ? "Valido" : "No Valido");
                    }
                }
            });

            phone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        new LocalyticsEventActionMrJeff(v.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.PHONEBILLING, false, phone.getText() != null ? phone.getText().toString() : "", FormValidatorMrJeff.validPhone(phone.getText() != null ? phone.getText().toString() : "") ? "Valido" : "No Valido");
                    }
                }
            });

            email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        new LocalyticsEventActionMrJeff(v.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.EMAILBILLING, false, email.getText() != null ? email.getText().toString() : "", FormValidatorMrJeff.isValidEmail(email.getText() != null ? email.getText().toString() : "") ? "Valido" : "No Valido");
                    }
                }
            });

            buttonpay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    save();
                }
            });
        }

        private void initData() {

            if (OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() == null ||
                    OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode() == null ||
                    OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address() == null ||
                    OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode() == null) {
                listener.onClickCancelChangeAddress();
            } else {
                putData();
            }

        }
        //calculo primera hora de recogida a mostrar
        private void putData() {
            Calendar calendar = putHoursPickUp();
            putHoursDelivery(calendar, OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2() != null);
            putDataUser();
        }

        private void putDataUser() {
            try {
                name.setText(FormValidatorMrJeff.validRequiredMin(3, OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getFirst_name()) ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getFirst_name() : (FormValidatorMrJeff.validRequiredMin(3, CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getFirst_name()) ? CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getFirst_name() : (FormValidatorMrJeff.validRequiredMin(3, CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name()) ? CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name() : "")));
                name.setText(name.getText().toString() + (FormValidatorMrJeff.validRequiredMin(3, CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getLast_name()) ? CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getLast_name() : ""));
                email.setText(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getEmail() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getEmail() : CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getEmail() != null ? CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getEmail() : CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
                phone.setText(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPhone() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPhone() : CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPhone());
            } catch (Exception e) {

            }
        }

        public Calendar putHoursPickUp() {
            Calendar calendar = null;
            try {
                String datePickUp  = getDatePickUp();
                if(datePickUp == null) {
                    datePickUp = DateTimeTableStorage.getInstance().getFirstDateHour(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT);
                }
                calendar = paintHourDatePickUp(datePickUp);
            } catch (Exception e) {
                dayPickUp.setText("");
                hourPickUp.setText("");
            }

            return calendar;
        }

        private String getDatePickUp(){
            String result = null;
            try {
                if (OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2() != null &&
                        OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2().length() > 16) {
                    result = OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2();
                }
            }catch (Exception e){
                Log.e("ERROR ", "Parse date in order edit");
            }

            return result;
        }


        public Calendar paintHourDatePickUp(String datePaint) throws ParseException {
            String[] parts = datePaint.split(" ");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = format.parse(parts[0]);
            hourPickUp.setText(parts[1]);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            long time = calendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
            if (Calendar.getInstance().get(Calendar.YEAR) == calendar.get(Calendar.YEAR) &&
                    Calendar.getInstance().get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {
                dayPickUp.setText(getResources().getString(R.string.today));
            } else if (time >= 0 && time <= MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY) {
                dayPickUp.setText(getResources().getString(R.string.tomorrow));
            } else if (time >= 0 && time <= (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * 5)) {
                setDay(calendar, dayPickUp);
            } else {
                dayPickUp.setText(parts[0]);
            }
            MrJeffApplication.entregaReference="";
            OrderStorage o=OrderStorage.getInstance(getActivity());
            o.getOrderSend().getBilling_address().setAddress_2(datePaint);
            o.saveJson();
            return calendar;
        }
        //calculo horas de entrega en dependencia de la de recogida
        public void putHoursDelivery(Calendar calendar, boolean reload) {
            try {
                String datePickUp = getDateDelivery();
                if(datePickUp == null || reload) {
                    calendar.setTimeInMillis(calendar.getTimeInMillis() + (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * getMinDays(calendar)));
                    datePickUp = DateTimeTableStorage.getInstance().getSecondDateHour(OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND, calendar);
                }
                paintHourDateDelivery(datePickUp);

            } catch (Exception e) {
                dayDelivery.setText("");
                hourDelivery.setText("");
            }
        }

        private Integer getMinDays(Calendar referenceDate) {
            long time = referenceDate.getTimeInMillis();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String date = format.format(calendar.getTime());
            Integer result = DateTimeTableStorage.getInstance().getMinDays(OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode(),date);
            try {
                if (OrderStorage.getInstance(getActivity()).getOrder() != null && OrderStorage.getInstance(getActivity()).getOrder().getLine_items() != null) {
                    for (es.coiasoft.mrjeff.domain.orders.response.Line_items item : OrderStorage.getInstance(getActivity()).getOrder().getLine_items()) {
                        Integer idProduct = item.getProduct_id();
                        Products product = ProductStorage.getInstance(getActivity()).getProduct(idProduct);
                        if (product.getAttributes() != null) {
                            for (Attributes attr : product.getAttributes()) {
                                Integer days = 0;
                                if (attr.getName().trim().toLowerCase().equals(MrJeffConstant.ORDER_DAYS_SEND.ATTR_DAYS.trim().toLowerCase())) {
                                    days = Integer.valueOf(attr.getOptions().get(0));
                                    if (result.compareTo(days) < 0) {
                                        result = days;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {

            }
            return result;
        }

        private String getDateDelivery(){
            String result = null;
            try {
                if (OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2() != null &&
                        OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2().length() > 16) {
                    result = OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2();
                }
            }catch (Exception e){
                Log.e("ERROR ", "Parse date in order edit");
            }

            return result;
        }


        public Calendar paintHourDateDelivery(String datePaint) throws ParseException {
            String[] parts = datePaint.split(" ");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = format.parse(parts[0]);
            hourDelivery.setText(parts[1]);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            long time = calendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
            if (Calendar.getInstance().get(Calendar.YEAR) == calendar.get(Calendar.YEAR) &&
                    Calendar.getInstance().get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {
                dayDelivery.setText(getResources().getString(R.string.today));
            } else if (time <= MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY) {
                dayDelivery.setText(getResources().getString(R.string.tomorrow));
            } else if (time <= (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * 5)) {
                setDay(calendar, dayDelivery);
            } else {
                dayDelivery.setText(parts[0]);
            }
            OrderStorage o=OrderStorage.getInstance(getActivity());
            o.getOrderSend().getShipping_address().setAddress_2(datePaint);
            o.saveJson();
            return calendar;
        }


        private void setDay(Calendar calendar, MrJeffTextView textView) {
            switch (calendar.get(Calendar.DAY_OF_WEEK)) {
                case Calendar.MONDAY:
                    textView.setText(getResources().getString(R.string.monday));
                    break;
                case Calendar.TUESDAY:
                    textView.setText(getResources().getString(R.string.tuesday));
                    break;
                case Calendar.WEDNESDAY:
                    textView.setText(getResources().getString(R.string.wednesday));
                    break;
                case Calendar.THURSDAY:
                    textView.setText(getResources().getString(R.string.thursday));
                    break;
                case Calendar.FRIDAY:
                    textView.setText(getResources().getString(R.string.friday));
                    break;
                case Calendar.SATURDAY:
                    textView.setText(getResources().getString(R.string.saturday));
                    break;
                case Calendar.SUNDAY:
                    textView.setText(getResources().getString(R.string.sunday));
                    break;
            }
        }


        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            if (listener != null) {
                listener.setTitlePageTwo();
            }
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
            if (listener != null) {
                listener.setTitlePageTwo();
            }
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }


        @Override
        public void update(final Observable observable, Object data) {
            Thread threadUpdate = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            if (observable instanceof SendOrderWorker) {

                                if (!zalando && Float.valueOf(OrderStorage.getInstance(getActivity()).getOrder().getTotal()).compareTo(MrJeffConstant.ORDER.MIN_PAID_ORDER) < 0) {
                                    Float price = MrJeffConstant.ORDER.MIN_PAID_ORDER - Float.valueOf(OrderStorage.getInstance(getActivity()).getOrder().getTotal());
                                    enventOrderMin();
                                    SendOrderWorkerMin worker = new SendOrderWorkerMin(OrderStorage.getInstance(getActivity()).getOrderSend(), price, Boolean.FALSE, Boolean.FALSE);
                                    worker.addObserver(instance);
                                    Thread threadOrder = new Thread(worker);
                                    threadOrder.start();
                                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTCART, Boolean.FALSE, "Creando tasa de pedido minimo");
                                } else {
                                    if (progressDialog != null && progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                    new LocalytisEvenMrJeff(getActivity(), Boolean.TRUE, MrJeffConstant.LOCALYTICS_EVENTS.NAME.ADDRESS, Boolean.TRUE);
                                    Intent show = new Intent(getActivity(), PaidCartActivity.class);
                                    show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSubscription);
                                    if(isSubscription && typeSuscription != null){
                                        show.putExtra(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION, typeSuscription);
                                    }
                                    show.putExtra(MrJeffConstant.INTENT_ATTR.ZALANDO, zalando);
                                    getActivity().startActivity(show);
                                }
                            } else if(observable instanceof SendOrderWorkerMin){
                                if (progressDialog != null && progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }

                                new LocalytisEvenMrJeff(getActivity(), Boolean.TRUE, MrJeffConstant.LOCALYTICS_EVENTS.NAME.ADDRESS, Boolean.TRUE);
                                Intent show = new Intent(getActivity(), PaidCartActivity.class);
                                show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSubscription);
                                if(isSubscription && typeSuscription != null){
                                    show.putExtra(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION, typeSuscription);
                                }
                                show.putExtra(MrJeffConstant.INTENT_ATTR.ZALANDO, zalando);
                                getActivity().startActivity(show);

                            }
                        }
                    });
                }
            });

            threadUpdate.start();

        }

        private void enventOrderMin() {
            LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(getActivity());
            event.addOrder();
            event.send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.ORDER_MIN);
        }


        public OrderAdressFragmentListener getListener() {
            return listener;
        }

        public void setListener(OrderAdressFragmentListener listener) {
            this.listener = listener;
        }

        private void showErrorOpenDate(int title, int text) {
            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(title).setMessage(text).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            }).show();
        }

        private void save() {

            String nameText = name.getText() != null ? name.getText().toString() : "";
            Boolean correct = FormValidatorMrJeff.validRequiredMin(3, nameText);

            if (!correct) {
                new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "No Valido");
                showErrorOpenDate(R.string.date_disabled, R.string.error_name_bill);
                return;
            } else {
                OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().setFirst_name(nameText);
            }

            String phonetext = phone.getText() != null ? phone.getText().toString() : "";
            correct = FormValidatorMrJeff.validPhone(phonetext);

            if (!correct) {
                new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "No Valido");
                showErrorOpenDate(R.string.date_disabled, R.string.error_phone_bill);
                return;
            } else {
                OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().setPhone(phonetext);
            }

            String emailtext = email.getText() != null ? email.getText().toString() : "";
            correct = FormValidatorMrJeff.isValidEmail(emailtext);

            if (!correct) {
                new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "No Valido");
                showErrorOpenDate(R.string.date_disabled, R.string.error_email_bill);
                return;
            } else {
                OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().setEmail(emailtext);
            }

//            correct = DateTimeTableStorage.getInstance().isValidDateRestrictedHour(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT, OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2().split(" ")[0], OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2().split(" ")[1]);
//            if (!correct) {
//                new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "No Valido");
//                showErrorOpenDate(R.string.date_disabled, R.string.error_date_bill);
//                return;
//            }

            correct = DateTimeTableStorage.getInstance().isValidDateRestrictedHour(OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND, OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2().split(" ")[0], OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2().split(" ")[1]);
            if (!correct) {
                new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "No Valido");
                showErrorOpenDate(R.string.date_disabled, R.string.error_date_ship);
                return;
            }

            if(getActivity() != null && getActivity() instanceof OrderAddressActivity && ((OrderAddressActivity)getActivity()).isSuscription()){
                new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSBILLING, false,"","No Valido");
                new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.addressNotValid).setMessage(R.string.nolavonotvalid).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }).show();
                correct = false;
                return;
            }

            if (correct) {
                new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "Valido");
                es.coiasoft.mrjeff.domain.orders.send.Order order = new es.coiasoft.mrjeff.domain.orders.send.Order();
                order.setBilling_address(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address());
                order.setShipping_address(OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address());
                order.setNote(OrderStorage.getInstance(getActivity()).getOrderSend().getNote());
                OrderStorage.getInstance(getActivity()).saveJson();

                progressDialog = ProgressDialogMrJeff.createAndStart(getActivity(), R.string.updateaddress);

                SendOrderWorker worker = new SendOrderWorker(order, Boolean.TRUE);
                worker.addObserver(this);
                Thread threadOrder = new Thread(worker);
                threadOrder.start();
            }

        }

        public void notifyChanges(){
        }


    }

    public static class FragmentOrderHourAddressSuscription extends Fragment implements Observer, SelectPanelListener {


        private OrderAdressFragmentListener listener;
        protected View v;
        private EditText name;
        private EditText email;
        private EditText phone;
        private MrJeffTextView hourPickUp;
        private MrJeffTextView dayPickUp;
        private LinearLayout datehourpickup;
        private Handler handler = new Handler();
        private FragmentOrderHourAddressSuscription instance;
        private ProgressDialog progressDialog;
        private RelativeLayout buttonpay;
        private String typeSuscription = null;
        private Boolean isSubscription = false;
        private Boolean zalando = true;
        private SelectDatePanel selectDatePanel;
        private String hour = null;
        private String numberDay=null;

        public static FragmentOrderHourAddressSuscription newInstance(Bundle options) {
            FragmentOrderHourAddressSuscription var1 = new FragmentOrderHourAddressSuscription();
            var1.setArguments(options);
            return var1;
        }

        public FragmentOrderHourAddressSuscription() {

            super();
            this.instance = this;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            if (this.listener instanceof OrderAdressFragmentListener) {
                this.listener = (OrderAdressFragmentListener) activity;
            }
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle arguments = getArguments();
            if (arguments != null) {
                isSubscription = arguments.getBoolean(MrJeffConstant.INTENT_ATTR.SUSCRIPTION);
                if(isSubscription){
                    try {
                        typeSuscription = arguments.getString(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION);
                    }catch (Exception e){

                    }
                }
                try {
                    zalando = arguments.getBoolean(MrJeffConstant.INTENT_ATTR.ZALANDO);
                }catch (Exception e){
                    zalando =  true;
                }
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v = inflater.inflate(R.layout.fragment_order_address_hour_suscription, container, false);
            initComponents(v);
            initEvent();
            initData();
            return v;
        }


        protected void initComponents(View v) {
            name = (EditText) v.findViewById(R.id.name);
            email = (EditText) v.findViewById(R.id.email);
            phone = (EditText) v.findViewById(R.id.phone);
            hourPickUp = (MrJeffTextView) v.findViewById(R.id.hourpickup);
            dayPickUp = (MrJeffTextView) v.findViewById(R.id.datepickup);
            datehourpickup = (LinearLayout) v.findViewById(R.id.datehourpickup);
            buttonpay = (RelativeLayout) v.findViewById(R.id.buttonpay);
            selectDatePanel = new SelectDatePanel(v, this);
        }


        private void initEvent() {
            datehourpickup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String dateHour = OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2();
                    String date = null;
                    String hour = null;
                    Calendar calendar = Calendar.getInstance();
                    if (dateHour != null && dateHour.split(" ").length == 2) {
                        String[] dateSplit = dateHour.split(" ");
                        date = dateSplit[0];
                        hour = dateSplit[1];
                    }
                    selectDatePanel.showPanelSelectDate(getActivity().getResources().getString(R.string.pickup), date, hour, OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT, null,null);
                }
            });

            name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        new LocalyticsEventActionMrJeff(v.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NAMEBILLING, false, name.getText() != null ? name.getText().toString() : "", FormValidatorMrJeff.validRequiredMin(3, name.getText() != null ? name.getText().toString() : "") ? "Valido" : "No Valido");
                    }
                }
            });

            phone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        new LocalyticsEventActionMrJeff(v.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.PHONEBILLING, false, phone.getText() != null ? phone.getText().toString() : "", FormValidatorMrJeff.validPhone(phone.getText() != null ? phone.getText().toString() : "") ? "Valido" : "No Valido");
                    }
                }
            });

            email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        new LocalyticsEventActionMrJeff(v.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.EMAILBILLING, false, email.getText() != null ? email.getText().toString() : "", FormValidatorMrJeff.isValidEmail(email.getText() != null ? email.getText().toString() : "") ? "Valido" : "No Valido");
                    }
                }
            });

            buttonpay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    save();
                }
            });
        }

        private void initData() {

            if (OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() == null ||
                    OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode() == null ||
                    OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address() == null ||
                    OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode() == null) {
                listener.onClickCancelChangeAddress();
            } else {
                putData();
            }

        }

        private void putData() {
            Calendar calendar = putHoursPickUp();
            //putHoursDelivery(calendar, OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2() != null);
            putDataUser();
        }

        private void putDataUser() {
            try {
                name.setText(FormValidatorMrJeff.validRequiredMin(3, OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getFirst_name()) ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getFirst_name() : (FormValidatorMrJeff.validRequiredMin(3, CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getFirst_name()) ? CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getFirst_name() : (FormValidatorMrJeff.validRequiredMin(3, CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name()) ? CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name() : "")));
                name.setText(name.getText().toString() + (FormValidatorMrJeff.validRequiredMin(3, CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getLast_name()) ? CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getLast_name() : ""));
                email.setText(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getEmail() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getEmail() : CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getEmail() != null ? CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getEmail() : CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
                phone.setText(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPhone() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPhone() : CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPhone());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public Calendar putHoursPickUp() {
            Calendar calendar = null;
            try {
                String datePickUp  = getDatePickUp();
                if(datePickUp == null) {
                    datePickUp = DateTimeTableStorage.getInstance().getFirstDateHour(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT);
                }
                calendar = paintHourDatePickUp(datePickUp);
            } catch (Exception e) {
                dayPickUp.setText("");
                hourPickUp.setText("");
            }

            return calendar;
        }

        @Override
        public void putHoursDelivery(Calendar calendar, boolean reload) {

        }

        private String getDatePickUp(){
            String result = null;
            try {
                if (OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2() != null &&
                        OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2().length() > 16) {
                    result = OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2();
                }
            }catch (Exception e){
                Log.e("ERROR ", "Parse date in order edit");
            }

            return result;
        }


        @Override
        public Calendar paintHourDatePickUp(String datePaint) throws ParseException {
            String[] parts = datePaint.split(" ");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = format.parse(parts[0]);
            hourPickUp.setText(parts[1]);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            setDay(calendar, dayPickUp);
            hour = parts[1];
            numberDay = String.valueOf(calendar.get(Calendar.DAY_OF_WEEK));


            Calendar aux = Calendar.getInstance();
            aux.setTimeInMillis(calendar.getTimeInMillis() + (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * 7));

            String paintDelivery =  DateTimeTableStorage.getInstance().getSecondDateHour(OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode(),MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND,aux);
            String[] partsDelivery = paintDelivery.split(" ");

            OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().setAddress_2(datePaint);
            OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().setAddress_2(partsDelivery[0] +" "+ parts[1]);
            return calendar;
        }

        @Override
        public Calendar paintHourDateDelivery(String date) throws ParseException {
            return null;
        }


        private void setDay(Calendar calendar, MrJeffTextView textView) {
            switch (calendar.get(Calendar.DAY_OF_WEEK)) {
                case Calendar.MONDAY:
                    textView.setText(getResources().getString(R.string.alldays) + " " + getResources().getString(R.string.monday));
                    break;
                case Calendar.TUESDAY:
                    textView.setText(getResources().getString(R.string.alldays) + " " + getResources().getString(R.string.tuesday));
                    break;
                case Calendar.WEDNESDAY:
                    textView.setText(getResources().getString(R.string.alldays) + " " + getResources().getString(R.string.wednesday));
                    break;
                case Calendar.THURSDAY:
                    textView.setText(getResources().getString(R.string.alldays) + " " + getResources().getString(R.string.thursday));
                    break;
                case Calendar.FRIDAY:
                    textView.setText(getResources().getString(R.string.alldays) + " " + getResources().getString(R.string.friday));
                    break;
                case Calendar.SATURDAY:
                    textView.setText(getResources().getString(R.string.alldays) + " " + getResources().getString(R.string.saturday));
                    break;
                case Calendar.SUNDAY:
                    textView.setText(getResources().getString(R.string.alldays) + " " + getResources().getString(R.string.sunday));
                    break;
            }
        }


        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            if (listener != null) {
                listener.setTitlePageTwo();
            }
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
            if (listener != null) {
                listener.setTitlePageTwo();
            }
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }


        @Override
        public void update(final Observable observable, Object data) {
            Thread threadUpdate = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            if (observable instanceof SendOrderWorker) {

                                if (!zalando && Float.valueOf(OrderStorage.getInstance(getActivity()).getOrder().getTotal()).compareTo(MrJeffConstant.ORDER.MIN_PAID_ORDER) < 0) {
                                    Float price = MrJeffConstant.ORDER.MIN_PAID_ORDER - Float.valueOf(OrderStorage.getInstance(getActivity()).getOrder().getTotal());
                                    enventOrderMin();
                                    SendOrderWorkerMin worker = new SendOrderWorkerMin(OrderStorage.getInstance(getActivity()).getOrderSend(), price, Boolean.FALSE, Boolean.FALSE);
                                    worker.addObserver(instance);
                                    Thread threadOrder = new Thread(worker);
                                    threadOrder.start();
                                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTCART, Boolean.FALSE, "Creando tasa de pedido minimo");
                                } else {
                                    if (progressDialog != null && progressDialog.isShowing()) {
                                        progressDialog.dismiss();
                                    }
                                    new LocalytisEvenMrJeff(getActivity(), Boolean.TRUE, MrJeffConstant.LOCALYTICS_EVENTS.NAME.ADDRESS, Boolean.TRUE);
                                    Intent show = new Intent(getActivity(), PaidCartActivity.class);
                                    show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSubscription);
                                    show.putExtra(MrJeffConstant.INTENT_ATTR.ZALANDO, zalando);
                                    if(isSubscription && typeSuscription != null){
                                        show.putExtra(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION, typeSuscription);
                                        show.putExtra(MrJeffConstant.INTENT_ATTR.DAY, numberDay);
                                        show.putExtra(MrJeffConstant.INTENT_ATTR.HOUR, hour);
                                    }
                                    getActivity().startActivity(show);
                                }
                            } else if(observable instanceof SendOrderWorkerMin){
                                if (progressDialog != null && progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }

                                new LocalytisEvenMrJeff(getActivity(), Boolean.TRUE, MrJeffConstant.LOCALYTICS_EVENTS.NAME.ADDRESS, Boolean.TRUE);
                                Intent show = new Intent(getActivity(), PaidCartActivity.class);
                                show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSubscription);
                                show.putExtra(MrJeffConstant.INTENT_ATTR.ZALANDO, zalando);
                                if(isSubscription && typeSuscription != null){
                                    show.putExtra(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION, typeSuscription);
                                    show.putExtra(MrJeffConstant.INTENT_ATTR.DAY, numberDay);
                                    show.putExtra(MrJeffConstant.INTENT_ATTR.HOUR, hour);
                                }
                                getActivity().startActivity(show);

                            }
                        }
                    });
                }
            });

            threadUpdate.start();

        }

        private void enventOrderMin() {
            LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(getActivity());
            event.addOrder();
            event.send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.ORDER_MIN);
        }


        public OrderAdressFragmentListener getListener() {
            return listener;
        }

        public void setListener(OrderAdressFragmentListener listener) {
            this.listener = listener;
        }

        private void showErrorOpenDate(int title, int text) {
            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(title).setMessage(text).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            }).show();
        }

        private void save() {

            String nameText = name.getText() != null ? name.getText().toString() : "";
            Boolean correct = FormValidatorMrJeff.validRequiredMin(3, nameText);

            if (!correct) {
                new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "No Valido");
                showErrorOpenDate(R.string.date_disabled, R.string.error_name_bill);
                return;
            } else {
                OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().setFirst_name(nameText);
            }

            String phonetext = phone.getText() != null ? phone.getText().toString() : "";
            correct = FormValidatorMrJeff.validPhone(phonetext);

            if (!correct) {
                new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "No Valido");
                showErrorOpenDate(R.string.date_disabled, R.string.error_phone_bill);
                return;
            } else {
                OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().setPhone(phonetext);
            }

            String emailtext = email.getText() != null ? email.getText().toString() : "";
            correct = FormValidatorMrJeff.isValidEmail(emailtext);

            if (!correct) {
                new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "No Valido");
                showErrorOpenDate(R.string.date_disabled, R.string.error_email_bill);
                return;
            } else {
                OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().setEmail(emailtext);
            }


            if (correct) {
                new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "Valido");
                es.coiasoft.mrjeff.domain.orders.send.Order order = new es.coiasoft.mrjeff.domain.orders.send.Order();
                order.setBilling_address(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address());
                order.setShipping_address(OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address());
                order.setNote(OrderStorage.getInstance(getActivity()).getOrderSend().getNote());
                OrderStorage.getInstance(getActivity()).saveJson();

                progressDialog = ProgressDialogMrJeff.createAndStart(getActivity(), R.string.updateaddress);

                SendOrderWorker worker = new SendOrderWorker(order, Boolean.TRUE);
                worker.addObserver(this);
                Thread threadOrder = new Thread(worker);
                threadOrder.start();
            }
        }

        public void notifyChanges(){
        }


    }

    public static class FragmentOrderSearchAddress extends Fragment implements Observer, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks  {

        public static final String PARAM_TYPE_ADDRESS ="type";
        public static final int ERROR_ADDRESS =-1;
        public static final int ERROR_CP =-2;

        private EditText autocompleteSearchGoogle;
        private GoogleApiClient googleApiClient;
        private PlaceArrayAdapter placeArrayAdapter;
        private LatLng location = null;
        private OrderAdressFragmentListener listener;
        private boolean isShipping = true;
        private Handler handler = new Handler();
        protected View v;
        ListView listView;

        public static FragmentOrderSearchAddress newInstance(Bundle options) {
            FragmentOrderSearchAddress var1 = new FragmentOrderSearchAddress();
            var1.setArguments(options);
            return var1;
        }

        public FragmentOrderSearchAddress() {
            super();


        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            if(this.listener instanceof  OrderAdressFragmentListener) {
                this.listener = (OrderAdressFragmentListener)activity;
            }
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            try {
                Bundle arguments = getArguments();
                if(arguments != null ) {
                    isShipping = arguments.getBoolean(PARAM_TYPE_ADDRESS);
                }
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
            } catch (Exception e) {

            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v =  inflater.inflate(R.layout.fragment_order_serch_address, container, false);
            initComponents(v);
            initGoogleApi();
            initEvent();
            setAutoCompletefFocus();
            return v;
        }

        protected void initGoogleApi(){

            if(googleApiClient == null || (!googleApiClient.isConnected() && !googleApiClient.isConnecting())) {
                googleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(Places.GEO_DATA_API).addOnConnectionFailedListener(this).addConnectionCallbacks(this).build();
                googleApiClient.connect();
            }
//            autocompleteSearchGoogle.setThreshold(3);
            if(location == null){
                location = new LatLng(40.417160,-3.703561);
            }
            placeArrayAdapter = new PlaceArrayAdapter(getActivity(), R.layout.item_text,new LatLngBounds(location,location),
                    new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).setCountry("ES").build());
            //autocompleteSearchGoogle.setAdapter(placeArrayAdapter);
            listView.setTextFilterEnabled(true);
            listView.setAdapter(placeArrayAdapter);
        }

        protected void initComponents(View v){
            autocompleteSearchGoogle = (EditText) v.findViewById(R.id.autocompleteSearchGoogle);
            listView= (ListView)v.findViewById(R.id.list);
        }

        @Override
        public void onDetach() {
            super.onDetach();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(autocompleteSearchGoogle.getWindowToken(), 0);
        }

        private void initEvent(){
            listView.setOnItemClickListener(getAutompleteClickListener());
            autocompleteSearchGoogle.setOnEditorActionListener(getClickListenerAutocomplete());
            autocompleteSearchGoogle.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    placeArrayAdapter.getFilter().filter(s.toString());
                }
            });
        }

        private TextView.OnEditorActionListener getClickListenerAutocomplete(){
            return new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEND
                            || actionId == EditorInfo.IME_ACTION_GO
                            || actionId == EditorInfo.IME_ACTION_DONE
                            ) {
                        try {
                            if (placeArrayAdapter != null && !placeArrayAdapter.isEmpty() && placeArrayAdapter.getCount() > 0) {
                                final PlaceAutoComplete item = placeArrayAdapter.getItem(0);
                                if(item != null) {
                                    searchAddress(item.getDescription().toString());
                                }
                            }
                        }catch (Exception e){

                        }
                    }
                    return false;
                }
            };
        }


        private void setAutoCompletefFocus(){
            autocompleteSearchGoogle.requestFocus();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(autocompleteSearchGoogle, InputMethodManager.SHOW_FORCED);
        }


        private  AdapterView.OnItemClickListener getAutompleteClickListener(){
            return new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    final PlaceAutoComplete item = placeArrayAdapter.getItem(position);
                    if(item != null) {
                        searchAddress(item.getDescription().toString());
                    }
                }
            };
        }

        public void searchAddress(String addressNameAux) {
            searchAddress(addressNameAux,false);
        }
        public void searchAddress(String addressNameAux,boolean second) {
            Geocoder geoCoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> list = geoCoder.getFromLocationName(addressNameAux, 5,0d,0d,0d,0d);

                if (!list.isEmpty()) {
                    String addressName = list.get(0).getAddressLine(0);
                    String postalCode = list.get(0).getPostalCode();
                    String city = list.get(0).getLocality();

                    String address = "";

                    String [] parts = addressName.split(",");
                    if((city == null || postalCode == null || parts == null || parts.length <=1) && !second){
                        String addressAux = (parts.length <=1? addressName + ",1":addressName) +(postalCode != null ? ","+postalCode:"") +(city != null ? ","+city:"");
                        searchAddress(addressAux,true);
                    } else {

                        int i = 0;
                        address += parts[0] + "," + (parts[1].contains("-")?parts[1].substring(0,parts[1].indexOf("-")):parts[1]);
                        for (String part : parts) {
                            if(i > 1) {
                                address += !part.contains(postalCode) && !part.contains(city) ? (address.length() > 0 ? ", " : "") + part : "";
                            }

                            i++;
                        }

                        int result = isValid(addressName, postalCode, city);
                        if (result > 0) {
                            OrderAddressActivity.toPosition= new LatLng(list.get(0).getLatitude(),list.get(0).getLongitude());
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(autocompleteSearchGoogle.getWindowToken(), 0);
                            if (listener != null) {
                                listener.onClickOkAddress();
                            } else if ( getActivity() instanceof ProfileActivity) {
                                ((ProfileActivity) getActivity()).onClickOkAddress();
                            } else if (getActivity() instanceof OrderAdressFragmentListener) {
                                ((OrderAdressFragmentListener) getActivity()).onClickOkAddress();
                            }
                        } else if (result == ERROR_CP) {
                            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.addressNotValid).setMessage(R.string.addressNotValidText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            }).show();
                        } else {
                            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            }).show();
                        }
                    }

                }else{
                    new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).show();
                }


            } catch (Exception e) {
                new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
            }
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            if(listener != null){
                listener.setTitlePageOne();
            }
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
            if(listener != null){
                listener.setTitlePageOne();
            }
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        @Override
        public void onStart() {
            if(googleApiClient != null && !googleApiClient.isConnected() && !googleApiClient.isConnecting()){
                Log.d("GOOGLE_API","Entra en conectar");
                googleApiClient.connect();
            }
            super.onStart();
        }

        @Override
        public void onPause(){
            if(googleApiClient != null && (googleApiClient.isConnected() || googleApiClient.isConnecting())){
                googleApiClient.disconnect();
            }
            super.onPause();
        }

        @Override
        public void onResume(){
            if(googleApiClient != null && !googleApiClient.isConnected() && !googleApiClient.isConnecting()){
                googleApiClient.connect();
            }
            super.onResume();
        }
        @Override
        public void onStop(){
            if(googleApiClient != null && (googleApiClient.isConnected() || googleApiClient.isConnecting())){
                googleApiClient.disconnect();
            }
            super.onStop();
        }

        @Override
        public void update(final Observable observable, Object data) {
        }

        @Override
        public void onConnected(Bundle bundle) {
            placeArrayAdapter.setGoogleApiClient(googleApiClient);
        }

        @Override
        public void onConnectionSuspended(int i) {
            Log.d("GOOGLE_API", "Parada la conexión");
        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            Log.e("GOOGLE_API",connectionResult.getErrorMessage());
            placeArrayAdapter.setGoogleApiClient(null);
        }

        public OrderAdressFragmentListener getListener() {
            return listener;
        }

        public void setListener(OrderAdressFragmentListener listener) {
            this.listener = listener;
        }


        private int isValid(String address, String cp, String city){
            if(!FormValidatorMrJeff.validAddress(address)){
                return ERROR_ADDRESS;
            }else if(!FormValidatorMrJeff.isValidPostalCode(cp)){
                return ERROR_CP;
            } else{
                if(!isShipping) {
                    setBillingAddress(address,cp,city);
                }else{
                    setShippingAddress(address,cp,city);
                }
            }

            return 1;
        }

        private void setBillingAddress(String address, String cp, String city){
            Billing_address result = new Billing_address();
            result.setAddress_1(address);
            result.setAddress_2(OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2() : null);
            result.setPostcode(cp);
            result.setCity(city);
            result.setEmail(CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
            result.setFirst_name(CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name());
            result.setLast_name(CustomerStorage.getInstance(getActivity()).getCustomer().getLast_name());
            result.setPhone(OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPhone() : CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address() != null ? CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPhone():null);
            OrderStorage.getInstance(getActivity()).getOrderSend().setBilling_address(result);
            OrderStorage.getInstance(getActivity()).saveJson();
        }


        private void setShippingAddress(String address, String cp, String city){
            Shipping_address result = new Shipping_address();
            result.setAddress_1(address);
            result.setAddress_2(OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2() : null);
            result.setPostcode(cp);
            result.setCity(city);
            result.setFirst_name(CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name());
            result.setLast_name(CustomerStorage.getInstance(getActivity()).getCustomer().getLast_name());

            OrderStorage.getInstance(getActivity()).getOrderSend().setShipping_address(result);
            OrderStorage.getInstance(getActivity()).saveJson();
        }
    }
}