package es.coiasoft.mrjeff;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Attributes;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.SuscriptionInfo;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.orders.send.Billing_address;
import es.coiasoft.mrjeff.domain.orders.send.Order;
import es.coiasoft.mrjeff.domain.orders.send.OrderMeta;
import es.coiasoft.mrjeff.domain.orders.send.Shipping_address;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.DateTimeTableStorage;
import es.coiasoft.mrjeff.domain.storage.MonthStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.OrdersCustomerStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.domain.storage.SuscriptionCustomerStorage;
import es.coiasoft.mrjeff.manager.worker.SearchTimeTableWorker;
import es.coiasoft.mrjeff.manager.worker.UpdateDateOrderWorker;
import es.coiasoft.mrjeff.manager.worker.UpdateSuscriptionWorker;
import es.coiasoft.mrjeff.view.adapter.PlaceArrayAdapter;
import es.coiasoft.mrjeff.view.adapter.domain.PlaceAutoComplete;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.components.SelectDatePanel;
import es.coiasoft.mrjeff.view.components.util.SelectPanelListener;
import es.coiasoft.mrjeff.view.listener.OrderAdressDashboardPresentFragmentListener;
import es.coiasoft.mrjeff.view.listener.OrderAdressFragmentListener;
import es.coiasoft.mrjeff.view.validator.FormValidatorMrJeff;

/**
 * Created by Paulino on 09/03/2016.
 */
public class OrderAddressDashboardPresentActivity extends MenuActivity implements OrderAdressDashboardPresentFragmentListener {

    private static final String FRAGMENT_ADDRESS_SHOW="show";
    private static final String FRAGMENT_ADDRESS_SERACH_SHOW="searchaddress";
    private static final String FRAGMENT_ADDRESS_TIME_SHOW_HOUR="dateaddresshour";
    protected TextView textCart;
    protected ImageView btnCart;
    private String typeSuscription = null;
    private int posFragment=0;
    private boolean isDefault=false;
    private boolean isEditable =true;
    private boolean isEdit = false;
    private Integer orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.addresstitle;
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume(){
        super.onResume();

        textCart = (TextView)findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView)findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);
        try {
            posFragment = getIntent().getExtras().getInt(MrJeffConstant.INTENT_ATTR.POSFRAGMENT);
            isDefault  = getIntent().getExtras().getBoolean(MrJeffConstant.INTENT_ATTR.DEFAULT);
            isEditable  = getIntent().getExtras().getBoolean(MrJeffConstant.INTENT_ATTR.EDITABLE);
            orderId = getIntent().getExtras().getInt(MrJeffConstant.INTENT_ATTR.ORDERID);
            isEdit  = getIntent().getExtras().getBoolean(MrJeffConstant.INTENT_ATTR.ISEDIT);
        }catch (Exception e){
            posFragment = 0;
        }
        getFragment();

    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_order_address_dash);
    }

    private void getFragment(){
        Log.d("ORDERADDRESS","Entra en getfragment " + posFragment);
        switch (posFragment) {
            case 0:
                Log.d("ORDERADDRESS","Abre mapa");
                openFragmentPanelInfo();
                break;
            case 1:
                Log.d("ORDERADDRESS","Abre busqueda");
                onClickSearchAddressBilling();
                break;
            default:
                Log.d("ORDERADDRESS","Abre mapa default");
                openFragmentPanelInfo();
                break;
        }
    }

    private void openFragmentPanelInfo(){
        try {
            if(!isFinishing()) {
                Bundle params = new Bundle();
                params.putBoolean(FragmentOrderShowDetail.PARAM_TYPE_DEFAULT, isDefault);
                params.putBoolean(FragmentOrderShowDetail.PARAM_TYPE_EDITABLE, isEditable);
                params.putBoolean(FragmentOrderShowDetail.PARAM_TYPE_ISEDIT, isEdit);
                params.putInt(FragmentOrderShowDetail.PARAM_TYPE_ORDER, orderId);
                FragmentOrderShowDetail fragment = FragmentOrderShowDetail.newInstance(params);
                fragment.setListener(this);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_ADDRESS_SHOW);
                ft.commitAllowingStateLoss();
            }
        }catch (Exception e){
            Log.e("ORDERADDRESS","Error al iniciar");
            throw e;
        }
    }



    @Override
    public void onBackPressed() {
        Intent show = null;
        switch (posFragment) {
            case 0:
                SuscriptionCustomerStorage.getInstance(this).setSuscriptionInfoSend(null);
                SuscriptionCustomerStorage.getInstance(this).saveJson();
                show =  new Intent(this, HomeActivity.class);
                show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 2);
                startActivity(show);
                break;
            case 1:
                onClickOkAddress();
                break;
            default:
                SuscriptionCustomerStorage.getInstance(this).setSuscriptionInfoSend(null);
                SuscriptionCustomerStorage.getInstance(this).saveJson();
                show =  new Intent(this, HomeActivity.class);
                show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 2);
                startActivity(show);
                break;
        }
    }

    @Override
    protected int posMenu() {
        return -1;
    }

    @Override
    protected String getNameScreen() {
        return MrJeffConstant.LOCALYTICS.SCREEN_ORDER_ADDRES;
    }

    @Override
    public void onClickSearchAddressBilling() {
        if(!isFinishing()) {
            posFragment = 1;
            Bundle params = new Bundle();
            params.putBoolean(FragmentOrderSearchAddress.PARAM_TYPE_ADDRESS, false);
            params.putBoolean(FragmentOrderSearchAddress.PARAM_TYPE_DEFAULT, isDefault);
            FragmentOrderSearchAddress fragment = FragmentOrderSearchAddress.newInstance(params);
            fragment.setListener(this);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinrigth, R.anim.fadeoutleft, R.anim.fadeinleft, R.anim.fadeoutrigth);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_ADDRESS_SERACH_SHOW);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }

    @Override
    public void onClickSearchAddressShipping() {
        if(!isFinishing()) {
            posFragment = 1;
            Bundle params = new Bundle();
            params.putBoolean(FragmentOrderSearchAddress.PARAM_TYPE_ADDRESS, true);
            FragmentOrderSearchAddress fragment = FragmentOrderSearchAddress.newInstance(params);
            fragment.setListener(this);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinrigth, R.anim.fadeoutleft, R.anim.fadeinleft, R.anim.fadeoutrigth);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_ADDRESS_SERACH_SHOW);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }



    @Override
    public void onClickOkAddress() {
        if(!isFinishing()) {
            posFragment = 0;
            Bundle params = new Bundle();
            params.putBoolean(FragmentOrderShowDetail.PARAM_TYPE_DEFAULT, isDefault);
            params.putBoolean(FragmentOrderShowDetail.PARAM_TYPE_EDITABLE, isEditable);
            params.putInt(FragmentOrderShowDetail.PARAM_TYPE_ORDER, orderId);
            params.putBoolean(FragmentOrderShowDetail.PARAM_TYPE_ISEDIT, true);
            FragmentOrderShowDetail fragment = FragmentOrderShowDetail.newInstance(params);

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            fragment.setListener(this);
            ft.setCustomAnimations(R.anim.fadeinleft, R.anim.fadeoutrigth, R.anim.fadeinrigth, R.anim.fadeoutleft);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_ADDRESS_SHOW);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
            disenabledButtonLocation();
        }
    }


    @Override
    public void onClickCancelChangeAddress() {
        if(!isFinishing()) {
            posFragment = 0;
            Bundle params = new Bundle();
            params.putBoolean(FragmentOrderShowDetail.PARAM_TYPE_DEFAULT, isDefault);
            params.putBoolean(FragmentOrderShowDetail.PARAM_TYPE_EDITABLE, isEditable);
            params.putInt(FragmentOrderShowDetail.PARAM_TYPE_ORDER, orderId);
            params.putBoolean(FragmentOrderShowDetail.PARAM_TYPE_ISEDIT, true);
            FragmentOrderShowDetail fragment = FragmentOrderShowDetail.newInstance(params);
            fragment.setListener(this);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinleft, R.anim.fadeoutrigth, R.anim.fadeinrigth, R.anim.fadeoutleft);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_ADDRESS_SHOW);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
            disenabledButtonLocation();
        }
    }

    @Override
    public void setTitlePageOne() {
        if(isDefault){
            textheader.setText(getResources().getString(R.string.das15));
        } else {
            textheader.setText(getResources().getString(R.string.das14));
        }
    }

    @Override
    public void setTitlePageTwo() {
        textheader.setText(getResources().getString(R.string.hour));
    }


    public void disenabledButtonLocation(){
        ImageView imageViewLocation = (ImageView) findViewById(R.id.btnLocation);
        imageViewLocation.setVisibility(View.GONE);
    }


    public static class FragmentOrderShowDetail extends Fragment implements Observer, SelectPanelListener {
        public static final int ERROR_ADDRESS = -1;
        public static final int ERROR_CP = -2;

        public static final String PARAM_TYPE_EDITABLE = "editable";
        public static final String PARAM_TYPE_DEFAULT = "default";
        public static final String PARAM_TYPE_ORDER = "order";
        public static final String PARAM_TYPE_ISEDIT = "isEdit";

        private OrderAdressDashboardPresentFragmentListener listener;
        private GoogleMap mMap;

        protected View v;
        private ProgressDialog progressDialog;
        private Handler handler = new Handler();
        private LinearLayout paneldate;
        private EditText date;
        private LinearLayout panelhour;
        private EditText hour;
        private LinearLayout panelstreet;
        private LinearLayout panelcontainer;
        private EditText street;
        private EditText number;
        private EditText door;
        private MrJeffTextView textvisit;
        private MrJeffTextView buttonupdate;
        private MrJeffTextView buttonupdatedefault;
        private MrJeffTextView buttonsave;
        private MrJeffTextView textdown;
        private ImageView infoicon;
        private boolean isDefault = true;
        private boolean isEditable = true;
        private Integer orderId;
        private boolean edit = false;
        private boolean pickup = true;
        private Order order;
        private Orders orders;
        private InputMethodManager imm;
        private ViewGroup container;
        private SelectDatePanel selectDatePanel;


        public static FragmentOrderShowDetail newInstance(Bundle options) {
            FragmentOrderShowDetail var1 = new FragmentOrderShowDetail();
            var1.setArguments(options);
            return var1;
        }

        public FragmentOrderShowDetail() {
            super();

        }


        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            if (activity instanceof OrderAdressDashboardPresentFragmentListener) {
                this.listener = (OrderAdressDashboardPresentFragmentListener) activity;
            }
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            try {
                Bundle arguments = getArguments();
                if (arguments != null) {
                    isEditable = arguments.getBoolean(PARAM_TYPE_EDITABLE);
                    isDefault = arguments.getBoolean(PARAM_TYPE_DEFAULT);
                    orderId = arguments.getInt(PARAM_TYPE_ORDER);
                    edit = arguments.getBoolean(PARAM_TYPE_ISEDIT);
                }
            } catch (Exception e) {
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            v = inflater.inflate(R.layout.fragment_edit_hour_dash, container, false);
            this.container = container;
            initComponents(v);
            initData();
            initEvent();
            if (getActivity() instanceof OrderAddressActivity) {
                ((OrderAddressActivity) getActivity()).disenabledButtonLocation();
            }
            return v;
        }

        @Override
        public void onLowMemory() {
            super.onLowMemory();
            System.gc();
        }


        protected void initComponents(View v) {
            selectDatePanel = new SelectDatePanel(v, this);
            paneldate = (LinearLayout) v.findViewById(R.id.paneldate);
            date = (EditText) v.findViewById(R.id.date);
            panelhour = (LinearLayout) v.findViewById(R.id.panelhour);
            hour = (EditText) v.findViewById(R.id.hour);
            panelstreet = (LinearLayout) v.findViewById(R.id.panelstreet);
            street = (EditText) v.findViewById(R.id.street);
            number = (EditText) v.findViewById(R.id.number);
            door = (EditText) v.findViewById(R.id.door);
            textvisit = (MrJeffTextView) v.findViewById(R.id.textvisit);
            buttonupdate = (MrJeffTextView) v.findViewById(R.id.buttonupdate);
            textdown = (MrJeffTextView) v.findViewById(R.id.textdown);
            buttonsave = (MrJeffTextView) v.findViewById(R.id.buttonsave);
            infoicon = (ImageView) v.findViewById(R.id.infoicon);
            buttonupdatedefault = (MrJeffTextView) v.findViewById(R.id.buttonupdatedefault);
            panelcontainer = (LinearLayout) v.findViewById(R.id.panelcontainer);

        }

        protected void initData() {

            order = OrderStorage.getInstance(getActivity()).getOrderSend();
            orders = OrdersCustomerStorage.getInstance(null).getOrder(orderId);

            if (!isEditable || !isDefault) {
                buttonupdate.setVisibility(View.GONE);
            }


            if (isDefault) {
                infoicon.setVisibility(View.VISIBLE);
                buttonupdatedefault.setVisibility(View.GONE);
                textdown.setText(getResources().getString(R.string.das13));
                initDataSuscription();
            } else {
                if (isEditable) {
                    infoicon.setVisibility(View.VISIBLE);
                    buttonupdatedefault.setVisibility(View.VISIBLE);
                    textdown.setVisibility(View.VISIBLE);
                    textdown.setText(getResources().getString(R.string.das12));
                } else {
                    infoicon.setVisibility(View.GONE);
                    buttonupdatedefault.setVisibility(View.GONE);
                    textdown.setText(getResources().getString(R.string.das12));
                    textdown.setVisibility(View.GONE);
                }
                textvisit.setGravity(Gravity.CENTER);
                initDataOrder();
            }



            if(edit && isEditable && isDefault){
                clickUpdate();
            }
        }

        protected void initDataSuscription() {
            SuscriptionInfo info = SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend() == null ? SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfo():SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend();
            String address1 = info.getDefaultAddress();
            String date = info.getDefaultDay();
            String hour = info.getDefaultHour();
            switch (orders.getStateEnum()) {
                case CONFIRM:
                    pickup = true;
                    break;
                case PICKUP:
                case WASHING:
                    pickup = false;
                    break;
                case DELIVERY:
                case FINALLY:
                    pickup = false;
                    break;
            }
            paint(address1, date, hour);
        }

        protected void initDataOrder() {
            if(orders != null &&
                    orders.getStateEnum() != null) {
                switch (orders.getStateEnum()) {
                    case CONFIRM:
                        pickup = true;
                        initDataOderPickup();
                        break;
                    case PICKUP:
                    case WASHING:
                        pickup = false;
                        initDataOderDelivery();
                        break;
                    case DELIVERY:
                    case FINALLY:
                        initDataOderDelivery();
                        isEditable = false;
                        buttonupdate.setVisibility(View.GONE);
                        break;
                }
            }else{
                pickup = true;
                initDataOderPickup();
            }
        }

        protected void initDataOderPickup() {
            String address1 = orders.getBilling_address().getAddress_1();
            String date = orders.getOrder_meta().getMyfield2();
            String hour = orders.getOrder_meta().getMyfield3();

            paint(address1, date, hour);
        }

        protected void initDataOderDelivery() {
            String address1 = orders.getShipping_address().getAddress_1();
            String date = orders.getOrder_meta().getMyfield4();
            String hour = orders.getOrder_meta().getMyfield5();

            paint(address1, date, hour);
        }

        protected void paint(String address, String date, String hour) {
            String[] partAddress = address.split(",");
            if (partAddress != null && partAddress.length >= 1) {
                street.setText(partAddress[0]);
            }
            if (partAddress != null && partAddress.length >= 2) {
                number.setText(partAddress[1]);
            }
            if (partAddress != null && partAddress.length >= 3) {
                door.setText(partAddress[2]);
            }
            try {
                SimpleDateFormat format = null;
                if (isDefault) {
                    int day = Integer.valueOf(date);
                    StringBuilder builderDate = new StringBuilder();
                    builderDate.append(getActivity().getString(R.string.alldays));
                    builderDate.append(" ");
                    builderDate.append(getActivity().getResources().getString(MonthStorage.days.get(day >= 7 || day < 0 ? 0 : day)));
                    this.date.setText(builderDate.toString());
                } else {
                    format = new SimpleDateFormat("dd-MM-yyyy");
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(format.parse(date));

                    StringBuilder builderDate = new StringBuilder();
                    builderDate.append(getActivity().getResources().getString(MonthStorage.days.get(calendar.get(Calendar.DAY_OF_WEEK))));
                    builderDate.append(", ");
                    builderDate.append(calendar.get(Calendar.DAY_OF_MONTH));
                    builderDate.append(" ");
                    builderDate.append(getActivity().getResources().getString(MonthStorage.months.get(calendar.get(Calendar.MONTH) + 1)));
                    builderDate.append(" ");
                    builderDate.append(calendar.get(Calendar.YEAR));
                    this.date.setText(builderDate.toString());
                }
                String hourInit = hour.split(":")[0];
                String hourPaint = Integer.valueOf(hourInit) + "h - " + (Integer.valueOf(hourInit) + 1) + "h";
                this.hour.setText(hourPaint);

            } catch (Exception e) {

            }
        }


        private void initEvent() {

            buttonupdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickUpdate();

                }
            });

            date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(edit && isValidUpdate()) {
                        openSelectDate();
                    }
                }
            });

            hour.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(edit && isValidUpdate()) {
                        openSelectDate();
                    }
                }
            });

            street.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null && edit){
                        listener.onClickSearchAddressBilling();
                    }
                }
            });

            buttonsave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend() == null){
                        SuscriptionCustomerStorage.getInstance(getActivity()).setSuscriptionInfoSend(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfo());
                    }

                    if(street.getText().toString() != null && !street.getText().toString().isEmpty() &&
                            number.getText().toString() != null && !number.getText().toString().isEmpty()){
                        StringBuilder build = new StringBuilder();
                        build.append(street.getText().toString());
                        build.append(",");
                        build.append(number.getText().toString());
                        if( door.getText().toString() != null && !door.getText().toString().isEmpty()) {
                            build.append(",");
                            build.append(door.getText().toString());
                        }
                        SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().setDefaultAddress(build.toString());


                        progressDialog = ProgressDialogMrJeff.createAndStart(getActivity(),R.string.das18);
                        UpdateSuscriptionWorker worker = new UpdateSuscriptionWorker(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend(),getActivity());
                        worker.addObserver(FragmentOrderShowDetail.this);
                        Thread thread = new Thread(worker);
                        thread.start();
                    } else {
                        new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setMessage(R.string.address_not_valid_title).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).show();
                    }
                }
            });

            buttonupdatedefault.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), OrderAddressDashboardPresentActivity.class);
                    intent.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 0);
                    intent.putExtra(MrJeffConstant.INTENT_ATTR.DEFAULT, true);
                    intent.putExtra(MrJeffConstant.INTENT_ATTR.ISEDIT,true);
                    intent.putExtra(MrJeffConstant.INTENT_ATTR.EDITABLE, SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getOrdersinteraction().intValue() != 0);
                    intent.putExtra(MrJeffConstant.INTENT_ATTR.ORDERID, Integer.valueOf(SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getOrderId()));
                    getActivity().startActivity(intent);
                }
            });
        }

        private void clickUpdate(){
            if (isValidUpdate()) {
                enableUpdate();
            } else {
                new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setMessage(R.string.das16).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
            }
        }

        private void openSelectDate(){
            if(SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getDefaultPostalCode() != null){
                if(pickup) {
                    Calendar today = DateTimeTableStorage.getInstance().getToday();
                    String date = null;
                    String hour = null;
                    selectDatePanel.showPanelSelectDate("", date, hour, SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getDefaultPostalCode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT, null,null);
                } else {
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                    Calendar pickup = Calendar.getInstance();
                    try {
                        pickup.setTime(format.parse(orders.getOrder_meta().getMyfield2()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar cal= (Calendar) pickup.clone();
                    cal.setTimeInMillis(pickup.getTimeInMillis() + (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * 7));
                    String ref= DateTimeTableStorage.getInstance().getSecondDateHour(orders.getShipping_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND, cal);
                    SimpleDateFormat formatTimeTable = new SimpleDateFormat("yyyy-MM-dd");
                    String hourreference = formatTimeTable.format(pickup.getTime()) + " 20:00-21:00";
                    String hour = ref.split(" ")[1];
                    String date = ref.split(" ")[0];
                    selectDatePanel.showPanelSelectDate("", date, hour, SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getDefaultPostalCode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND, hourreference,7);
                }
            } else {
                new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setMessage(R.string.das17).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
            }
        }

        private Integer getMinDays(Calendar referenceDate) {
            long time = referenceDate.getTimeInMillis();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String date = format.format(calendar.getTime());
            Integer result = DateTimeTableStorage.getInstance().getMinDays(SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getDefaultPostalCode(),date);
            try {
                if (orders != null && orders.getLine_items() != null) {
                    for (es.coiasoft.mrjeff.domain.orders.response.Line_items item : orders.getLine_items()) {
                        Integer idProduct = item.getProduct_id();
                        Products product = ProductStorage.getInstance(getActivity()).getProduct(idProduct);
                        if (product.getAttributes() != null) {
                            for (Attributes attr : product.getAttributes()) {
                                if (attr.getName().trim().toLowerCase().equals(MrJeffConstant.ORDER_DAYS_SEND.ATTR_DAYS.trim().toLowerCase())) {
                                    Integer days = Integer.valueOf(attr.getOptions().get(0));
                                    if (result.compareTo(days) < 0) {
                                        result = days;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {

            }
            return result;
        }

        private void enableUpdate() {
            edit = true;
            buttonupdate.setVisibility(View.GONE);
            buttonsave.setVisibility(View.VISIBLE);
            number.setEnabled(true);
            door.setEnabled(true);
            panelcontainer.setBackgroundResource(R.drawable.border_btn_complete_pink_with);
        }

        private boolean isValidUpdate() {
            boolean result = true;
            try {
                String stringPick=orders.getOrder_meta().getMyfield2() + " " + orders.getOrder_meta().getMyfield3().split("-")[0].trim();
                String stringDrop=orders.getOrder_meta().getMyfield4() + " " + orders.getOrder_meta().getMyfield5().split("-")[0].trim();
                DateTime datePick= DateTimeFormat.forPattern("dd-MM-yyyy HH:mm").parseDateTime(stringPick);
                DateTime dateDrop= DateTimeFormat.forPattern("dd-MM-yyyy HH:mm").parseDateTime(stringDrop);
                DateTime today= new DateTime();
                if(pickup){
                    if(today.getDayOfMonth()==datePick.getDayOfMonth() && today.getMonthOfYear()== datePick.getMonthOfYear())
                        result=false;
                    else if(today.isBefore(datePick))
                        result = true;
                    else if(today.isAfter(datePick))
                        result=false;
                }else if(!pickup){
                    if(today.getDayOfMonth()==dateDrop.getDayOfMonth() && today.getMonthOfYear()== dateDrop.getMonthOfYear())
                        result=false;
                    else if(today.isBefore(dateDrop))
                        result = true;
                    else if(today.isAfter(dateDrop))
                        result=false;
                }
//                Calendar today = DateTimeTableStorage.getInstance().getToday();
//                String hour = orders.getOrder_meta().getMyfield3().split("-")[0].trim();
//                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm");
//                Calendar calendarPickUp = Calendar.getInstance();
//                calendarPickUp.setTime(format.parse(orders.getOrder_meta().getMyfield2() + " " + hour ));
//
//                long diffToday = calendarPickUp.getTimeInMillis() - today.getTimeInMillis();
//                if (diffToday > MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_TWO_HOUR.longValue()) {
//                    result = true;
//                    pickup = true;
//                } else if ((diffToday * (-1)) > MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_TWO_HOUR.longValue()) {
//                    //Comprobamos si se puede modificar la fecha de entrega
//                    pickup = false;
//                    hour = orders.getOrder_meta().getMyfield5().split("-")[0].trim();
//                    Calendar calendarDelivery = Calendar.getInstance();
//                    calendarDelivery.setTime(format.parse(orders.getOrder_meta().getMyfield4() +" " + hour ));
//                    diffToday = calendarDelivery.getTimeInMillis() - today.getTimeInMillis();
//                    if (diffToday > MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_TWO_HOUR.longValue()) {
//                        result = true;
//                    } else {
//                        result = false;
//                    }
//                } else {
//                    result = false;
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(result){
                if(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend() == null){
                    SuscriptionCustomerStorage.getInstance(getActivity()).setSuscriptionInfoSend(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfo());
                }
                SimpleDateFormat formatSend = new SimpleDateFormat("dd-MM-yyyy");
                if(pickup){
                    Calendar calendar = Calendar.getInstance();
                    try {
                        calendar.setTime(formatSend.parse(orders.getOrder_meta().getMyfield2()));
                    }catch (Exception e){

                    }
                    int day = Integer.valueOf(calendar.get(Calendar.DAY_OF_WEEK))-1;
                    SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().setDefaultDay(String.valueOf(day));
                    SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().setDayComplete(orders.getOrder_meta().getMyfield2());
                    SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().setOnlyDelivery("0");
                } else {
                    Calendar calendar = Calendar.getInstance();
                    try {
                        calendar.setTime(formatSend.parse(orders.getOrder_meta().getMyfield4()));
                    }catch (Exception e){

                    }
                    int day = Integer.valueOf(calendar.get(Calendar.DAY_OF_WEEK))-1;
                    SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().setDefaultDay(String.valueOf(day));
                    SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().setDayComplete(orders.getOrder_meta().getMyfield4());
                    SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().setOnlyDelivery("1");
                }

            }
            return result;
        }


        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            if (listener != null) {
                listener.setTitlePageOne();
            }
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
            if (listener != null) {
                listener.setTitlePageOne();
            }
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        @Override
        public void onDetach() {
            super.onDetach();
        }


        @Override
        public void onStart() {
            super.onStart();
            imm.hideSoftInputFromWindow(container.getWindowToken(), 0);
            loadCP(SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getDefaultPostalCode() != null ? SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getDefaultPostalCode() : "28001");
        }

        private void loadCP(String cp) {
            if (!DateTimeTableStorage.getInstance().isLoadCp(cp)) {
                SearchTimeTableWorker worker = new SearchTimeTableWorker(cp);
                Thread thread = new Thread(worker);
                thread.start();
            }
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
        }


        @Override
        public void update(final Observable observable, final Object data) {
            Thread threadUpdate = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            if(data != null && data instanceof Boolean && ((Boolean)data)){
                                try {
                                    Order send = new Order();
                                    send.setOrder_meta(new OrderMeta());
                                    Orders orders = OrdersCustomerStorage.getInstance(getActivity()).getOrder(Integer.valueOf(SuscriptionCustomerStorage.getInstance(getActivity()).getOrder().getOrderId()));
                                    if (pickup) {
                                        send.getOrder_meta().setMyfield2(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().getDayComplete());
                                        send.getOrder_meta().setMyfield3(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().getDefaultHour());
                                        send.getOrder_meta().setMyfield4(orders.getOrder_meta().getMyfield4());
                                        send.getOrder_meta().setMyfield5(orders.getOrder_meta().getMyfield5());
                                    } else {
                                        send.getOrder_meta().setMyfield2(orders.getOrder_meta().getMyfield2());
                                        send.getOrder_meta().setMyfield3(orders.getOrder_meta().getMyfield3());
                                        send.getOrder_meta().setMyfield4(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().getDayComplete());
                                        send.getOrder_meta().setMyfield5(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().getDefaultHour());
                                    }


                                    UpdateDateOrderWorker worker = new UpdateDateOrderWorker(send,Integer.valueOf(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfo().getOrderId()));
                                    Thread thread = new Thread(worker);
                                    thread.start();
                                }catch (Exception e){

                                }
                                new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setMessage(R.string.das19).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        SuscriptionCustomerStorage.getInstance(getActivity()).setSuscriptionInfoSend(null);
                                        SuscriptionCustomerStorage.getInstance(getActivity()).saveJson();
                                        Intent show =  new Intent(getActivity(), HomeActivity.class);
                                        show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 2);
                                        getActivity().startActivity(show);
                                    }
                                }).show();
                            } else {
                                new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setMessage(R.string.das20).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    }
                                }).show();
                            }
                        }
                    });
                }
            });

            threadUpdate.start();

        }

        public void setListener(OrderAdressDashboardPresentFragmentListener listener) {
            this.listener = listener;
        }


        @Override
        public Calendar putHoursPickUp() {
            return null;
        }

        @Override
        public void putHoursDelivery(Calendar calendar, boolean reload) {

        }

        @Override
        public Calendar paintHourDatePickUp(String datePaint) throws ParseException {
            return calendarPaintDate(datePaint);
        }

        @Override
        public Calendar paintHourDateDelivery(String date) throws ParseException {
            return calendarPaintDate(date);
        }


        private Calendar calendarPaintDate(String datePaint) throws ParseException {

            if(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend() == null){
                SuscriptionCustomerStorage.getInstance(getActivity()).setSuscriptionInfoSend(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfo());
            }

            String[] parts = datePaint.split(" ");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = format.parse(parts[0]);
            String hourInit = parts[1].split(":")[0];
            String hourPaint = Integer.valueOf(hourInit) + "h - " + (Integer.valueOf(hourInit) + 1) + "h";
            this.hour.setText(hourPaint);

            SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().setDefaultHour(parts[1]);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);


            SimpleDateFormat formatSend = new SimpleDateFormat("dd-MM-yyyy");
            int day = Integer.valueOf(calendar.get(Calendar.DAY_OF_WEEK))-1;
            StringBuilder builderDate = new StringBuilder();
            builderDate.append(getActivity().getString(R.string.alldays));
            builderDate.append(" ");
            builderDate.append(getActivity().getResources().getString(MonthStorage.days.get(day >= 7 || day < 0 ? 0 : day)));
            this.date.setText(builderDate.toString());

            SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().setDefaultDay(String.valueOf(day));
            SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().setDayComplete(formatSend.format(date));

            SuscriptionCustomerStorage.getInstance(getActivity()).saveJson();

            return calendar;
        }


        @Override
        public void notifyChanges() {

        }
    }

    public static class FragmentOrderSearchAddress extends Fragment implements Observer,
            GoogleApiClient.OnConnectionFailedListener,
            GoogleApiClient.ConnectionCallbacks  {

        public static final String PARAM_TYPE_ADDRESS ="type";
        public static final String PARAM_TYPE_DEFAULT ="default";
        public static final int ERROR_ADDRESS =-1;
        public static final int ERROR_CP =-2;

        protected View v;
        private EditText autocompleteSearchGoogle;
        private GoogleApiClient googleApiClient;
        private PlaceArrayAdapter placeArrayAdapter;
        private LatLng location = null;
        private OrderAdressDashboardPresentFragmentListener listener;
        private boolean isShipping = true;
        private boolean isDefault = true;
        private Handler handler = new Handler();
        ListView listView;

        public static FragmentOrderSearchAddress newInstance(Bundle options) {
            FragmentOrderSearchAddress var1 = new FragmentOrderSearchAddress();
            var1.setArguments(options);
            return var1;
        }

        public FragmentOrderSearchAddress() {
            super();


        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            if(this.listener instanceof  OrderAdressDashboardPresentFragmentListener) {
                this.listener = (OrderAdressDashboardPresentFragmentListener)activity;
            }
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            try {
                Bundle arguments = getArguments();
                if(arguments != null ) {
                    isShipping = arguments.getBoolean(PARAM_TYPE_ADDRESS);
                    isDefault = arguments.getBoolean(PARAM_TYPE_DEFAULT);
                }
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
            } catch (Exception e) {

            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v =  inflater.inflate(R.layout.fragment_order_serch_address, container, false);
            initComponents(v);
            initGoogleApi();
            initEvent();
            setAutoCompletefFocus();

            return v;
        }

        protected void initGoogleApi(){

            if(googleApiClient == null || (!googleApiClient.isConnected() && !googleApiClient.isConnecting())) {
                googleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(Places.GEO_DATA_API).addOnConnectionFailedListener(this).addConnectionCallbacks(this).build();
                googleApiClient.connect();
            }
//            autocompleteSearchGoogle.setThreshold(3);
            if(location == null){
                location = new LatLng(40.417160,-3.703561);
            }
            placeArrayAdapter = new PlaceArrayAdapter(getActivity(), R.layout.item_text,new LatLngBounds(location,location), null);
            listView.setTextFilterEnabled(true);
            listView.setAdapter(placeArrayAdapter);
        }

        protected void initComponents(View v){
            autocompleteSearchGoogle = (EditText) v.findViewById(R.id.autocompleteSearchGoogle);
            listView= (ListView)v.findViewById(R.id.list);
        }



        private void initEvent(){
            listView.setOnItemClickListener(getAutompleteClickListener());
            autocompleteSearchGoogle.setOnEditorActionListener(getClickListenerAutocomplete());
            autocompleteSearchGoogle.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    placeArrayAdapter.getFilter().filter(s.toString());
                }
            });
        }

        private TextView.OnEditorActionListener getClickListenerAutocomplete(){
            return new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEND
                            || actionId == EditorInfo.IME_ACTION_GO
                            || actionId == EditorInfo.IME_ACTION_DONE
                            ) {
                        try {
                            if (placeArrayAdapter != null && !placeArrayAdapter.isEmpty() && placeArrayAdapter.getCount() > 0) {
                                final PlaceAutoComplete item = placeArrayAdapter.getItem(0);
                                if(item != null) {
                                    searchAddress(item.getDescription().toString());
                                }
                            }
                        }catch (Exception e){

                        }
                    }
                    return false;
                }
            };
        }


        private void setAutoCompletefFocus(){
            autocompleteSearchGoogle.requestFocus();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(autocompleteSearchGoogle, InputMethodManager.SHOW_FORCED);
        }


        private  AdapterView.OnItemClickListener getAutompleteClickListener(){
            return new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    final PlaceAutoComplete item = placeArrayAdapter.getItem(position);
                    if(item != null) {
                        searchAddress(item.getDescription().toString());
                    }
                }
            };
        }

        public void searchAddress(String addressNameAux) {
            searchAddress(addressNameAux,false);
        }
        public void searchAddress(String addressNameAux,boolean second) {
            Geocoder geoCoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> list = geoCoder.getFromLocationName(addressNameAux, 5,0d,0d,0d,0d);

                if (!list.isEmpty()) {
                    String addressName = list.get(0).getAddressLine(0);
                    String postalCode = list.get(0).getPostalCode();
                    String city = list.get(0).getLocality();

                    String address = "";

                    String [] parts = addressName.split(",");
                    if((city == null || postalCode == null || parts == null || parts.length <=1) && !second){
                        String addressAux = (parts.length <=1? addressName + ",1":addressName) +(postalCode != null ? ","+postalCode:"") +(city != null ? ","+city:"");
                        searchAddress(addressAux,true);
                    } else {

                        int i = 0;
                        address += parts[0] + "," + (parts[1].contains("-")?parts[1].substring(0,parts[1].indexOf("-")):parts[1]);
                        for (String part : parts) {
                            if(i > 1) {
                                address += !part.contains(postalCode) && !part.contains(city) ? (address.length() > 0 ? ", " : "") + part : "";
                            }

                            i++;
                        }

                        int result = isValid(addressName, postalCode, city);
                        if (result > 0) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(autocompleteSearchGoogle.getWindowToken(), 0);
                            if (listener != null) {
                                listener.onClickOkAddress();
                            } else if ( getActivity() instanceof ProfileActivity) {
                                ((ProfileActivity) getActivity()).onClickOkAddress();
                            } else if (getActivity() instanceof OrderAdressFragmentListener) {
                                ((OrderAdressFragmentListener) getActivity()).onClickOkAddress();
                            }
                        } else if (result == ERROR_CP) {
                            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.addressNotValid).setMessage(R.string.addressNotValidText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            }).show();
                        } else {
                            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            }).show();
                        }
                    }

                }else{
                    new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).show();
                }


            } catch (Exception e) {
                new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
            }
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            if(listener != null){
                listener.setTitlePageOne();
            }
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
            if(listener != null){
                listener.setTitlePageOne();
            }
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        @Override
        public void onStart() {
            if(googleApiClient != null && !googleApiClient.isConnected() && !googleApiClient.isConnecting()){
                Log.d("GOOGLE_API","Entra en conectar");
                googleApiClient.connect();
            }
            super.onStart();
        }

        @Override
        public void onPause(){
            if(googleApiClient != null && (googleApiClient.isConnected() || googleApiClient.isConnecting())){
                googleApiClient.disconnect();
            }
            super.onPause();
        }

        @Override
        public void onResume(){
            if(googleApiClient != null && !googleApiClient.isConnected() && !googleApiClient.isConnecting()){
                googleApiClient.connect();
            }
            super.onResume();
        }
        @Override
        public void onStop(){
            if(googleApiClient != null && (googleApiClient.isConnected() || googleApiClient.isConnecting())){
                googleApiClient.disconnect();
            }
            super.onStop();
        }

        @Override
        public void update(final Observable observable, Object data) {
       /* Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dimiss();
                        }
                        if(observable instanceof  LocationStorage) {
                            findLocation(true);
                        }
                    }
                });
            }
        });

        threadUpdate.start();*/

        }

        @Override
        public void onConnected(Bundle bundle) {
            placeArrayAdapter.setGoogleApiClient(googleApiClient);
        }

        @Override
        public void onConnectionSuspended(int i) {
            Log.d("GOOGLE_API", "Parada la conexión");
        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            Log.e("GOOGLE_API",connectionResult.getErrorMessage());
            placeArrayAdapter.setGoogleApiClient(null);

        }

        public OrderAdressDashboardPresentFragmentListener getListener() {
            return listener;
        }

        public void setListener(OrderAdressDashboardPresentFragmentListener listener) {
            this.listener = listener;
        }


        private int isValid(String address, String cp, String city){
            if(!FormValidatorMrJeff.validAddress(address)){
                return ERROR_ADDRESS;
            }else if(!FormValidatorMrJeff.isValidPostalCode(cp)){
                return ERROR_CP;
            } else{
                if(isDefault) {
                    setDefaultAddress(address,cp,city);
                } else {
                    if (!isShipping) {
                        setBillingAddress(address, cp, city);
                    } else {
                        setShippingAddress(address, cp, city);
                    }
                }
            }

            return 1;
        }

        private void setDefaultAddress(String address, String cp, String city){
            if(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend() == null){
                SuscriptionCustomerStorage.getInstance(getActivity()).setSuscriptionInfoSend(SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfo());
            }
            SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().setDefaultAddress(address);
            SuscriptionCustomerStorage.getInstance(getActivity()).getSuscriptionInfoSend().setDefaultPostalCode(cp);
            SuscriptionCustomerStorage.getInstance(getActivity()).saveJson();
        }

        private void setBillingAddress(String address, String cp, String city){
            Billing_address result = new Billing_address();
            result.setAddress_1(address);
            result.setAddress_2(OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2() : null);
            result.setPostcode(cp);
            result.setCity(city);
            result.setEmail(CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
            result.setFirst_name(CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name());
            result.setLast_name(CustomerStorage.getInstance(getActivity()).getCustomer().getLast_name());
            result.setPhone(OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPhone() : CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address() != null ? CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPhone():null);

            OrderStorage.getInstance(getActivity()).getOrderSend().setBilling_address(result);
            OrderStorage.getInstance(getActivity()).saveJson();
        }


        private void setShippingAddress(String address, String cp, String city){
            Shipping_address result = new Shipping_address();
            result.setAddress_1(address);
            result.setAddress_2(OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2() : null);
            result.setPostcode(cp);
            result.setCity(city);
            result.setFirst_name(CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name());
            result.setLast_name(CustomerStorage.getInstance(getActivity()).getCustomer().getLast_name());

            OrderStorage.getInstance(getActivity()).getOrderSend().setShipping_address(result);
            OrderStorage.getInstance(getActivity()).saveJson();
        }


    }

}