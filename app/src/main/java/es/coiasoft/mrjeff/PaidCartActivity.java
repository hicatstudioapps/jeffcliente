package es.coiasoft.mrjeff;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.ShippingAddress;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.exception.AuthenticationException;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import es.coiasoft.mrjeff.Utils.ApiServiceGenerator;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.Utils.event.LocalytisEvenMrJeff;
import es.coiasoft.mrjeff.domain.ChargeStripe;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.ResultCoupon;
import es.coiasoft.mrjeff.domain.UpdateResponse;
import es.coiasoft.mrjeff.domain.orders.response.Line_items;
import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.domain.orders.send.Product;
import es.coiasoft.mrjeff.domain.storage.CouponDenegy;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.JeffApi;
import es.coiasoft.mrjeff.manager.worker.CouponWorker;
import es.coiasoft.mrjeff.manager.worker.CreateSuscriptionWorker;
import es.coiasoft.mrjeff.manager.worker.DeleteCouponWorker;
import es.coiasoft.mrjeff.manager.worker.GetSuscriptionWorker;
import es.coiasoft.mrjeff.manager.worker.PutZalandoUserWorker;
import es.coiasoft.mrjeff.manager.worker.SendOrderNoLavoWorker;
import es.coiasoft.mrjeff.manager.worker.SendOrderWorker;
import es.coiasoft.mrjeff.manager.worker.SendOrderWorkerMin;
import es.coiasoft.mrjeff.manager.worker.StripeNoLavoWorker;
import es.coiasoft.mrjeff.manager.worker.StripeWorker;
import es.coiasoft.mrjeff.view.adapter.HolderProductOrder;
import es.coiasoft.mrjeff.view.adapter.ListPorductResumenAdapter;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.listener.FocusChangeEditTextListener;
import retrofit2.http.Part;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by linovm on 21/10/15.
 */
public class PaidCartActivity extends OnResumeActivity implements Observer {


    protected Handler handler = new Handler();
    protected LinearLayout btncreditCard;
    protected LinearLayout btnPayPal;
    protected LinearLayout panelPaidCard;
    protected ImageView btnCoupon;
    protected ListView listProduct;
    protected TextView textCart;
    protected TextView textTotal;
    protected ImageView btnCart;
    protected PaidCartActivity instance;
    private EditText numberCard;
    private EditText numberCVC;
    //private EditText datePicker;
    private EditText datePickerMonth;
    private EditText datePickerYear;
    private EditText couponText;
    private ImageView checkCoupon;
    private ImageView checkcard;
    private ImageView checkPayPal;
    private Stripe stripe;
    private ImageView imageCard;
    protected LinearLayout panelCard;
    protected LinearLayout couponpanelLinear;
    protected LinearLayout panelCVC;
    protected LinearLayout panelDate;
    protected LinearLayout panelPaidAux;
    protected LinearLayout panelPaid;
    protected LinearLayout panelSelecctionDate;
    protected LinearLayout onlycard;
    protected LinearLayout pickdrop;
    protected TextView finishPaid;
    protected LinearLayout panel_recurrency;
    protected ImageView recurrency;
    protected ProgressDialogMrJeff progressDialog;
    protected int wizard;
    private boolean isSuscription = false;
    private boolean zalando = true;
    private String typeSuscription = null;
    private String coderes = null;
    private String nameresidencia = null;
    private String description;
    private AppCompatSpinner suscription;
    private String hour = null;
    private String numberDay=null;
    private boolean needPayment = false;
    private String tempCoupon;
    es.coiasoft.mrjeff.domain.orders.send.Order orderSend;
    boolean isStripe=true;

    public boolean isHasChange() {
        return hasChange;
    }

    public void setHasChange(boolean hasChange) {
        if(hasChange) {
            save.setVisibility(View.VISIBLE);
            textTotal.setText("");
        }
        this.hasChange = hasChange;
    }

    boolean hasChange= false;


    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
            .clientId(MrJeffConstant.PAYPAL.CONFIG_CLIENT_ID);
    private LinearLayout panelPagos;
    final boolean[] hasFocus = {false};
    private View save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.paidMethod;
        orderSend=OrderStorage.getInstance(getApplicationContext()).getOrderSend();
        super.onCreate(savedInstanceState);
        KeyboardVisibilityEvent.setEventListener(
                this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        // some code depending on keyboard visiblity status
                        if(isOpen){
                            listProduct.setVisibility(View.INVISIBLE);
                            if(hasFocus[0]){
                                panelPagos.setVisibility(View.INVISIBLE);
                            }
                        }else{
                            listProduct.setVisibility(View.VISIBLE);
                            panelPagos.setVisibility(View.VISIBLE);
                        }
                    }
                });
    }


    @Override
    protected void onResume(){
        Order o=OrderStorage.getInstance(getApplicationContext()).getOrder();
//        if(o!= null)
//            Toast.makeText(this,"ID_PEDIDO "+o.getOrder_number(),Toast.LENGTH_LONG).show();
        MrJeffApplication.isPaying=true;
        MrJeffApplication.orderSend=orderSend;
        super.onResume();
        initComponents();
        initRecurrency();
        initListAdapter();
        initPaidForms();
        initEvent();
        validationInit();
        isSuscription();
        notificationProductMin();
    }

    private void initComponents(){
        instance = this;
        handler = new Handler();
        textCart = (TextView) findViewById(R.id.numberCartHeader);
        textTotal = (TextView) findViewById(R.id.textTotal);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView) findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);
        btnCoupon = (ImageView) findViewById(R.id.btnCoupon);
        btnCoupon.setVisibility(View.VISIBLE);
        listProduct = (ListView) findViewById(R.id.listProductsResume);
        panelPagos = (LinearLayout) findViewById(R.id.stripeform);
        numberCard = (EditText) findViewById(R.id.numbercard);
        numberCVC = (EditText) findViewById(R.id.numbercvc);
        datePickerMonth = (EditText) findViewById(R.id.datemonth);
        datePickerYear = (EditText) findViewById(R.id.dateyear);
        panelCard = (LinearLayout) findViewById(R.id.panelNumberCard);
        panelCVC = (LinearLayout) findViewById(R.id.panelCVC);
        panelDate = (LinearLayout) findViewById(R.id.panelDate);
        couponpanelLinear = (LinearLayout) findViewById(R.id.panelCoupon);
        couponText = (EditText) findViewById(R.id.couponName);
        checkCoupon = (ImageView) findViewById(R.id.checkCoupon);
        panel_recurrency = (LinearLayout) findViewById(R.id.panel_recurrency);
        imageCard = (ImageView) findViewById(R.id.imageCard);
        panelPaidAux = (LinearLayout) findViewById(R.id.panelPaidAux);
        panelPaid = (LinearLayout) findViewById(R.id.panelPaid);
        btncreditCard = (LinearLayout) findViewById(R.id.btncreditCard);
        btnPayPal = (LinearLayout) findViewById(R.id.btnPayPal);
        btnPayPal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBuyPressed(v);
            }
        });
        finishPaid = (TextView)findViewById(R.id.finishPaid);
        suscription = (AppCompatSpinner)findViewById(R.id.sp_type_suscription);
        recurrency = (ImageView)findViewById(R.id.recurrency);
        panelPaidCard = (LinearLayout)findViewById(R.id.panelPaidCard);
        checkcard = (ImageView)findViewById(R.id.checkcard);
        checkPayPal = (ImageView)findViewById(R.id.checkpaypal);
        panelSelecctionDate = (LinearLayout)findViewById(R.id.panelSelecctionDate);
        onlycard = (LinearLayout)findViewById(R.id.onlycard);
        pickdrop = (LinearLayout)findViewById(R.id.pickdrop);
        save= findViewById(R.id.save_changes);
        numberCard.addTextChangedListener(new FourDigitCardFormatWatcher());


    }

    private void initRecurrency(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.suscriptionstype, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.suscription.setAdapter(adapter);
        this.suscription.setSelection(0);
        this.suscription.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                btnPayPal.setVisibility(position == 0 ? View.VISIBLE : View.INVISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                btnPayPal.setVisibility(View.VISIBLE);
            }
        });
    }

    private void initPaidForms(){
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        try {
            stripe = new Stripe(this,MrJeffConstant.STRIPE.PUBLIC_KEY_STRIPE_PRO);
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }
    }

    private void validationInit(){
        if (OrderStorage.getInstance(getApplicationContext()).getOrder() != null
                && OrderStorage.getInstance(getApplicationContext()).getOrder().getBilling_address() != null) {
            eventAppsFlys();
            validEntryCoupon();
        }

        if (CustomerStorage.getInstance(getApplicationContext()).getCoupon() != null &&
                !CustomerStorage.getInstance(getApplicationContext()).getCoupon().isEmpty() &&
                btnCoupon.getVisibility() != View.GONE) {
            //progressDialog = ProgressDialogMrJeff.createAndStart(PaidCartActivity.this,R.string.calculatecoupon);
            couponText.setText(CustomerStorage.getInstance(getApplicationContext()).getCoupon());
//            checkCoupon.performClick();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkCoupon(CustomerStorage.getInstance(getApplicationContext()).getCoupon());
                }
            },1000);

            //checkCoupon(CustomerStorage.getInstance(getApplicationContext()).getCoupon());
        }
    }

    private void notificationProductMin() {
        if (OrderStorage.getInstance(getApplicationContext()).getProductMinOrder() != null) {
            new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.minorder).setMessage(R.string.minordertext).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            }).show();
        }
    }

    private void isSuscription() {
        isSuscription = getIntent().getExtras().getBoolean(MrJeffConstant.INTENT_ATTR.SUSCRIPTION);
        try {
            zalando = getIntent().getExtras().getBoolean(MrJeffConstant.INTENT_ATTR.ZALANDO);
        }catch (Exception e){
            zalando = false;
        }
        if (isSuscription) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            panelSelecctionDate.setVisibility(View.GONE);
            onlycard.setVisibility(View.VISIBLE);
            couponpanelLinear.setVisibility(View.GONE);
            panelCard.setVisibility(View.VISIBLE);
            panelPaidCard.setVisibility(View.VISIBLE);
            panel_recurrency.setVisibility(View.GONE);
            btnCoupon.setVisibility(View.GONE);
            try {
                typeSuscription = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION);
                if(typeSuscription != null && typeSuscription.equals(MrJeffConstant.INTENT_ATTR.TYPE_SUS_RES)){
                    pickdrop.setVisibility(View.VISIBLE);
                }
            }catch (Exception e){
                typeSuscription = null;
            }

            try {
                coderes = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.CODERESIDENCIA);
            }catch (Exception e){
                typeSuscription = null;
            }


            try {
                nameresidencia = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.RESIDENCIA);
            }catch (Exception e){
                nameresidencia = null;
            }

            try {
                description = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.DESCRIPTION);
            }catch (Exception e){
                description = null;
            }

            try {
                hour = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.HOUR);
            }catch (Exception e){
                hour = null;
            }

            try {
                numberDay = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.DAY);
            }catch (Exception e){
                numberDay = null;
            }

            try {
                needPayment = getIntent().getExtras().getBoolean(MrJeffConstant.INTENT_ATTR.NEEDPAYMENT);
            }catch (Exception e){
                needPayment = false;
            }


            if(needPayment){
                panelPaid.setVisibility(View.GONE);
                panelPaidAux.setVisibility(View.VISIBLE);
            }
        }

        if(zalando){
            panel_recurrency.setVisibility(View.GONE);
            btnCoupon.setVisibility(View.GONE);
        }
   }


    private void validEntryCoupon() {
        ArrayList<Line_items> products = OrderStorage.getInstance(getApplicationContext()).getOrder().getLine_items();
        for (Line_items item : products) {
            Float toal = Float.valueOf(item.getTotal());

            if (toal.compareTo(new Float(0f)) < 0) {

                btnCoupon.setVisibility(View.GONE);
            }
        }
    }

    private void onClickCreditCard() {
        if (panelCard.getVisibility() == View.VISIBLE ||
                panelCVC.getVisibility() == View.VISIBLE ||
                panelDate.getVisibility() == View.VISIBLE) {
            checkcard.setImageResource(R.drawable.checkpaid);
            checkPayPal.setImageResource(R.drawable.checkpaid);
            panelPaidCard.setVisibility(View.GONE);
            panelCard.setVisibility(View.GONE);
            panelCVC.setVisibility(View.GONE);
            panelDate.setVisibility(View.GONE);
            InputMethodManager inputMethodManager = (InputMethodManager) instance.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(instance.getCurrentFocus().getWindowToken(), 0);
        } else {
            new LocalyticsEventActionMrJeff(getApplicationContext(),MrJeffConstant.LOCALYTICS_EVENTS.ACTION.STRIPE,isSuscription,"Stripe");
            checkcard.setImageResource(R.drawable.checkpaidsel);
            checkPayPal.setImageResource(R.drawable.checkpaid);
            couponpanelLinear.setVisibility(View.GONE);
            panelPaidCard.setVisibility(View.VISIBLE);
            panelCard.setVisibility(View.VISIBLE);
            numberCard.setFocusable(true);
            numberCard.requestFocus(numberCard.getText().length());
            numberCard.addTextChangedListener(new FourDigitCardFormatWatcher());
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
    }

    private void initEvent() {
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createOrder();
            }
        });
        btncreditCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickCreditCard();

            }
        });
        this.recurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suscription.performClick();
            }
        });

        numberCard.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId==EditorInfo.IME_ACTION_NEXT) {
                    if (validCard(v.getText().toString().replace(" ",""))) {
                        panelCard.setVisibility(View.GONE);
                        panelCVC.setVisibility(View.VISIBLE);
                        numberCVC.setFocusable(true);
                        numberCVC.requestFocus(numberCVC.getText().length());
                        v.setTextColor(getResources().getColor(R.color.blueMrJeff));
                        new LocalyticsEventActionMrJeff(getApplicationContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CARD, isSuscription, "XXX");
                    } else {
                        v.setTextColor(getResources().getColor(R.color.red));
                    }
                }
                return false;
            }
        });
        numberCard.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                    if (((EditText) v).getText().length() == 2) {
                        imageCard.setImageResource(getImageCard(((EditText) v).getText().toString()));
                    } else if (((EditText) v).getText().length() < 2) {
                        imageCard.setImageResource(R.drawable.card_grey);
                    }
                    if(KeyEvent.ACTION_UP == event.getAction() && keyCode != KeyEvent.KEYCODE_DEL) {
                        if (validCard(((EditText) v).getText().toString().replace(" ",""))) {
                            panelCard.setVisibility(View.GONE);
                            panelCVC.setVisibility(View.VISIBLE);
                            numberCVC.setFocusable(true);
                            numberCVC.requestFocus(numberCVC.getText().length());
                            ((EditText) v).setTextColor(getResources().getColor(R.color.blueMrJeff));
                            new LocalyticsEventActionMrJeff(getApplicationContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CARD, isSuscription, "XXX");
                        }
                        if(((EditText) v).getText().toString().replace(" ","").length() % 4 == 0){
                            ((EditText) v).setText(((EditText) v).getText().toString()+" ");
                            ((EditText) v).setSelection(((EditText) v).getText().length());
                        }
                    }else if(KeyEvent.ACTION_UP == event.getAction() && keyCode == KeyEvent.KEYCODE_DEL)  {

                    }
                return false;
            }
        });

        numberCVC.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId==EditorInfo.IME_ACTION_NEXT) {
                    if (validCvc(numberCard.getText().toString(), v.getText().toString())) {
                        panelCVC.setVisibility(View.GONE);
                        panelDate.setVisibility(View.VISIBLE);
                        datePickerMonth.setFocusable(true);
                        datePickerMonth.requestFocus(datePickerMonth.getText().length());
                        v.setTextColor(getResources().getColor(R.color.blueMrJeff));
                        new LocalyticsEventActionMrJeff(getApplicationContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CVC, isSuscription, "XXX");
                    } else {
                        v.setTextColor(getResources().getColor(R.color.red));
                    }
                }
                return false;
            }
        });

        numberCVC.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if(KeyEvent.ACTION_UP == event.getAction() && keyCode != KeyEvent.KEYCODE_DEL) {
                    Card card = new Card(
                            numberCard.getText().toString().replace(" ",""),
                            12,
                            2026,
                            ((EditText) v).getText().toString()
                    );

                    if (((card.getType().equals(Card.AMERICAN_EXPRESS) && ((EditText) v).getText().toString().length() == 4) || (!card.getType().equals(Card.AMERICAN_EXPRESS) && ((EditText) v).getText().toString().length() == 3)) &&
                            card.validateCard()) {
                        panelCVC.setVisibility(View.GONE);
                        panelDate.setVisibility(View.VISIBLE);
                        datePickerMonth.setFocusable(true);
                        datePickerMonth.requestFocus(datePickerMonth.getText().length());
                        ((EditText) v).setTextColor(getResources().getColor(R.color.blueMrJeff));
                        new LocalyticsEventActionMrJeff(getApplicationContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CVC, isSuscription, "XXX");
                    }
                }
                return false;
            }
        });

        datePickerMonth.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if(KeyEvent.ACTION_UP == event.getAction() && keyCode != KeyEvent.KEYCODE_DEL) {
                    if (((EditText) v).getText().length() == 2) {
                        datePickerYear.setFocusable(true);
                        datePickerYear.requestFocus(datePickerYear.getText().length());
                    }
                }
                return false;
            }
        });
        datePickerMonth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length()==2){
                    datePickerYear.setFocusable(true);
                    datePickerYear.requestFocus(datePickerYear.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        datePickerYear.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId==EditorInfo.IME_ACTION_NEXT) {
                    if (validDate(numberCard.getText().toString().replace(" ",""), numberCVC.getText().toString(), datePickerMonth.getText().toString() + "/" + v.getText().toString())) {
                        panelDate.setVisibility(View.GONE);
                        new LocalyticsEventActionMrJeff(getApplicationContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.DATECARD, isSuscription, "XXX");
                        v.setTextColor(getResources().getColor(R.color.blueMrJeff));
                        InputMethodManager inputMethodManager = (InputMethodManager) instance.getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(instance.getCurrentFocus().getWindowToken(), 0);
                        onBuyStripe();
                        panelCard.setVisibility(View.VISIBLE);
                        numberCard.setVisibility(View.VISIBLE);
                    } else {
                        v.setTextColor(getResources().getColor(R.color.red));
                    }
                }
                return false;
            }
        });

        datePickerYear.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                try {

                    if(KeyEvent.ACTION_UP == event.getAction() && keyCode != KeyEvent.KEYCODE_DEL) {
                        Card card = new Card(
                                numberCard.getText().toString().replace(" ",""),
                                Integer.valueOf(datePickerMonth.getText().toString()),
                                Integer.valueOf(((EditText) v).getText().toString()),
                                numberCVC.getText().toString()
                        );
                        if (card.validateCard()) {
                            panelDate.setVisibility(View.GONE);
                            new LocalyticsEventActionMrJeff(getApplicationContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.DATECARD, isSuscription, "XXX");
                            ((EditText) v).setTextColor(getResources().getColor(R.color.blueMrJeff));
                            InputMethodManager inputMethodManager = (InputMethodManager) instance.getSystemService(INPUT_METHOD_SERVICE);
                            inputMethodManager.hideSoftInputFromWindow(instance.getCurrentFocus().getWindowToken(), 0);
                            onBuyStripe();
                        }
                    }
                }catch (Exception e){

                }
                Animation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(150); //You can manage the blinking time with this parameter
                anim.setStartOffset(20);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(20);
                textTotal.startAnimation(anim);
                return false;
            }
        });

        finishPaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(hasChange)
//                showSaveDialog();
//                else
                isStripe=true;
                if(!validateCardFields())
                    return;
                if(numberCard.getText() != null &&
                        datePickerMonth.getText() != null &&
                        datePickerYear.getText() != null &&
                        numberCVC.getText() != null) {
                    String cardNumber = numberCard.getText().toString().replace(" ","");

                    Integer cardExpMonth = -1;
                    Integer cardExpYear = -1;
                    try {
                        cardExpMonth = Integer.valueOf(datePickerMonth.getText().toString());
                        cardExpYear = Integer.valueOf(datePickerYear.getText().toString());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    String cardCVC = numberCVC.getText().toString();

                    Card card = new Card(
                            cardNumber,
                            cardExpMonth,
                            cardExpYear,
                            cardCVC
                    );



                    if (!card.validateCard()) {
                        if (panelCard.getVisibility() == View.VISIBLE ||
                                panelCVC.getVisibility() == View.VISIBLE ||
                                panelDate.getVisibility() == View.VISIBLE) {
                            new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.carddata).setMessage(R.string.carddataerror).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            }).show();
                        } else {
                            couponpanelLinear.setVisibility(View.GONE);
                            panelCard.setVisibility(View.VISIBLE);
                            panelCVC.setVisibility(View.GONE);
                            panelDate.setVisibility(View.GONE);
                            numberCard.setFocusable(true);
                            numberCard.requestFocus(numberCard.getText().length());
                        }
                    } else {

                        paid(card);
                    }
                } else {
                    if (panelCard.getVisibility() == View.VISIBLE ||
                            panelCVC.getVisibility() == View.VISIBLE ||
                            panelDate.getVisibility() == View.VISIBLE) {

                        if(panelCard.getVisibility() == View.VISIBLE){
                            panelCard.setVisibility(View.GONE);
                            panelCVC.setVisibility(View.VISIBLE);
                            panelDate.setVisibility(View.GONE);
                        } else if(panelCVC.getVisibility() == View.VISIBLE) {
                            panelCard.setVisibility(View.GONE);
                            panelCVC.setVisibility(View.GONE);
                            panelDate.setVisibility(View.VISIBLE);
                        } else {
                            new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.carddata).setMessage(R.string.carddataerror).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            }).show();
                        }
                    } else {
                        couponpanelLinear.setVisibility(View.GONE);
                        panelPaidCard.setVisibility(View.VISIBLE);
                        panelCard.setVisibility(View.VISIBLE);
                        panelCVC.setVisibility(View.GONE);
                        panelDate.setVisibility(View.GONE);
                        numberCard.setFocusable(true);
                        numberCard.requestFocus(numberCard.getText().length());
                    }
                }
            }
        });

        btnCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(couponText.getWindowToken(), 0);
                if (couponpanelLinear.getVisibility() == View.VISIBLE) {
                    couponpanelLinear.setVisibility(View.GONE);

                } else {
                    if(!hasChange)
                    couponpanelLinear.setVisibility(View.VISIBLE); else
                        showSaveDialog();

                }


            }
        });



        checkCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listProduct.setVisibility(View.VISIBLE);
                checkCoupon(couponText.getText().toString());

            }
        });
        couponText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    checkCoupon(couponText.getText().toString());
                }
                return false;
            }
        });
        couponText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                listProduct.setVisibility(View.INVISIBLE);
            }
        });
        couponText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    listProduct.setVisibility(View.INVISIBLE);
            }
        });

        numberCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                couponpanelLinear.setVisibility(View.GONE);
                //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            }
        });

        datePickerMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                couponpanelLinear.setVisibility(View.GONE);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            }
        });

//        numberCard.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if(!b){
//                    hasFocus[0] =false;
//                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
//                }
//                else {getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
//                      hasFocus[0] =true;}
//            }
//        });

        panelPaidAux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                es.coiasoft.mrjeff.domain.orders.send.Order order = new es.coiasoft.mrjeff.domain.orders.send.Order();
                order.setStatus(MrJeffConstant.ORDER.UPDATE_STATE_FINALLY);
                SendOrderWorker worker = new SendOrderWorker(order, Boolean.TRUE);
                Thread thread = new Thread(worker);
                thread.start();
                showMessageCorrect();
            }
        });

    }

    private boolean validateCardFields() {
        checkcard.setImageResource(R.drawable.checkpaidsel);
        checkPayPal.setImageResource(R.drawable.checkpaid);
        if(!validCard(numberCard.getText().toString().replace(" ",""))){
            listProduct.setVisibility(View.INVISIBLE);
            panelPaidCard.setVisibility(View.VISIBLE);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            panelCard.setVisibility(View.VISIBLE);
            numberCard.setFocusable(true);
            numberCard.requestFocus(numberCard.getText().length());
            numberCard.addTextChangedListener(new FourDigitCardFormatWatcher());
            new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.carddata).setMessage(R.string.invalid_card_number).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            }).show();
            return false;
        }
        if(!validCvc(numberCard.getText().toString().replace(" ",""),numberCVC.getText().toString())){
            listProduct.setVisibility(View.INVISIBLE);
            panelCard.setVisibility(View.GONE);
            panelPaidCard.setVisibility(View.VISIBLE);
            panelCVC.setFocusable(true);
            panelCVC.requestFocus(numberCard.getText().length());
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            panelCVC.setVisibility(View.VISIBLE);
            new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.carddata).setMessage(R.string.invalid_cvc).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            }).show();
            return false;
        }
        if(!validDate(numberCard.getText().toString().replace(" ",""),numberCVC.getText().toString(),datePickerMonth.getText().toString()+"/"
        +datePickerYear.getText().toString())){
            listProduct.setVisibility(View.INVISIBLE);
            panelCVC.setVisibility(View.GONE);
            panelPaidCard.setVisibility(View.VISIBLE);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            panelDate.setVisibility(View.VISIBLE);
            new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.carddata).setMessage(R.string.invalid_date_field).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            }).show();
            return false;
        }
        return true;
    }

    private void checkCoupon(String coupon) {
        if (coupon.length() > 0) {
            if (CouponDenegy.isCopuponDenegy(coupon)) {
                new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.coupon_denegy).setMessage(R.string.coupon_denegy_text).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                }).show();
            } else {
                CouponWorker worker = new CouponWorker(coupon);
                worker.addObserver(this);
                Thread thread = new Thread(worker);
                thread.start();
                progressDialog = ProgressDialogMrJeff.createAndStart(PaidCartActivity.this,R.string.calculatecoupon);
                InputMethodManager imm = (InputMethodManager) instance.getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(couponText.getWindowToken(), 0);
            }
        }
    }
    void showSaveDialog(){
        new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle("Guardar").setMessage("Usted ha modificado su pedido, debe guardar los cambios para continuar").setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                createOrder();
            }
        }).show();
    }

    public int getImageCard(String text) {
        int result = R.drawable.card_grey;

        if ("40".equals(text) || "42".equals(text) || "49".equals(text)) {
            result = R.drawable.visa;
        } else if ("55".equals(text) || "52".equals(text) || "51".equals(text)) {
            result = R.drawable.mastercard;
        } else if ("37".equals(text)) {
            result = R.drawable.amrecian;
        } else if ("60".equals(text)) {
            result = R.drawable.discover;
        }
        if ("30".equals(text) || "38".equals(text)) {
            result = R.drawable.dinners;
        } else if ("35".equals(text)) {
            result = R.drawable.jcb;
        }

        return result;
    }

    private boolean validCard(String cardNumber) {
        Card card = new Card(
                cardNumber,
                12,
                2026,
                "234"
        );

        return card.validateCard();
    }

    private boolean validCvc(String cardNumber, String cvc) {
        Card card = new Card(
                cardNumber,
                12,
                2026,
                cvc
        );

        return card.validateCVC();
    }

    private boolean validDate(String cardNumber, String cvc, String date) {
        String[] split = date.split("/");
        if (split.length != 2 || split[1].length() != 2) {
            return false;
        }

        Integer moth = Integer.valueOf(split[0]);
        if (moth.intValue() < 1 || moth > 12) {
            return false;
        }

        Integer year = Integer.valueOf(split[1]);

        Card card = new Card(
                cardNumber,
                moth,
                year,
                cvc
        );

        return card.validateCard();
    }

    protected void createOrder(){
        super.createOrder();
        removeTaxMin();
        es.coiasoft.mrjeff.domain.orders.send.Order  order = getOrder();
        progressDialog = ProgressDialogMrJeff.createAndStart(this,R.string.initcard);
        SendOrderWorker worker = new SendOrderWorker(order);
        worker.addObserver(this);
        Thread threadOrder = new Thread(worker);
        threadOrder.start();
    }

    protected void removeTaxMin(){
        if(OrderStorage.getInstance(getApplicationContext()).getProductMinOrder() != null){
            es.coiasoft.mrjeff.domain.orders.send.Line_items itemRemove = null;
            for(es.coiasoft.mrjeff.domain.orders.send.Line_items item: OrderStorage.getInstance(getApplicationContext()).getOrderSend().getLine_items()){
                if(item.getProduct_id().intValue() == OrderStorage.getInstance(getApplicationContext()).getProductMinOrder().intValue()){
                    itemRemove = item;
                    break;
                }
            }
            if(itemRemove != null) {
                OrderStorage.getInstance(getApplicationContext()).getOrderSend().getLine_items().remove(itemRemove);
            }
        }
    }

    private es.coiasoft.mrjeff.domain.orders.send.Order getOrder(){

        es.coiasoft.mrjeff.domain.orders.send.Order result = OrderStorage.getInstance(getApplicationContext()).getOrderSend();
        if(result == null){
            result = new es.coiasoft.mrjeff.domain.orders.send.Order();
        }
        result.setLine_items(new ArrayList<es.coiasoft.mrjeff.domain.orders.send.Line_items>());

        if(OrderProductsStorage.getInstance(getApplicationContext()).getNumTotal().intValue() > 0){
            Map<Integer,Integer> products = OrderProductsStorage.getInstance(getApplicationContext()).getProducts();

            if(products != null && !products.isEmpty()){
                Set<Integer> keys = products.keySet();
                for(Integer key:keys){
                    if(products.get(key) != null && products.get(key).intValue() > 0) {
                        es.coiasoft.mrjeff.domain.orders.send.Line_items item = new es.coiasoft.mrjeff.domain.orders.send.Line_items();
                        item.setProduct_id(key);
                        item.setQuantity(products.get(key));
                        result.getLine_items().add(item);
                    }
                }
            }
        }

        OrderStorage.getInstance(getApplicationContext()).setOrderSend(result);
        OrderStorage.getInstance(getApplicationContext()).saveJson();
        return result;
    }

    private void initListAdapter() {
        Thread threadNotiy = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        listProduct.setVisibility(View.VISIBLE);
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        textTotal.setText(OrderStorage.getInstance(getApplicationContext()).getOrder().getTotal() + "€");
                        Float total = Float.valueOf(OrderStorage.getInstance(getApplicationContext()).getOrder().getTotal());
                        if ((total.compareTo(new Float(0.1f)) <= 0 && total.compareTo(new Float(-0.1f)) >= 0 && !isSuscription) || needPayment) {
                            panelPaid.setVisibility(View.GONE);
                            panelPaidAux.setVisibility(View.VISIBLE);
                        } else {
                            panelPaid.setVisibility(View.VISIBLE);
                            panelPaidAux.setVisibility(View.GONE);
                        }
                        listProduct.setAdapter(new ListPorductResumenAdapter(instance, R.layout.product_order, OrderStorage.getInstance(getApplicationContext()).getOrder().getLine_items(), listProduct,isSuscription) {
                            @Override
                            public void onEntrada(final Object entrada, final HolderProductOrder h) {

                                Log.d("ProductoID",((Line_items) entrada).getProduct_id()+"");
                                h.getText_name().setText(((Line_items) entrada).getName());
                                Float total = Float.valueOf(((Line_items) entrada).getTotal());
                                Float totalTax = Float.valueOf(((Line_items) entrada).getTotal_tax());
                                total = total + totalTax;
                                DecimalFormat format = new DecimalFormat("#0.00");
                                h.getText_price().setText(format.format(total) + "€");
                                h.getProduct_price_sus().setText(format.format(total) + "€");
                                h.getText_count().setText(((Line_items) entrada).getQuantity().toString());
                                h.getText_count().setVisibility(View.VISIBLE);
                                Integer idProduct = ((Line_items) entrada).getProduct_id();
                                Products product = ProductStorage.getInstance(PaidCartActivity.this).getProduct(idProduct);
                                if(product != null && product.getDescription() != null && !product.getDescription().isEmpty() ){
                                    if(product.getDescription().contains("<")){
                                        h.getProduct_desc().setText(Html.fromHtml(product.getDescription()));
                                        h.getProduct_desc().setTextColor(getResources().getColor(R.color.grey80));
                                    } else {
                                        int lines = product.getDescription().split("\n").length;
                                        if (lines > 0) {
                                            h.getProduct_desc().setLines(lines);
                                        }
                                        h.getProduct_desc().setText(product.getDescription());
                                    }
                                }

                                if (total.intValue() < 0) {
                                    h.getText_price().setTextColor(getResources().getColor(R.color.green));
                                    h.getProduct_price_sus().setTextColor(getResources().getColor(R.color.green));
                                    h.getText_name().setTextColor(getResources().getColor(R.color.green));
                                    h.getProduct_trash().setVisibility(View.VISIBLE);
                                    h.getText_count().setVisibility(View.GONE);
                                    initButtonTrash(h.getProduct_trash());
                                }

                                if (OrderStorage.getInstance(getApplicationContext()).getProductMinOrder() != null && ((es.coiasoft.mrjeff.domain.orders.response.Line_items) entrada).getProduct_id().intValue() == OrderStorage.getInstance(getApplicationContext()).getProductMinOrder().intValue()) {
                                    h.getProduct_trash().setVisibility(View.INVISIBLE);
                                    h.getText_count().setVisibility(View.GONE);
                                    h.getText_name().setTextColor(getApplicationContext().getResources().getColor(R.color.redmrjeff));
                                    h.getText_price().setTextColor(getApplicationContext().getResources().getColor(R.color.redmrjeff));
                                    h.getProduct_price_sus().setTextColor(getApplicationContext().getResources().getColor(R.color.redmrjeff));
                                }

                            }
                        });
                    }
                });
            }
        });

        threadNotiy.start();

    }

    private void initButtonTrash(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.deleteCoupon).setMessage(getString(R.string.deleteCoupontext)).setNeutralButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        progressDialog = ProgressDialogMrJeff.createAndStart(PaidCartActivity.this,R.string.deletecopupon);
                        DeleteCouponWorker worker = new DeleteCouponWorker();
                        worker.addObserver(instance);
                        Thread thread = new Thread(worker);
                        thread.start();
                    }

                }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }
        });
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_paid);
    }

    protected int posMenu() {
        return -1;
    }

    private void onBuyStripe() {
        String cardNumber = numberCard.getText().toString().replace(" ","");

        Integer cardExpMonth = Integer.valueOf(datePickerMonth.getText().toString());
        Integer cardExpYear = Integer.valueOf(datePickerYear.getText().toString());
        String cardCVC = numberCVC.getText().toString();

        Card card = new Card(
                cardNumber,
                cardExpMonth,
                cardExpYear,
                cardCVC
        );

        if (!card.validateCard()) {
            validateCardFields();
        } else {
            paid(card);
        }
    }

    private void paid(Card card) {

        progressDialog = ProgressDialogMrJeff.createAndStart(PaidCartActivity.this,R.string.updatepago);
        try {
            stripe.createToken(
                    card,
                    new TokenCallback() {
                        public void onSuccess(final Token token) {

                            ChargeStripe stripe = getChargeStripe(token);
                            if (stripe != null) {
                                 paidStripe(stripe);
                            }
                        }

                        public void onError(Exception error) {
                            showError();
                        }
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void paidStripe(ChargeStripe stripe ){
        StripeWorker worker = null;
        String couponCode = null;
        String discount = null;
        ResultCoupon couponApply = OrderStorage.getInstance(getApplicationContext()).getCoupon();
        Integer totalAmount = Integer.valueOf(stripe.getStripeAmount());
        int haveSuscription = suscription.getSelectedItemPosition() > 0?1:0;
        int typeSuscription = suscription.getSelectedItemPosition();
        if (couponApply != null && haveSuscription == 1) {
            couponCode = OrderStorage.getInstance(getApplicationContext()).getOrder().getId().toString();
            Float discF = couponApply.getDiscount() != null ? couponApply.getDiscount() * -100f : 0f;
            discount = String.valueOf(discF.intValue());
            totalAmount = totalAmount + Integer.valueOf(discF.intValue());
        }
        String email =  CustomerStorage.getInstance(getApplicationContext()).getCustomer() != null ? CustomerStorage.getInstance(getApplicationContext()).getCustomer().getEmail() : null;
        String idSuscription = OrderStorage.getInstance(getApplicationContext()).getOrder().getId().toString();
        worker = new StripeWorker(totalAmount.toString(),stripe.getStripeToken(),email, stripe.getStripeDescription(),couponCode, discount, stripe.getStripeCurrency(),haveSuscription,typeSuscription,idSuscription);
        worker.addObserver(instance);
        Thread thread = new Thread(worker);
        thread.start();
    }

    private void paidStripeNoLavo(ChargeStripe stripe ){
        StripeNoLavoWorker worker = null;
        String couponCode = null;
        String discount = null;
        ResultCoupon couponApply = OrderStorage.getInstance(getApplicationContext()).getCoupon();
        Integer totalAmount = Integer.valueOf(stripe.getStripeAmount());
        int haveSuscription = suscription.getSelectedItemPosition() > 0?1:0;
        int typeSuscription = suscription.getSelectedItemPosition();
        if (couponApply != null && haveSuscription == 1) {
            couponCode = OrderStorage.getInstance(getApplicationContext()).getOrder().getId().toString();
            Float discF = couponApply.getDiscount() != null ? couponApply.getDiscount() * -100f : 0f;
            discount = String.valueOf(discF.intValue());
            totalAmount = totalAmount + Integer.valueOf(discF.intValue());
        }
        String email =  CustomerStorage.getInstance(getApplicationContext()).getCustomer() != null ? CustomerStorage.getInstance(getApplicationContext()).getCustomer().getEmail() : null;
        String idSuscription = OrderStorage.getInstance(getApplicationContext()).getOrder().getId().toString();
        worker = new StripeNoLavoWorker(totalAmount.toString(),stripe.getStripeToken(),email, stripe.getStripeDescription(), stripe.getStripeCurrency());
        worker.addObserver(instance);
        Thread thread = new Thread(worker);
        thread.start();
    }

    public void onBuyPressed(View pressed) {
        checkcard.setImageResource(R.drawable.checkpaid);
        checkPayPal.setImageResource(R.drawable.checkpaidsel);
        new LocalyticsEventActionMrJeff(getApplicationContext(),MrJeffConstant.LOCALYTICS_EVENTS.ACTION.PAYPAL,isSuscription,"PayPal");
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(PaidCartActivity.this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        isStripe=false;
        startActivityForResult(intent, MrJeffConstant.PAYPAL.REQUEST_CODE_PAYMENT);
    }


    private PayPalPayment getThingToBuy(String paymentIntent) {
        Order order = OrderStorage.getInstance(getApplicationContext()).getOrder();
        PayPalPayment payment = new PayPalPayment(new BigDecimal(order.getTotal()), MrJeffConstant.PAYPAL.CURRENCY, "MrJeff",
                paymentIntent);
        payment.custom(MrJeffConstant.PAYPAL.DESCRIPTION + order.getId());
        return payment;
    }

    /*
     * This method shows use of optional payment details and item list.
     */
    private PayPalPayment getStuffToBuy(String paymentIntent) {
        Order order = OrderStorage.getInstance(getApplicationContext()).getOrder();

        PayPalItem[] items = new PayPalItem[order.getLine_items().size()];
        int position = 0;
        for (Line_items item : order.getLine_items()) {
            Float total = Float.valueOf(item.getTotal());
            PayPalItem payPalItem = new PayPalItem(item.getName(), item.getQuantity(), BigDecimal.valueOf(total.doubleValue()).setScale(2, RoundingMode.DOWN), MrJeffConstant.PAYPAL.CURRENCY, item.getId().toString());
            items[position++] = payPalItem;

        }

        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        subtotal = subtotal.setScale(2, RoundingMode.DOWN);
        Float total = Float.valueOf(order.getTotal());
        Float totaltax = Float.valueOf(order.getTotal_tax());
        BigDecimal shipping = BigDecimal.ZERO;
        shipping = shipping.setScale(2, RoundingMode.DOWN);
        BigDecimal tax = new BigDecimal(totaltax.doubleValue());
        tax = tax.setScale(2, RoundingMode.DOWN);
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(BigDecimal.ZERO, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        amount = amount.setScale(2, RoundingMode.DOWN);
        PayPalPayment payment = new PayPalPayment(amount, MrJeffConstant.PAYPAL.CURRENCY, "MrJeff", paymentIntent);
        payment.items(items).paymentDetails(paymentDetails);

        //--- set other optional fields like invoice_number, custom field, and soft_descriptor
        payment.custom(MrJeffConstant.PAYPAL.DESCRIPTION + order.getId());

        addAppProvidedShippingAddress(payment);

        return payment;
    }

    /*
     * Add app-provided shipping address to payment
     */
    private void addAppProvidedShippingAddress(PayPalPayment paypalPayment) {
        Order order = OrderStorage.getInstance(getApplicationContext()).getOrder();
        ShippingAddress shippingAddress =
                new ShippingAddress().recipientName(order.getBilling_address().getAddress_1()).line1(order.getBilling_address().getAddress_1())
                        .city(order.getBilling_address().getCity()).postalCode(order.getBilling_address().getPostcode()).countryCode(order.getBilling_address().getCountry());
        paypalPayment.providedShippingAddress(shippingAddress);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MrJeffConstant.PAYPAL.REQUEST_CODE_PAYMENT) {
            if (resultCode == RESULT_OK) {
                es.coiasoft.mrjeff.domain.orders.send.Order order = new es.coiasoft.mrjeff.domain.orders.send.Order();
                order.setStatus(MrJeffConstant.ORDER.UPDATE_STATE_FINALLY);
                SendOrderWorker worker = new SendOrderWorker(order, Boolean.TRUE);
                Thread thread = new Thread(worker);
                thread.start();
                showMessageCorrect();
            } else if (resultCode == RESULT_CANCELED) {
                showError();
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                showError();
            }
        } else {

        }
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        MrJeffApplication.isPaying=false;
        super.onDestroy();
    }

    private void showError() {
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        if(isSuscription){
                            btncreditCard.setVisibility(View.GONE);
                            btnPayPal.setVisibility(View.GONE);
                            couponpanelLinear.setVisibility(View.GONE);
                            panelCard.setVisibility(View.VISIBLE);
                        }
                        new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.errorBuy).setMessage(R.string.errorBuyText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                panelCard.setVisibility(View.VISIBLE);
                                numberCard.setVisibility(View.VISIBLE);
                            }
                        }).show();
                    }
                });
            }
        });
        threadUpdate.start();
    }

    private void showMessageCorrect() {
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(isSuscription){
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            progressDialog = ProgressDialogMrJeff.createAndStart(PaidCartActivity.this,R.string.suscriptioncreate);
                            CreateSuscriptionWorker worker = new CreateSuscriptionWorker(OrderStorage.getInstance(PaidCartActivity.this).getOrder().getId().toString(),CustomerStorage.getInstance(PaidCartActivity.this).getCustomer().getId().toString(),typeSuscription,coderes,numberDay,hour,OrderStorage.getInstance(PaidCartActivity.this).getOrder().getBilling_address().getAddress_1(),OrderStorage.getInstance(PaidCartActivity.this).getOrder().getBilling_address().getPostcode(),PaidCartActivity.this);
                            worker.addObserver(PaidCartActivity.this);
                            Thread thread = new Thread(worker);
                            thread.start();
                        } else {
                            if(zalando){
                                PutZalandoUserWorker worker = new PutZalandoUserWorker(CustomerStorage.getInstance(PaidCartActivity.this).getCustomer().getId().toString(),PaidCartActivity.this);
                                Thread thread = new Thread(worker);
                                thread.start();
                            }
                            Intent show = new Intent(PaidCartActivity.this, FinallyCartActivity.class);
                            show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
                            startActivity(show);

                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }
                    }
                });
            }
        });
        threadUpdate.start();
    }

    private void showMessageCorrectSuscription() {
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            GetSuscriptionWorker worker = new GetSuscriptionWorker(CustomerStorage.getInstance(PaidCartActivity.this).getCustomer().getId().toString(),PaidCartActivity.this);
                            Thread thread = new Thread(worker);
                            thread.start();

                            Intent show = new Intent(PaidCartActivity.this, FinallyCartActivity.class);
                            show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
                            startActivity(show);

                    }
                });
            }
        });
        threadUpdate.start();
    }


    private ChargeStripe getChargeStripe(Token token) {
        ChargeStripe stripe = null;
        if (OrderStorage.getInstance(getApplicationContext()).getOrder() != null &&
                OrderStorage.getInstance(getApplicationContext()).getOrder().getTotal() != null) {
            try {
                Float total = Float.valueOf(OrderStorage.getInstance(getApplicationContext()).getOrder().getTotal());
                total = total * 100;
                stripe = new ChargeStripe();
                stripe.setStripeAmount(String.valueOf(total.intValue()));
                stripe.setStripeCurrency(MrJeffConstant.STRIPE.CURRENCY);
                stripe.setStripeToken(token.getId());
                stripe.setStripeDescription(MrJeffConstant.STRIPE.DESCRIPTION + OrderStorage.getInstance(getApplicationContext()).getOrder().getId());
            } catch (Exception e) {

            }
        }

        return stripe;
    }

    private void enventLocalytics(String cupon) {
        LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(getApplicationContext());
        event.addOrder();
        event.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.COUPON, cupon);
        event.send(isSuscription?MrJeffConstant.LOCALYTICS_EVENTS.NAME.COUPON_PAID_SUSCRIPTION:MrJeffConstant.LOCALYTICS_EVENTS.NAME.COUPON_PAID);
    }

    private void enventLocalyticsDelete() {
        LocalytisEvenMrJeff event = new LocalytisEvenMrJeff(getApplicationContext());
        event.addOrder();
        event.send(isSuscription ? MrJeffConstant.LOCALYTICS_EVENTS.NAME.DELETE_PAID_SUSCRIPTION : MrJeffConstant.LOCALYTICS_EVENTS.NAME.DELETE_PAID);
    }


    public void update(Observable observable, Object data) {
        Thread threadProgress = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(progressDialog != null && progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                });
            }
        });
        threadProgress.start();
        if (!(observable instanceof CouponWorker) &&
                !(observable instanceof DeleteCouponWorker)&&
                !(observable instanceof StripeNoLavoWorker) &&
                !(observable instanceof CreateSuscriptionWorker)) {
            if (data instanceof Boolean && (Boolean) data) {
//                es.coiasoft.mrjeff.domain.orders.send.Order order = new es.coiasoft.mrjeff.domain.orders.send.Order();
//                order.setStatus(MrJeffConstant.ORDER.UPDATE_STATE_FINALLY);
//
//                SendOrderWorker worker = new SendOrderWorker(order, Boolean.TRUE);
//                Thread thread = new Thread(worker);
//                thread.start();
                updateOrderFinal();

            } else {
                showError();
            }
        } else if (observable instanceof CreateSuscriptionWorker) {
            showMessageCorrectSuscription();
        }else if (observable instanceof CouponWorker) {
            Thread threadUpdate = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            couponText.setText("");
                            if (progressDialog != null) {
                                progressDialog.dismiss();
                            }
                        }
                    });
                }
            });
            threadUpdate.start();

            if (data instanceof ResultCoupon) {
                switch (((ResultCoupon) data).getCodeResponse()) {
                    case CouponWorker.RESULT_INIT:
                        break;
                    case CouponWorker.RESULT_ERROR_COUPON_NOT_FOUND:
                        showErrorCouponNotFound(R.string.couponnotfound);
                        break;
                    case CouponWorker.RESULT_ERROR_APPLY_DISCOUNT:
                        showErrorCouponNotFound(R.string.couponnotvalid);
                        break;
                    case CouponWorker.RESULT_ERROR_RECOVERY_ORDER:
                        showErrorCouponNotFound(R.string.couponnotvalid);
                        break;
                    case CouponWorker.RESULT_ERROR_EXPIRED:
                        showErrorCouponNotFound(R.string.couponexpired);
                        break;
                    case CouponWorker.RESULT_ERROR_AMOUNT:
                        showErrorCouponNotFoundVariable(R.string.couponamount, ((ResultCoupon) data).getAmount());
                        break;
                    case CouponWorker.RESULT_ERROR_USAGE_LIMIT:
                        showErrorCouponNotFound(R.string.couponusagelimit);
                        break;
                    case CouponWorker.RESULT_ERROR_USAGE_LIMIT_USER:
                        showErrorCouponNotFound(R.string.couponusagelimituser);
                        break;
                    case CouponWorker.RESULT_ERROR_USER_NOT_VALID:
                        showErrorCouponNotFound(R.string.couponusernotvalid);
                        break;
                    case CouponWorker.RESULT_ERROR_PRODUCT_NOT_VALID:
                        showErrorCouponNotFound(R.string.couponproductnotvalid);
                        break;
                    case CouponWorker.RESULT_ERROR_REFERRAL_NOT_VALID:
                        showErrorCouponNotFound(R.string.couponreferraltnotvalid);
                        break;
                    case CouponWorker.RESULT_SUCCESS:
                        OrderStorage.getInstance(instance.getApplicationContext()).setCoupon(((ResultCoupon) data));
                        initListAdapter();
                        applyCopupon();
                        break;
                    default:
                        break;
                }
            }
        } else if (observable instanceof DeleteCouponWorker) {
            if (data instanceof Integer) {
                switch ((Integer) data) {
                    case DeleteCouponWorker.RESULT_ERROR:
                        showErrorCouponNotFound(R.string.couponnotvalid);
                        break;
                    case DeleteCouponWorker.RESULT_SUCCESS:
                        OrderStorage.getInstance(instance.getApplicationContext()).setCoupon(null);
                        initListAdapter();
                        desApplyCopupon();
                        break;
                    default:

                        break;
                }
            }
        } else if (observable instanceof StripeNoLavoWorker) {
            if (data instanceof Boolean && (Boolean) data) {
                SendOrderNoLavoWorker worker = new SendOrderNoLavoWorker(OrderStorage.getInstance(PaidCartActivity.this).getOrderSend());
                Thread thread = new Thread(worker);
                thread.start();
                showMessageCorrect();
            } else {
                showError();
            }
        }

    }

    private void updateOrderFinal() {
        ApiServiceGenerator.createService(JeffApi.class).sendUpdate(
                MrJeffConstant.URL.UPDATE_ORDER_FINAL, OrderStorage.getInstance(null).getOrder().getId() + "",
                isStripe ? "Card" : "PAYPAL")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<UpdateResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Thread threadUpdate = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                       new MaterialDialog.Builder(PaidCartActivity.this)
                                               .content(R.string.order_state_fail)
                                               .positiveText(R.string.retry)
                                               .negativeText(R.string.cancel)
                                               .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                   @Override
                                                   public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                       updateOrderFinal();
                                                       dialog.dismiss();
                                                   }
                                               })
                                               .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                   @Override
                                                   public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                       dialog.dismiss();
                                                       progressDialog.dismiss();
                                                   }
                                               }).show();

                                    }
                                });
                            }
                        });
                        threadUpdate.start();
                    }

                    @Override
                    public void onNext(ArrayList<UpdateResponse> updateResponse) {
                        if(updateResponse.get(0).getCodigo().toLowerCase().trim().equals("ok")){
                            showMessageCorrect();
                        }else{
                            Thread threadUpdate = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            new MaterialDialog.Builder(PaidCartActivity.this)
                                                    .content(R.string.order_state_fail)
                                                    .positiveText(R.string.retry)
                                                    .negativeText(R.string.cancel)
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            updateOrderFinal();
                                                            dialog.dismiss();
                                                        }
                                                    })
                                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            dialog.dismiss();
                                                            progressDialog.dismiss();
                                                        }
                                                    }).show();

                                        }
                                    });
                                }
                            });
                            threadUpdate.start();
                        }
                    }
                });
    }

    private void applyCopupon() {
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        enventLocalytics(couponText.getEditableText().toString());
                        btnCoupon.setVisibility(View.GONE);
                        couponpanelLinear.setVisibility(View.GONE);
                    }
                });
            }
        });
        threadUpdate.start();
    }

    private void desApplyCopupon() {
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        btnCoupon.setVisibility(View.VISIBLE);
                        enventLocalyticsDelete();
                    }
                });
            }
        });
        threadUpdate.start();
    }

    private void showErrorCouponNotFound(final int idMessage) {
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.errorCoupon).setMessage(idMessage).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }).show();
                    }
                });
            }
        });
        progressDialog.dismiss();
        threadUpdate.start();
    }

    private void showErrorCouponNotFoundVariable(final int idMessage, Object value) {
        final String message = getResources().getText(idMessage).toString().replace("$$", value.toString());
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setTitle(R.string.errorCoupon).setMessage(message).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }).show();
                    }
                });
            }
        });
        threadUpdate.start();
    }


    @Override
    public void onBackPressed() {
        if (panelDate.getVisibility() == View.VISIBLE) {
            panelDate.setVisibility(View.GONE);
            panelCVC.setVisibility(View.VISIBLE);
            numberCVC.setFocusable(true);
            numberCVC.requestFocus(numberCVC.getText().length());
        } else if (panelCVC.getVisibility() == View.VISIBLE) {
            panelCVC.setVisibility(View.GONE);
            panelCard.setVisibility(View.VISIBLE);
            numberCard.setFocusable(true);
            numberCard.requestFocus(numberCard.getText().length());
        } else {
            if(isSuscription) {
                if(typeSuscription != null && typeSuscription.equalsIgnoreCase(MrJeffConstant.INTENT_ATTR.TYPE_SUS_RES)){
                    Intent show = new Intent(this, StudienInfoActivity.class);
                    show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
                    show.putExtra(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION,typeSuscription);
                    show.putExtra(MrJeffConstant.INTENT_ATTR.RESIDENCIA, nameresidencia);
                    show.putExtra(MrJeffConstant.INTENT_ATTR.CODERESIDENCIA,coderes);
                    show.putExtra(MrJeffConstant.INTENT_ATTR.DESCRIPTION,description);
                    show.putExtra(MrJeffConstant.INTENT_ATTR.NEEDPAYMENT,needPayment);
                    startActivity(show);
                } else {
                    Intent show = new Intent(this, OrderAddressActivity.class);
                    show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
                    show.putExtra(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION,typeSuscription);
                    show.putExtra(MrJeffConstant.INTENT_ATTR.ZALANDO,zalando);
                    show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 3);
                    startActivity(show);
                }
            } else {
                Intent show = new Intent(this, OrderAddressActivity.class);
                show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
                show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 2);
                show.putExtra(MrJeffConstant.INTENT_ATTR.ZALANDO,zalando);
                startActivity(show);
            }
        }
    }


    private void eventAppsFlys() {
        StringBuilder builderName = new StringBuilder();
        builderName.append(OrderStorage.getInstance(getApplicationContext()).getOrder().getBilling_address().getFirst_name());
        builderName.append(" ");
        builderName.append(OrderStorage.getInstance(getApplicationContext()).getOrder().getBilling_address().getLast_name());

        StringBuilder builderAddress = new StringBuilder();
        builderAddress.append(OrderStorage.getInstance(getApplicationContext()).getOrder().getBilling_address().getAddress_1());
        builderAddress.append(", ");
        builderAddress.append(OrderStorage.getInstance(getApplicationContext()).getOrder().getBilling_address().getPostcode());
        builderAddress.append(", ");
        builderAddress.append(OrderStorage.getInstance(getApplicationContext()).getOrder().getBilling_address().getCity());

        Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put(AFInAppEventParameterName.PARAM_1, builderName.toString());
        eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, builderAddress.toString());
        AppsFlyerLib.trackEvent(this, AFInAppEventType.PURCHASE, eventValue);
    }

    protected String getNameScreen() {
        return MrJeffConstant.LOCALYTICS.SCREEN_PAID;
    }

    public static class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
        }
    }
}
