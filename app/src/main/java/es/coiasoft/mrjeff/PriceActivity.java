package es.coiasoft.mrjeff;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.ValidateVersion;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.view.adapter.HolderRecyclerPrices;
import es.coiasoft.mrjeff.view.adapter.RecyclerAdapterMrJeff;
import es.coiasoft.mrjeff.view.components.RecyclerViewMrJeff;


public class PriceActivity extends MenuActivity {

    protected Activity instance;
    protected TextView textCart;
    protected ImageView btnCart;
    protected RecyclerViewMrJeff listProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.prices;
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume(){
        super.onResume();
        instance = this;
        textCart = (TextView)findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView)findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);

        listProducts = (RecyclerViewMrJeff) findViewById(R.id.listProducts);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listProducts.setLayoutManager(mLinearLayoutManager);

        initListAdapter();
    }

    @Override
    protected void setContentView(){
        setContentView(R.layout.activity_price);
    }


    private void initListAdapter(){
        listProducts.setAdapter(new RecyclerAdapterMrJeff<HolderRecyclerPrices,Products>(this,ProductStorage.getInstance(this).getAllProduct(),R.layout.product_price) {
            @Override
            public HolderRecyclerPrices getInstanceHolder(View v) {
                return new HolderRecyclerPrices(v);
            }
        });
    }

    protected int posMenu(){
        return 3;
    }

    @Override
    public void onBackPressed() {
        Intent show = new Intent(this, HomeActivity.class);
        startActivity(show);
    }

    protected String getNameScreen(){
        return MrJeffConstant.LOCALYTICS.SCREEN_PRICES;
    }
}
