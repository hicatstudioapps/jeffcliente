package es.coiasoft.mrjeff;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.ValidateVersion;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.orders.response.Billing_address;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.LocationStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.manager.worker.UpdateCustomerWorker;
import es.coiasoft.mrjeff.view.adapter.PlaceArrayAdapter;
import es.coiasoft.mrjeff.view.adapter.domain.PlaceAutoComplete;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.fragments.SupporMapFragmentDragable;
import es.coiasoft.mrjeff.view.listener.FocusChangeEditTextListener;
import es.coiasoft.mrjeff.view.listener.ListenerMap;
import es.coiasoft.mrjeff.view.listener.OrderAdressFragmentListener;
import es.coiasoft.mrjeff.view.listener.ProfileAdressFragmentListener;
import es.coiasoft.mrjeff.view.validator.FormValidatorMrJeff;


public class ProfileActivity extends MenuActivity implements ProfileAdressFragmentListener{

    private static final String FRAGMENT_PROFILE_VIEW="profileview";
    private static final String FRAGMENT_PROFILE_EDIT="profileedit";
    private static final String FRAGMENT_PROFILE_SEARCH_ADDRESS="profilesearchaddress";
    private static FragmentProfileEdit fragment;
    protected static final long maxTime = 86400000;
    protected Activity instance;
    protected TextView textCart;
    protected ImageView btnCart;
    protected ImageView btnEdit;
    protected int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.myaccount;
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume(){
        super.onResume();
        initActivity();
    }

    private void initActivity(){
        instance = this;
        textCart = (TextView)findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView)findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationStorage.getInstance().initLocation(locationManager);
        FragmentProfileView fragment = FragmentProfileView.newInstance(null);
        //fragment.setListener(this);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_PROFILE_VIEW);
        ft.commit();
        fragment.setRetainInstance(true);

    }

    @Override
    protected void setContentView(){
        setContentView(R.layout.activity_profile);
    }

    @Override
    public void onBackPressed() {
        if(position == 2){
            onClickOkAddress();
        } else if(position == 1){
            onClickOkEdit();
        } else {
            Intent show = new Intent(this, HomeActivity.class);
            show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT,2);
            startActivity(show);
        }
    }

    protected int posMenu(){
        return 1;
    }


    protected String getNameScreen(){
        return MrJeffConstant.LOCALYTICS.SCREEN_PROFILE;
    }

    @Override
    public void onClickSearchAddress() {
        if(!isFinishing()) {
            position = 2;
            Bundle params = new Bundle();
            params.putBoolean(FragmentProfileSearchAddress.PARAM_TYPE_ADDRESS, false);
            FragmentProfileSearchAddress fragment = FragmentProfileSearchAddress.newInstance(params);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinleft, R.anim.fadeoutrigth, R.anim.fadeinrigth, R.anim.fadeoutleft);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_PROFILE_SEARCH_ADDRESS);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }

    @Override
    public void onClickOkAddress() {
        if(!isFinishing()) {
            position = 1;
            Bundle params = new Bundle();
            fragment = FragmentProfileEdit.newInstance(params);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinrigth, R.anim.fadeoutleft, R.anim.fadeinleft, R.anim.fadeoutrigth);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_PROFILE_EDIT);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }

    @Override
    public void onClickGoToEditAddress() {
        if(!isFinishing()) {
            position = 1;
            Bundle params = new Bundle();
            fragment = FragmentProfileEdit.newInstance(params);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinleft, R.anim.fadeoutrigth, R.anim.fadeinrigth, R.anim.fadeoutleft);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_PROFILE_EDIT);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }

    @Override
    public void onClickCancelChangeAddress() {
        if(!isFinishing()) {
            position = 1;
            Bundle params = new Bundle();
            fragment = FragmentProfileEdit.newInstance(params);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinrigth, R.anim.fadeoutleft, R.anim.fadeinleft, R.anim.fadeoutrigth);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_PROFILE_EDIT);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }

    @Override
    public void onClickOkEdit() {
        if(!isFinishing()) {
            position = 0;
            Bundle params = new Bundle();
            FragmentProfileView fragment = FragmentProfileView.newInstance(params);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.fadeinrigth, R.anim.fadeoutleft, R.anim.fadeinleft, R.anim.fadeoutrigth);
            ft.replace(R.id.fragmentLayoutAddress, fragment, FRAGMENT_PROFILE_EDIT);
            ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }

    @Override
    public void setTitlePageOne() {
        textheader.setText(getResources().getString(R.string.myaccount));
    }

    @Override
    public void setTitlePageTwo() {
        textheader.setText(getResources().getString(R.string.myaccount));
    }

    @SuppressLint("ValidFragment")
    public static class FragmentProfileEdit extends Fragment implements OnMapReadyCallback, Observer, ListenerMap {
        public static final int ERROR_ADDRESS =-1;
        public static final int ERROR_CP =-2;
        public static FragmentProfileEdit var1;
        private GoogleMap mMap;
        private SupporMapFragmentDragable mapFragment;

        protected View v;
        private MrJeffTextView textBilling;
        private EditText textNumberBilling;
        private EditText textFloorBilling;
        private MrJeffTextView textCityBill;
        private MrJeffTextView textCPBill;
        private EditText name;
        private EditText email;
        private EditText phone;
        private LinearLayout buttonSave;
        private ProgressDialog progressDialog;
        private ImageView pointmap, pointmapbig, geomrjeff;
        private Handler handler = new Handler();

        public static FragmentProfileEdit newInstance(Bundle options) {
            if(var1 == null) {
                var1 = new FragmentProfileEdit();
                var1.setArguments(options);
            }
            return var1;
        }

        public FragmentProfileEdit() {
            super();
        }


        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v = inflater.inflate(R.layout.fragment_edit_profile, container, false);
            initComponents(v);
            initMap();
            initEvent();
            initText();
            return v;
        }

        protected void initMap() {

            final int ZOOM = 100;


            mapFragment = SupporMapFragmentDragable.newInstanceDrawable(null);
            mapFragment.setListenerMap(ProfileActivity.FragmentProfileEdit.this);
            getChildFragmentManager().beginTransaction().add(R.id.contentmap, mapFragment).commit();
            mapFragment.getMapAsync(this);
        }


        @Override
        public void setOnDragStarted() {
            pointmap.setVisibility(View.GONE);
            pointmapbig.setVisibility(View.VISIBLE);
        }

        @Override
        public void setOnDragEnd(GoogleMap gMap) {
            if(gMap==null)
                return;
            pointmap.setVisibility(View.VISIBLE);
            pointmapbig.setVisibility(View.GONE);
            putAddress(gMap.getCameraPosition().target.latitude, gMap.getCameraPosition().target.longitude);
        }

        protected void initComponents(View v) {
            textBilling = (MrJeffTextView) v.findViewById(R.id.textAddressBill);
            textNumberBilling = (EditText) v.findViewById(R.id.textAddressBillNumber);
            textFloorBilling = (EditText) v.findViewById(R.id.textAddressBillDoor);
            textCityBill = (MrJeffTextView) v.findViewById(R.id.textCityBill);
            textCPBill = (MrJeffTextView) v.findViewById(R.id.textCPBill);
            pointmap = (ImageView) v.findViewById(R.id.pointmap);
            pointmapbig = (ImageView) v.findViewById(R.id.pointmapbig);
            geomrjeff = (ImageView) v.findViewById(R.id.geomrjeff);
            buttonSave = (LinearLayout) v.findViewById(R.id.buttonsave);
            name = (EditText) v.findViewById(R.id.name);
            phone =  (EditText) v.findViewById(R.id.phone);
        }

        protected void initText(){
            if(CustomerStorage.getInstance(getActivity()).isLogged()){
                Customer customer = CustomerStorage.getInstance(getActivity()).getCustomer();
                if(customer.getBilling_address() != null){
                    name.setText((customer.getFirst_name() != null?customer.getFirst_name():"") + " " + (customer.getLast_name() != null?customer.getLast_name():"") );
                    phone.setText((customer.getBilling_address().getPhone() != null?customer.getBilling_address().getPhone() :""));
                }
            }
        }

        private void initEvent() {

            textBilling.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ProfileActivity) getActivity()).onClickSearchAddress();
                }
            });

            geomrjeff.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMessageMyLocation();
                }
            });

            buttonSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (inputMethodManager != null) {
                            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        }
                    }catch (Exception e){

                    }
                    save();
                }
            });

        }


        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        @Override
        public void onDetach() {
            super.onDetach();
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMap = googleMap;
            mapFragment.setMap(mMap);
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            LatLng location = null;
            if (LocationStorage.getInstance().getLastLocation() != null) {
                location = new LatLng(LocationStorage.getInstance().getLastLocation().getLatitude(), LocationStorage.getInstance().getLastLocation().getLongitude());
            } else {
                location = new LatLng(40.417160, -3.703561);
            }
            MarkerOptions options = new MarkerOptions();
            options.position(location);
            options.draggable(false);
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.mrjeff_geolocalization));
            options.alpha(0f);
            mMap.addMarker(options);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 16.0f));

            if (CustomerStorage.getInstance(getActivity()).isLogged()
                    && CustomerStorage.getInstance(getActivity()).getCustomer() != null
                    && CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address() != null
                    && CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getAddress_1() != null) {

                if (searchAddress(CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getAddress_1() + ", " + CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPostcode() + ", " + CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getCity(), true, textBilling)) {

                    String[] part = CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getAddress_1().split(",");
                    textBilling.setText(part.length > 0 ? part[0] : OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1());
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.ADDRESSBILING, false, textBilling.getText().toString(),"Valido");
                    textNumberBilling.setText(part.length > 1 ? part[1] : "");
                    if (part.length > 2) {
                        String others = "";
                        for (int i = 2; i < part.length; i++) {
                            others = (i > 2 ? ", " : "") + part[i];
                        }
                        textFloorBilling.setText(others);
                    }
                    textCPBill.setText(CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPostcode());
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CPBILLING, false, "",CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPostcode());
                    textCityBill.setText(CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getCity());
                }

            }

        }



        private void setBillingAddressTextView() {
            es.coiasoft.mrjeff.domain.orders.response.Billing_address result = new es.coiasoft.mrjeff.domain.orders.response.Billing_address();
            boolean has = textBilling.getText() != null && !textBilling.getText().toString().isEmpty();
            boolean hasNumber = textNumberBilling.getText() != null && !textNumberBilling.getText().toString().isEmpty();
            boolean hasOther = textFloorBilling.getText() != null && !textFloorBilling.getText().toString().isEmpty();
            result.setAddress_1(has ? textBilling.getText().toString() + (hasNumber ? ", " + textNumberBilling.getText().toString() + (hasOther ? " , " + textFloorBilling.getText().toString() : "") : "") : "");
            has = OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode() != null && FormValidatorMrJeff.isValidPostalCode(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode());
            result.setPostcode(textCPBill.getText().toString());
            result.setCity(textCityBill.getText().toString());
            result.setEmail(CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
            result.setFirst_name(CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name());
            result.setLast_name(CustomerStorage.getInstance(getActivity()).getCustomer().getLast_name());

            CustomerStorage.getInstance(getActivity()).getCustomer().setBilling_address(result);
            OrderStorage.getInstance(getActivity()).saveJson();
        }


        public boolean searchAddress(String addressName, boolean clear, TextView textView) {
            boolean result = false;
            Geocoder geoCoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> address = geoCoder.getFromLocationName(addressName, 1);

                if (address.size() > 0) {
                    result = true;
                    Double lat = (double) (address.get(0).getLatitude());
                    Double lon = (double) (address.get(0).getLongitude());

                    final LatLng user = new LatLng(lat, lon);
                    mMap.clear();
                    MarkerOptions options = new MarkerOptions();
                    options.position(user);
                    options.draggable(true);
                    options.alpha(0f);
                    options.icon(BitmapDescriptorFactory.fromResource(R.drawable.mrjeff_geolocalization));
                    Marker hamburg = mMap.addMarker(options);
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(user));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);

                }


            } catch (Exception e) {

            }

            return result;
        }

        private void putAddress(Double latitude, Double longitude) {
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> list = geocoder.getFromLocation(latitude, longitude, 1);
                if (!list.isEmpty()) {
                    String addressName = list.get(0).getAddressLine(0);
                    String postalCode = list.get(0).getPostalCode();
                    String city = list.get(0).getLocality();

                    String[] part = addressName.split(",");
                    textBilling.setText(part.length > 0 ? part[0] : OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_1());
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.ADDRESSBILING, false, "","Valido");
                    textNumberBilling.setText(part.length > 1 ? part[1] : "");
                    textCPBill.setText(postalCode);
                    new LocalyticsEventActionMrJeff(getActivity(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CPBILLING, false, "","Valido");
                    textCityBill.setText(city);

                }
            } catch (Exception e) {

            }
        }

        @Override
        public void update(final Observable observable, Object data) {
            Thread threadUpdate = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            if (observable instanceof LocationStorage) {
                                findLocation(true);
                            }

                            if(observable instanceof UpdateCustomerWorker){
                                ((ProfileActivity)getActivity()).onClickOkEdit();
                            }
                        }
                    });
                }
            });

            threadUpdate.start();

        }

        private void showErrorOpenDate(int title, int text) {
            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(title).setMessage(text).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            }).show();
        }

        private void save() {

            Boolean isCorrect = Boolean.TRUE;
            try {
                isCorrect = FormValidatorMrJeff.validAddress(textBilling.getText().toString() + ", " + textNumberBilling.getText().toString() + (textFloorBilling.getText() != null && !textFloorBilling.getText().toString().isEmpty() ? "," + textFloorBilling.getText().toString() : ""));
                if (!isCorrect) {
                    showErrorOpenDate(R.string.date_disabled, R.string.error_address_bill_not_valid);
                    return;
                }

                isCorrect = textNumberBilling.getText() != null && textNumberBilling.getText().length() >0 ;
                if (!isCorrect) {
                    showErrorOpenDate(R.string.date_disabled, R.string.error_address_bill_not_valid);
                    return;
                }

                isCorrect = searchAddressValid(textBilling.getText().toString() + ", " + textNumberBilling.getText().toString() + (textFloorBilling.getText() != null && !textFloorBilling.getText().toString().isEmpty() ? "," + textFloorBilling.getText() .toString(): "") + "," + textCPBill.getText().toString() + "," + textCityBill.getText().toString());

                if (!isCorrect) {
                    return;
                } else {
                    setBillingAddressTextView();
                }
                if(TextUtils.isEmpty(textNumberBilling.getText().toString().trim())){
                    new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.date_disabled).setMessage(R.string.stree_number).setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    })
                            .setPositiveButton(R.string.continue1, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    textNumberBilling.setText("S/N");

                                }
                            }).show();
                    return;
                }
                if (isCorrect) {
                    boolean haveError = !FocusChangeEditTextListener.validRequired(name.getText().toString());
                    if(haveError){
                        new AlertDialog.Builder(new ContextThemeWrapper(name.getContext(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.error_name_bill).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).show();
                        return;
                    }
                    haveError = !FocusChangeEditTextListener.validPhone(phone.getText().toString());
                    if(haveError){
                        new AlertDialog.Builder(new ContextThemeWrapper(name.getContext(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.error_phone_bill).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).show();
                        return;
                    }
                    if(!haveError){
                        upadteData();
                    }
                }

            }catch (Exception e){
                Log.e("ADDRESS", "Error al validar dirección", e);
                showErrorOpenDate(R.string.date_disabled, R.string.error_address_bill_not_valid);
            }

        }


        protected void upadteData(){

            progressDialog = ProgressDialogMrJeff.createAndStart(getActivity(), R.string.updatedata);

            CustomerStorage.getInstance(getActivity()).getCustomer().setFirst_name(name.getText().toString());
            CustomerStorage.getInstance(getActivity()).getCustomer().setLast_name("");
            if(CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address() != null){
                CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().setFirst_name(name.getText().toString());
                CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().setLast_name("");
                CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().setPhone(phone.getText().toString());
                CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().setEmail(CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());

                es.coiasoft.mrjeff.domain.orders.response.Shipping_address saddress = new es.coiasoft.mrjeff.domain.orders.response.Shipping_address();
                saddress.setAddress_1(CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getAddress_1());
                saddress.setAddress_2("");
                saddress.setCity(CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getCity());
                saddress.setCountry("España");
                saddress.setFirst_name(CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getFirst_name());
                saddress.setLast_name("");
                saddress.setPostcode(CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPostcode());
                saddress.setState(CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getCity());

                CustomerStorage.getInstance(getActivity()).getCustomer().setShipping_address(saddress);
            }



            UpdateCustomerWorker worker = new UpdateCustomerWorker(CustomerStorage.getInstance(getActivity()).getCustomer());
            worker.addObserver(this);
            Thread threadOrder = new Thread(worker);
            threadOrder.start();
        }

        public boolean searchAddressValid(String addressName) {
            boolean result = false;
            Geocoder geoCoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> address = geoCoder.getFromLocationName(addressName, 1);

                if (address.size() > 0) {
                    Double lat = (double) (address.get(0).getLatitude());
                    Double lon = (double) (address.get(0).getLongitude());

                    result = putAddressValid(lat,lon);

                }


            } catch (Exception e) {

            }

            return result;
        }


        private boolean putAddressValid(Double latitude, Double longitude) {
            boolean resultBoolean = false;
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> list = geocoder.getFromLocation(latitude, longitude, 1);
                if (!list.isEmpty()) {
                    String addressName = list.get(0).getAddressLine(0);
                    String postalCode = list.get(0).getPostalCode();
                    String city = list.get(0).getLocality();

                    String address = "";

                    String[] parts = addressName.split(",");
                    for (String part : parts) {
                        address += !part.contains(postalCode) && !part.contains(city) ? (address.length() > 0 ? ", " : "") + part : "";
                    }

                    int result = isValid(addressName, postalCode, city);
                    if (result > 0) {
                        resultBoolean = true;
                    } else if (result == ERROR_CP) {
                        new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.addressNotValid).setMessage(R.string.addressNotValidText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }).show();
                    } else {
                        new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        }).show();
                    }

                }
            } catch (Exception e) {

            }

            return resultBoolean;
        }

        private int isValid(String address, String cp, String city){
            if(!FormValidatorMrJeff.validAddress(address)){
                return ERROR_ADDRESS;
            }else if(!FormValidatorMrJeff.isValidPostalCode(cp)){
                return ERROR_CP;
            }

            return 1;
        }


        protected void questionEnableGPS() {
            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.location_gps_disabled).setMessage(R.string.location_gps_disabled_text).setNeutralButton(R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(gpsOptionsIntent);
                }
            }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            }).show();


        }


        protected void findLocation(boolean showMessage) {
            if (LocationStorage.getInstance().getInit()) {
                if (LocationStorage.getInstance().validStorage()) {
                    getAddress();
                } else if (LocationStorage.getInstance().getLocationActive()) {
                    LocationStorage.getInstance().addObserver(this);
                    progressDialog = ProgressDialogMrJeff.createAndStart(getActivity(),R.string.searchlocation);
                } else {
                    if (showMessage) {
                        questionEnableGPS();
                    }
                }

            } else {
                if (LocationStorage.getInstance().getLocationActive()) {
                    try {
                        if (LocationStorage.getInstance().validStorage()) {
                            getAddress();
                        } else if (LocationStorage.getInstance().getLocationActive()) {
                            LocationStorage.getInstance().addObserver(this);
                            progressDialog = new ProgressDialog(getActivity());
                            progressDialog.setMessage(getActivity().getString(R.string.location_search));
                            progressDialog.setCanceledOnTouchOutside(false);
                            progressDialog.show();
                        }
                    } catch (Exception e) {
                        Log.e("LOCATION", "Error al reactivar GPS");
                    }
                } else {
                    if (showMessage) {
                        questionEnableGPS();
                    }
                }
            }
        }

        protected void getAddress() {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            LocationStorage.getInstance().deleteObserver(this);
            if (LocationStorage.getInstance().getLastLocation() != null &&
                    LocationStorage.getInstance().validStorage() &&
                    LocationStorage.getInstance().getLastLocation().getLatitude() != 0.0 && LocationStorage.getInstance().getLastLocation().getLongitude() != 0.0) {

                Double lat = LocationStorage.getInstance().getLastLocation().getLatitude();
                Double lon = LocationStorage.getInstance().getLastLocation().getLongitude();

                final LatLng user = new LatLng(lat, lon);
                mMap.clear();
                MarkerOptions options = new MarkerOptions();
                options.position(user);
                options.draggable(true);
                options.alpha(0f);
                options.icon(BitmapDescriptorFactory.fromResource(R.drawable.mrjeff_geolocalization));
                Marker hamburg = mMap.addMarker(options);
                mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(user));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);
                putAddress(LocationStorage.getInstance().getLastLocation().getLatitude(), LocationStorage.getInstance().getLastLocation().getLongitude());
            }
        }

        public void showMessageMyLocation() {
            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setMessage(R.string.address_question).setNeutralButton(R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    findLocation(true);
                }
            }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            }).show();
        }




    }

    public static class FragmentProfileSearchAddress extends Fragment implements GoogleApiClient.OnConnectionFailedListener,  GoogleApiClient.ConnectionCallbacks  {

        public static final String PARAM_TYPE_ADDRESS ="type";
        public static final int ERROR_ADDRESS =-1;
        public static final int ERROR_CP =-2;

        protected View v;
        private EditText autocompleteSearchGoogle;
        private GoogleApiClient googleApiClient;
        private PlaceArrayAdapter placeArrayAdapter;
        private LatLng location = null;
        private ProfileAdressFragmentListener listener;
        private Handler handler = new Handler();
        ListView listView;

        public static FragmentProfileSearchAddress newInstance(Bundle options) {
            FragmentProfileSearchAddress var1 = new FragmentProfileSearchAddress();
            var1.setArguments(options);
            return var1;
        }

        public FragmentProfileSearchAddress() {
            super();

        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            if(this.listener instanceof OrderAdressFragmentListener) {
                this.listener = (ProfileAdressFragmentListener)activity;
            }
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            try {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
            } catch (Exception e) {

            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v =  inflater.inflate(R.layout.fragment_order_serch_address, container, false);
            initComponents(v);
            initGoogleApi();
            initEvent();
            setAutoCompletefFocus();
            return v;
        }

        protected void initGoogleApi(){

            if(googleApiClient == null || (!googleApiClient.isConnected() && !googleApiClient.isConnecting())) {
                googleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(Places.GEO_DATA_API).addOnConnectionFailedListener(this).addConnectionCallbacks(this).build();
                googleApiClient.connect();
            }
//            autocompleteSearchGoogle.setThreshold(3);
            if(location == null){
                location = new LatLng(40.417160,-3.703561);
            }
            placeArrayAdapter = new PlaceArrayAdapter(getActivity(), R.layout.item_text,new LatLngBounds(location,location), new AutocompleteFilter.Builder()
            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).build());
            listView.setTextFilterEnabled(true);
            listView.setAdapter(placeArrayAdapter);
        }

        protected void initComponents(View v){
            autocompleteSearchGoogle = (EditText) v.findViewById(R.id.autocompleteSearchGoogle);
            listView= (ListView)v.findViewById(R.id.list);
            autocompleteSearchGoogle.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    placeArrayAdapter.getFilter().filter(s.toString());
                }
            });
        }



        private void initEvent(){
            listView.setOnItemClickListener(getAutompleteClickListener());
            autocompleteSearchGoogle.setOnEditorActionListener(getClickListenerAutocomplete());

        }

        private void setAutoCompletefFocus(){
            autocompleteSearchGoogle.requestFocus();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(autocompleteSearchGoogle, InputMethodManager.SHOW_FORCED);
        }


        private  AdapterView.OnItemClickListener getAutompleteClickListener(){
            return new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    final PlaceAutoComplete item = placeArrayAdapter.getItem(position);
                    searchAddress(item.getDescription().toString());
                }
            };
        }



        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            if(listener != null){
                listener.setTitlePageOne();
            }
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
            if(listener != null){
                listener.setTitlePageOne();
            }
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        private TextView.OnEditorActionListener getClickListenerAutocomplete(){
            return new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEND
                            || actionId == EditorInfo.IME_ACTION_GO
                            || actionId == EditorInfo.IME_ACTION_DONE
                            ) {
                        try {
                            if (placeArrayAdapter != null && !placeArrayAdapter.isEmpty() && placeArrayAdapter.getCount() > 0) {
                                final PlaceAutoComplete item = placeArrayAdapter.getItem(0);
                                searchAddress(item.getDescription().toString());
                            }
                        }catch (Exception e){

                        }
                    }
                    return false;
                }
            };
        }


        public void searchAddress(String addressNameAux) {
            searchAddress(addressNameAux,false);
        }
        public void searchAddress(String addressNameAux,boolean second) {
            Geocoder geoCoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> list = geoCoder.getFromLocationName(addressNameAux, 5,0d,0d,0d,0d);

                if (!list.isEmpty()) {
                    String addressName = list.get(0).getAddressLine(0);
                    String postalCode = list.get(0).getPostalCode();
                    String city = list.get(0).getLocality();

                    String address = "";

                    String [] parts = addressName.split(",");
                    if((city == null || postalCode == null || parts == null || parts.length <=1) && !second){
                        String addressAux = (parts.length <=1? addressName + ",1":addressName) +(postalCode != null ? ","+postalCode:"") +(city != null ? ","+city:"");
                        searchAddress(addressAux,true);
                    } else {
                        for (String part : parts) {
                            address += !part.contains(postalCode) && !part.contains(city) ? (address.length() > 0 ? ", " : "") + part : "";
                        }

                        int result = isValid(addressName, postalCode, city);
                        if (result > 0) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(autocompleteSearchGoogle.getWindowToken(), 0);
                            if (listener != null) {
                                listener.onClickOkAddress();
                            } else if (getActivity() instanceof OrderAdressFragmentListener || getActivity() instanceof  ProfileActivity) {
                                ((ProfileActivity) getActivity()).onClickOkAddress();
                            }
                        } else if (result == ERROR_CP) {
                            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.addressNotValid).setMessage(R.string.addressNotValidText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            }).show();
                        } else {
                            new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            }).show();
                        }
                    }

                } else {
                    new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).show();
                }


            } catch (Exception e) {
                new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
            }
        }


        @Override
        public void onStart() {
            if(googleApiClient != null && !googleApiClient.isConnected() && !googleApiClient.isConnecting()){
                Log.d("GOOGLE_API","Entra en conectar");
                googleApiClient.connect();
            }
            super.onStart();
        }

        @Override
        public void onDetach() {
            super.onDetach();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(autocompleteSearchGoogle.getWindowToken(), 0);
        }

        @Override
        public void onPause(){
            if(googleApiClient != null && (googleApiClient.isConnected() || googleApiClient.isConnecting())){
                googleApiClient.disconnect();
            }
            super.onPause();
        }

        @Override
        public void onResume(){
            if(googleApiClient != null && !googleApiClient.isConnected() && !googleApiClient.isConnecting()){
                googleApiClient.connect();
            }
            super.onResume();
        }
        @Override
        public void onStop(){
            if(googleApiClient != null && (googleApiClient.isConnected() || googleApiClient.isConnecting())){
                googleApiClient.disconnect();
            }
            super.onStop();
        }

        @Override
        public void onConnected(Bundle bundle) {
            placeArrayAdapter.setGoogleApiClient(googleApiClient);
        }

        @Override
        public void onConnectionSuspended(int i) {
            Log.d("GOOGLE_API", "Parada la conexión");
        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            Log.e("GOOGLE_API",connectionResult.getErrorMessage());
            placeArrayAdapter.setGoogleApiClient(null);

        }

        public ProfileAdressFragmentListener getListener() {
            return listener;
        }

        public void setListener(ProfileAdressFragmentListener listener) {
            this.listener = listener;
        }


        private int isValid(String address, String cp, String city){
            if(!FormValidatorMrJeff.validAddress(address)){
                return ERROR_ADDRESS;
            }else if(!FormValidatorMrJeff.isValidPostalCode(cp)){
                return ERROR_CP;
            } else{
                setBillingAddress(address,cp,city);
            }

            return 1;
        }

        private void setBillingAddress(String address, String cp, String city){
            Billing_address result = new Billing_address();
            result.setAddress_1(address);
            result.setAddress_2(OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2() : null);
            result.setPostcode(cp);
            result.setCity(city);
            result.setEmail(CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
            result.setFirst_name(CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name());
            result.setLast_name(CustomerStorage.getInstance(getActivity()).getCustomer().getLast_name());
            result.setPhone(OrderStorage.getInstance(getActivity()).getOrderSend() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPhone() : null);

            CustomerStorage.getInstance(getActivity()).getCustomer().setBilling_address(result);
            OrderStorage.getInstance(getActivity()).saveJson();
        }


    }

    public static class FragmentProfileView extends Fragment {

        protected View v;
        private Handler handler = new Handler();
        private LinearLayout buttonedit;
        private LinearLayout btnclose;
        protected Activity instance;
        protected TextView textName;
        protected TextView textEmail;
        protected TextView textAddress;
        protected TextView textPostalCode;
        protected TextView textNumber;
        protected TextView textFloor;
        protected TextView textPhone;

        public static FragmentProfileView newInstance(Bundle options) {
            FragmentProfileView var1 = new FragmentProfileView();
            var1.setArguments(options);
            return var1;
        }

        public FragmentProfileView() {
            super();
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            v =  inflater.inflate(R.layout.fragment_view_profile, container, false);
            initComponents(v);
            initEvent();
            initData();
            return v;
        }


        protected void initComponents(View v){
            textName = (TextView)v.findViewById(R.id.profileName);
            textEmail = (TextView)v.findViewById(R.id.profileEmail);
            textAddress = (TextView)v.findViewById(R.id.profileAddress);

            textPostalCode = (TextView)v.findViewById(R.id.profilepostalcode);
            textNumber = (TextView)v.findViewById(R.id.profileNumber);
            textFloor = (TextView)v.findViewById(R.id.profileFloor);

            textPhone = (TextView)v.findViewById(R.id.profilePhone);
            buttonedit = (LinearLayout) v.findViewById(R.id.buttonedit);
            btnclose = (LinearLayout) v.findViewById(R.id.btnclose);


        }



        private void initEvent(){
            buttonedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ProfileActivity)getActivity()).onClickGoToEditAddress();
                }
            });

            btnclose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CustomerStorage.getInstance(null).loginOut();
                    Intent show = new Intent(v.getContext(), LoginNewAccountActivity.class);
                    show.putExtra(MrJeffConstant.LOGIN_EXTRA.LOGIN_ATTR, MrJeffConstant.LOGIN_EXTRA.HOME_LOGIN);
                    startActivityForResult(show, 0);
                }
            });
        }

        private void initData(){

            if(CustomerStorage.getInstance(getActivity()).isLogged()){
                Customer customer = CustomerStorage.getInstance(getActivity()).getCustomer();
                StringBuilder builderName = new StringBuilder("");
                builderName.append(customer.getFirst_name() != null ? customer.getFirst_name() + " ": "");
                builderName.append(customer.getLast_name() != null ? customer.getLast_name() + " " : "");
                textName.setText(builderName.toString());

                StringBuilder builderEmail = new StringBuilder("");
                builderEmail.append(customer.getEmail() != null ? customer.getEmail() + " ": "");
                textEmail.setText(builderEmail.toString());

                if(customer.getBilling_address() != null){
                    StringBuilder builderAddress = new StringBuilder("");
                    if(customer.getBilling_address().getAddress_1() != null && customer.getBilling_address().getAddress_1().contains(",")) {
                        String [] split = customer.getBilling_address().getAddress_1().split(",");
                        builderAddress.append(split[0] + ", ");
                        if(split.length >=2){
                            textNumber.setText(split[1]);
                        }

                        if(split.length >=3){
                            textFloor.setText(split[2]);
                        }

                    } else {
                        builderAddress.append(customer.getBilling_address().getAddress_1() != null ?customer.getBilling_address().getAddress_1() + ", ": "");
                    }
                    builderAddress.append(customer.getBilling_address().getCity() != null ?customer.getBilling_address().getCity(): "");
                    textAddress.setText(builderAddress.toString());

                    textPostalCode.setText(customer.getBilling_address().getPostcode() != null ?customer.getBilling_address().getPostcode(): "");

                    StringBuilder builderPhone = new StringBuilder("");
                    builderPhone.append(customer.getBilling_address().getPhone() != null ?customer.getBilling_address().getPhone() + " ": "");
                    textPhone.setText(builderPhone.toString());
                }


            }

        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
        }

        @Override
        public void onViewStateRestored(Bundle savedInstanceState) {
            super.onViewStateRestored(savedInstanceState);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }


    }

}
