package es.coiasoft.mrjeff;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.Utils.event.LocalytisEvenMrJeff;
import es.coiasoft.mrjeff.domain.InfoResidencia;
import es.coiasoft.mrjeff.domain.orders.send.Billing_address;
import es.coiasoft.mrjeff.domain.orders.send.Shipping_address;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.DateTimeTableStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.manager.worker.SearchInfoResidenciaWorker;
import es.coiasoft.mrjeff.manager.worker.SearchTimeTableWorker;
import es.coiasoft.mrjeff.manager.worker.SendOrderWorker;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;

/**
 * Created by linovm on 3/11/15.
 */
public class ResidenciaActivity  extends MenuActivity  implements Animation.AnimationListener, Observer {

    protected Activity instance;
    protected Handler handler = new Handler();

    protected TextView textCart;
    protected ImageView btnCart;
    private boolean isSuscription = false;
    private String typeSuscription = null;
    private ProgressDialogMrJeff progressDialog;
    private LinearLayout buttonknow;
    private MrJeffTextView send;
    private EditText codetext;
    protected LinearLayout btnCall;
    protected ImageView imagePhone;
    protected RelativeLayout panelinfo;
    private Animation bottomUp;
    private Animation bottomDown;
    private boolean show = false;
    private InfoResidencia info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.titleres;
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume(){
        super.onResume();
        instance = this;
        try {
            isSuscription = getIntent().getExtras().getBoolean(MrJeffConstant.INTENT_ATTR.SUSCRIPTION);
        }catch (Exception e){
            isSuscription = false;
        }

        if(isSuscription) {
            try {
                typeSuscription = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION);
            }catch (Exception e){
                typeSuscription = null;
            }
        }

        textCart = (TextView)findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView)findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);

        codetext = (EditText) findViewById(R.id.codetext);
        buttonknow = (LinearLayout) findViewById(R.id.buttonknow);
        send = (MrJeffTextView) findViewById(R.id.send);
        btnCall = (LinearLayout)findViewById(R.id.buttonCall);
        imagePhone= (ImageView) findViewById(R.id.imagePhone);
        imagePhone.setColorFilter(getResources().getColor(R.color.redmrjeff), PorterDuff.Mode.MULTIPLY );
        panelinfo = (RelativeLayout) findViewById(R.id.panelinfo);

        bottomUp = AnimationUtils.loadAnimation(this, R.anim.bottom_up);
        bottomUp.setAnimationListener(this);
        bottomDown= AnimationUtils.loadAnimation(this, R.anim.bottom_down);
        bottomDown.setAnimationListener(this);
        setEven();

    }

    protected void setEven(){
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCall();
            }
        });

        panelinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(show){
                    hiddenPanel();
                }
            }
        });

        buttonknow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!show){
                    showPanel();
                }
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCode();
            }
        });

    }

    private void sendCode(){
        String code = codetext.getText().toString();
        if(code != null && code.length() < 3){
            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Theme_AppCompat_Light)).setMessage(R.string.testcodeerrorempty).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            }).show();
        } else {
            progressDialog = ProgressDialogMrJeff.createAndStart(this, R.string.testcode);
            SearchInfoResidenciaWorker worker = new SearchInfoResidenciaWorker(code, CustomerStorage.getInstance(this).getCustomer().getId().toString(),this);
            worker.addObserver(this);
            Thread thread = new Thread(worker);
            thread.start();
        }
    }

    public void btnCall(){
        finishActivity = Boolean.FALSE;
        new LocalyticsEventActionMrJeff(getApplicationContext(),MrJeffConstant.LOCALYTICS_EVENTS.ACTION.SUSCRIPTIONCALL,Boolean.TRUE,"Correcto");
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(MrJeffConstant.CALL.phoneMrJeff));
        startActivity(callIntent);
    }

    private void  showPanel(){
        show = true;
        panelinfo.startAnimation(bottomUp);
        panelinfo.setVisibility(View.VISIBLE);
    }


    private void  hiddenPanel(){
        show = false;
        panelinfo.startAnimation(bottomDown);
        panelinfo.setVisibility(View.GONE);
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_residencia);
    }

    @Override
    protected int posMenu() {
        return -1;
    }

    @Override
    protected String getNameScreen() {
        return MrJeffConstant.LOCALYTICS.SCREEN_RESIDENCIA;
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void update(final Observable observable, final Object data) {
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                        if(observable instanceof SearchInfoResidenciaWorker){
                            if(data == null || !(data instanceof List)){
                                new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setMessage(R.string.testcodeerror).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                }).show();
                            } else {
                                List<InfoResidencia> infos = (List<InfoResidencia>)data;
                                InfoResidencia info =  infos.get(0);
                                if (info.getCodigo() == null || info.getCodigo().trim().equalsIgnoreCase("error")) {
                                    new AlertDialog.Builder(new ContextThemeWrapper(instance, R.style.Theme_AppCompat_Light)).setMessage(R.string.testcodeerror).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                        }
                                    }).show();
                                } else {
                                    setInfo(info);
                                    setAddressAndHour();
                                }

                            }
                        } else if(observable instanceof SearchTimeTableWorker){
                            setHour();
                        } else if(observable instanceof SendOrderWorker){
                            LocalytisEvenMrJeff localytics = new LocalytisEvenMrJeff(instance, Boolean.TRUE, MrJeffConstant.LOCALYTICS_EVENTS.NAME.CODE_RES, Boolean.FALSE);
                            localytics.addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.COUPON,codetext.getText().toString());
                            localytics.send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.CODE_RES);
                            Intent show = new Intent(instance, StudienInfoActivity.class);
                            show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
                            show.putExtra(MrJeffConstant.INTENT_ATTR.RESIDENCIA, info.getResidenceName());
                            show.putExtra(MrJeffConstant.INTENT_ATTR.CODERESIDENCIA,codetext.getText().toString());
                            show.putExtra(MrJeffConstant.INTENT_ATTR.DESCRIPTION,info.getSuscriptionDescription());
                            show.putExtra(MrJeffConstant.INTENT_ATTR.NEEDPAYMENT,info.getNeedPayment().trim().equals("0"));
                            if(isSuscription && typeSuscription != null){
                                show.putExtra(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION, typeSuscription);
                            }
                            instance.startActivity(show);
                        }
                    }
                });
            }
        });

        threadUpdate.start();

    }

    private void setInfo(InfoResidencia info){
        this.info = info;
    }

    private void setAddressAndHour(){
        setAddressBilling();
        setAddressShipping();
        if(isHourCache()){
            setHour();
        }
    }

    private void setHour(){
        try {
            String dateHourPickup = DateTimeTableStorage.getInstance().getFirstDateHourAndDayWeek(info.getPostalCode(),  MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT, Integer.valueOf(info.getPickupDay()));
            String datePickUp = dateHourPickup.split(" ")[0];
            String hourPickUp = dateHourPickup.split(" ")[1];
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(format.parse(datePickUp));
            calendar.setTimeInMillis(calendar.getTimeInMillis() + MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY);
            String dateHourDropoff = DateTimeTableStorage.getInstance().getSecondDateHourAndDayWeek(info.getPostalCode(),  MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND, calendar, Integer.valueOf(info.getDropoffDay()));
            String dateDropOff = dateHourDropoff.split(" ")[0];
            String hourDropOff = dateHourDropoff.split(" ")[1];


            String datePickUpFinal = datePickUp;
            if(info.getPickupHour() == null || info.getPickupHour().trim().isEmpty()){
                datePickUpFinal += " " + hourPickUp.trim().replace(" ","");;
            } else {
                datePickUpFinal += " " + info.getPickupHour().trim().replace(" ","");;
            }

            String dateDropOffFinal = dateDropOff;
            if(info.getDropoffHour() == null || info.getDropoffHour().trim().isEmpty()){
                dateDropOffFinal += " " + hourDropOff.trim().replace(" ","");
            } else {
                dateDropOffFinal += " " + info.getDropoffHour().trim().replace(" ","");;
            }

            OrderStorage.getInstance(this).getOrderSend().getBilling_address().setAddress_2(datePickUpFinal);
            OrderStorage.getInstance(this).getOrderSend().getShipping_address().setAddress_2(dateDropOffFinal);

            progressDialog = ProgressDialogMrJeff.createAndStart(this, R.string.updateaddress);

            es.coiasoft.mrjeff.domain.orders.send.Order order = new es.coiasoft.mrjeff.domain.orders.send.Order();
            order.setBilling_address(OrderStorage.getInstance(this).getOrderSend().getBilling_address());
            order.setShipping_address(OrderStorage.getInstance(this).getOrderSend().getShipping_address());
            order.setNote(OrderStorage.getInstance(this).getOrderSend().getNote());
            OrderStorage.getInstance(this).saveJson();
            SendOrderWorker worker = new SendOrderWorker(order, Boolean.TRUE);
            worker.addObserver(this);
            Thread threadOrder = new Thread(worker);
            threadOrder.start();
            
        }catch (Exception e){

        }
    }

    private boolean isHourCache(){
        boolean result = false;
        if (!DateTimeTableStorage.getInstance().isLoadCp(info.getPostalCode())) {
            progressDialog = ProgressDialogMrJeff.createAndStart(this, R.string.loadtimetable);
            SearchTimeTableWorker worker = new SearchTimeTableWorker(info.getPostalCode());
            worker.addObserver(this);
            Thread thread = new Thread(worker);
            thread.start();
        } else{
            result = true;
        }

        return result;
    }

    private void setAddressBilling(){
        Billing_address result = new Billing_address();
        result.setAddress_1(info.getAddress() + "," + (info.getNumber()!=null?info.getNumber():"1"));
        result.setPostcode(info.getPostalCode());
        result.setCity(info.getCity());
        result.setEmail(CustomerStorage.getInstance(this).getCustomer().getEmail());
        result.setFirst_name(CustomerStorage.getInstance(this).getCustomer().getFirst_name());
        result.setLast_name(CustomerStorage.getInstance(this).getCustomer().getLast_name());
        result.setPhone(OrderStorage.getInstance(this).getOrderSend() != null && OrderStorage.getInstance(this).getOrderSend().getBilling_address() != null ? OrderStorage.getInstance(this).getOrderSend().getBilling_address().getPhone() : CustomerStorage.getInstance(this).getCustomer().getBilling_address() != null ? CustomerStorage.getInstance(this).getCustomer().getBilling_address().getPhone():null);

        OrderStorage.getInstance(this).getOrderSend().setBilling_address(result);
    }

    private void setAddressShipping(){
        Shipping_address shipping_address = new Shipping_address();
        shipping_address.setAddress_1(OrderStorage.getInstance(this).getOrderSend().getBilling_address().getAddress_1());
        shipping_address.setCity(OrderStorage.getInstance(this).getOrderSend().getBilling_address().getCity());
        shipping_address.setPostcode(OrderStorage.getInstance(this).getOrderSend().getBilling_address().getPostcode());
        shipping_address.setFirst_name(OrderStorage.getInstance(this).getOrderSend().getBilling_address().getFirst_name());
        shipping_address.setLast_name(OrderStorage.getInstance(this).getOrderSend().getBilling_address().getLast_name());
        OrderStorage.getInstance(this).getOrderSend().setShipping_address(shipping_address);
    }

    @Override
    public void onBackPressed() {
        Intent show = null;
        show = new Intent(this, HomeActivity.class);
        show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
        show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 2);
        startActivity(show);
    }
}
