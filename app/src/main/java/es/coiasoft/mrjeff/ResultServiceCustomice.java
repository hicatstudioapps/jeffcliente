package es.coiasoft.mrjeff;

/**
 * Created by linovm on 4/12/15.
 */
public class ResultServiceCustomice {

    private String estado;
    private String mensaje;
    private String Cupon;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getCupon() {
        return Cupon;
    }

    public void setCupon(String cupon) {
        Cupon = cupon;
    }
}
