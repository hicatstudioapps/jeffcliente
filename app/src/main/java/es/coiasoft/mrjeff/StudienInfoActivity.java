package es.coiasoft.mrjeff;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.Utils.event.LocalytisEvenMrJeff;
import es.coiasoft.mrjeff.domain.InfoResidencia;
import es.coiasoft.mrjeff.domain.orders.send.Billing_address;
import es.coiasoft.mrjeff.domain.orders.send.Shipping_address;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.DateTimeTableStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.manager.worker.SearchInfoResidenciaWorker;
import es.coiasoft.mrjeff.manager.worker.SearchTimeTableWorker;
import es.coiasoft.mrjeff.manager.worker.SendOrderWorker;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.components.SelectDatePanel;
import es.coiasoft.mrjeff.view.validator.FormValidatorMrJeff;

/**
 * Created by linovm on 3/11/15.
 */
public class StudienInfoActivity extends MenuActivity implements Observer {

    protected Activity instance;
    protected Handler handler = new Handler();

    protected TextView textCart;
    protected ImageView btnCart;
    private boolean isSuscription = false;
    private String typeSuscription = null;
    private String nameresidencia = null;
    private String coderes = null;
    private String description;
    private boolean needPayment = false;
    private ProgressDialogMrJeff progressDialog;
    private EditText name;
    private EditText email;
    private EditText phone;
    private MrJeffTextView hourPickUp;
    private MrJeffTextView dayPickUp;
    private MrJeffTextView hourDelivery;
    private MrJeffTextView dayDelivery;
    private MrJeffTextView descriptionservice;
    private RelativeLayout buttonpay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        titleWindowId = R.string.empty;
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume(){
        super.onResume();
        instance = this;
        try {
            isSuscription = getIntent().getExtras().getBoolean(MrJeffConstant.INTENT_ATTR.SUSCRIPTION);
        }catch (Exception e){
            isSuscription = false;
        }

        if(isSuscription) {
            try {
                typeSuscription = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION);
            }catch (Exception e){
                typeSuscription = null;
            }
            try {
                coderes = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.CODERESIDENCIA);
            }catch (Exception e){
                coderes = null;
            }
            try {
                nameresidencia = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.RESIDENCIA);
            }catch (Exception e){
                nameresidencia = null;
            }

            try {
                description = getIntent().getExtras().getString(MrJeffConstant.INTENT_ATTR.DESCRIPTION);
            }catch (Exception e){
                description = null;
            }

            try {
                needPayment = getIntent().getExtras().getBoolean(MrJeffConstant.INTENT_ATTR.NEEDPAYMENT);
            }catch (Exception e){
                needPayment = false;
            }

        }

        textCart = (TextView)findViewById(R.id.numberCartHeader);
        textCart.setVisibility(View.GONE);
        btnCart = (ImageView)findViewById(R.id.btnCart);
        btnCart.setVisibility(View.GONE);
        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        phone = (EditText) findViewById(R.id.phone);
        hourPickUp = (MrJeffTextView) findViewById(R.id.hourpickup);
        dayPickUp = (MrJeffTextView) findViewById(R.id.datepickup);
        hourDelivery = (MrJeffTextView) findViewById(R.id.hourdelivery);
        dayDelivery = (MrJeffTextView) findViewById(R.id.datedelivery);
        buttonpay = (RelativeLayout) findViewById(R.id.buttonpay);
        descriptionservice = (MrJeffTextView)findViewById(R.id.descriptionservice);
        if(description != null){
            descriptionservice.setText(description);
        }
        setEven();
        putData();

    }

    @Override
    public void onBackPressed() {
        Intent show = null;
        show = new Intent(this, HomeActivity.class);
        show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
        show.putExtra(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION, typeSuscription);
        show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 2);
        startActivity(show);
    }

    protected void setEven(){
        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    new LocalyticsEventActionMrJeff(v.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NAMEBILLING, false, name.getText() != null ? name.getText().toString() : "", FormValidatorMrJeff.validRequiredMin(3, name.getText() != null ? name.getText().toString() : "") ? "Valido" : "No Valido");
                }
            }
        });

        phone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    new LocalyticsEventActionMrJeff(v.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.PHONEBILLING, false, phone.getText() != null ? phone.getText().toString() : "", FormValidatorMrJeff.validPhone(phone.getText() != null ? phone.getText().toString() : "") ? "Valido" : "No Valido");
                }
            }
        });

        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    new LocalyticsEventActionMrJeff(v.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.EMAILBILLING, false, email.getText() != null ? email.getText().toString() : "", FormValidatorMrJeff.isValidEmail(email.getText() != null ? email.getText().toString() : "") ? "Valido" : "No Valido");
                }
            }
        });

        buttonpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

    }

    private void putData() {
        try {
            paintHourDatePickUp(OrderStorage.getInstance(this).getOrderSend().getBilling_address().getAddress_2());
            paintHourDateDelivery(OrderStorage.getInstance(this).getOrderSend().getShipping_address().getAddress_2());
        }catch (Exception e){

        }
        putDataUser();
    }
    public Calendar paintHourDateDelivery(String datePaint) throws ParseException {
        String[] parts = datePaint.split(" ");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse(parts[0]);
        hourDelivery.setText(parts[1]);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        long time = calendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
        if (Calendar.getInstance().get(Calendar.YEAR) == calendar.get(Calendar.YEAR) &&
                Calendar.getInstance().get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {
            dayDelivery.setText(getResources().getString(R.string.today));
        } else if (time <= MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY) {
            dayDelivery.setText(getResources().getString(R.string.tomorrow));
        } else if (time <= (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * 5)) {
            setDay(calendar, dayDelivery);
        } else {
            dayDelivery.setText(parts[0]);
        }

        return calendar;
    }


    public Calendar paintHourDatePickUp(String datePaint) throws ParseException {
        String[] parts = datePaint.split(" ");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse(parts[0]);
        hourPickUp.setText(parts[1]);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        long time = calendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
        if (Calendar.getInstance().get(Calendar.YEAR) == calendar.get(Calendar.YEAR) &&
                Calendar.getInstance().get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {
            dayPickUp.setText(getResources().getString(R.string.today));
        } else if (time >= 0 && time <= MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY) {
            dayPickUp.setText(getResources().getString(R.string.tomorrow));
        } else if (time >= 0 && time <= (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * 5)) {
            setDay(calendar, dayPickUp);
        } else {
            dayPickUp.setText(parts[0]);
        }

        return calendar;
    }

    private void setDay(Calendar calendar, MrJeffTextView textView) {
        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                textView.setText(getResources().getString(R.string.monday));
                break;
            case Calendar.TUESDAY:
                textView.setText(getResources().getString(R.string.tuesday));
                break;
            case Calendar.WEDNESDAY:
                textView.setText(getResources().getString(R.string.wednesday));
                break;
            case Calendar.THURSDAY:
                textView.setText(getResources().getString(R.string.thursday));
                break;
            case Calendar.FRIDAY:
                textView.setText(getResources().getString(R.string.friday));
                break;
            case Calendar.SATURDAY:
                textView.setText(getResources().getString(R.string.saturday));
                break;
            case Calendar.SUNDAY:
                textView.setText(getResources().getString(R.string.sunday));
                break;
        }
    }

    private void putDataUser() {
        try {
            name.setText(FormValidatorMrJeff.validRequiredMin(3, OrderStorage.getInstance(this).getOrderSend().getBilling_address().getFirst_name()) ? OrderStorage.getInstance(this).getOrderSend().getBilling_address().getFirst_name() : (FormValidatorMrJeff.validRequiredMin(3, CustomerStorage.getInstance(this).getCustomer().getBilling_address().getFirst_name()) ? CustomerStorage.getInstance(this).getCustomer().getBilling_address().getFirst_name() : (FormValidatorMrJeff.validRequiredMin(3, CustomerStorage.getInstance(this).getCustomer().getFirst_name()) ? CustomerStorage.getInstance(this).getCustomer().getFirst_name() : "")));
            name.setText(name.getText().toString() + (FormValidatorMrJeff.validRequiredMin(3, CustomerStorage.getInstance(this).getCustomer().getBilling_address().getLast_name()) ? CustomerStorage.getInstance(this).getCustomer().getBilling_address().getLast_name() : ""));
            email.setText(OrderStorage.getInstance(this).getOrderSend().getBilling_address().getEmail() != null ? OrderStorage.getInstance(this).getOrderSend().getBilling_address().getEmail() : CustomerStorage.getInstance(this).getCustomer().getBilling_address().getEmail() != null ? CustomerStorage.getInstance(this).getCustomer().getBilling_address().getEmail() : CustomerStorage.getInstance(this).getCustomer().getEmail());
            phone.setText(OrderStorage.getInstance(this).getOrderSend().getBilling_address().getPhone() != null ? OrderStorage.getInstance(this).getOrderSend().getBilling_address().getPhone() : CustomerStorage.getInstance(this).getCustomer().getBilling_address().getPhone());
        } catch (Exception e) {

        }
    }
    
    private void save() {

        String nameText = name.getText() != null ? name.getText().toString() : "";
        Boolean correct = FormValidatorMrJeff.validRequiredMin(3, nameText);

        if (!correct) {
            new LocalyticsEventActionMrJeff(this, MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "No Valido");
            showErrorOpenDate(R.string.date_disabled, R.string.error_name_bill);
            return;
        } else {
            OrderStorage.getInstance(this).getOrderSend().getBilling_address().setFirst_name(nameText);
        }

        String phonetext = phone.getText() != null ? phone.getText().toString() : "";
        correct = FormValidatorMrJeff.validPhone(phonetext);

        if (!correct) {
            new LocalyticsEventActionMrJeff(this, MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "No Valido");
            showErrorOpenDate(R.string.date_disabled, R.string.error_phone_bill);
            return;
        } else {
            OrderStorage.getInstance(this).getOrderSend().getBilling_address().setPhone(phonetext);
        }

        String emailtext = email.getText() != null ? email.getText().toString() : "";
        correct = FormValidatorMrJeff.isValidEmail(emailtext);

        if (!correct) {
            new LocalyticsEventActionMrJeff(this, MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "No Valido");
            showErrorOpenDate(R.string.date_disabled, R.string.error_email_bill);
            return;
        } else {
            OrderStorage.getInstance(this).getOrderSend().getBilling_address().setEmail(emailtext);
        }


        if (correct) {
            new LocalyticsEventActionMrJeff(this, MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTSSHIPPING, false, "", "Valido");
            es.coiasoft.mrjeff.domain.orders.send.Order order = new es.coiasoft.mrjeff.domain.orders.send.Order();
            order.setBilling_address(OrderStorage.getInstance(this).getOrderSend().getBilling_address());
            order.setShipping_address(OrderStorage.getInstance(this).getOrderSend().getShipping_address());
            order.setNote(OrderStorage.getInstance(this).getOrderSend().getNote());
            OrderStorage.getInstance(this).saveJson();

            progressDialog = ProgressDialogMrJeff.createAndStart(this, R.string.updateaddress);

            SendOrderWorker worker = new SendOrderWorker(order, Boolean.TRUE);
            worker.addObserver(this);
            Thread threadOrder = new Thread(worker);
            threadOrder.start();
        }

    }

    private void showErrorOpenDate(int title, int text) {
        new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Theme_AppCompat_Light)).setTitle(title).setMessage(text).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        }).show();
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_studient);
    }

    @Override
    protected int posMenu() {
        return -1;
    }

    @Override
    protected String getNameScreen() {
        return MrJeffConstant.LOCALYTICS.SCREEN_RESIDENCIA;
    }

    @Override
    public void update(final Observable observable, final Object data) {
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                         if(observable instanceof SendOrderWorker){
                            new LocalytisEvenMrJeff(instance, Boolean.TRUE, MrJeffConstant.LOCALYTICS_EVENTS.NAME.CODE_RES, Boolean.TRUE);
                            Intent show = new Intent(instance, PaidCartActivity.class);
                            show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, isSuscription);
                            if(isSuscription && typeSuscription != null){
                                show.putExtra(MrJeffConstant.INTENT_ATTR.TYPESUSCRIPTION, typeSuscription);
                                show.putExtra(MrJeffConstant.INTENT_ATTR.CODERESIDENCIA,coderes);
                                show.putExtra(MrJeffConstant.INTENT_ATTR.NEEDPAYMENT,needPayment);
                            }
                            instance.startActivity(show);
                        }
                    }
                });
            }
        });

        threadUpdate.start();

    }

}
