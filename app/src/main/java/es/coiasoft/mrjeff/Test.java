package es.coiasoft.mrjeff;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.ParseException;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.domain.Attributes;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.orders.send.Billing_address;
import es.coiasoft.mrjeff.domain.orders.send.Order;
import es.coiasoft.mrjeff.domain.orders.send.SendOrder;
import es.coiasoft.mrjeff.domain.orders.send.Shipping_address;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.DateTimeTableStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.worker.SearchTimeTableWorker;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;
import es.coiasoft.mrjeff.view.components.SelectDatePanel;
import es.coiasoft.mrjeff.view.components.util.SelectPanelListener;
import es.coiasoft.mrjeff.view.listener.OrderAdressFragmentListener;
import es.coiasoft.mrjeff.view.validator.FormValidatorMrJeff;



public class Test extends Fragment implements SelectPanelListener {
    private OrderAdressFragmentListener listener;
    protected View v;
    private EditText name;
    private EditText email;
    private EditText phone;
    private MrJeffTextView hourPickUp;
    private MrJeffTextView dayPickUp;
    private MrJeffTextView hourDelivery;
    private MrJeffTextView dayDelivery;
    private LinearLayout datehourpickup;
    private LinearLayout datehourdelivery;
    private Handler handler = new Handler();
    private Test instance;
    private ProgressDialog progressDialog;
    private RelativeLayout buttonpay;
    private String typeSuscription = null;
    private Boolean isSubscription = false;
    private Boolean zalando = false;
    private SelectDatePanel selectDatePanel;

    public static Test newInstance(Bundle options) {
        Test var1 = new Test();
        OrderStorage orderStorage= OrderStorage.getInstance(null);
        Order order= new Order();
        Billing_address billing_address= new Billing_address();
        billing_address.setPostcode("28004");
        order.setBilling_address(billing_address);
        Shipping_address shipping_address= new Shipping_address();
        shipping_address.setPostcode("28004");
        order.setShipping_address(shipping_address);
        SendOrder sendOrder= new SendOrder();
        sendOrder.setOrder(order);
        orderStorage.setOrderSend(order);
        orderStorage.saveJson();
        var1.setArguments(options);
        return var1;
    }

    public Test() {
        super();
        this.instance = this;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (this.listener instanceof OrderAdressFragmentListener) {
            this.listener = (OrderAdressFragmentListener) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        isSubscription=false;
        zalando=false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        v = inflater.inflate(R.layout.fragment_order_address_hour, container, false);
        initComponents(v);
        initEvent();
        initData();
        return v;
    }


    protected void initComponents(View v) {
        hourPickUp = (MrJeffTextView) v.findViewById(R.id.hourpickup);
        dayPickUp = (MrJeffTextView) v.findViewById(R.id.datepickup);
        hourDelivery = (MrJeffTextView) v.findViewById(R.id.hourdelivery);
        dayDelivery = (MrJeffTextView) v.findViewById(R.id.datedelivery);
        datehourpickup = (LinearLayout) v.findViewById(R.id.datehourpickup);
        datehourdelivery = (LinearLayout) v.findViewById(R.id.datehourdelivery);
        buttonpay = (RelativeLayout) v.findViewById(R.id.buttonpay);
        selectDatePanel = new SelectDatePanel(v, this);
    }

    //click abrir horas de recogida
    private void initEvent() {
        datehourpickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dateHour = OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2();
                String date = null;
                String hour = null;
                if (dateHour != null && dateHour.split(" ").length == 2) {
                    String[] dateSplit = dateHour.split(" ");
                    date = dateSplit[0];
                    hour = dateSplit[1];
                }
                selectDatePanel.showPanelSelectDate(getActivity().getResources().getString(R.string.pickup), date, hour, OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT, null,null);
            }
        });
//click abrir horas de entrega
        datehourdelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dateHour = OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2();
                String date = null;
                String hour = null;
                Calendar calendar = Calendar.getInstance();
                if (dateHour != null && dateHour.split(" ").length == 2) {
                    String[] dateSplit = dateHour.split(" ");
                    date = dateSplit[0];
                    hour = dateSplit[1];
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        calendar.setTime(format.parse(date));
                    }catch (Exception e){

                    }
                }
                selectDatePanel.showPanelSelectDate(getActivity().getResources().getString(R.string.delivery), date, hour, OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND, OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2(),getMinDays(calendar));
            }
        });

    }

    private void initData() {
        loadCP(null);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                putData();
            }
        },5000);
//                   putData();
       }

    //calculo primera hora de recogida a mostrar
    private void putData() {
        Calendar calendar = putHoursPickUp();
        putHoursDelivery(calendar, OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address() != null && OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2() != null);
        putDataUser();
    }

    private void loadCP(MrJeffTextView cp){
        String cpp="28004";
        if(cp==null || TextUtils.isEmpty(cp.getText()))
            cpp="28004";
        else
            cpp=cp.getText().toString();
        if (!DateTimeTableStorage.getInstance().isLoadCp(cpp)) {
            SearchTimeTableWorker worker = new SearchTimeTableWorker(cpp);
            Thread thread = new Thread(worker);
            thread.start();
        }
    }

    private void putDataUser() {
        try {
            name.setText(FormValidatorMrJeff.validRequiredMin(3, OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getFirst_name()) ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getFirst_name() : (FormValidatorMrJeff.validRequiredMin(3, CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getFirst_name()) ? CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getFirst_name() : (FormValidatorMrJeff.validRequiredMin(3, CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name()) ? CustomerStorage.getInstance(getActivity()).getCustomer().getFirst_name() : "")));
            name.setText(name.getText().toString() + (FormValidatorMrJeff.validRequiredMin(3, CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getLast_name()) ? CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getLast_name() : ""));
            email.setText(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getEmail() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getEmail() : CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getEmail() != null ? CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getEmail() : CustomerStorage.getInstance(getActivity()).getCustomer().getEmail());
            phone.setText(OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPhone() != null ? OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getPhone() : CustomerStorage.getInstance(getActivity()).getCustomer().getBilling_address().getPhone());
        } catch (Exception e) {

        }
    }

    public Calendar putHoursPickUp() {
        Calendar calendar = null;
        try {
            String datePickUp  = getDatePickUp();
            if(datePickUp == null) {
                datePickUp = DateTimeTableStorage.getInstance().getFirstDateHour("28004", MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT);
            }
            calendar = paintHourDatePickUp(datePickUp);
        } catch (Exception e) {
            dayPickUp.setText("");
            hourPickUp.setText("");
        }

        return calendar;
    }

    private String getDatePickUp(){
        String result = null;
        try {
            if (OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2() != null &&
                    OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2().length() > 16) {
                result = OrderStorage.getInstance(getActivity()).getOrderSend().getBilling_address().getAddress_2();
            }
        }catch (Exception e){
            Log.e("ERROR ", "Parse date in order edit");
        }

        return result;
    }


    public Calendar paintHourDatePickUp(String datePaint) throws java.text.ParseException {
        String[] parts = datePaint.split(" ");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse(parts[0]);
        hourPickUp.setText(parts[1]);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        long time = calendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
        if (Calendar.getInstance().get(Calendar.YEAR) == calendar.get(Calendar.YEAR) &&
                Calendar.getInstance().get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {
            dayPickUp.setText(getResources().getString(R.string.today));
        } else if (time >= 0 && time <= MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY) {
            dayPickUp.setText(getResources().getString(R.string.tomorrow));
        } else if (time >= 0 && time <= (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * 5)) {
            setDay(calendar, dayPickUp);
        } else {
            dayPickUp.setText(parts[0]);
        }
        OrderStorage o= OrderStorage.getInstance(getActivity());
        o.getOrderSend().getBilling_address().setAddress_2(datePaint);
        o.saveJson();
        return calendar;
    }
    //calculo horas de entrega en dependencia de la de recogida
    public void putHoursDelivery(Calendar calendar, boolean reload) {
        try {
            String datePickUp = getDateDelivery();
            if(datePickUp == null || reload) {
                calendar.setTimeInMillis(calendar.getTimeInMillis() + (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * getMinDays(calendar)));
                datePickUp = DateTimeTableStorage.getInstance().getSecondDateHour(OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND, calendar);
            }
            paintHourDateDelivery(datePickUp);

        } catch (Exception e) {
            dayDelivery.setText("");
            hourDelivery.setText("");
        }
    }

    private Integer getMinDays(Calendar referenceDate) {
        long time = referenceDate.getTimeInMillis();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(calendar.getTime());
        Integer result = DateTimeTableStorage.getInstance().getMinDays(OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getPostcode(),date);
        try {
            if (OrderStorage.getInstance(null).getOrder() != null && OrderStorage.getInstance(null).getOrder().getLine_items() != null) {
                for (es.coiasoft.mrjeff.domain.orders.response.Line_items item : OrderStorage.getInstance(getActivity()).getOrder().getLine_items()) {
                    Integer idProduct = item.getProduct_id();
                    Products product = ProductStorage.getInstance(getActivity()).getProduct(idProduct);
                    if (product.getAttributes() != null) {
                        for (Attributes attr : product.getAttributes()) {
                            Integer days = 0;
                            if (attr.getName().trim().toLowerCase().equals(MrJeffConstant.ORDER_DAYS_SEND.ATTR_DAYS.trim().toLowerCase())) {
                                days = Integer.valueOf(attr.getOptions().get(0));
                                if (result.compareTo(days) < 0) {
                                    result = days;
                                }
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {

        }
        return result;
    }

    private String getDateDelivery(){
        String result = null;
        try {
            if (OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2() != null &&
                    OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2().length() > 16) {
                result = OrderStorage.getInstance(getActivity()).getOrderSend().getShipping_address().getAddress_2();
            }
        }catch (Exception e){
            Log.e("ERROR ", "Parse date in order edit");
        }

        return result;
    }


    public Calendar paintHourDateDelivery(String datePaint) throws java.text.ParseException {
        String[] parts = datePaint.split(" ");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse(parts[0]);
        hourDelivery.setText(parts[1]);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        long time = calendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
        if (Calendar.getInstance().get(Calendar.YEAR) == calendar.get(Calendar.YEAR) &&
                Calendar.getInstance().get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)) {
            dayDelivery.setText(getResources().getString(R.string.today));
        } else if (time <= MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY) {
            dayDelivery.setText(getResources().getString(R.string.tomorrow));
        } else if (time <= (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * 5)) {
            setDay(calendar, dayDelivery);
        } else {
            dayDelivery.setText(parts[0]);
        }
        OrderStorage o= OrderStorage.getInstance(getActivity());
        o.getOrderSend().getShipping_address().setAddress_2(datePaint);
        o.saveJson();
        return calendar;
    }


    private void setDay(Calendar calendar, MrJeffTextView textView) {
        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                textView.setText(getResources().getString(R.string.monday));
                break;
            case Calendar.TUESDAY:
                textView.setText(getResources().getString(R.string.tuesday));
                break;
            case Calendar.WEDNESDAY:
                textView.setText(getResources().getString(R.string.wednesday));
                break;
            case Calendar.THURSDAY:
                textView.setText(getResources().getString(R.string.thursday));
                break;
            case Calendar.FRIDAY:
                textView.setText(getResources().getString(R.string.friday));
                break;
            case Calendar.SATURDAY:
                textView.setText(getResources().getString(R.string.saturday));
                break;
            case Calendar.SUNDAY:
                textView.setText(getResources().getString(R.string.sunday));
                break;
        }
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
       
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    private void showErrorOpenDate(int title, int text) {
        new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.Theme_AppCompat_Light)).setTitle(title).setMessage(text).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        }).show();
    }

    private void save() {
       
    }

    public void notifyChanges(){
    }


}
