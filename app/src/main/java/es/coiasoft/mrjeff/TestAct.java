package es.coiasoft.mrjeff;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TestAct extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_test,Test.newInstance(null)).commit();

    }
}
