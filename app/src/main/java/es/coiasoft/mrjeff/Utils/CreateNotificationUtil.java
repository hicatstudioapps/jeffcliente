package es.coiasoft.mrjeff.Utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import es.coiasoft.mrjeff.R;

/**
 * Created by Paulino on 04/02/2016.
 */
public class CreateNotificationUtil {

    private static int mNotificationId = 1;

    public static NotificationCompat.Builder createNotification(Context context, int resourceTitle, int resourceText){
       return new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_stat_notification)
                        .setContentTitle(context.getResources().getString(resourceTitle))
                        .setContentText(context.getResources().getString(resourceText)).setAutoCancel(true);
    }


    public static void showNotification(Context context,NotificationCompat.Builder nBuilder){
        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, nBuilder.build());
    }
}
