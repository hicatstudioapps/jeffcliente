package es.coiasoft.mrjeff.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.ContextThemeWrapper;

import java.util.Calendar;

import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;

/**
 * Created by Paulino on 04/02/2016.
 */
public class DialogAssessment {

    public static void showDialog(final Context context){
        new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.Theme_AppCompat_Light)).setTitle(R.string.rate_title).setMessage(R.string.rate_text).setNeutralButton(R.string.rate_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                ConfigRepository db = new ConfigRepository(context);
                db.updateRegister(ConfigRepository.RATE, "-1");
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=ID_DE_LA_APP")));
            }
        }).setNegativeButton(R.string.rate_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                String time = String.valueOf(Calendar.getInstance().getTimeInMillis());
                ConfigRepository db = new ConfigRepository(context);
                db.updateRegister(ConfigRepository.RATE, time);
            }
        }).setPositiveButton(R.string.rate_neutral, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                ConfigRepository db = new ConfigRepository(context);
                db.updateRegister(ConfigRepository.RATE, "-1");
            }
        }).show();
    }
}
