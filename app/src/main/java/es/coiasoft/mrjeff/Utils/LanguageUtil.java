package es.coiasoft.mrjeff.Utils;

import android.util.Log;

import java.util.Locale;

/**
 * Created by Paulino on 03/02/2016.
 */
public class LanguageUtil {

    public static boolean isSpanish(){
        String language = Locale.getDefault().getLanguage();
        Log.d("LANGUAGE", language);
        return language != null && (language.contains("es") || language.contains("gl")|| language.contains("eu")) || language.contains("ca");
    }

    public static boolean isCatala(){
        String language = Locale.getDefault().getLanguage();
        Log.d("LANGUAGE", language);
        return language != null && language.contains("NO ACTIVADO");
    }
}
