package es.coiasoft.mrjeff.Utils;

import com.paypal.android.sdk.payments.PayPalConfiguration;

/**
 * Created by linovm on 2/12/15.
 */
public class MrJeffConstant {

    public static final boolean ENVIROMENT_TEST = false;

    public static class URL {

        //public static final String URL_BASE = "https://www.idontwash.com/";
        public static final String URL_BASE_PRO = "https://mrjeffapp.com/";
        public static final String URL_BASE_TEST = "https://www.idontwash.com/";
        public static final String URL_BASE_AUX = "http://ec2-52-51-106-207.eu-west-1.compute.amazonaws.com/";
        public static final String URL_BASE_PROXY = "http://ec2-52-210-150-117.eu-west-1.compute.amazonaws.com:8181/";

        public static final String URL_BASE = MrJeffConstant.ENVIROMENT_TEST ? URL_BASE_TEST : URL_BASE_PRO;

        public static final String URL_GET_POSTAL_CODE = URL_BASE_AUX + "codigopostalv2.php";

        public static final String URL_POST_PC_MAIL = URL_BASE_AUX + "ecp.php";

        public static final String URL_POST_PHONE_MAIL = URL_BASE_AUX + "androidleadv2.php";

        public static final String URL_POST_VALIDATE_VERSION = URL_BASE_AUX + "newversion.php";

        public static final String SEPARATOR_ARGS = "?";

        public static final String META_TAG="filter[meta]=true&";

        public static final String URL_KEYS_API_TEST = "consumer_key=ck_7c88e569d5a24fdf549df164388c9c6e&consumer_secret=cs_c98790791a4118cdc875a68aba71ee65";
        public static final String URL_KEYS_API_PRO = "consumer_key=ck_04ef92d3a3ff36ccd8427d24f289c47c&consumer_secret=cs_0b609d5cc4cd3d199cd3ba927b4b5ce7";
        public static final String URL_KEYS_PROXY = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJuYW1lIjpudWxsLCJlbWFpbCI6Imlvc0BhZG1pbi5jb20iLCJwcm9maWxlIjpudWxsLCJzdXJuYW1lIjpudWxsLCJpYXQiOjE0NzI0NjQwMzEsImV4cCI6MTUwNDAwMDAzMX0=.s61O0o-tfUaQy-4dPbQnZ8Zgbe-deHQDckabrwFkDw5wecvouPr7p1Q0k9CyD-AfcEe7X44Q2tP9iS3F5IDVSLHi6VSbm8BXDNYQHDA3gzqv1pk57tz6F1PjrkG-x5oW4gdhEfQ2uBHVjEZpYSHxEQV-JhrplMzlyXMD2pKwEzOdRdpWsplFOF4YtDdUtJjxo13tkTcoNl_whXufeeJpcQuqP3zeuvAykl3D8GZfKxTTVuZexmmDY57LTV68hzKD_xqmwhjwXrj13bwrh41mnAs6qOBk9HpMO0lSpke0S7QVWoB31Eb4T2jmN1wennZBRtwqHbr6gAtLjc-TCoo7Gg==";
        public static final String URL_KEYS_API = MrJeffConstant.ENVIROMENT_TEST ? URL_KEYS_API_TEST : URL_KEYS_API_PRO;

        public static final String URL_KEYS_DELETE_API = "force=true&" + URL_KEYS_API;

        public static final String URL_GET_PRODUCT = URL_BASE_PRO + "wc-api/v1/products" + SEPARATOR_ARGS + URL_KEYS_API;

        public static final String URL_DELETE_PRODUCT = URL_BASE + "wc-api/v2/products/<product>" + SEPARATOR_ARGS + URL_KEYS_DELETE_API;

        public static final String ARG_PRODUCT = "<product>";

        public static final String URL_POST_PRODUCT = URL_BASE + "wc-api/v2/products" + SEPARATOR_ARGS + URL_KEYS_API;

        public static final String URL_SEARCH_COUPON = URL_BASE + "wc-api/v2/coupons/code/<coupon>" + SEPARATOR_ARGS + URL_KEYS_API;

        public static final String ARG_COUPON = "<coupon>";

        public static final String URL_DELETE_ORDER = URL_BASE + "wc-api/v2/orders/<order>" + SEPARATOR_ARGS+URL_KEYS_DELETE_API;

        public static final String URL_POST_ORDER = URL_BASE + "wc-api/v2/orders" + SEPARATOR_ARGS + URL_KEYS_API;

        public static final String URL_UPDATE_ORDER = URL_BASE + "wc-api/v2/orders/<order>" + SEPARATOR_ARGS + URL_KEYS_API;

        public static final String URL_GET_ORDER = URL_BASE + "wc-api/v2/orders/<order>" + SEPARATOR_ARGS+META_TAG+URL_KEYS_DELETE_API;

        public static final String ARG_ORDER = "<order>";

        public static final String URL_STRIPE_PAYMENT = URL_BASE+ "cgi-bin/paymentandroid.cgi";

        public static final String URL_STRIPE_PAYMENT_SUBSCRIPTION = URL_BASE+ "cgi-bin/subscripcionv2.cgi";

        public static final String URL_STRIPE_PAYMENT_SUBSCRIPTION_99 = URL_BASE+ "cgi-bin/subscripcion99.cgi";

        public static final String URL_POST_CUSTOMER = URL_BASE + "wc-api/v2/customers" + SEPARATOR_ARGS + URL_KEYS_API;

        public static final String URL_GET_CUSTOMER = URL_BASE + "wc-api/v2/customers/email/<email>" + SEPARATOR_ARGS + URL_KEYS_API;

        public static final String ARG_CUSTOMER = "<email>";

        public static final String URL_POST_LOGIN = URL_BASE_AUX + "loginv2.php";

        public static final String URL_POST_TIMETABLE = URL_BASE_AUX +"timetablev2.php?postalCode=";

        public static final String URL_GET_ORDER_BY_USERS = URL_BASE +  "wc-api/v2/customers/<id>/orders"  + SEPARATOR_ARGS +  META_TAG  + URL_KEYS_API;

        public static final String ARG_ID = "<id>";

        public static final String URL_PUT_UPDATE_CUSTOMER = URL_BASE + "wc-api/v2/customers/<id>"  + SEPARATOR_ARGS + URL_KEYS_API;

        public static final String URL_POST_GET_REFERRAL_USER = URL_BASE_AUX + "returnreferralv2.php";

        public static final String URL_POST_GET_REFERRAL_USER_PARAM_ID = "ID";

        public static final String URL_POST_GET_REFERRAL_USER_PARAM_COUPON = "Cupon";

        public static final String URL_POST_CREATE_COUPON = URL_BASE + "wc-api/v2/coupons" + SEPARATOR_ARGS + URL_KEYS_API;

        public static final String URL_POST_ADD_REFERRAL_USER = URL_BASE_AUX + "referralinsertionv2.php";

        public static final String URL_GET_COUNT_COUPON_USER = URL_BASE_AUX + "couponperuserv2.php";

        public static final String URL_POST_SEARCH_FAQS = URL_BASE_PRO + "wp-content/ep/faqservice.php";

        public static final String ARG_ID_PRODUCT = "product_id";

        public static final String URL_POST_IDS_NOLAVO = URL_BASE_AUX+ "packorderstatusv2.php";

        public static final String URL_UPDATE_DATE_ORDER =  URL_BASE_AUX+ "updatenotificationv2.php";

        public static final String URL_GET_STATUS_ORDER = URL_BASE_AUX+"orderstatusv2.php?ids=";

        public static final String URL_UPDATE_CACHE_PRODUCTS= URL_BASE_AUX+"productcache.php?day=";

        public static final String URL_UPDATE_CACHE_FAQS= URL_BASE_AUX+"faqcache.php?day=";

        public static final String URL_UPDATE_CACHE_ORDERS= URL_BASE_AUX+"ordercache.php?id=##ID##&day=##DAY##";

        public static final String URL_COUPON_RESIDENCIA= URL_BASE_AUX+"cuponb2b2c.php";

        public static final String URL_COUPON_RESIDENCIA_ATTR_CODE= "residencecoupon";

        public static final String URL_COUPON_RESIDENCIA_ATTR_USER= "userid";

        public static final String URL_CREATE_SUSCRIPTION = URL_BASE_AUX+ "createsuscription.php";

        public static final String URL_CREATE_SUSCRIPTION_ATTR_CODECOUPON = "couponCode";
        public static final String URL_CREATE_SUSCRIPTION_ATTR_CLIENT = "clientId";
        public static final String URL_CREATE_SUSCRIPTION_ATTR_TYPESUS = "suscriptionType";
        public static final String URL_CREATE_SUSCRIPTION_ATTR_CODEORDER = "orderId";
        public static final String URL_CREATE_SUSCRIPTION_ATTR_DAY = "defaultDay";
        public static final String URL_CREATE_SUSCRIPTION_ATTR_HOUR = "defaultHour";
        public static final String URL_CREATE_SUSCRIPTION_ATTR_ADDRESS = "defaultAddress";
        public static final String URL_CREATE_SUSCRIPTION_ATTR_CP = "defaultPostalCode";

        public static final String URL_GET_RATED = URL_BASE_PROXY + "mrjeff/order/valoration/find?idOrders=[<ID_ORDERS>]";
        public static final String URL_GET_RATED_PARAM = "<ID_ORDERS>";

        public static final String URL_POST_RATE = URL_BASE_PROXY + "mrjeff/order/valoration";

        public static final String URL_HAVE_SUSCRIPTION = URL_BASE_AUX+ "clientsuscriptionv2.php";
        public static final String URL_HAVE_SUSCRIPTION_ATTR_CLIENT = "clientId";

        public static final String URL_UPDATE_SUSCRIPTION = URL_BASE_AUX+ "updatedefaultdayv2.php";
        public static final String URL_UPDATE_ATTR_ID_SUSCRIPTION="idSuscription";
        public static final String URL_UPDATE_ATTR_DEFAUlT_DAY="defaultDay";
        public static final String URL_UPDATE_ATTR_DEFAUlT_HOUR="defaultHour";
        public static final String URL_UPDATE_ATTR_DEFAUlT_ADDRESS="defaultAddress";
        public static final String URL_UPDATE_ATTR_DEFAUlT_POSTAL_CODE="defaultPostalCode";
        public static final String URL_UPDATE_ATTR_ONLY_DELIVERY ="onlydelivery";
        public static final String URL_UPDATE_ATTR_ORDER_ID ="orderId";

        public static final String URL_ZALANDO_CODE = URL_BASE_AUX+ "couponzalando.php";

        public static final String URL_ZALANDO_CODE_ATTR_COUPON = "couponZalando";

        public static final String URL_ZALANDO_CODE_ATTR_USER = "userid";

        public static final String URL_ZALANDO_CODE_USE = URL_BASE_AUX+ "couponwasted.php";

        public static final String UPDATE_ORDER_FINAL="http://ec2-52-51-106-207.eu-west-1.compute.amazonaws.com/neworderapp.php";

    }

    public static class DEEP_LINKING{

        public static final String PATTER_PRODUCT = "/producto/";

        public static final String PATTER_ORDER ="/mi-cuenta/view-order/";

        public static final String PATTERN_SUSCRIPCION = "/suscripcion/";

        public static final String PATTER_COUPON_ORDER ="mrjeff://mi-cuenta/view-order/";

        public static final String PATTER_COUPON ="mrjeff://cupon/";

        public static final String PATTER_TIMETABLE ="mrjeff://edit-order/";

        public static final String PATTER_TIMETABLE2 ="mrjeff://mi-cuenta/editorder/";

        public static final String PATTER_RATE ="mrjeff://rate/";
    }

    public static class RESULT_SERVICE_DATE_INVALID {

        public static int ONLY_SEND = 1;

        public static int ONLY_COLLECT = 0;

        public static int BOTH = 2;
    }


    public static class STRIPE {

        public static final String RESPONSE_SUCCESS = "succeeded";

        public static final String RESPONSE_SUCCESS_SUBSCRIPTION1 = "created";

        public static final String RESPONSE_SUCCESS_SUBSCRIPTION2 = "subscriptions";

        public static final String PUBLIC_KEY_STRIPE_PRO = "pk_live_KZt8jaDgEIAKqCA0oGqYvWXH";
//public static final String PUBLIC_KEY_STRIPE_PRO = "pk_test_dhOBW1MdFjP5txXGqUIJL5AT";
        public static final String PUBLIC_KEY_STRIPE_TEST = "pk_test_dhOBW1MdFjP5txXGqUIJL5AT";

        public static final String PUBLIC_KEY_STRIPE = MrJeffConstant.ENVIROMENT_TEST ? PUBLIC_KEY_STRIPE_TEST : PUBLIC_KEY_STRIPE_PRO;


        public static final String CURRENCY = "eur";

        public static final String DESCRIPTION = "Compra efectuada por App Android. Pedido: ";
    }

    public static class PAYPAL {

        public static final String CURRENCY = "EUR";

        public static final String DESCRIPTION = "Compra efectuada por App Android. Pedido: ";

        public static final String CONFIG_ENVIRONMENT = MrJeffConstant.ENVIROMENT_TEST ? PayPalConfiguration.ENVIRONMENT_SANDBOX:PayPalConfiguration.ENVIRONMENT_PRODUCTION;

        public static final String CONFIG_CLIENT_ID = "AQSRU0WE-ANsrlZYy8lqHkKGApRW9qj1YquHW-NSkHS0uAo_TUDoCBpHNhHiHI8nU2vXh7gGFQ33M1GQ";

        public static final int REQUEST_CODE_PAYMENT = 1;
    }


    public static class ORDER {
        public static final String UPDATE_STATE_FINALLY = "processing";

        public static final Float MIN_PAID_ORDER = 20.0f;

        public static final String COUPON_REFERRAL="0M9J1A";

        public static final String COUPON_REFERRAL_SAMPLE="referallcaracteristics";
    }

    public static class SESSION {
        public static final long TIME_EXPIRED = 7200000;
    }

    public static class LOGIN_EXTRA {

        public static final String LOGIN_ATTR = "typebutton";

        public static final String LOGIN_BUTTON = "loginbutton";

        public static final String REGIS_BUTTON = "regisbutton";

        public static final String HOME_LOGIN = "homeLogin";

    }

    public static class ACTIVITY_RESULT {

        public static final int GOOGLE_SIG_IN = 110;
    }

    public static class HELP_FUNCTION_TEXT {

        public static final String HTML_ERROR_TAG = "##TEXTO##";

        public static final String HTML_ERROR = "<p style='background:#FF4A55; color:#FFFFFF'>"+HTML_ERROR_TAG+"</p>";

    }

    public static class ORDER_DAYS_SEND {
        public static final String ATTR_DAYS = "DiasEnvio";

        public static final Long MILSECONDS_DAY = 86400000L;

        public static final Long MILSECONDS_MIN_ORDER = 5400000L;

        public static final Long MILSECONDS_TWO_HOUR = 5400000L;

        public static final Integer MIN_DAYS = 2;
    }


    public static class CALL {

        public static final String emailMrJeff = "hola@mrjeffapp.com";

        public static final String phoneMrJeff = "tel:0034644446163";
    }

    public static class INTENT_ATTR {

        public static final String ORDER = "ORDER";
        public static final String IDPRODUCT = "IDPRODUCT";
        public static final String SUSCRIPTION = "SUSCRIPTION";
        public static final String POSFRAGMENT = "POSTFRAGMENT";
        public static final String TYPESUSCRIPTION = "TYPESUSCRIPTION";
        public static final String RESIDENCIA = "RESIDENCIA";
        public static final String CODERESIDENCIA = "CODERESIDENCIA";
        public static final String HOME_MESSAGE = "MESSAGE";
        public static final String DESCRIPTION = "DESCRIPTION";
        public static final String DEFAULT = "DEFAULT";
        public static final String EDITABLE = "EDITABLE";
        public static final String ISEDIT = "EDIT";
        public static final String ORDERID = "ORDERID";
        public static final String DAY = "DAY";
        public static final String HOUR = "HOUR";
        public static final String ZALANDO = "ZALANDO";
        public static final String NEEDPAYMENT = "NEEDPAYMENT";

        public static final String TYPE_SUS_RES = "4";

    }

    public static class LOCALYTICS {

        public static final String POSTAL_CODE = "POSTAL_CODE";
        public static final String CITY = "CITY";
        public static final String NUMBER_ORDERS = "NUMBER_ORDERS";

        public static final String SCREEN_LOGIN = "Screen login";
        public static final String SCREEN_PRODUCTS_PACKS = "Screen ropa packs";
        public static final String SCREEN_PRODUCTS_WASH = "Screen Lavandería";
        public static final String SCREEN_PRODUCTS_TINTORERIA = "Screen ropa Tintorería";
        public static final String SCREEN_PRODUCTS_OTROS = "Screen ropa Otros";
        public static final String SCREEN_CART = "Screen vista carro";
        public static final String SCREEN_ORDER_ADDRES = "Screen dirección pedido";
        public static final String SCREEN_BILLING_ADDRES = "Screen dirección recogida";
        public static final String SCREEN_SHIPPING_ADDRES = "Screen dirección devolución";
        public static final String SCREEN_PAID = "Screen Pago";
        public static final String SCREEN_FINALLY = "Screen pago finalizado";
        public static final String SCREEN_PROFILE = "Screen mi perfil";
        public static final String SCREEN_MY_WASH = "Screen mis coladas";
        public static final String SCREEN_MY_ONE_WASH = "Screen una colada";
        public static final String SCREEN_PRICES = "Screen precios";
        public static final String SCREEN_CALL = "Screen hablanos";
        public static final String SCREEN_VIDEO = "Screen video inicio";
        public static final String SCREEN_POSTAL_CODE = "Screen código postal";
        public static final String SCREEN_COUPON = "Screen coupon";
        public static final String SCREEN_EDIT_PROFILE = "Edit profile";
        public static final String SCREEN_ONE_PRODUCT = "Producto ";
        public static final String SCREEN_DEEP_LINKING = "Deep-linking";
        public static final String SCREEN_RESIDENCIA = "Codigo residencia";
        public static final String SCREEN_ZALANDO = "ZALANADO";

    }

    public static class TWITTER {
        public static final String OAuthConsumerKey = "tyKPAInIVXQH7o1hnSH3JCt62";
        public static final String OAuthConsumerSecret = "hPmLBHTxzXEtPWtm7QXgNrvInsZFccbE8oZ2FjqLfaUwkYVkXC";
        public static final String OAuthAccessToken = "3334152339-Gi8lXtoqNz28dr4nRsyt5LL8T9vYA7OiIsbAtvU";
        public static final String OAuthAccessTokenSecret = "sv9SSqVVyZx6p2c5Am59Bi9IT37fk0bPgXiJlx0GOsMuz";
    }

    public static class SUSCRIPTION {

        public static final String SUSCRIPTION_6x6 = "6x6";
        public static final String SUSCRIPTION_FAMILAIR = "familiar";
        public static final int ID_6x6_ES = 8908;
        public static final int ID_6x6_EN  = 8913;
        public static final int ID_6x6_CA = 9917;
        public static final int ID_FAMILIAR_ES  = 8909;
        public static final int ID_FAMILIAR_EN  = 8911;
        public static final int ID_FAMILIAR_CA  = 9921;
    }

    public static class LOCALYTICS_EVENTS {

        public static class ATTR {
            public static final String FECHA = "HORAEVENTO";
            public static final String EMAIL = "Email";
            public static final String ID_PEDIDO = "ID PEDIDO";
            public static final String N_PRODUCTOS = "N PRODUCTOS";
            public static final String TOTAL = "TOTAL";
            public static final String CP_RECOGIDA = "CP RECOGIDA";
            public static final String CIUDAD = "CIUDAD";
            public static final String FECHA_RECOGIDA = "FECHA_RECOGIDA";
            public static final String HORA_RECOGIDA = "HORA_RECOGIDA";
            public static final String DIRECCION_RE = "DIRECCION_RE";
            public static final String CP_DEVOLUCION = "CP DEVOLUCION";
            public static final String FECHA_DEVOLUCION = "FECHA_DEVOLUCACION";
            public static final String HORA_DEVOLUCION = "HORA_DEVOLUCION";
            public static final String DIRECCION_DV = "DIRECCION_DE";
            public static final String COUPON = "Cupon";
            public static final String METHOD = "Metodo";
            public static final String TYPE_SUSCRIPTION = "Suscripcion";
            public static final String TYPE = "tipo";
            public static final String DATE = "FECGA_EVENTO";
            public static final String VALUE = "valor";
            public static final String CITY = "city";

        }

        public static class NAME {
            public static final String FINAL = "Se ha finalizado el pedido";
            public static final String FINAL_SUSCRIPTION = "Se ha finalizado la suscripcion";
            public static final String ITEM_PURCHASED = "Item Purchased";
            public static final String SHARED_REFERRAL = "Compartio referral";
            public static final String ADD_DATE_CALENDAR= "Anhadio la fecha de su pedido al calendario";
            public static final String COUPON_PAID = "Se ha introducido un cupon en el pedido";
            public static final String COUPON_PAID_SUSCRIPTION = "Se ha introducido un cupon en la suscripción";
            public static final String DELETE_PAID = "Se quita el cupon del pedido";
            public static final String DELETE_PAID_SUSCRIPTION = "Se quita el cupon de la suscripción";
            public static final String ORDER_MIN = "Se aplica tasa de pedido minimo";
            public static final String ACTION = "Accion usuario";
            public static final String ADDRESS="Se ha introducido la dirección del pedido";
            public static final String CODE_RES="Se ha introducido cupon residencia";
            public static final String ADDRESS_SUBS="Se ha introducido la dirección de la suscripción";
            public static final String NEW_CART="Se ha creado un carro";
            public static final String ENTRA_SUSCRIPTION="Visualiza suscripción";
            public static final String CREATE_SUSCRIPTION="Inicia una suscripción";
            public static final String NEW_USER="Nuevo usuario";
            public static final String COUPON_INIT="Cupón en la ventana inicia";
            public static final String NOLAVO_DISABLED="SUSCRIPCION_DESACTIVADA";

        }


        public static class ACTION {
            public static final String STRIPE = "PULSA PAGO TARJETA";
            public static final String PAYPAL = "PULSA PAGO PAYPAL";
            public static final String CARD = "INTRODUCE Nº TARJETA";
            public static final String CVC = "INTRODUCE CVC TARJETA";
            public static final String DATECARD = "INTRODUCE FECHA TARJETA";
            public static final String SAMEADDRESS = "PULSA MISMA DIRECCION";
            public static final String TYPEADDRESS = "PULSA TIPO DIRECCION ENTREGA";
            public static final String TYPEADDRESSBILLING = "PULSA TIPO DIRECCION RECOGIDA";
            public static final String NAMESHIPPING = "INTRODUCE NOMBRE DIRECCION ENTREGA";
            public static final String ADDRESSSHIPPING = "INTRODUCE DIRECCION ENTREGA";
            public static final String CPSHIPPING = "INTRODUCE CODIGO POSTAL ENTREGA";
            public static final String DATESHIPPING = "INTRODUCE FECHA ENTREGA";
            public static final String NAMEBILLING = "INTRODUCE NOMBRE DIRECCION RECOGIDA";
            public static final String ADDRESSBILING = "INTRODUCE DIRECCION RECOGIDA";
            public static final String CPBILLING = "INTRODUCE CODIGO POSTAL RECOGIDA";
            public static final String DATEBILLING = "INTRODUCE FECHA RECOGIDA";
            public static final String NEXTSBILLING = "PULSA IR A HORARIO";
            public static final String NEXTSSHIPPING = "PULSA IR A PAGAR";
            public static final String NAMESPROFILE= "INTRODUCE NOMBRE EN EDITAR PERFIL";
            public static final String ADDRESSPROFILE = "INTRODUCE DIRECCION EN EDITAR PERFIL";
            public static final String CPPROFILE = "INTRODUCE CODIFO POSTAL EN EDITAR PERFIL";
            public static final String PHONEPROFILE = "INTRODUCE TLF EN EDITAR PERFIL";
            public static final String PHONEPSHIPPING = "INTRODUCE TLF EN DIRECCION ENTREGA";
            public static final String PHONEBILLING = "INTRODUCE TLF EN DIRECCION RECOGIDA";
            public static final String EMAILPROFILE = "INTRODUCE EMAIL EN EDITAR PERFIL";
            public static final String EMAILPSHIPPING = "INTRODUCE EMAIL EN DIRECCION ENTREGA";
            public static final String EMAILBILLING = "INTRODUCE EMAIL EN DIRECCION RECOGIDA";
            public static final String CARTMINUS = "BOTON MENOS EN CARRITO";
            public static final String CARTPLUS = "BOTON MAS EN CARRITO";
            public static final String CARTDELETE = "BOTON BORRAR EN CARRITO";
            public static final String NEXTCART = "PULSA IR A DIRECCION RECOGIDA";
            public static final String HOMEADD = "AÑADE PRODUCTO A CESTA";
            public static final String HOMEREMOVE = "BORRA PRODUCTO DE CESTA";
            public static final String HOMECART= "PULSA IR AL CARRO";
            public static final String HOMEINFO = "VISUALIZA DESCRIPCION DE PRODUCTO";
            public static final String HOMESUSCRIPCION = "ENTRA EN PRODUCTO DE SUBSCRIPCION";
            public static final String SUSCRIPTIONCALL = "LLAMAR DESDE SUBSCRIPCION";
            public static final String SUSCRIPTIONWHATSAPP = "WHATSAPP DESDE SUBSCRIPCION";
            public static final String NEXTVIDEO = "PULSA EN EMPEZAR. VIDEO";
            public static final String HELPCP="Introduce codigo postal inicio";
            public static final String HELPEMAIL="Introduce el mail, al no encontrar código postal";
            public static final String NEXTHELP="Va a la ventana de cupón";
            public static final String SEARCHBILLING="Buscar dirección de recogida";
            public static final String SEARCHSHIPPING="Buscar dirección de devolucion";
            public static final String UPDATE_ORDER = "Se actualzia las fechas del pedido";
            public static final String UPDATE_PICKUP_ORDER = "MODIFICA FECHA RECOGIDA";
            public static final String UPDATE_DELIVERY_ORDER = "MODIFICA FECHA ENTREGA";

        }

    }

    public static class PRODUCT_ATTR {

        public static final String CLAIM = "Claim";
        public static final String DIAS_ENVIO = "DiasEnvio";
        public static final String SUSCRIPTION_TYPE = "SuscriptionType";
    }


}
