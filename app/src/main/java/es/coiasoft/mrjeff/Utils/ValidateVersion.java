package es.coiasoft.mrjeff.Utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.ContextThemeWrapper;

import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.R;

import es.coiasoft.mrjeff.manager.model.ConfigRepository;
import es.coiasoft.mrjeff.manager.worker.ValidateVersionWorker;

/**
 * Created by linovm on 5/11/15.
 */
public class ValidateVersion implements Observer {

    private static final String VERSION = "1.13.0";
    private static final String LABEL = "android";
    private Activity activity;
    private Handler handler;

    public void sendConsult(Activity activity) {
        this.activity = activity;
        this.handler = new Handler();
        ConfigRepository dataSource = new ConfigRepository(activity);
        String value = dataSource.getValueConfig(ConfigRepository.BLOCK);
        if(value == null || !value.equalsIgnoreCase("1")){
            ValidateVersionWorker worker = ValidateVersionWorker.getInstance(LABEL, VERSION);
            worker.addObserver(this);
            Thread validateThread = new Thread(worker);
            validateThread.start();
        } else {
            showMessageError();
        }

    }

    private void showMessageError() {
        new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.Theme_AppCompat_Light)).setTitle(R.string.modalUpdateVersionTitle).setMessage(R.string.modalUpdateVersionText).setNeutralButton(R.string.modalUpdateVersionButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                final String appPackageName = activity.getPackageName(); // getPackageName() from Context or Activity object
                try {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                final String appPackageName = activity.getPackageName(); // getPackageName() from Context or Activity object
                try {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        }).show();

    }


    @Override
    public void update(Observable observable, Object data) {
        if (data instanceof Boolean && !(Boolean) data) {
            Runnable blockRunnable = new Runnable() {
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            ConfigRepository dataSource = new ConfigRepository(activity);
                            dataSource.updateRegister(ConfigRepository.BLOCK,"1");
                            showMessageError();
                        }
                    });
                }
            };
            blockRunnable.run();
        }
    }
}
