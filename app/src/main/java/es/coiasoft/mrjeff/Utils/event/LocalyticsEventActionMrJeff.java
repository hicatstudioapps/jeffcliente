package es.coiasoft.mrjeff.Utils.event;

import android.content.Context;

import com.localytics.android.Localytics;

import java.util.HashMap;
import java.util.Map;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.orders.response.Billing_address;
import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.domain.orders.response.Shipping_address;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;

/**
 * Created by Paulino on 18/02/2016.
 */
public class LocalyticsEventActionMrJeff extends LocalytisEvenMrJeff{


    protected Context context;
    protected Map<String, String> values = new HashMap<String, String>();
    protected StringBuilder builder = new StringBuilder();

    public LocalyticsEventActionMrJeff(Context context,String action,Boolean isSuscripcion,String... param){
        super(context);
        addOrder();
        addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.TYPE,action);
        if(param != null && param.length > 0){
            for(int i=0; i < param.length; i++){
                addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.VALUE+i,param[i]);
            }
        }
        addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.TYPE_SUSCRIPTION,isSuscripcion?"SI":"NO");
        send(MrJeffConstant.LOCALYTICS_EVENTS.NAME.ACTION);
    }

    public void addOrder(){
        if(OrderStorage.getInstance(context).getOrder() != null){
            Order order = OrderStorage.getInstance(context).getOrder();
            if(order.getId() != null){
                addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.ID_PEDIDO, order.getId().toString());
            }
        }
    }




}
