package es.coiasoft.mrjeff.Utils.event;

import android.content.Context;
import android.util.Log;

import com.localytics.android.Localytics;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.PostalCode;
import es.coiasoft.mrjeff.domain.orders.response.Billing_address;
import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.domain.orders.response.Shipping_address;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.PostalCodeStorage;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;

/**
 * Created by Paulino on 18/02/2016.
 */
public class LocalytisEvenMrJeff {


    protected Context context;
    protected Map<String, String> values = new HashMap<String, String>();
    protected StringBuilder builder = new StringBuilder();
    private ConfigRepository dataSource;

    public LocalytisEvenMrJeff(Context context){
        this(context, Boolean.FALSE, null, Boolean.FALSE, null);
    }

    public LocalytisEvenMrJeff(Context context,Boolean order, String name, Boolean automatic){
        this(context,order,name,automatic,null);
    }

    public LocalytisEvenMrJeff(Context context,Boolean order, String name, Boolean automatic,Integer paid){
        this.context = context;
        addCustomerEmail();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.DATE, format.format(Calendar.getInstance().getTime()));
        setCity();
        if(order){
            addOrder();
        }

        if(automatic && name!=null){
            if(paid == null) {
                send(name);
            } else {
                send(name, paid);
            }
        }

        try {
            Calendar calendar = Calendar.getInstance();
            values.put(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.FECHA, (calendar.get(Calendar.HOUR_OF_DAY)<10?"0":"")+calendar.get(Calendar.HOUR_OF_DAY)+":"+(calendar.get(Calendar.MINUTE)<10?"0":"")+calendar.get(Calendar.MINUTE)+":"+(calendar.get(Calendar.SECOND)<10?"0":"")+calendar.get(Calendar.SECOND));
        }catch (Exception e){

        }
    }


    private void setCity(){
        if(OrderStorage.getInstance(context).getOrder() != null &&
                OrderStorage.getInstance(context).getOrder().getBilling_address() != null &&
                OrderStorage.getInstance(context).getOrder().getBilling_address().getPostcode() != null){
            addAttributeCity(OrderStorage.getInstance(context).getOrder().getBilling_address().getPostcode());
        } else if(CustomerStorage.getInstance(context).getCustomer() != null &&
                CustomerStorage.getInstance(context).getCustomer().getBilling_address() != null &&
                CustomerStorage.getInstance(context).getCustomer().getBilling_address().getPostcode() != null ){
            addAttributeCity(CustomerStorage.getInstance(context).getCustomer().getBilling_address().getPostcode() );
        } else {
            try {
                dataSource = new ConfigRepository(context);
                String city = dataSource.getValueConfig(ConfigRepository.CITY);
                if(city != null){
                    addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.CITY, city);
                }
            }catch (Exception e){

            }
        }
    }

    private void addAttributeCity(String postalCode){
        if(postalCode != null && PostalCodeStorage.getInstance(context).getLoad() &&
                PostalCodeStorage.getInstance(context).getCity(postalCode) != null){
            addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.CITY, PostalCodeStorage.getInstance(context).getCity(postalCode));
        }

    }



    public void addAttribute(String key, String value){
        values.put(key,value);
        builder.append(" - " + value);
    }


    public void addCustomerEmail(){
        if (CustomerStorage.getInstance(context).isLogged()) {
            addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.EMAIL, CustomerStorage.getInstance(context).getCustomer().getEmail());
        } else if(OrderStorage.getInstance(context).getOrder() != null &&
                OrderStorage.getInstance(context).getOrder().getBilling_address() != null &&
                OrderStorage.getInstance(context).getOrder().getBilling_address().getEmail() != null ){
            addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.EMAIL, OrderStorage.getInstance(context).getOrder().getBilling_address().getEmail());
        }
    }

    public void addOrder(){
        if(OrderStorage.getInstance(context).getOrder() != null){
            Order order = OrderStorage.getInstance(context).getOrder();
            if(order.getId() != null){
                addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.ID_PEDIDO, order.getId().toString());
            }

            Integer countProduct = order.getLine_items() != null ? order.getLine_items().size() : 0;
            addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.N_PRODUCTOS, countProduct.toString());
            if(order.getTotal() != null) {
                addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.TOTAL, order.getTotal());
            }

            addBillingOrder();
            addShippingOrder();
        }
    }

    public void addBillingOrder(){
        if(OrderStorage.getInstance(context).getOrder() != null &&
                OrderStorage.getInstance(context).getOrder().getBilling_address() != null){
            Billing_address address = OrderStorage.getInstance(context).getOrder().getBilling_address();

            if(address.getPostcode() != null){
                addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.CP_RECOGIDA, address.getPostcode());
            }

            if(address.getCity() != null){
                addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.CIUDAD, address.getCity());
            }

            if(address.getAddress_2() != null &&
                    address.getAddress_2().trim().split(" ").length >= 2){
                String fecha = address.getAddress_2().trim().split(" ")[0];
                String hora = address.getAddress_2().trim().split(" ")[1];
                addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.FECHA_RECOGIDA, fecha);
                addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.HORA_RECOGIDA, hora);
            }

            if(address.getAddress_1() != null){
                addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.DIRECCION_RE, address.getAddress_1());
            }
        }
    }

    public void addShippingOrder(){
        if(OrderStorage.getInstance(context).getOrder() != null &&
                OrderStorage.getInstance(context).getOrder().getShipping_address() != null){
            Shipping_address address = OrderStorage.getInstance(context).getOrder().getShipping_address();

            if(address.getPostcode() != null){
                addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.CP_DEVOLUCION, address.getPostcode());
            }

            if(address.getAddress_2() != null &&
                    address.getAddress_2().trim().split(" ").length >= 2){
                String fecha = address.getAddress_2().trim().split(" ")[0];
                String hora = address.getAddress_2().trim().split(" ")[1];
                addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.FECHA_DEVOLUCION, fecha);
                addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.HORA_DEVOLUCION, hora);
            }

            if(address.getAddress_1() != null){
                addAttribute(MrJeffConstant.LOCALYTICS_EVENTS.ATTR.DIRECCION_DV, address.getAddress_1());
            }
        }
    }

    public void send(String name){
        send(name,null);
    }

    public void send(String name, Integer paid){
        if(paid != null) {
            Localytics.tagEvent(name, values, paid);
        }else{
            Localytics.tagEvent(name, values);
        }
    }
}
