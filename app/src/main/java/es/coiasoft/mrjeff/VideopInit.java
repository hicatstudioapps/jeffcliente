package es.coiasoft.mrjeff;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.manager.worker.UpdateCacheproductWorker;
import es.coiasoft.mrjeff.view.components.MrJeffVieoView;


public class VideopInit extends ConnectionActivity{


    final Handler handler = new Handler();
    private MrJeffVieoView videoView;
    private LinearLayout close_video;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume(){
        super.onResume();
        close_video = (LinearLayout)findViewById(R.id.close_video);
        close_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (videoView != null) {
                        videoView.stopPlayback();
                    }
                }catch (Exception e){
                } finally {
                    next();
                }
            }
        });
        initVideo();

        UpdateCacheproductWorker worker = new UpdateCacheproductWorker(VideopInit.this);
        Thread thread = new Thread(worker);
        thread.start();
    }

    @Override
    protected void setContentViewActivity() {
        setContentView(R.layout.activity_help_video);
    }

    public void initVideo(){

        try {
            videoView = (MrJeffVieoView) findViewById(R.id.videoInit);
            Uri path = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.videoinittren2);
            videoView.setVideoURI(path);

            videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    videoView.setVisibility(View.GONE);
                    return false;
                }
            });
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                }
            });
            videoView.start();
        }catch (Exception e){
            next();
        }
    }

    @Override
    public void startActivity (Intent intent){
        super.startActivity(intent);
        videoView.stopPlayback();
        videoView.clearAnimation();
    }

    public void next(){
        new LocalyticsEventActionMrJeff(getApplicationContext(),MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NEXTVIDEO,Boolean.FALSE,"Correcto");
        Intent myIntent = new Intent(this, HelpInit.class);
        startActivity(myIntent);
    }

    @Override
    public void onBackPressed() {
    }

    protected String getNameScreen(){
        return MrJeffConstant.LOCALYTICS.SCREEN_VIDEO;
    }
}

