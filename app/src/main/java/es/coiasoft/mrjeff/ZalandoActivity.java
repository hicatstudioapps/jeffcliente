package es.coiasoft.mrjeff;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.LanguageUtil;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalytisEvenMrJeff;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.ZalandoCoupon;
import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.domain.orders.send.OrderMeta;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.service.GetZalandoService;
import es.coiasoft.mrjeff.manager.worker.FindProductsWorker;
import es.coiasoft.mrjeff.manager.worker.GetZalandoCouponWorker;
import es.coiasoft.mrjeff.manager.worker.SendOrderWorker;
import es.coiasoft.mrjeff.view.adapter.HolderRecyclerHome;
import es.coiasoft.mrjeff.view.adapter.RecyclerAdapterMrJeff;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;
import es.coiasoft.mrjeff.view.validator.FormValidatorMrJeff;


public class ZalandoActivity extends ConnectionActivity implements Observer {

    protected Activity instance;
    protected TextView btnOk;
    private  TextView text1;
    private  TextView text2;
    private  TextView text3;
    private  TextView textkg;
    private  TextView textprendas;
    private  TextView textkg2;
    private  TextView textprendas2;
    protected EditText coupontext;
    protected Handler handler = new Handler();
    private ProgressDialog progressDialog;
    private Products product;
    private String key = "zalando";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        finishActivity = Boolean.FALSE;
        super.onCreate(savedInstanceState);

    }
    public void initActivity() {
        finishActivity = Boolean.FALSE;
        instance = this;
        btnOk = (TextView) findViewById(R.id.btnok);
        textkg = (TextView) findViewById(R.id.textkg);
        textprendas = (TextView) findViewById(R.id.textprendas);
        text1 = (TextView) findViewById(R.id.text1);
        text2 = (TextView) findViewById(R.id.text2);
        text3 = (TextView) findViewById(R.id.text3);
        textkg2 = (TextView) findViewById(R.id.textkg2);
        textprendas2 = (TextView) findViewById(R.id.textprendas2);
        coupontext = (EditText) findViewById(R.id.coupontext);

    }

    @Override
    public void onResume() {
        super.onResume();
        initActivity();
        initEvent();
        searchKey();
    }

    @Override
    protected void setContentViewActivity() {
        setContentView(R.layout.activity_zalando);
    }


    protected int posMenu() {
        return 4;
    }


    protected String getNameScreen() {
        return MrJeffConstant.LOCALYTICS.SCREEN_ZALANDO;
    }

    private void initEvent() {
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
    }

    private void save() {
        String text = coupontext.getText().toString();

        if (FormValidatorMrJeff.validRequired(text)) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(coupontext.getWindowToken(), 0);
            progressDialog = ProgressDialogMrJeff.createAndStart(this);
            GetZalandoCouponWorker worker = new GetZalandoCouponWorker(text, CustomerStorage.getInstance(this).getCustomer().getId().toString(), this);
            worker.addObserver(this);
            Thread thread = new Thread(worker);
            thread.start();
        }

    }


    public void showError(int message) {
        new android.support.v7.app.AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Theme_AppCompat_Light)).setMessage(message).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                setResult(RESULT_OK);
            }
        }).show();
    }

    public void createOrder(ZalandoCoupon coupon){
        OrderStorage.getInstance(this).setProductMinOrder(null);
        es.coiasoft.mrjeff.domain.orders.send.Order  order = getOrder(coupon);
        progressDialog = ProgressDialogMrJeff.createAndStart(this,R.string.initcard);
        SendOrderWorker worker = new SendOrderWorker(order);
        worker.addObserver(this);
        Thread threadOrder = new Thread(worker);
        threadOrder.start();
    }

    @Override
    public void update(Observable observable, final Object data) {
        if (observable instanceof GetZalandoCouponWorker) {
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            if (data instanceof ZalandoCoupon) {
                                ZalandoCoupon coupon = (ZalandoCoupon) data;
                                switch (coupon.getCodeError()) {
                                    case GetZalandoService.RESULT_OK:
                                        createOrder(coupon);
                                        break;
                                    case GetZalandoService.RESULT_INACTIVO:
                                        showError(R.string.errorzalando2);
                                        break;
                                    case GetZalandoService.RESULT_NOTVALID:
                                        showError(R.string.errorzalando1);
                                        break;
                                    case GetZalandoService.RESULT_USES:
                                        showError(R.string.errorzalando3);
                                        break;
                                    default:
                                        showError(R.string.errorzalando1);
                                        break;
                                }
                            }
                        }
                    });
                }
            });
            threadNotiy.start();
        } else if (observable instanceof SendOrderWorker) {
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            if (OrderStorage.getInstance(getApplicationContext()).getError()) {
                                new android.support.v7.app.AlertDialog.Builder(new ContextThemeWrapper(ZalandoActivity.this, R.style.Theme_AppCompat_Light)).setTitle(R.string.errrorConnection).setMessage(R.string.errrorConnectiontext).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        setResult(RESULT_OK);
                                    }
                                }).show();
                            } else {
                                new LocalytisEvenMrJeff(getApplicationContext(), Boolean.TRUE, MrJeffConstant.LOCALYTICS_EVENTS.NAME.NEW_CART, Boolean.FALSE);
                                Intent show = new Intent(progressDialog.getContext(), OrderAddressActivity.class);
                                show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, false);
                                show.putExtra(MrJeffConstant.INTENT_ATTR.ZALANDO, true);
                                startActivity(show);
                            }
                        }
                    });
                }
            });
            threadNotiy.start();
        } else if(observable instanceof FindProductsWorker){

            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            final ArrayList<Products> products = ProductStorage.getInstance(ZalandoActivity.this).getCategory(key);

                            if (products != null && !products.isEmpty()) {
                                product = products.get(0);
                                setData();
                            }
                        }
                    });
                }
            });
            threadNotiy.start();

        }
    }

    private es.coiasoft.mrjeff.domain.orders.send.Order getOrder(ZalandoCoupon coupon){
        es.coiasoft.mrjeff.domain.orders.send.Order result = OrderStorage.getInstance(getApplicationContext()).getOrderSend();
        OrderStorage.getInstance(getApplicationContext()).setOrder(new Order());
        if(result == null){
            result = new es.coiasoft.mrjeff.domain.orders.send.Order();
        }
        result.setLine_items(new ArrayList<es.coiasoft.mrjeff.domain.orders.send.Line_items>());
        OrderProductsStorage.getInstance(getApplicationContext()).clear();
        es.coiasoft.mrjeff.domain.orders.send.Line_items item = new es.coiasoft.mrjeff.domain.orders.send.Line_items();
        if(product != null) {
            item.setProduct_id(product.getId());
        }
        item.setQuantity(1);
        result.getLine_items().add(item);

        result.setOrder_meta(new OrderMeta());
        result.getOrder_meta().setIssuscription("0");

        OrderStorage.getInstance(getApplicationContext()).setOrderSend(result);
        OrderStorage.getInstance(getApplicationContext()).saveJson();

        return result;
    }

    private void setData(){
        ArrayList<String> attr = product.getAttributeByKey(MrJeffConstant.PRODUCT_ATTR.CLAIM);
        if(attr != null && attr.size()>=1){
            text1.setText(attr.get(0));
        } else {
            text1.setVisibility(View.GONE);
        }
        if(attr != null && attr.size()>=2){
            text2.setText(attr.get(1));
        } else {
            text2.setVisibility(View.GONE);
        }
        if(attr != null && attr.size()>=3){
            text3.setText(attr.get(2));
        } else {
            text3.setVisibility(View.GONE);
        }
        if(attr != null && attr.size()>=4){
            textkg.setText(attr.get(3));
        } else {
            textkg.setVisibility(View.GONE);
        }
        if(attr != null && attr.size()>=5){
            textkg2.setText(attr.get(4));
        } else {
            textkg2.setVisibility(View.GONE);
        }
        if(attr != null && attr.size()>=6){
            textprendas.setText(attr.get(5));
        } else {
            textprendas.setVisibility(View.GONE);
        }
        if(attr != null && attr.size()>=7){
            textprendas2.setText(attr.get(6));
        } else {
            textprendas2.setVisibility(View.GONE);
        }
    }


    private void searchKey(){
        if(key != null) {
            final ArrayList<Products> products = ProductStorage.getInstance(this).getCategory(key);

            if (products != null && !products.isEmpty()) {
                product = products.get(0);
                setData();
            } else {
                if(progressDialog == null) {
                    progressDialog = ProgressDialogMrJeff.createAndStart(this, R.string.searchmywash);
                }else {
                    progressDialog.show();
                }
                FindProductsWorker worker = FindProductsWorker.getInstance(this);
                worker.addObserver(this);
                Thread thread = new Thread(worker);
                thread.start();

            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent show = null;
        show = new Intent(this, HomeActivity.class);
        show.putExtra(MrJeffConstant.INTENT_ATTR.SUSCRIPTION, false);
        show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, 2);
        startActivity(show);
    }
}
