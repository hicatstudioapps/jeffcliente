package es.coiasoft.mrjeff.domain;

import java.util.ArrayList;

public class Attributes {
	
	private String name;
	private String slug;
	private Integer position;
	private Boolean visible;
	private Boolean variation;
	private ArrayList<String> options;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	public Boolean getVisible() {
		return visible;
	}
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	public Boolean getVariation() {
		return variation;
	}
	public void setVariation(Boolean variation) {
		this.variation = variation;
	}
	public ArrayList<String> getOptions() {
		return options;
	}
	public void setOptions(ArrayList<String> options) {
		this.options = options;
	}
	
	public Attributes() {
		super();
	}
	public Attributes(String name, String slug, Integer position,
			Boolean visible, Boolean variation, ArrayList<String> options) {
		super();
		this.name = name;
		this.slug = slug;
		this.position = position;
		this.visible = visible;
		this.variation = variation;
		this.options = options;
	}
	
	
}
