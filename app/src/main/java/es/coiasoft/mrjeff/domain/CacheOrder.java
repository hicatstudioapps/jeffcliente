package es.coiasoft.mrjeff.domain;

/**
 * Created by Paulino on 21/07/2016.
 */
public class CacheOrder {

    private Integer id;
    private String lastupdate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(String lastupdate) {
        this.lastupdate = lastupdate;
    }
}
