package es.coiasoft.mrjeff.domain;

import java.util.ArrayList;

/**
 * Created by linovm on 8/10/15.
 */
public class Catalog {

    private ArrayList<Products> products;

    public ArrayList<Products> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Products> products) {
        this.products = products;
    }

    public Catalog(ArrayList<Products> products) {
        this.products = products;
    }

    public Catalog() {

    }
}
