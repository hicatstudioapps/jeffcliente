package es.coiasoft.mrjeff.domain;

/**
 * Created by linovm on 6/11/15.
 */
public class ChargeStripe {

    private String stripeAmount;
    private String stripeCurrency;
    private String stripeToken;
    private String stripeDescription;
    private String suscripcion;
    private String email;
    private String coupon;
    private String discount;
    private int haveSuscription;
    private int typeSuscription;
    private String idSuscription;




    public String getStripeAmount() {
        return stripeAmount;
    }

    public void setStripeAmount(String stripeAmount) {
        this.stripeAmount = stripeAmount;
    }

    public String getStripeCurrency() {
        return stripeCurrency;
    }

    public void setStripeCurrency(String stripeCurrency) {
        this.stripeCurrency = stripeCurrency;
    }

    public String getStripeToken() {
        return stripeToken;
    }

    public void setStripeToken(String stripeToken) {
        this.stripeToken = stripeToken;
    }

    public String getStripeDescription() {
        return stripeDescription;
    }

    public void setStripeDescription(String stripeDescription) {
        this.stripeDescription = stripeDescription;
    }

    public String getSuscripcion() {
        return suscripcion;
    }

    public void setSuscripcion(String suscripcion) {
        this.suscripcion = suscripcion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public int getHaveSuscription() {
        return haveSuscription;
    }

    public void setHaveSuscription(int haveSuscription) {
        this.haveSuscription = haveSuscription;
    }

    public int getTypeSuscription() {
        return typeSuscription;
    }

    public void setTypeSuscription(int typeSuscription) {
        this.typeSuscription = typeSuscription;
    }

    public String getIdSuscription() {
        return idSuscription;
    }

    public void setIdSuscription(String idSuscription) {
        this.idSuscription = idSuscription;
    }
}
