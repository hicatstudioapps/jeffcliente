package es.coiasoft.mrjeff.domain;

import java.io.Serializable;
import java.util.ArrayList;

import es.coiasoft.mrjeff.domain.orders.response.Billing_address;
import es.coiasoft.mrjeff.domain.orders.response.Shipping_address;

/**
 * Created by linovm on 3/12/15.
 */
public class Customer implements Serializable{

    private Integer id;
    private String created_at;
    private String email;
    private String first_name;
    private String last_name;
    private String username;
    private String password;
    private Integer last_order_id;
    private String last_order_date;
    private Integer orders_count;
    private String total_spent;
    private String avatar_url;
    private Billing_address billing_address;
    private Shipping_address shipping_address;

    public Integer getId() {
//        return 393;
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getLast_order_id() {
        return last_order_id;
    }

    public void setLast_order_id(Integer last_order_id) {
        this.last_order_id = last_order_id;
    }

    public String getLast_order_date() {
        return last_order_date;
    }

    public void setLast_order_date(String last_order_date) {
        this.last_order_date = last_order_date;
    }

    public Integer getOrders_count() {
        return orders_count;
    }

    public void setOrders_count(Integer orders_count) {
        this.orders_count = orders_count;
    }

    public String getTotal_spent() {
        return total_spent;
    }

    public void setTotal_spent(String total_spent) {
        this.total_spent = total_spent;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public Billing_address getBilling_address() {
        return billing_address;
    }

    public void setBilling_address(Billing_address billing_address) {
        this.billing_address = billing_address;
    }

    public Shipping_address getShipping_address() {
        return shipping_address;
    }

    public void setShipping_address(Shipping_address shipping_address) {
        this.shipping_address = shipping_address;
    }
}
