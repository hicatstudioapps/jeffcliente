package es.coiasoft.mrjeff.domain;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;

/**
 * Created by Paulino on 27/03/2016.
 */
public class DatetimeMrJeff {


    private String city;
    private String postalCode;
    private String day;
    private String ordersClosing;
    private String pickupTimetable;
    private String dropoffTimetable;
    private String minTiming;


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getOrdersClosing() {
        return ordersClosing;
    }

    public void setOrdersClosing(String ordersClosing) {
        this.ordersClosing = ordersClosing;
    }

    public String getPickupTimetable() {
        return pickupTimetable;
    }

    public void setPickupTimetable(String pickupTimetable) {
        this.pickupTimetable = pickupTimetable;
    }

    public String getDropoffTimetable() {
        return dropoffTimetable;
    }

    public void setDropoffTimetable(String dropoffTimetable) {
        this.dropoffTimetable = dropoffTimetable;
    }

    public String getMinTiming() {
        return minTiming;
    }

    public void setMinTiming(String minTiming) {
        this.minTiming = minTiming;
    }

    public String[] hourClosing(){
        String [] hours = null;

        if(ordersClosing!=null) {
            hours = ordersClosing.split(";");
        }

        return hours;
    }


    public String[] hourPickUp(){
        String [] hours = null;

        if(pickupTimetable!=null) {
            hours = pickupTimetable.split(";");
        }

        return hours;
    }


    public String[] hourDropoff(){
        String [] hours = null;

        if(dropoffTimetable!=null) {
            hours = dropoffTimetable.split(";");
        }

        return hours;
    }

    public Boolean validDate(int type){
        if(MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND == type){
            return hourDropoff() != null && hourDropoff().length > 0;
        }else {
            return hourPickUp() != null && hourPickUp().length > 0;
        }
    }

    public Boolean validHour(int type,String hour){
        if(MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND == type){
            return getDropoffTimetable() != null && getDropoffTimetable().contains(hour);
        }else {
            return getPickupTimetable() != null && getPickupTimetable().contains(hour);
        }
    }

    public String[] getHoursType(int type){
        if(MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND == type){
            return hourDropoff();
        }else {
            return hourPickUp();
        }
    }

    public int minDays(){
        int result = 2;

        if(minTiming != null && !minTiming.isEmpty()){
            try {
                result = Integer.valueOf(minTiming).intValue();
                if(result <= 0){
                    result = 2;
                }
            }catch (Exception e){

            }
        }

        return result;
    }



}
