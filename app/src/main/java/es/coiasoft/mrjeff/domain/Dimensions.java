package es.coiasoft.mrjeff.domain;

public class Dimensions {
	
	private String length;
	private String width;
	private String height;
	private String unit;
	
	public String getLength() {
		return length;
	}
	public void setLength(String length) {
		this.length = length;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public Dimensions(String length, String width, String height, String unit) {
		super();
		this.length = length;
		this.width = width;
		this.height = height;
		this.unit = unit;
	}
	public Dimensions() {
		super();
	}
	
	
}
