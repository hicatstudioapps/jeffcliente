package es.coiasoft.mrjeff.domain;

import com.google.gson.annotations.SerializedName;

import es.coiasoft.mrjeff.R;

/**
 * Created by Paulino on 14/05/2016.
 */
public enum EnumStateOrder {
    @SerializedName("0")
    CONFIRM(0, R.string.stateconfirm, R.drawable.process1bis),
    @SerializedName("1")
    PICKUP(1, R.string.statepickup, R.drawable.recogida),
    @SerializedName("2")
    WASHING(2, R.string.statewashing, R.drawable.lavando),
    @SerializedName("3")
    DELIVERY(3, R.string.statedelivery, R.drawable.entrega),
    @SerializedName("4")
    FINALLY(4, R.string.statefinalizada, R.drawable.confirmation);

    private int state;
    private int title;
    private int image;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }
    public void setImage(int image) {
        this.image = image;
    }
    EnumStateOrder(int state, int title, int image) {
        this.state = state;
        this.title = title;
        this.image = image;
    }
}
