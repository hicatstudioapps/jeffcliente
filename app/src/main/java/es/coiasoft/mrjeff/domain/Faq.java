package es.coiasoft.mrjeff.domain;

/**
 * Created by Paulino on 16/02/2016.
 */
public class Faq {

    private String question;
    private String answer;


    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
