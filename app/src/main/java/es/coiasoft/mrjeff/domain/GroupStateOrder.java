package es.coiasoft.mrjeff.domain;

import java.util.ArrayList;

import es.coiasoft.mrjeff.domain.orders.response.Orders;

/**
 * Created by Paulino on 21/01/2016.
 */
public class GroupStateOrder implements Comparable<GroupStateOrder>{

    private EnumStateOrder state;
    private ArrayList<Orders> orders;

    public EnumStateOrder getState() {
        return state;
    }

    public void setState(EnumStateOrder state) {
        this.state = state;
    }

    public ArrayList<Orders> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Orders> orders) {
        this.orders = orders;
    }

    @Override
    public int compareTo(GroupStateOrder another) {
        int result = 0;
        if(state.getState() < another.state.getState()){
            result = 1;
        } else if(state.getState() > another.state.getState()){
            result = -1;
        } else {
           result = 0;
        }

        return result * (-1);
    }
}
