package es.coiasoft.mrjeff.domain;

public class Images {

	private String id;
	private String created_at;
	private String updated_at;
	private String src;
	private String title;
	private String alt;
	private Integer position;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAlt() {
		return alt;
	}
	public void setAlt(String alt) {
		this.alt = alt;
	}
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	

	public Images() {
		super();
	}
	public Images(String id, String created_at, String updated_at, String src,
			String title, String alt, Integer position) {
		super();
		this.id = id;
		this.created_at = created_at;
		this.updated_at = updated_at;
		this.src = src;
		this.title = title;
		this.alt = alt;
		this.position = position;
	}
	
	
	
	
}
