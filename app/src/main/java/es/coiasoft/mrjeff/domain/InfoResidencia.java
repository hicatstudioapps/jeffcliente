package es.coiasoft.mrjeff.domain;

/**
 * Created by Paulino on 20/08/2016.
 */
public class InfoResidencia {

    private String codigo;
    private String residenceName;
    private String address;
    private String number;
    private String postalCode;
    private String city;
    private String pickupDay;
    private String pickupHour;
    private String dropoffDay;
    private String dropoffHour;
    private String discountNormalOrder;
    private String needPayment;
    private String suscriptionDescription;
    private String cupon;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getResidenceName() {
        return residenceName;
    }

    public void setResidenceName(String residenceName) {
        this.residenceName = residenceName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPickupDay() {
        return pickupDay;
    }

    public void setPickupDay(String pickupDay) {
        this.pickupDay = pickupDay;
    }

    public String getPickupHour() {
        return pickupHour;
    }

    public void setPickupHour(String pickupHour) {
        this.pickupHour = pickupHour;
    }

    public String getDropoffDay() {
        return dropoffDay;
    }

    public void setDropoffDay(String dropoffDay) {
        this.dropoffDay = dropoffDay;
    }

    public String getDropoffHour() {
        return dropoffHour;
    }

    public void setDropoffHour(String dropoffHour) {
        this.dropoffHour = dropoffHour;
    }

    public String getDiscountNormalOrder() {
        return discountNormalOrder;
    }

    public void setDiscountNormalOrder(String discountNormalOrder) {
        this.discountNormalOrder = discountNormalOrder;
    }

    public String getNeedPayment() {
        return needPayment;
    }

    public void setNeedPayment(String needPayment) {
        this.needPayment = needPayment;
    }

    public String getSuscriptionDescription() {
        return suscriptionDescription;
    }

    public void setSuscriptionDescription(String suscriptionDescription) {
        this.suscriptionDescription = suscriptionDescription;
    }

    public String getCupon() {
        return cupon;
    }

    public void setCupon(String cupon) {
        this.cupon = cupon;
    }
}
