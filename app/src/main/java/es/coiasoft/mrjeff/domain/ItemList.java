package es.coiasoft.mrjeff.domain;

public class ItemList {

    private int image;
    private String title;
    private int id;

    public ItemList() {
        super();
    }

    public ItemList(int image, String title, int id) {
        super();
        this.image = image;
        this.title = title;
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
