package es.coiasoft.mrjeff.domain;

/**
 * Created by Paulino on 08/03/2016.
 */
public class LocationAddress {

    private String address;
    private String postalCode;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
