package es.coiasoft.mrjeff.domain;

import es.coiasoft.mrjeff.domain.Customer;

/**
 * Created by linovm on 3/12/15.
 */
public class MessageCustomer {

    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
