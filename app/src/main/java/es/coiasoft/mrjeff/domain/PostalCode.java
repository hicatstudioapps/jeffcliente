package es.coiasoft.mrjeff.domain;

/**
 * Created by linovm on 7/10/15.
 */
public class PostalCode {

    private String pais;
    private String ciudad;
    private String codigo;


    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }


    public PostalCode(String codigo, String ciudad, String pais) {
        this.pais = pais;
        this.ciudad = ciudad;
        this.codigo = codigo;
    }

    public PostalCode() {
    }
}
