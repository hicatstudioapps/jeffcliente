package es.coiasoft.mrjeff.domain;

import android.util.Log;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Products implements Comparable<Products>{

	 private String title;
	 private Integer id;
	 private String created_at;
	 private String updated_at;
	 private String type;
	 private String status;
	 private Boolean downloadable;
	 private Boolean virtual;
	 private String permalink;
	 private String sku;
	 private String price;
	 private String regular_price;
	 private String sale_price;
	 private String price_html;
	 private Boolean taxable;
	 private String tax_status;
	 private String tax_class;
	 private Boolean managing_stock;
	 private Integer stock_quantity;
	 private Boolean in_stock;
	 private Boolean backorders_allowed;
	 private Boolean backordered;
	 private Boolean sold_individually;
	 private Boolean purchaseable;
	 private Boolean featured;
	 private Boolean visible;
	 private String catalog_visibility;
	 private Boolean on_sale;
	 private String weight;
	 private Dimensions dimensions;
	 private Boolean shipping_required;
	 private Boolean shipping_taxable;
	 private String shipping_class;
	 private String shipping_class_id;
	 private String description;
	 private String short_description;
	 private Boolean reviews_allowed;
	 private String average_rating;
	 private Integer rating_count;
	 private ArrayList<Integer> related_ids;
	 private ArrayList<Integer> upsell_ids;
	 private ArrayList<Integer> cross_sell_ids;
	 private Integer parent_id;
	 private ArrayList<String> categories;
	 private ArrayList<String> tags;
	 private ArrayList<Images> images;
	 private String featured_src;
	 private ArrayList<Attributes> attributes;
	 private ArrayList<String> downloads;
	 private Integer download_limit;
	 private Integer download_expiry;
	 private String download_type;
	 private String purchase_note;
	 private Integer total_sales;
	 private ArrayList<Images> variations;
	 private ArrayList<String> parent;
	
	 
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getDownloadable() {
		return downloadable;
	}
	public void setDownloadable(Boolean downloadable) {
		this.downloadable = downloadable;
	}
	public Boolean getVirtual() {
		return virtual;
	}
	public void setVirtual(Boolean virtual) {
		this.virtual = virtual;
	}
	public String getPermalink() {
		return permalink;
	}
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getRegular_price() {
		return regular_price;
	}
	public void setRegular_price(String regular_price) {
		this.regular_price = regular_price;
	}
	public String getSale_price() {
		return sale_price;
	}
	public void setSale_price(String sale_price) {
		this.sale_price = sale_price;
	}
	public String getPrice_html() {
		return price_html;
	}
	public void setPrice_html(String price_html) {
		this.price_html = price_html;
	}
	public Boolean getTaxable() {
		return taxable;
	}
	public void setTaxable(Boolean taxable) {
		this.taxable = taxable;
	}
	public String getTax_status() {
		return tax_status;
	}
	public void setTax_status(String tax_status) {
		this.tax_status = tax_status;
	}
	public String getTax_class() {
		return tax_class;
	}
	public void setTax_class(String tax_class) {
		this.tax_class = tax_class;
	}
	public Boolean getManaging_stock() {
		return managing_stock;
	}
	public void setManaging_stock(Boolean managing_stock) {
		this.managing_stock = managing_stock;
	}
	public Integer getStock_quantity() {
		return stock_quantity;
	}
	public void setStock_quantity(Integer stock_quantity) {
		this.stock_quantity = stock_quantity;
	}
	public Boolean getIn_stock() {
		return in_stock;
	}
	public void setIn_stock(Boolean in_stock) {
		this.in_stock = in_stock;
	}
	public Boolean getBackorders_allowed() {
		return backorders_allowed;
	}
	public void setBackorders_allowed(Boolean backorders_allowed) {
		this.backorders_allowed = backorders_allowed;
	}
	public Boolean getBackordered() {
		return backordered;
	}
	public void setBackordered(Boolean backordered) {
		this.backordered = backordered;
	}
	public Boolean getSold_individually() {
		return sold_individually;
	}
	public void setSold_individually(Boolean sold_individually) {
		this.sold_individually = sold_individually;
	}
	public Boolean getPurchaseable() {
		return purchaseable;
	}
	public void setPurchaseable(Boolean purchaseable) {
		this.purchaseable = purchaseable;
	}
	public Boolean getFeatured() {
		return featured;
	}
	public void setFeatured(Boolean featured) {
		this.featured = featured;
	}
	public Boolean getVisible() {
		return visible;
	}
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	public String getCatalog_visibility() {
		return catalog_visibility;
	}
	public void setCatalog_visibility(String catalog_visibility) {
		this.catalog_visibility = catalog_visibility;
	}
	public Boolean getOn_sale() {
		return on_sale;
	}
	public void setOn_sale(Boolean on_sale) {
		this.on_sale = on_sale;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public Dimensions getDimensions() {
		return dimensions;
	}
	public void setDimensions(Dimensions dimensions) {
		this.dimensions = dimensions;
	}
	public Boolean getShipping_required() {
		return shipping_required;
	}
	public void setShipping_required(Boolean shipping_required) {
		this.shipping_required = shipping_required;
	}
	public Boolean getShipping_taxable() {
		return shipping_taxable;
	}
	public void setShipping_taxable(Boolean shipping_taxable) {
		this.shipping_taxable = shipping_taxable;
	}
	public String getShipping_class() {
		return shipping_class;
	}
	public void setShipping_class(String shipping_class) {
		this.shipping_class = shipping_class;
	}
	public String getShipping_class_id() {
		return shipping_class_id;
	}
	public void setShipping_class_id(String shipping_class_id) {
		this.shipping_class_id = shipping_class_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getShort_description() {
		return short_description;
	}
	public void setShort_description(String short_description) {
		this.short_description = short_description;
	}
	public Boolean getReviews_allowed() {
		return reviews_allowed;
	}
	public void setReviews_allowed(Boolean reviews_allowed) {
		this.reviews_allowed = reviews_allowed;
	}
	public String getAverage_rating() {
		return average_rating;
	}
	public void setAverage_rating(String average_rating) {
		this.average_rating = average_rating;
	}
	public Integer getRating_count() {
		return rating_count;
	}
	public void setRating_count(Integer rating_count) {
		this.rating_count = rating_count;
	}
	public ArrayList<Integer> getRelated_ids() {
		return related_ids;
	}
	public void setRelated_ids(ArrayList<Integer> related_ids) {
		this.related_ids = related_ids;
	}
	public ArrayList<Integer> getUpsell_ids() {
		return upsell_ids;
	}
	public void setUpsell_ids(ArrayList<Integer> upsell_ids) {
		this.upsell_ids = upsell_ids;
	}
	public ArrayList<Integer> getCross_sell_ids() {
		return cross_sell_ids;
	}
	public void setCross_sell_ids(ArrayList<Integer> cross_sell_ids) {
		this.cross_sell_ids = cross_sell_ids;
	}
	public Integer getParent_id() {
		return parent_id;
	}
	public void setParent_id(Integer parent_id) {
		this.parent_id = parent_id;
	}
	public ArrayList<String> getCategories() {
		return categories;
	}
	public void setCategories(ArrayList<String> categories) {
		this.categories = categories;
	}
	public ArrayList<String> getTags() {
		return tags;
	}
	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}
	public ArrayList<Images> getImages() {
		return images;
	}
	public void setImages(ArrayList<Images> images) {
		this.images = images;
	}
	public String getFeatured_src() {
		return featured_src;
	}
	public void setFeatured_src(String featured_src) {
		this.featured_src = featured_src;
	}
	public ArrayList<Attributes> getAttributes() {
		return attributes;
	}
	public void setAttributes(ArrayList<Attributes> attributes) {
		this.attributes = attributes;
	}
	public ArrayList<String> getDownloads() {
		return downloads;
	}
	public void setDownloads(ArrayList<String> downloads) {
		this.downloads = downloads;
	}
	public Integer getDownload_limit() {
		return download_limit;
	}
	public void setDownload_limit(Integer download_limit) {
		this.download_limit = download_limit;
	}
	public Integer getDownload_expiry() {
		return download_expiry;
	}
	public void setDownload_expiry(Integer download_expiry) {
		this.download_expiry = download_expiry;
	}
	public String getDownload_type() {
		return download_type;
	}
	public void setDownload_type(String download_type) {
		this.download_type = download_type;
	}
	public String getPurchase_note() {
		return purchase_note;
	}
	public void setPurchase_note(String purchase_note) {
		this.purchase_note = purchase_note;
	}
	public Integer getTotal_sales() {
		return total_sales;
	}
	public void setTotal_sales(Integer total_sales) {
		this.total_sales = total_sales;
	}
	public ArrayList<Images> getVariations() {
		return variations;
	}
	public void setVariations(ArrayList<Images> variations) {
		this.variations = variations;
	}
	public ArrayList<String> getParent() {
		return parent;
	}
	public void setParent(ArrayList<String> parent) {
		this.parent = parent;
	}
	
	public float getPriceFloat(){
		float result = 0f;
		if(price != null && !price.isEmpty()){
			try{
				result = Float.valueOf(price);
			} catch(Exception e){
				
			}
		}
		
		return result;
	}
	
	public float getRegularPriceFloat(){
		float result = 0f;
		if(regular_price != null && !regular_price.isEmpty()){
			try{
				result = Float.valueOf(regular_price);
			} catch(Exception e){
				
			}
		}
		
		return result;
	}
	
	public float getSalePriceFloat(){
		float result = 0f;
		if(sale_price != null && !sale_price.isEmpty()){
			try{
				result = Float.valueOf(sale_price);
			} catch(Exception e){
				
			}
		}
		
		return result;
	}
	
	public Products() {
		super();
	}
	
	
	public Products(String title, Integer id, String created_at,
					String updated_at, String type, String status,
					Boolean downloadable, Boolean virtual, String permalink,
					String sku, String price, String regular_price, String sale_price,
					String price_html, Boolean taxable, String tax_status,
					String tax_class, Boolean managing_stock, Integer stock_quantity,
					Boolean in_stock, Boolean backorders_allowed, Boolean backordered,
					Boolean sold_individually, Boolean purchaseable, Boolean featured,
					Boolean visible, String catalog_visibility, Boolean on_sale,
					String weight, Dimensions dimensions, Boolean shipping_required,
					Boolean shipping_taxable, String shipping_class,
					String shipping_class_id, String description,
					String short_description, Boolean reviews_allowed,
					String average_rating, Integer rating_count,
					ArrayList<Integer> related_ids, ArrayList<Integer> upsell_ids,
					ArrayList<Integer> cross_sell_ids, Integer parent_id,
					ArrayList<String> categories, ArrayList<String> tags,
					ArrayList<Images> images, String featured_src,
					ArrayList<Attributes> attributes, ArrayList<String> downloads,
					Integer download_limit, Integer download_expiry,
					String download_type, String purchase_note, Integer total_sales,
					ArrayList<Images> variations, ArrayList<String> parent) {
		super();
		this.title = title;
		this.id = id;
		this.created_at = created_at;
		this.updated_at = updated_at;
		this.type = type;
		this.status = status;
		this.downloadable = downloadable;
		this.virtual = virtual;
		this.permalink = permalink;
		this.sku = sku;
		this.price = price;
		this.regular_price = regular_price;
		this.sale_price = sale_price;
		this.price_html = price_html;
		this.taxable = taxable;
		this.tax_status = tax_status;
		this.tax_class = tax_class;
		this.managing_stock = managing_stock;
		this.stock_quantity = stock_quantity;
		this.in_stock = in_stock;
		this.backorders_allowed = backorders_allowed;
		this.backordered = backordered;
		this.sold_individually = sold_individually;
		this.purchaseable = purchaseable;
		this.featured = featured;
		this.visible = visible;
		this.catalog_visibility = catalog_visibility;
		this.on_sale = on_sale;
		this.weight = weight;
		this.dimensions = dimensions;
		this.shipping_required = shipping_required;
		this.shipping_taxable = shipping_taxable;
		this.shipping_class = shipping_class;
		this.shipping_class_id = shipping_class_id;
		this.description = description;
		this.short_description = short_description;
		this.reviews_allowed = reviews_allowed;
		this.average_rating = average_rating;
		this.rating_count = rating_count;
		this.related_ids = related_ids;
		this.upsell_ids = upsell_ids;
		this.cross_sell_ids = cross_sell_ids;
		this.parent_id = parent_id;
		this.categories = categories;
		this.tags = tags;
		this.images = images;
		this.featured_src = featured_src;
		this.attributes = attributes;
		this.downloads = downloads;
		this.download_limit = download_limit;
		this.download_expiry = download_expiry;
		this.download_type = download_type;
		this.purchase_note = purchase_note;
		this.total_sales = total_sales;
		this.variations = variations;
		this.parent = parent;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Products products = (Products) o;

		return created_at.equals(products.created_at);

	}

	@Override
	public int hashCode() {
		return created_at.hashCode();
	}

	@Override
	public int compareTo(Products another) {

		if(this.created_at == null && (another == null || another.getCreated_at() == null)){
			return 0;
		} else if(this.created_at == null){
			return 1;
		} else if(another == null || another.getCreated_at() == null){
			return -1;
		} else {
			Date source = null;
			Date target = null;

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			try {
				source = format.parse(this.created_at.toLowerCase().replace("t", " ").replace("z", ""));
			}catch (Exception e){
				Log.e("Products","Error parseando fecha de creación",e);
			}

			try {
				target = format.parse(another.getCreated_at().toLowerCase().replace("t", " ").replace("z",""));
			}catch (Exception e){
				Log.e("Products","Error parseando fecha de creación",e);
			}

			if(source == null && target== null){
				return 0;
			} else if(source == null){
				return 1;
			} else if(target == null){
				return -1;
			} else {
				return source.compareTo(target) * (-1);
			}
		}
	}

	public String getUrlImage(){
		String result = null;
		if (getImages() != null && !getImages().isEmpty()
				&& getImages().get(0).getSrc() != null) {

			Boolean find = Boolean.FALSE;
			try {
				if (getAttributes() != null && !getAttributes().isEmpty()) {
					ArrayList<String> options = getAttributeByKey("URLimagenandroid");
					if (options!= null && !options.isEmpty()) {
						result = options.get(0);
						find = Boolean.TRUE;
					}
				}
			} catch (Exception e) {

			}

			if (!find) {
				result = getImages().get(0).getSrc();
			}
		}

		return result;
	}


	public ArrayList<String> getAttributeByKey(String key){
		ArrayList<String> result = null;
		if (getAttributes() != null && !getAttributes().isEmpty()) {
			for (Attributes attribute : getAttributes()) {
				if (key.toLowerCase().equals(attribute.getName().toLowerCase().trim())) {
					if (attribute.getOptions() != null && !attribute.getOptions().isEmpty()) {
						result = attribute.getOptions();
					}
				}
			}
		}

		return result;
	}
}
