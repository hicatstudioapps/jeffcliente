package es.coiasoft.mrjeff.domain;

/**
 * Created by linovm on 22/11/15.
 */
public class ResultCoupon {

    private int codeResponse;
    private String amount;
    private String code;
    private Float discount;

    public int getCodeResponse() {
        return codeResponse;
    }

    public void setCodeResponse(int codeResponse) {
        this.codeResponse = codeResponse;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public Float getDiscount() {
        return discount;
    }
    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public ResultCoupon(int codeResponse, String amount) {
        this.codeResponse = codeResponse;
        this.amount = amount;
    }
}
