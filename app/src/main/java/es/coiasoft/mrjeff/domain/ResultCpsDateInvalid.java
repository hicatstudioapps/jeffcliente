package es.coiasoft.mrjeff.domain;

import java.util.Date;

/**
 * Created by linovm on 18/12/15.
 */
public class ResultCpsDateInvalid {

    private String diafestivo;
    private String codigo;

    public String getDiafestivo() {
        return diafestivo;
    }

    public void setDiafestivo(String diafestivo) {
        this.diafestivo = diafestivo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }


}
