package es.coiasoft.mrjeff.domain;

import java.util.ArrayList;

import es.coiasoft.mrjeff.view.validator.DateTimeMrJeff;

/**
 * Created by Paulino on 11/05/2016.
 */
public class ResultDatetimeMrJeff {

    private String date;
    private ArrayList<DatetimeMrJeff> timetable;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<DatetimeMrJeff> getTimetable() {
        return timetable;
    }

    public void setTimetable(ArrayList<DatetimeMrJeff> timetable) {
        this.timetable = timetable;
    }
}
