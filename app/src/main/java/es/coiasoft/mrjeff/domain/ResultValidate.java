package es.coiasoft.mrjeff.domain;

/**
 * Created by linovm on 5/11/15.
 */
public class ResultValidate {

    public static final String STATE_OK = "1";

    private String estado;
    private String mensaje;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

}
