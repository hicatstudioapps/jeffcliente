package es.coiasoft.mrjeff.domain;

/**
 * Created by Paulino on 30/05/2016.
 */
public class StatusOrder {

    private String id;
    private String estado;
    private int idstatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getIdstatus() {
        return idstatus;
    }

    public void setIdstatus(int idstatus) {
        this.idstatus = idstatus;
    }
}
