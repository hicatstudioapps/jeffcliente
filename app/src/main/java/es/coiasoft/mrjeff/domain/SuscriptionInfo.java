package es.coiasoft.mrjeff.domain;

/**
 * Created by Paulino on 30/08/2016.
 */
public class SuscriptionInfo {

    private String estado;
    private String mensaje;
    private String idSuscription;
    private String orderId;
    private String actualkilos;
    private String actualOrders;
    private String actualShirts;
    private String endDate;
    private String suscriptionType;
    private String defaultDay;
    private String defaultHour;
    private String maxKilos;
    private String maxOrders;
    private String maxShirts;
    private Integer ordervisitstext;
    private Integer ordersinteraction;
    private String defaultAddress;
    private String defaultPostalCode;
    private String dayComplete;
    private String onlyDelivery;


    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getIdSuscription() {
        return idSuscription;
    }

    public void setIdSuscription(String idSuscription) {
        this.idSuscription = idSuscription;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getActualkilos() {
        return actualkilos;
    }

    public void setActualkilos(String actualkilos) {
        this.actualkilos = actualkilos;
    }

    public String getActualOrders() {
        return actualOrders;
    }

    public void setActualOrders(String actualOrders) {
        this.actualOrders = actualOrders;
    }

    public String getActualShirts() {
        return actualShirts;
    }

    public void setActualShirts(String actualShirts) {
        this.actualShirts = actualShirts;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getSuscriptionType() {
        return suscriptionType;
    }

    public void setSuscriptionType(String suscriptionType) {
        this.suscriptionType = suscriptionType;
    }

    public String getDefaultDay() {
        return defaultDay;
    }

    public void setDefaultDay(String defaultDay) {
        this.defaultDay = defaultDay;
    }

    public String getDefaultHour() {
        return defaultHour;
    }

    public void setDefaultHour(String defaultHour) {
        this.defaultHour = defaultHour;
    }

    public String getMaxKilos() {
        return maxKilos;
    }

    public void setMaxKilos(String maxKilos) {
        this.maxKilos = maxKilos;
    }

    public String getMaxOrders() {
        return maxOrders;
    }

    public void setMaxOrders(String maxOrders) {
        this.maxOrders = maxOrders;
    }

    public String getMaxShirts() {
        return maxShirts;
    }

    public void setMaxShirts(String maxShirts) {
        this.maxShirts = maxShirts;
    }

    public Integer getOrdervisitstext() {
        return ordervisitstext;
    }

    public void setOrdervisitstext(Integer ordervisitstext) {
        this.ordervisitstext = ordervisitstext;
    }

    public Integer getOrdersinteraction() {
        return ordersinteraction;
    }

    public void setOrdersinteraction(Integer ordersinteraction) {
        this.ordersinteraction = ordersinteraction;
    }

    public String getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(String defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public String getDefaultPostalCode() {
        return defaultPostalCode;
    }

    public void setDefaultPostalCode(String defaultPostalCode) {
        this.defaultPostalCode = defaultPostalCode;
    }

    public String getDayComplete() {
        return dayComplete;
    }

    public void setDayComplete(String dayComplete) {
        this.dayComplete = dayComplete;
    }

    public String getOnlyDelivery() {
        return onlyDelivery;
    }

    public void setOnlyDelivery(String onlyDelivery) {
        this.onlyDelivery = onlyDelivery;
    }
}
