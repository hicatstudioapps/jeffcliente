package es.coiasoft.mrjeff.domain;

/**
 * Created by admin on 09/03/2017.
 */

public class UpdateResponse {

    String codigo;
    String mensaje;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMemnsaje() {
        return mensaje;
    }

    public void setMemnsaje(String memnsaje) {
        this.mensaje = memnsaje;
    }
}
