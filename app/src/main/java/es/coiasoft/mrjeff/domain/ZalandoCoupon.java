package es.coiasoft.mrjeff.domain;

/**
 * Created by Paulino on 15/09/2016.
 */
public class ZalandoCoupon {

    private String codigo;
    private String couponCode;
    private String idproductespanol;
    private String idproductingles;
    private String mensaje;
    private Integer codeError;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getIdproductespanol() {
        return idproductespanol;
    }

    public void setIdproductespanol(String idproductespanol) {
        this.idproductespanol = idproductespanol;
    }

    public String getIdproductingles() {
        return idproductingles;
    }

    public void setIdproductingles(String idproductingles) {
        this.idproductingles = idproductingles;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Integer getCodeError() {
        return codeError;
    }

    public void setCodeError(Integer codeError) {
        this.codeError = codeError;
    }
}
