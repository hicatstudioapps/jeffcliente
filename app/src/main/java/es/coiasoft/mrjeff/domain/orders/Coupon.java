package es.coiasoft.mrjeff.domain.orders;

/**
 * Created by linovm on 17/11/15.
 */
public class Coupon {

    private String id;
    private String code;
    private String type;
    private String created_at;
    private String updated_at;
    private String amount;
    private Boolean individual_use;
    private String[] product_ids;
    private String[] exclude_product_ids;
    private Integer usage_limit;
    private Integer usage_limit_per_user;
    private Integer limit_usage_to_x_items;
    private Integer usage_count;
    private String expiry_date;
    private Boolean enable_free_shipping;
    private String[] product_category_ids;
    private String[] exclude_product_category_ids;
    private Boolean exclude_sale_items;
    private String minimum_amount;
    private String maximum_amount;
    private String[] customer_emails;
    private String description;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Boolean getIndividual_use() {
        return individual_use;
    }

    public void setIndividual_use(Boolean individual_use) {
        this.individual_use = individual_use;
    }

    public String[] getProduct_ids() {
        return product_ids;
    }

    public void setProduct_ids(String[] product_ids) {
        this.product_ids = product_ids;
    }

    public String[] getExclude_product_ids() {
        return exclude_product_ids;
    }

    public void setExclude_product_ids(String[] exclude_product_ids) {
        this.exclude_product_ids = exclude_product_ids;
    }

    public Integer getUsage_limit() {
        return usage_limit;
    }

    public void setUsage_limit(Integer usage_limit) {
        this.usage_limit = usage_limit;
    }

    public Integer getUsage_limit_per_user() {
        return usage_limit_per_user;
    }

    public void setUsage_limit_per_user(Integer usage_limit_per_user) {
        this.usage_limit_per_user = usage_limit_per_user;
    }

    public Integer getLimit_usage_to_x_items() {
        return limit_usage_to_x_items;
    }

    public void setLimit_usage_to_x_items(Integer limit_usage_to_x_items) {
        this.limit_usage_to_x_items = limit_usage_to_x_items;
    }

    public Integer getUsage_count() {
        return usage_count;
    }

    public void setUsage_count(Integer usage_count) {
        this.usage_count = usage_count;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public Boolean getEnable_free_shipping() {
        return enable_free_shipping;
    }

    public void setEnable_free_shipping(Boolean enable_free_shipping) {
        this.enable_free_shipping = enable_free_shipping;
    }

    public String[] getProduct_category_ids() {
        return product_category_ids;
    }

    public void setProduct_category_ids(String[] product_category_ids) {
        this.product_category_ids = product_category_ids;
    }

    public String[] getExclude_product_category_ids() {
        return exclude_product_category_ids;
    }

    public void setExclude_product_category_ids(String[] exclude_product_category_ids) {
        this.exclude_product_category_ids = exclude_product_category_ids;
    }

    public Boolean getExclude_sale_items() {
        return exclude_sale_items;
    }

    public void setExclude_sale_items(Boolean exclude_sale_items) {
        this.exclude_sale_items = exclude_sale_items;
    }

    public String getMinimum_amount() {
        return minimum_amount;
    }

    public void setMinimum_amount(String minimum_amount) {
        this.minimum_amount = minimum_amount;
    }

    public String getMaximum_amount() {
        return maximum_amount;
    }

    public void setMaximum_amount(String maximum_amount) {
        this.maximum_amount = maximum_amount;
    }

    public String[] getCustomer_emails() {
        return customer_emails;
    }

    public void setCustomer_emails(String[] customer_emails) {
        this.customer_emails = customer_emails;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
