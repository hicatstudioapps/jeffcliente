package es.coiasoft.mrjeff.domain.orders;

/**
 * Created by linovm on 17/11/15.
 */
public class ResultSearchCoupon {

    private Coupon coupon;

    public Coupon getCoupon() {
        return coupon;
    }

    public void setCoupon(Coupon coupon) {
        this.coupon = coupon;
    }
}
