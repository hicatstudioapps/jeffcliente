package es.coiasoft.mrjeff.domain.orders.response;

import java.util.ArrayList;

public class Line_items {

	private Integer id;
	private String subtotal;
	private String subtotal_tax;
	private String total;
	private String total_tax;
	private String price;
	private Integer quantity;
	private String tax_class;
	private String name;
	private Integer product_id;
	private String sku;
	private ArrayList<String> meta;
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public String getSubtotal_tax() {
		return subtotal_tax;
	}
	public void setSubtotal_tax(String subtotal_tax) {
		this.subtotal_tax = subtotal_tax;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getTotal_tax() {
		return total_tax;
	}
	public void setTotal_tax(String total_tax) {
		this.total_tax = total_tax;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getTax_class() {
		return tax_class;
	}
	public void setTax_class(String tax_class) {
		this.tax_class = tax_class;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public ArrayList<String> getMeta() {
		return meta;
	}
	public void setMeta(ArrayList<String> meta) {
		this.meta = meta;
	}
	public Line_items() {
		super();
	}
	public Line_items(Integer id, String subtotal, String subtotal_tax,
			String total, String total_tax, String price, Integer quantity,
			String tax_class, String name, Integer product_id, String sku,
			ArrayList<String> meta) {
		super();
		this.id = id;
		this.subtotal = subtotal;
		this.subtotal_tax = subtotal_tax;
		this.total = total;
		this.total_tax = total_tax;
		this.price = price;
		this.quantity = quantity;
		this.tax_class = tax_class;
		this.name = name;
		this.product_id = product_id;
		this.sku = sku;
		this.meta = meta;
	}
    		
    
    
    
}
