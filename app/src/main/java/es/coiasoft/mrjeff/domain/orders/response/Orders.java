package es.coiasoft.mrjeff.domain.orders.response;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.orders.send.OrderMeta;

public class Orders implements Comparable<Orders>, Serializable {

    private Integer id;
    private Integer order_number;
    private String created_at;
    private String updated_at;
    private String completed_at;
    private String status;
    private String currency;
    private String total;
    private String subtotal;
    private Integer total_line_items_quantity;
    private String total_tax;
    private String total_shipping;
    private String cart_tax;
    private String shipping_tax;
    private String total_discount;
    private String shipping_methods;
    private String note;
    private String customer_ip;
    private String customer_user_agent;
    private Integer customer_id;
    private String view_order_url;
    private Payment_details payment_details;
    private Billing_address billing_address;
    private Shipping_address shipping_address;
    private ArrayList<Line_items> line_items;
    private ArrayList<Shipping_lines> shipping_lines;
    private ArrayList<Tax_lines> tax_lines;
    private ArrayList<Fee_Lines> fee_lines;
    private ArrayList<Coupon_lines> coupon_lines;
    private Customer customer;
    private OrderMeta order_meta;
    private EnumStateOrder stateEnum;
    private boolean haveRate;

    protected Orders(Parcel in) {
        created_at = in.readString();
        updated_at = in.readString();
        completed_at = in.readString();
        status = in.readString();
        currency = in.readString();
        total = in.readString();
        subtotal = in.readString();
        total_tax = in.readString();
        total_shipping = in.readString();
        cart_tax = in.readString();
        shipping_tax = in.readString();
        total_discount = in.readString();
        shipping_methods = in.readString();
        note = in.readString();
        customer_ip = in.readString();
        customer_user_agent = in.readString();
        view_order_url = in.readString();
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrder_number() {
        return order_number;
    }

    public void setOrder_number(Integer order_number) {
        this.order_number = order_number;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCompleted_at() {
        return completed_at;
    }

    public void setCompleted_at(String completed_at) {
        this.completed_at = completed_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public Integer getTotal_line_items_quantity() {
        return total_line_items_quantity;
    }

    public void setTotal_line_items_quantity(Integer total_line_items_quantity) {
        this.total_line_items_quantity = total_line_items_quantity;
    }

    public String getTotal_tax() {
        return total_tax;
    }

    public void setTotal_tax(String total_tax) {
        this.total_tax = total_tax;
    }

    public String getTotal_shipping() {
        return total_shipping;
    }

    public void setTotal_shipping(String total_shipping) {
        this.total_shipping = total_shipping;
    }

    public String getCart_tax() {
        return cart_tax;
    }

    public void setCart_tax(String cart_tax) {
        this.cart_tax = cart_tax;
    }

    public String getShipping_tax() {
        return shipping_tax;
    }

    public void setShipping_tax(String shipping_tax) {
        this.shipping_tax = shipping_tax;
    }

    public String getTotal_discount() {
        return total_discount;
    }

    public void setTotal_discount(String total_discount) {
        this.total_discount = total_discount;
    }

    public String getShipping_methods() {
        return shipping_methods;
    }

    public void setShipping_methods(String shipping_methods) {
        this.shipping_methods = shipping_methods;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCustomer_ip() {
        return customer_ip;
    }

    public void setCustomer_ip(String customer_ip) {
        this.customer_ip = customer_ip;
    }

    public String getCustomer_user_agent() {
        return customer_user_agent;
    }

    public void setCustomer_user_agent(String customer_user_agent) {
        this.customer_user_agent = customer_user_agent;
    }

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public String getView_order_url() {
        return view_order_url;
    }

    public void setView_order_url(String view_order_url) {
        this.view_order_url = view_order_url;
    }

    public Payment_details getPayment_details() {
        return payment_details;
    }

    public void setPayment_details(Payment_details payment_details) {
        this.payment_details = payment_details;
    }

    public Billing_address getBilling_address() {
        return billing_address;
    }

    public void setBilling_address(Billing_address billing_address) {
        this.billing_address = billing_address;
        convertDateFromBackend();
    }

    public Shipping_address getShipping_address() {
        return shipping_address;
    }

    public void setShipping_address(Shipping_address shipping_address) {
        this.shipping_address = shipping_address;
        convertDateFromBackend();
    }

    public ArrayList<Line_items> getLine_items() {
        return line_items;
    }

    public void setLine_items(ArrayList<Line_items> line_items) {
        this.line_items = line_items;
    }

    public ArrayList<Shipping_lines> getShipping_lines() {
        return shipping_lines;
    }

    public void setShipping_lines(ArrayList<Shipping_lines> shipping_lines) {
        this.shipping_lines = shipping_lines;
    }

    public ArrayList<Tax_lines> getTax_lines() {
        return tax_lines;
    }

    public void setTax_lines(ArrayList<Tax_lines> tax_lines) {
        this.tax_lines = tax_lines;
    }

    public ArrayList<Fee_Lines> getFee_lines() {
        return fee_lines;
    }

    public void setFee_lines(ArrayList<Fee_Lines> fee_lines) {
        this.fee_lines = fee_lines;
    }

    public ArrayList<Coupon_lines> getCoupon_lines() {
        return coupon_lines;
    }

    public void setCoupon_lines(ArrayList<Coupon_lines> coupon_lines) {
        this.coupon_lines = coupon_lines;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public OrderMeta getOrder_meta() {
        return order_meta;
    }

    public void setOrder_meta(OrderMeta order_meta) {
        this.order_meta = order_meta;
    }

    public EnumStateOrder getStateEnum() {
        return stateEnum;
    }

    public void setStateEnum(EnumStateOrder stateEnum) {
        this.stateEnum = stateEnum;
    }

    public boolean isHaveRate() {
        return haveRate;
    }

    public void setHaveRate(boolean haveRate) {
        this.haveRate = haveRate;
    }

    public Orders() {
        super();
    }

    public Orders(Integer id, Integer order_number, String created_at,
                  String updated_at, String completed_at, String status,
                  String currency, String total, String subtotal,
                  Integer total_line_items_quantity, String total_tax,
                  String total_shipping, String cart_tax, String shipping_tax,
                  String total_discount, String shipping_methods, String note,
                  String customer_ip, String customer_user_agent,
                  Integer customer_id, String view_order_url,
                  Payment_details payment_details, Billing_address billing_address,
                  Shipping_address shipping_address,
                  ArrayList<Line_items> line_items,
                  ArrayList<Shipping_lines> shipping_lines,
                  ArrayList<Tax_lines> tax_lines, ArrayList<Fee_Lines> fee_lines,
                  ArrayList<Coupon_lines> coupon_lines, Customer customer) {
        super();
        this.id = id;
        this.order_number = order_number;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.completed_at = completed_at;
        this.status = status;
        this.currency = currency;
        this.total = total;
        this.subtotal = subtotal;
        this.total_line_items_quantity = total_line_items_quantity;
        this.total_tax = total_tax;
        this.total_shipping = total_shipping;
        this.cart_tax = cart_tax;
        this.shipping_tax = shipping_tax;
        this.total_discount = total_discount;
        this.shipping_methods = shipping_methods;
        this.note = note;
        this.customer_ip = customer_ip;
        this.customer_user_agent = customer_user_agent;
        this.customer_id = customer_id;
        this.view_order_url = view_order_url;
        this.payment_details = payment_details;
        this.billing_address = billing_address;
        this.shipping_address = shipping_address;
        this.line_items = line_items;
        this.shipping_lines = shipping_lines;
        this.tax_lines = tax_lines;
        this.fee_lines = fee_lines;
        this.coupon_lines = coupon_lines;
        this.customer = customer;
    }


    @Override
    public int compareTo(Orders another) {
        if (this.created_at == null && (another == null || another.getCreated_at() == null)) {
            return 0;
        } else if (this.created_at == null) {
            return 1;
        } else if (another == null || another.getCreated_at() == null) {
            return -1;
        } else {
            Date source = null;
            Date target = null;

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            try {
                source = format.parse(this.created_at.toLowerCase().replace("t", " ").replace("z", ""));
            } catch (Exception e) {
                Log.e("Products", "Error parseando fecha de creación", e);
            }

            try {
                target = format.parse(another.getCreated_at().toLowerCase().replace("t", " ").replace("z", ""));
            } catch (Exception e) {
                Log.e("Products", "Error parseando fecha de creación", e);
            }

            if (source == null && target == null) {
                return 0;
            } else if (source == null) {
                return 1;
            } else if (target == null) {
                return -1;
            } else {
                return source.compareTo(target) * (-1);
            }
        }
    }

    public void convertDateToBackend() {
        if (billing_address != null) {
            billing_address.convertDateToBackend();
        }
        if (shipping_address != null) {
            shipping_address.convertDateToBackend();
        }
    }

    public void convertDateFromBackend() {
        if (billing_address != null) {
            billing_address.convertDateFromBackend();
        }
        if (shipping_address != null) {
            shipping_address.convertDateFromBackend();
        }
    }

}
