package es.coiasoft.mrjeff.domain.orders.response;

public class Payment_details {

	private String method_id;
	private String method_title;
	private boolean paid;
	public String getMethod_id() {
		return method_id;
	}
	public void setMethod_id(String method_id) {
		this.method_id = method_id;
	}
	public String getMethod_title() {
		return method_title;
	}
	public void setMethod_title(String method_title) {
		this.method_title = method_title;
	}
	public boolean isPaid() {
		return paid;
	}
	public void setPaid(boolean paid) {
		this.paid = paid;
	}
	
	public Payment_details() {
		super();
	}
	public Payment_details(String method_id, String method_title, boolean paid) {
		super();
		this.method_id = method_id;
		this.method_title = method_title;
		this.paid = paid;
	}
	
	
	
	
}
