package es.coiasoft.mrjeff.domain.orders.response;


/**
 * Created by linovm on 21/10/15.
 */
public class ResponseOrder {

    private Order order;

    public void setOrder(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }

    public ResponseOrder() {
    }

    public ResponseOrder(Order order) {
        this.order = order;
    }
}
