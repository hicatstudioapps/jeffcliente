package es.coiasoft.mrjeff.domain.orders.response;

import java.util.ArrayList;

/**
 * Created by Paulino on 21/01/2016.
 */
public class ResultOrders {
    private ArrayList<Orders> orders;

    public ArrayList<Orders> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Orders> orders) {
        this.orders = orders;
    }
}
