package es.coiasoft.mrjeff.domain.orders.response;

public class Shipping_address {
	
	private String first_name;
	private String last_name;
	private String company;
	private String address_1;
	private String address_2;
	private String city;
	private String state;
	private String postcode;
	private String country;
	
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getAddress_1() {
		return address_1;
	}
	public void setAddress_1(String address_1) {
		this.address_1 = address_1;
	}
	public String getAddress_2() {
		return address_2;
	}
	public void setAddress_2(String address_2) {
		this.address_2 = address_2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Shipping_address() {
		super();
	}
	public Shipping_address(String first_name, String last_name,
			String address_1, String address_2, String city, String state,
			String postcode, String country) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
		this.address_1 = address_1;
		this.address_2 = address_2;
		this.city = city;
		this.state = state;
		this.postcode = postcode;
		this.country = country;
	}

	public Shipping_address(es.coiasoft.mrjeff.domain.orders.send.Shipping_address address) {
		super();
		if (address != null) {
			this.first_name = address.getFirst_name();
			this.last_name = address.getLast_name();
			this.address_1 = address.getAddress_1();
			this.address_2 = address.getAddress_2();
			this.city = address.getCity();
			this.state = address.getState();
			this.postcode = address.getPostcode();
			this.country = address.getCountry();
		}
	}
	
	public void copyBillingAddress(Billing_address billing){
		this.first_name = billing.getFirst_name();
		this.last_name = billing.getLast_name();
		this.address_1 = billing.getAddress_1();
		this.address_2 = billing.getAddress_2();
		this.city = billing.getCity();
		this.state = billing.getState();
		this.postcode = billing.getPostcode();
		this.country = billing.getCountry();
	}

	public void convertDateToBackend(){
		convertFormChar("-", "/");
	}

	public void convertDateFromBackend(){
		convertFormChar("/","-");
	}

	private void convertFormChar(String patterSource, String patternTarget){
		if(address_2 != null){
			String[] parts = address_2.split(" ");
			if(parts != null && parts.length == 2 && parts[1].length() == 10){
				String [] partsDate = parts[0].split(patterSource);
				if(parts != null && parts.length == 3){
					address_2 = partsDate[2] + patternTarget +  partsDate[1] + patternTarget + partsDate[0] + " " + parts[1];
				}
			}
		}
	}
	
	
}
