package es.coiasoft.mrjeff.domain.orders.response;

public class Shipping_lines {

	private Integer id;
	private String method_id;
	private String method_title;
	private String total;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMethod_id() {
		return method_id;
	}
	public void setMethod_id(String method_id) {
		this.method_id = method_id;
	}
	public String getMethod_title() {
		return method_title;
	}
	public void setMethod_title(String method_title) {
		this.method_title = method_title;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public Shipping_lines() {
		super();
	}
	public Shipping_lines(Integer id, String method_id, String method_title,
						  String total) {
		super();
		this.id = id;
		this.method_id = method_id;
		this.method_title = method_title;
		this.total = total;
	}
	
	
	
}
