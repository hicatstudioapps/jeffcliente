package es.coiasoft.mrjeff.domain.orders.response;

public class Tax_lines {

	private Integer id;
	private String rate_id;
	private String code;
	private String title;
	private String total;
	private boolean compound;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRate_id() {
		return rate_id;
	}
	public void setRate_id(String rate_id) {
		this.rate_id = rate_id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public boolean isCompound() {
		return compound;
	}
	public void setCompound(boolean compound) {
		this.compound = compound;
	}
	
	public Tax_lines() {
		super();
	}
	public Tax_lines(Integer id, String rate_id, String code, String title,
			String total, boolean compound) {
		super();
		this.id = id;
		this.rate_id = rate_id;
		this.code = code;
		this.title = title;
		this.total = total;
		this.compound = compound;
	}
	
	
}
