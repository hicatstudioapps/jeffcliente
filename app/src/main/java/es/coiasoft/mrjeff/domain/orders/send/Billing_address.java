package es.coiasoft.mrjeff.domain.orders.send;

public class Billing_address {
	
	private String first_name;
	private String last_name;
	private String address_1;
	private String address_2;
	private String city;
	private String state;
	private String postcode;
	private String country;
	private String email;
	private String phone;
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getAddress_1() {
		return address_1;
	}
	public void setAddress_1(String address_1) {
		this.address_1 = address_1;
	}
	public String getAddress_2() {
		return address_2;
	}
	public void setAddress_2(String address_2) {
		this.address_2 = address_2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Billing_address() {
		super();
	}
	public Billing_address(String first_name, String last_name,
			String address_1, String address_2, String city, String state,
			String postcode, String country, String email, String phone) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
		this.address_1 = address_1;
		this.address_2 = address_2;
		this.city = city;
		this.state = state;
		this.postcode = postcode;
		this.country = country;
		this.email = email;
		this.phone = phone;
	}


	public Billing_address(es.coiasoft.mrjeff.domain.orders.response.Billing_address address) {
		super();
		if(address != null) {
			this.first_name = address.getFirst_name();
			this.last_name = address.getLast_name();
			this.address_1 = address.getAddress_1();
			this.address_2 = address.getAddress_2();
			this.city = address.getCity();
			this.state = address.getState();
			this.postcode = address.getPostcode();
			this.country = address.getCountry();
			this.email = address.getEmail();
			this.phone = address.getPhone();
		}
	}

	public void convertDateToBackend(Order order){
		if(address_2 != null){
			String[] parts = address_2.split(" ");
			if(parts != null && parts.length == 2 && parts[0].length() == 10){
				String [] partsDate = parts[0].split("-");
				if(partsDate != null && partsDate.length == 3){
					OrderMeta metadata = order.getOrder_meta() == null? new OrderMeta():order.getOrder_meta();
					metadata.setMyfield2(partsDate[2] + "-" +  partsDate[1] + "-" + partsDate[0]);
					metadata.setMyfield3(parts[1].replace("-"," - "));
					address_2 = null;
					order.setOrder_meta(metadata);
				}
			}
		}
	}

	public void convertDateFromBackend(Order order){
		if(address_2 != null && address_2.length() > 10) {
			convertFormChar("/", "-", false);
		} else {
			convertFrommMeta(order);
		}
	}

	private void convertFormChar(String patterSource, String patternTarget, boolean space){
		if(address_2 != null){
			String[] parts = address_2.split(" ");
			if(parts != null && ((parts.length == 2 &&space) || (parts.length == 4 && !space)) && parts[0].length() == 10){
				String [] partsDate = parts[0].split(patterSource);
				if(partsDate != null && partsDate.length == 3){
					address_2 = partsDate[2] + patternTarget +  partsDate[1] + patternTarget + partsDate[0] + " " + (space?parts[1].replace("-"," - "):parts[1]+parts[2]+parts[3]);
				}
			}
		}
	}

	private void convertFrommMeta(Order order){
		if(order.getOrder_meta() != null && order.getOrder_meta().getMyfield2() != null && order.getOrder_meta().getMyfield3() != null){
			String hour = order.getOrder_meta().getMyfield3().replace(" - ","-");
			String[] dateParts = order.getOrder_meta().getMyfield2().split("-");
			String date = "";
			if(dateParts != null && dateParts.length == 3){
				date = dateParts[2]+"-"+dateParts[1]+"-"+dateParts[0];
			}

			address_2 = date + " " + hour;
		}
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		Billing_address target = new Billing_address();
		target.setFirst_name(first_name);
		target.setLast_name(last_name);
		target.setAddress_1(address_1);
		target.setAddress_2(address_2);
		target.setCity(city);
		target.setState(state);
		target.setPostcode(postcode);
		target.setCountry(country);
		target.setEmail(email);
		target.setPhone(phone);

		return target;

	}
	
}
