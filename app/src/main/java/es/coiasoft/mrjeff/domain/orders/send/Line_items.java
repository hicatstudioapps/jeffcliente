package es.coiasoft.mrjeff.domain.orders.send;

import android.widget.LinearLayout;

import java.util.ArrayList;

public class Line_items {
	
	private Integer product_id;
	private Integer id;
	private Integer quantity;
	private Float total;
	private Float subtotal;
    private ArrayList<String> variations;

	public Integer getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public ArrayList<String> getVariations() {
		return variations;
	}
	public void setVariations(ArrayList<String> variations) {
		this.variations = variations;
	}
	public Integer getId() {
		return id;
	}


	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public Float getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Float subtotal) {
		this.subtotal = subtotal;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Line_items() {
		super();
	}


	public Line_items(Integer product_id, Integer quantity,
			ArrayList<String> variations) {
		super();
		this.product_id = product_id;
		this.quantity = quantity;
		this.variations = variations;
	}

	public Line_items(es.coiasoft.mrjeff.domain.orders.response.Line_items item) {
		super();
		if(item != null) {
			this.product_id = item.getProduct_id();
			this.quantity = item.getQuantity();
			this.variations = null;
		}
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		Line_items line = new Line_items();
		line.setProduct_id(product_id);
		line.setQuantity(quantity);
		return line;
	}
}
