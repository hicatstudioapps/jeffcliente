package es.coiasoft.mrjeff.domain.orders.send;

import java.util.ArrayList;

import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.stripe.Metadata;

public class Order {

    private Payment_details payment_details;
    private Billing_address billing_address;
    private Shipping_address shipping_address;
    private Integer customer_id;
    private String status;
    private String note;
    private ArrayList<Line_items> line_items;
    private ArrayList<Shipping_lines> shipping_lines;
    private String total_discount;
    private ArrayList<Coupon_lines> coupon_lines;
    private OrderMeta order_meta;

    public Payment_details getPayment_details() {
        return payment_details;
    }

    public void setPayment_details(Payment_details payment_details) {
        this.payment_details = payment_details;
    }

    public Billing_address getBilling_address() {
        return billing_address;
    }

    public void setBilling_address(Billing_address billing_address) {
        this.billing_address = billing_address;
    }

    public Shipping_address getShipping_address() {
        return shipping_address;
    }

    public void setShipping_address(Shipping_address shipping_address) {
        this.shipping_address = shipping_address;
    }

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public ArrayList<Line_items> getLine_items() {
        return line_items;
    }

    public void setLine_items(ArrayList<Line_items> line_items) {
        this.line_items = line_items;
    }

    public ArrayList<Shipping_lines> getShipping_lines() {
        return shipping_lines;
    }

    public void setShipping_lines(ArrayList<Shipping_lines> shipping_lines) {
        this.shipping_lines = shipping_lines;
    }


    public String getTotal_discount() {
        return total_discount;
    }

    public void setTotal_discount(String total_discount) {
        this.total_discount = total_discount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Coupon_lines> getCoupon_lines() {
        return coupon_lines;
    }

    public void setCoupon_lines(ArrayList<Coupon_lines> coupon_lines) {
        this.coupon_lines = coupon_lines;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public OrderMeta getOrder_meta() {
        return order_meta;
    }

    public void setOrder_meta(OrderMeta order_meta) {
        this.order_meta = order_meta;
    }

    public Order() {
        super();
    }

    public Order(Payment_details payment_details,
                 Billing_address billing_address, Shipping_address shipping_address,
                 Integer customer_id, ArrayList<Line_items> line_items,
                 ArrayList<Shipping_lines> shipping_lines) {
        super();
        this.payment_details = payment_details;
        this.billing_address = billing_address;
        this.shipping_address = shipping_address;
        this.customer_id = customer_id;
        this.line_items = line_items;
        this.shipping_lines = shipping_lines;
    }

    public Order(Orders order) {
        this.status = order.getStatus();
        //this.total_discount = order.getTotal_discount();
        this.customer_id = order.getCustomer_id();
        /*if(order.getPayment_details()  != null && order.getPayment_details().getMethod_id() != null) {
			this.payment_details = new Payment_details(order.getPayment_details());
		}*/
        if (order.getBilling_address() != null) {
            this.billing_address = new Billing_address(order.getBilling_address());
        }

        if (order.getShipping_address() != null) {
            this.shipping_address = new Shipping_address(order.getShipping_address());
        }
        if (order.getLine_items() != null && !order.getLine_items().isEmpty()) {
            this.line_items = new ArrayList<Line_items>();
            for (es.coiasoft.mrjeff.domain.orders.response.Line_items item : order.getLine_items()) {
                this.line_items.add(new Line_items(item));
            }
        }

        if (order.getCoupon_lines() != null && !order.getCoupon_lines().isEmpty()) {
            this.coupon_lines = new ArrayList<Coupon_lines>();
            for (es.coiasoft.mrjeff.domain.orders.response.Coupon_lines item : order.getCoupon_lines()) {
                this.coupon_lines.add(new Coupon_lines(item));
            }
        }
    }


    public void convertDateToBackend() {
        if (billing_address != null) {
            billing_address.convertDateToBackend(this);
        }
        if (shipping_address != null) {
            shipping_address.convertDateToBackend(this);
        }
    }

    public void convertDateFromBackend() {
        if (billing_address != null) {
            billing_address.convertDateFromBackend(this);
        }
        if (shipping_address != null) {
            shipping_address.convertDateFromBackend(this);
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Order target = new Order();
        target.setNote(note);
        target.setStatus(status);
        target.setCustomer_id(customer_id);
        target.setTotal_discount(total_discount);
        target.setShipping_address(shipping_address != null ? (Shipping_address) shipping_address.clone():null);
        target.setBilling_address(billing_address != null ? (Billing_address) billing_address.clone():null);
        if(line_items != null && !line_items.isEmpty()){
            target.setLine_items(new ArrayList<Line_items>());
            for(Line_items item: line_items){
                target.getLine_items().add((Line_items) item.clone());
            }
        }

        return target;
    }
}
