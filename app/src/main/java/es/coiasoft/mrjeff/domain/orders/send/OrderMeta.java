package es.coiasoft.mrjeff.domain.orders.send;

import java.io.Serializable;

/**
 * Created by Paulino on 11/05/2016.
 */
public class OrderMeta  implements Serializable {

    private String subscription_number;
    private String subscription_children;
    private String subscription_son;
    private String dispositvo = "Android";
    private String myfield2;
    private String myfield3;
    private String myfield4;
    private String myfield5;
    private String issuscription;
    private String typesuscription;


    public String getSubscription_number() {
        return subscription_number;
    }

    public void setSubscription_number(String subscription_number) {
        this.subscription_number = subscription_number;
    }

    public String getSubscription_children() {
        return subscription_children;
    }

    public void setSubscription_children(String subscription_children) {
        this.subscription_children = subscription_children;
    }

    public String getSubscription_son() {
        return subscription_son;
    }

    public void setSubscription_son(String subscription_son) {
        this.subscription_son = subscription_son;
    }

    public String getDispositvo() {
        return dispositvo;
    }

    public void setDispositvo(String dispositvo) {
        this.dispositvo = dispositvo;
    }

    public String getMyfield2() {
        return myfield2;
    }

    public void setMyfield2(String myfield2) {
        this.myfield2 = myfield2;
    }

    public String getMyfield3() {
        return myfield3;
    }

    public void setMyfield3(String myfield3) {
        this.myfield3 = myfield3;
    }

    public String getMyfield4() {
        return myfield4;
    }

    public void setMyfield4(String myfield4) {
        this.myfield4 = myfield4;
    }

    public String getMyfield5() {
        return myfield5;
    }

    public void setMyfield5(String myfield5) {
        this.myfield5 = myfield5;
    }

    public String getIssuscription() {
        return issuscription;
    }

    public void setIssuscription(String issuscription) {
        this.issuscription = issuscription;
    }

    public String getTypesuscription() {
        return typesuscription;
    }

    public void setTypesuscription(String typesuscription) {
        this.typesuscription = typesuscription;
    }
}
