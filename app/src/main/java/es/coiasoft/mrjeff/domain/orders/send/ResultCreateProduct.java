package es.coiasoft.mrjeff.domain.orders.send;

import es.coiasoft.mrjeff.domain.Products;

/**
 * Created by linovm on 22/11/15.
 */
public class ResultCreateProduct {

    private Products product;

    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }
}
