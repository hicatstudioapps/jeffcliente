package es.coiasoft.mrjeff.domain.orders.send;

/**
 * Created by linovm on 21/10/15.
 */
public class SendOrder {

    private Order order;

    public void setOrder(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }

    public SendOrder() {
    }

    public SendOrder(Order order) {
        this.order = order;
    }
}
