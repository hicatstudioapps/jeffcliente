package es.coiasoft.mrjeff.domain.orders.send;

/**
 * Created by linovm on 22/11/15.
 */
public class SendProducts {

    private Product product;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
