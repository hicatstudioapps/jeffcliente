package es.coiasoft.mrjeff.domain.orders.send;

import es.coiasoft.mrjeff.domain.orders.response.*;

public class Shipping_lines {

	private String method_id;
	private String method_title;
	private String total;
	
	public String getMethod_id() {
		return method_id;
	}
	public void setMethod_id(String method_id) {
		this.method_id = method_id;
	}
	public String getMethod_title() {
		return method_title;
	}
	public void setMethod_title(String method_title) {
		this.method_title = method_title;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public Shipping_lines() {
		super();
	}
	public Shipping_lines(String method_id, String method_title, String total) {
		super();
		this.method_id = method_id;
		this.method_title = method_title;
		this.total = total;
	}

	public Shipping_lines(es.coiasoft.mrjeff.domain.orders.response.Shipping_lines item) {
		super();
		if(item != null) {
			this.method_id = item.getMethod_id();
			this.method_title = item.getMethod_title();
			this.total = item.getTotal();
		}
	}
	
	
}
