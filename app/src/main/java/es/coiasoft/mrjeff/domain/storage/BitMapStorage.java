package es.coiasoft.mrjeff.domain.storage;

import android.graphics.Bitmap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by linovm on 10/10/15.
 */
public class BitMapStorage {

    private Map<String, Bitmap> mBitmapCache = new HashMap<String, Bitmap>();

    private static BitMapStorage instance;

    public Map<String, Bitmap> getmBitmapCache() {
        return mBitmapCache;
    }

    public void setmBitmapCache(Map<String, Bitmap> mBitmapCache) {
        this.mBitmapCache = mBitmapCache;
    }

    private BitMapStorage(){

    }

    public static BitMapStorage getInstance() {
        if(instance == null){
            instance = new BitMapStorage();
        }
        return instance;
    }


}
