package es.coiasoft.mrjeff.domain.storage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paulino on 14/01/2016.
 */
public class CouponDenegy {

    private static List<String> couponDenegy = new ArrayList<String>();

    static {
        couponDenegy.add("mdv");
    }

    public static boolean isCopuponDenegy(String coupon){
        boolean result = false;

        for(String couponDeg: couponDenegy){
            if(couponDeg.trim().toLowerCase().equals(coupon.trim().toLowerCase())){
                result = true;
                break;
            }
        }

        return result;
    }
}
