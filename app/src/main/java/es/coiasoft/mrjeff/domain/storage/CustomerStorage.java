package es.coiasoft.mrjeff.domain.storage;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.localytics.android.Localytics;

import java.util.Calendar;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;

/**
 * Created by linovm on 3/12/15.
 */
public class CustomerStorage {

    private Customer customer;

    private String coupon;

    private static CustomerStorage instance;

    private Context context;

    //private long lastTime;


    private CustomerStorage(){

    }

    public static CustomerStorage getInstance(Context context){
        if(instance == null){
            instance = new CustomerStorage();
            if(context != null) {
                instance.context = context;
            }
            getCustomerBD();
        }

        if(context != null){
            instance.context = context;
        }

        if(instance.customer == null){
            getCustomerBD();
        }
        return instance;
    }

    private static void getCustomerBD() {
        if (instance.context != null) {
            try {
                ConfigRepository db = new ConfigRepository(instance.context);
                Gson gson = new Gson();
                String customerJson = db.getValueConfig(ConfigRepository.CUSTOMER);
                if (customerJson != null && !customerJson.isEmpty() && !customerJson.equals("0")) {
                    instance.customer = gson.fromJson(customerJson, Customer.class);
                }
            } catch (Exception e) {
                Log.e("LOGIN", "Error recogiendo user");
            }
        }
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        //lastTime = Calendar.getInstance().getTimeInMillis();
        this.customer = customer;
        if(customer != null){
            if(context != null){
                ConfigRepository db = new ConfigRepository(context);
                Gson gson = new Gson();
                String jsonCustomer = gson.toJson(customer);
                db.updateRegister(ConfigRepository.CUSTOMER,jsonCustomer);
            }
            Localytics.setCustomerId(customer.getId().toString());
            Localytics.setCustomerFirstName(customer.getFirst_name() != null ? customer.getFirst_name() : "");
            Localytics.setCustomerLastName(customer.getLast_name() != null ? customer.getLast_name() : "");
            Localytics.setCustomerEmail(customer.getEmail() != null ? customer.getEmail() : "");
            Localytics.setProfileAttribute(MrJeffConstant.LOCALYTICS.POSTAL_CODE, customer.getBilling_address() != null && customer.getBilling_address().getPostcode() != null ? customer.getBilling_address().getPostcode() : "", Localytics.ProfileScope.APPLICATION);
            Localytics.setProfileAttribute(MrJeffConstant.LOCALYTICS.CITY,customer.getBilling_address() != null && customer.getBilling_address().getCity() != null? customer.getBilling_address().getCity()  : "",Localytics.ProfileScope.APPLICATION);
            Localytics.setProfileAttribute(MrJeffConstant.LOCALYTICS.NUMBER_ORDERS,customer.getOrders_count() != null ? customer.getOrders_count()  : 0,Localytics.ProfileScope.APPLICATION);
        } else {
            if(context != null) {
                ConfigRepository db = new ConfigRepository(context);
                db.updateRegister(ConfigRepository.CUSTOMER, "");
                Log.d("LOGIN", "Se guarda vacio");
            }
        }
    }

    public boolean isLogged(){
        boolean result = false;
        /*if(customer != null){
            long time = Calendar.getInstance().getTimeInMillis();
            long aux =  time - lastTime;
            if(Math.abs(aux) < MrJeffConstant.SESSION.TIME_EXPIRED){
                result = true;
                lastTime = time;
            } else {
                lastTime = 0;
                customer = null;
            }
        }*/
        return customer != null && customer.getId() != null;
    }

    public void loginOut(){
        customer = null;
        if(context != null) {
            ConfigRepository db = new ConfigRepository(context);
            db.updateRegister(ConfigRepository.CUSTOMER, "");
            Log.d("LOGIN", "Se guarda vacio");
        }
        //lastTime = 0;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }
}
