package es.coiasoft.mrjeff.domain.storage;

import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import es.coiasoft.mrjeff.MrJeffApplication;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.DatetimeMrJeff;
import es.coiasoft.mrjeff.view.validator.DateTimeMrJeff;

/**
 * Created by linovm on 18/12/15.
 */
@SuppressWarnings("WrongConstant")
public class DateTimeTableStorage {

    private static DateTimeTableStorage instance;
    private long difference =0l;

    public static final String patternDate = "yyyy-MM-dd";

    public Map<String, Map<String, DatetimeMrJeff>> getDateTimeTable() {
        return dateTimeTable;
    }

    private Map<String,Map<String,DatetimeMrJeff>> dateTimeTable = new HashMap<String,Map<String,DatetimeMrJeff>>();

    private Set<String> cpLoad = new HashSet<>();

    public long getDifference() {
        return difference;
    }

    public void setDifference(long difference) {
        this.difference = difference;
    }

    public static DateTimeTableStorage getInstance() {
        if(instance == null){
            instance = new DateTimeTableStorage();
        }
        return instance;
    }

    public static void setInstance(DateTimeTableStorage instance) {
        DateTimeTableStorage.instance = instance;
    }

    public void addDate(String cp,String date, DatetimeMrJeff time ){
        if(!dateTimeTable.containsKey(cp)){
            dateTimeTable.put(cp, new HashMap<String,DatetimeMrJeff>());
            cpLoad.add(cp);
        }

        if(!dateTimeTable.get(cp).containsKey(date)){
            dateTimeTable.get(cp).put(date, time);
        } else {
            dateTimeTable.get(cp).put(date, time);
        }


    }

    public boolean isLoadCp(String cp){
        return cpLoad.contains(cp);
    }

    public boolean isValidDate(String cp,Integer type, String date){
        return dateTimeTable.containsKey(cp) &&
                dateTimeTable.get(cp).containsKey(date) &&
                dateTimeTable.get(cp).get(date).validDate(type);
    }

    public boolean isValidHour(String cp,Integer type, String date,String hour){
        return isValidDate(cp,type,date) &&
                dateTimeTable.get(cp).get(date).validHour(type,hour);
    }

    public String[] getHour(String cp,Integer type, String date){
        String [] hours = null;
        if(isValidDate(cp,type,date)){
            hours = dateTimeTable.get(cp).get(date).getHoursType(type);
        }

        return hours;
    }

    public String[] getValidHour(String cp,Integer type, String date){
        String [] hours = null;
        if(isValidDateRestricted(cp,type,date)){
            String [] hoursAux = dateTimeTable.get(cp).get(date).getHoursType(type);
            DateTime dateTime= DateTimeFormat.forPattern(patternDate).parseDateTime(date);
            DateTime now= new DateTime();
            if(type == MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND
                    && (now.getMonthOfYear()!= dateTime.getMonthOfYear())
                    && (now.getDayOfMonth()!= dateTime.getDayOfMonth())){
                return hoursAux;
            } else {
                SimpleDateFormat format = new SimpleDateFormat(patternDate);
                Calendar dateCal = Calendar.getInstance();
                try {
                    dateCal.setTime(format.parse(date));
                    dateCal.set(Calendar.HOUR_OF_DAY, 23);
                    dateCal.set(Calendar.MINUTE, 59);
                    DateTime calD= new DateTime(dateCal);
                    String dateC= calD.toString("yyyy-MM-dd HH:mm:ss");
                    Calendar dateInit = Calendar.getInstance();
                    dateInit.setTime(format.parse(date));
                    dateInit.set(Calendar.HOUR_OF_DAY, 00);
                    dateInit.set(Calendar.MINUTE, 01);
                    DateTime initD= new DateTime(dateInit);
                    String dateI= initD.toString("yyyy-MM-dd HH:mm:ss");
                    DateTime dat= new DateTime();
                    Calendar today = Calendar.getInstance();
                    today.setTime(format.parse(dat.toString("yyyy-MM-dd")));
                    today.set(Calendar.HOUR_OF_DAY,dat.hourOfDay().get());
                    today.set(Calendar.MINUTE,dat.minuteOfHour().get());
                    today.setTimeInMillis(today.getTimeInMillis() + difference);
                    String dateT= dat.toString("yyyy-MM-dd HH:mm:ss");
                    if(dat.isBefore(calD) && dat.isAfter(initD)){
                        ArrayList<String> hoursFinally = new ArrayList<>();
                        for(String hour: hoursAux){
                            if(isValidHour(hour,dat,dateTimeTable.get(cp).get(date).getOrdersClosing()))  {
                                hoursFinally.add(hour);
                            }
                        }
                        hours = new String[hoursFinally.size()];
                        return hoursFinally.toArray(hours);
                    } else {
                        return hoursAux;
                    }
//                    if (today.getTimeInMillis() < dateCal.getTimeInMillis() && today.getTimeInMillis() > dateInit.getTimeInMillis()) {
//                        ArrayList<String> hoursFinally = new ArrayList<>();
//                        for(String hour: hoursAux){
//                            if(isValidHour(hour,today))  {
//                                hoursFinally.add(hour);
//                            }
//                        }
//                        hours = new String[hoursFinally.size()];
//                        return hoursFinally.toArray(hours);
//                    } else {
//                        return hoursAux;
//                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                    return hoursAux;
                }
            }

        } else {
            return hours;
        }


    }

    private boolean isValidHour(String hour, DateTime calendar, String ordersClosing){
        boolean result = false;
//
//        Calendar calendarCompare = Calendar.getInstance();
//        calendar.setTimeInMillis(calendar.getTimeInMillis());
//        String hClo = hour.split("-")[0].split(":")[0];
//        String mClo = hour.split("-")[0].split(":")[1];
//        calendarCompare.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hClo));
//        calendarCompare.set(Calendar.MINUTE, Integer.valueOf(mClo));
//        calendarCompare.set(Calendar.SECOND, 0);
//
//        result = calendar.getTimeInMillis() < calendarCompare.getTimeInMillis() && (calendarCompare.getTimeInMillis() - calendar.getTimeInMillis()) > MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_MIN_ORDER.longValue();
        DateTime today= new DateTime();
        String stringToday= new DateTime().toString("yyyy-MM-dd");
        DateTime today1= DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(stringToday+" "+hour.split("-")[0]+":00");
        String [] orderClosingArray= ordersClosing.split(";");
        String toda1String= today1.toString("yyyy-MM-dd HH:mm:ss");
        if(orderClosingArray.length==1){
            DateTime datimeOrderClosing= DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(stringToday+" "+orderClosingArray[0]+":00");
            String datimeOrderClosingString= datimeOrderClosing.toString("yyyy-MM-dd HH:mm:ss");
            if(today.isBefore(datimeOrderClosing))
            if(today.isBefore(today1))
                return true;
        }else {
            DateTime datimeOrderClosingStart= DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(stringToday+" "+orderClosingArray[0]+":00");
            DateTime datimeOrderClosingFinish= DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(stringToday+" "+orderClosingArray[1]+":00");
            String datimeOrderClosingStartString= datimeOrderClosingStart.toString("yyyy-MM-dd HH:mm:ss");
            String datimeOrderClosingFinishString= datimeOrderClosingFinish.toString("yyyy-MM-dd HH:mm:ss");
            if(today.isBefore(datimeOrderClosingStart)){
                if(today.isBefore(today1))
                    return true;
            }
            else {
                if(today.isBefore(today1) && today1.isAfter(datimeOrderClosingFinish))
                    return true;
            }
//            if(today1.isBefore(datimeOrderClosingStart) || today1.isAfter(datimeOrderClosingFinish))
//                return true;
        }
            return false;

    }



    public int getMinDays(String cp, String date){
        int result = 2;
        try {
            result = dateTimeTable.get(cp).get(date).minDays();
        }catch (Exception e){

        }

        return result;
    }

    public boolean isValidDateRestricted(String cp,Integer type, String date){
        boolean result = false;

        if(isValidDate(cp,type,date)){
            try {
                if(type!= 0)
                    return true;
                String[] hours = dateTimeTable.get(cp).get(date).hourClosing();
                if(hours.length==1) {
                    DateTime dateCal = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(date + " " + hours[hours.length - 1] + ":00");
                    DateTime today = new DateTime();
                    if (today.isBefore(dateCal)) {
                        result = true;
                    }
                    else result=false;
                }else{
                    DateTime dateCal = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(date + " " +
                            hours[hours.length-1] + ":00");
                    DateTime today = new DateTime();
                    if (today.isBefore(dateCal)) {
                        result = true;
                    }
                    else result=false;
                }
            }catch (Exception e){
                e.printStackTrace();
                Log.e("DATETIMESTORAGE", "Error al parsear fecha");
            }
        }

        return result;
    }

    public boolean isValidDateRestrictedDelivery(String cp,Integer type, String date,String dateReference, Integer minDays){
        boolean result = false;

        if(isValidDate(cp,type,date)){
            try {
                SimpleDateFormat format = new SimpleDateFormat(patternDate);
                Calendar dateCal = Calendar.getInstance();
                dateCal.setTime(format.parse(date));
                String[] hours = dateTimeTable.get(cp).get(date).hourClosing();
                String hourClose = hours[hours.length-1];
                String hClo = hourClose.split(":")[0];
                String mClo = hourClose.split(":")[1];
                dateCal.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hClo));
                dateCal.set(Calendar.MINUTE, Integer.valueOf(mClo));
                dateCal.set(Calendar.SECOND, 0);
                Calendar dateCalRef = Calendar.getInstance();
                dateCalRef.setTime(format.parse(dateReference.split(" ")[0]));
                int min = minDays.intValue() >  getMinDays(cp,dateReference.split(" ")[0]) ? minDays.intValue():getMinDays(cp,dateReference.split(" ")[0]);
                dateCalRef.setTimeInMillis(dateCalRef.getTimeInMillis() + (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY *min) + difference );

                if(dateCalRef.getTimeInMillis() < dateCal.getTimeInMillis()){
                    result = true;
                }
            }catch (Exception e){
                Log.e("DATETIMESTORAGE", "Error al parsear fecha");
            }
        }

        return true;
    }


    public boolean isValidDateRestrictedHour(String cp,Integer type, String date,String hour){
        boolean result = false;

        if(isValidDate(cp,type,date)){
            try {
                SimpleDateFormat format = new SimpleDateFormat(patternDate);
                Calendar dateCal = Calendar.getInstance();
                dateCal.setTime(format.parse(date));
                String[] hours = dateTimeTable.get(cp).get(date).hourClosing();
                String hourClose = hours[hours.length-1];
                String hClo = hourClose.split(":")[0];
                String mClo = hourClose.split(":")[1];

                if(Integer.valueOf(hour.trim().split(":")[0]).intValue() <  Integer.valueOf(hClo).intValue()){
                    hourClose = hours[0];
                    hClo = hourClose.split(":")[0];
                    mClo = hourClose.split(":")[1];
                }


                dateCal.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hClo));
                dateCal.set(Calendar.MINUTE, Integer.valueOf(mClo));
                dateCal.set(Calendar.SECOND, 0);



                Calendar today = Calendar.getInstance();

                if(today.getTimeInMillis() < dateCal.getTimeInMillis()){
                    result = true;
                }
            }catch (Exception e){
                Log.e("DATETIMESTORAGE", "Error al parsear fecha");
            }
        }

        return result;
    }

    //obtengo la primera combinacion de fecha y hora
    public String getFirstDateHour(String cp, int type){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(calendar.getTime());
        int days = 0;
        while (!isValidDateRestricted(cp, type, date)) {
            calendar.setTimeInMillis(calendar.getTimeInMillis() + MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY);
            date = format.format(calendar.getTime());
            days++;
        }

        String [] hours = getHour(cp,type,date);
        String hour = "";
        boolean notfound =  true;
        for(int i = 0; i < hours.length && notfound; i++){
            hour = hours[i];
            notfound = !isValidDateRestrictedHour(cp,type,date,hour);
        }

        return date + " " + hour;

    }

    public String getSecondDateHour(String cp, int type, Calendar calendar){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(calendar.getTime());
        int days = 0;
        while (!isValidDateRestricted(cp, type, date)) {
            calendar.setTimeInMillis(calendar.getTimeInMillis() + MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY);
            date = format.format(calendar.getTime());
            days++;
        }
        String [] hours = getHour(cp,type,date);
        String hour = "";
        boolean notfound =  true;
        for(int i = 0; i < hours.length && notfound; i++){
            hour = hours[i];
            notfound = !isValidDateRestrictedHour(cp,type,date,hour);
        }
        if(MrJeffApplication.entregaReference.equals(""))
        MrJeffApplication.entregaReference=date;
        return date + " " + hour;

    }


    public ArrayList<String> getDateType(String cp, int type, String dateReference, Integer minDays){
        ArrayList<String> days = new ArrayList<String>();
        Map<String,DatetimeMrJeff> dayOfCp = dateTimeTable.get(cp);
        if(dayOfCp != null && !dayOfCp.isEmpty()){
            for(String key: dayOfCp.keySet()){
                DatetimeMrJeff day = dayOfCp.get(key);
                if(day != null && day.validDate(type) &&  ((type == MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT &&   isValidDateRestricted(cp,type,key)) || (dateReference != null && type == MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND && minDays != null && isValidDateRestrictedDelivery(cp,type,key,dateReference,minDays)))){
                    days.add(key);
                }
            }
        }
        Collections.sort(days);
        if(type == MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT ) {
            days = new ArrayList<String>(days.subList(0,days.size()-3));
        }
        return days;
    }


    public String getFirstDateHourAndDayWeek(String cp, int type,int day){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(calendar.getTime());
        int days = 0;
        while (!isValidDateRestricted(cp, type, date) || !(calendar.get(Calendar.DAY_OF_WEEK) == (day == 7?1:day+1)) && days < 30) {
            calendar.setTimeInMillis(calendar.getTimeInMillis() + MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY);
            date = format.format(calendar.getTime());
            days++;
        }

        String [] hours = getHour(cp,type,date);
        String hour = "";
        boolean notfound =  true;
        for(int i = 0; i < hours.length && notfound; i++){
            hour = hours[i];
            notfound = !isValidDateRestrictedHour(cp,type,date,hour);
        }

        return date + " " + hour;

    }

    public String getSecondDateHourAndDayWeek(String cp, int type, Calendar calendar,int day){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(calendar.getTime());
        int days = 0;
        while (!isValidDateRestricted(cp, type, date) || !(calendar.get(Calendar.DAY_OF_WEEK) == (day >= 7?1:day+1)) && days < 30) {
            calendar.setTimeInMillis(calendar.getTimeInMillis() + MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY);
            date = format.format(calendar.getTime());
            days++;
        }

        String [] hours = getHour(cp,type,date);
        String hour = "";
        boolean notfound =  true;
        for(int i = 0; i < hours.length && notfound; i++){
            hour = hours[i];
            notfound = !isValidDateRestrictedHour(cp,type,date,hour);
        }

        return date + " " + hour;

    }

    public Calendar getToday(){
        Calendar today = Calendar.getInstance();
        today.setTimeInMillis(today.getTimeInMillis() + DateTimeTableStorage.getInstance().getDifference());
        return today;
    }


}
