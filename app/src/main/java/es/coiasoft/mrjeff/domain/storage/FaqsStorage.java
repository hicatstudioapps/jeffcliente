package es.coiasoft.mrjeff.domain.storage;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.coiasoft.mrjeff.domain.Faq;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;

public class FaqsStorage {

	private Map<String,List<Faq>> faqs;
	private long time= 0;
	private Context context;

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	private static FaqsStorage instance;

	private FaqsStorage(){
		faqs = new HashMap<String, List<Faq>>();
	}
	
	public static FaqsStorage getInstance(Context context){
		if(instance == null){
			instance = new FaqsStorage();
			if(context != null) {
				instance.context = context;
			}
			instance.getJson();
		} else if(instance.faqs == null || instance.faqs.isEmpty()){
			instance.getJson();
		}

		return instance;
	}

	public void addProduct(String product, List<Faq> faqsSave){
		if(product != null && faqsSave != null && !faqsSave.isEmpty()){
			faqs.put(product, faqsSave);
		}
	}
	
	public  List<Faq> findProduct(String product){
		return faqs.get(product);
	}


	public void clear(){
		faqs = new HashMap<String, List<Faq>>();
	}


	public void saveJson(){
		if(context != null && faqs != null && !faqs.isEmpty()){
			ConfigRepository db = new ConfigRepository(context);
			Gson gson = new Gson();
			JSONFaqsStorage json = new JSONFaqsStorage(faqs);
			String jsonProducts = gson.toJson(json);
			printLog("UPDATEFAQS",jsonProducts);
			db.updateRegister(ConfigRepository.FAQS, jsonProducts);
		} else if(context != null){
			ConfigRepository db = new ConfigRepository(context);
			db.updateRegister(ConfigRepository.FAQS, "0");
		}

	}

	public void getJson(){
		if(context != null){
			ConfigRepository db = new ConfigRepository(context);
			String json = null;
			json = db.getValueConfig(ConfigRepository.FAQS);

			if(json != null && !json.isEmpty() && !json.equals("0")){
				try{
					Gson gson = new Gson();
					JSONFaqsStorage jsonFaqsStorage = gson.fromJson(json, JSONFaqsStorage.class);
					faqs = jsonFaqsStorage.getFaqs();
				}catch (Exception e){
					Log.e("ProductStroage", "Error al recoger de bd", e);
				}
			}
		}
	}

    private void printLog(String TAG, String sb){
        if (sb.length() > 4000) {
            Log.d(TAG, "sb.length = " + sb.length());
            int chunkCount = sb.length() / 4000;     // integer division
            for (int i = 0; i <= chunkCount; i++) {
                int max = 4000 * (i + 1);
                if (max >= sb.length()) {
                    Log.v(TAG, sb.substring(4000 * i));
                } else {
                    Log.v(TAG, sb.substring(4000 * i, max));
                }
            }
        } else {
            Log.v(TAG, sb.toString());
        }
    }


	private class JSONFaqsStorage implements Serializable{

		private Map<String,List<Faq>> faqs;

		public Map<String, List<Faq>> getFaqs() {
			return faqs;
		}

		public void setFaqs(Map<String, List<Faq>> faqs) {
			this.faqs = faqs;
		}

		public JSONFaqsStorage( Map<String,List<Faq>> faqs) {
			this.faqs = faqs;
		}
	}



	
}
