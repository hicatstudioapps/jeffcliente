package es.coiasoft.mrjeff.domain.storage;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import java.util.Calendar;
import java.util.Observable;

import es.coiasoft.mrjeff.view.listener.LocationAddressListener;

/**
 * Created by Paulino on 07/03/2016.
 */
public class LocationStorage extends Observable{

    private static final long timeValid = 60000;
    private static LocationStorage instance;
    private Location lastLocation;
    private Boolean locationActive = Boolean.TRUE;
    private LocationManager mlocManager;
    private LocationAddressListener mlocListener;


    private Boolean init = Boolean.FALSE;

    private long timeLastSearch;


    private LocationStorage(){}

    public static LocationStorage getInstance(){
        if(instance == null){
            instance = new LocationStorage();
        }

        return instance;
    }

    public void initLocation(LocationManager mlocManager){
        if(mlocManager != null && !init){
            try {
                this.mlocManager = mlocManager;
                mlocListener = new LocationAddressListener();
                mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mlocListener);
                //Obtenemos la última posición conocida
                lastLocation = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                timeLastSearch = lastLocation.getTime();
                init = Boolean.TRUE;
            }catch (Exception e){
                init = Boolean.FALSE;
            }
        }
    }

    public static long getTimeValid() {
        return timeValid;
    }


    public static void setInstance(LocationStorage instance) {
        LocationStorage.instance = instance;
    }

    public Location getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(Location lastLocation) {
        this.lastLocation = lastLocation;
        timeLastSearch = Calendar.getInstance().getTimeInMillis();
        try {
            setChanged();
            notifyObservers(this.lastLocation);
        }catch (Exception e){
            Log.e("LOCATION","Error notificando observadores", e);
        }
    }

    public Boolean getLocationActive() {
        return mlocManager != null && mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void setLocationActive(Boolean locationActive) {
        this.locationActive = locationActive;
    }

    public Boolean getInit() {
        return init;
    }

    public void setInit(Boolean init) {
        this.init = init;
    }

    public long getTimeLastSearch() {
        return timeLastSearch;
    }

    public void setTimeLastSearch(long timeLastSearch) {
        this.timeLastSearch = timeLastSearch;
    }


    public boolean validStorage(){
        boolean result = true;

        if(lastLocation == null){
            result = false;
        } else if(timeLastSearch <= 0){
            result = false;
        } else {
            long timeNow = Calendar.getInstance().getTimeInMillis();
            result = (timeNow - timeLastSearch) <= timeValid;
        }

        return result;
    }

    public void removeListener(Boolean removeListener){
        if(removeListener) {
            if (mlocManager != null && mlocListener != null) {
                try {
                    mlocManager.removeUpdates(mlocListener);
                } catch (Exception e) {
                    Log.e("LOCATION", "Error al borrar el listener de ubicación");
                }
            }
            mlocManager = null;
            mlocListener = null;
            init = Boolean.FALSE;
        }
    }


}
