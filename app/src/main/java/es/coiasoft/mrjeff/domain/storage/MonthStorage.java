package es.coiasoft.mrjeff.domain.storage;

import java.util.HashMap;
import java.util.Map;

import es.coiasoft.mrjeff.R;

/**
 * Created by linovm on 12/1/16.
 */
public class MonthStorage {


    public static Map<Integer,Integer> months = new HashMap<Integer,Integer>();
    public static Map<Integer,Integer> days = new HashMap<Integer,Integer>();


    static {
        months.put(1, R.string.month1);
        months.put(2, R.string.month2);
        months.put(3, R.string.month3);
        months.put(4, R.string.month4);
        months.put(5, R.string.month5);
        months.put(6, R.string.month6);
        months.put(7, R.string.month7);
        months.put(8, R.string.month8);
        months.put(9, R.string.month9);
        months.put(10, R.string.month10);
        months.put(11, R.string.month11);
        months.put(12, R.string.month12);

        days.put(1,R.string.monday);
        days.put(2,R.string.tuesday);
        days.put(3,R.string.wednesday);
        days.put(4,R.string.thursday);
        days.put(5,R.string.friday);
        days.put(6,R.string.saturday);
        days.put(0,R.string.sunday);
    }
}
