package es.coiasoft.mrjeff.domain.storage;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.coiasoft.mrjeff.Utils.LanguageUtil;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;

/**
 * Created by linovm on 9/10/15.
 */
public class OrderProductsStorage {

    private Map<Integer,Integer> products;

    private static OrderProductsStorage instance;

    private Integer numTotal = 0;

    private Context context;

    private OrderProductsStorage(){
        products= new HashMap<Integer,Integer>();
    }

    public Integer getNumTotal() {
        return numTotal;
    }

    public static OrderProductsStorage getInstance(Context context) {
        if(instance == null){
            instance = new OrderProductsStorage();
            if(context != null) {
                instance.context = context;
            }
            instance.getJson();
        } else if(instance.products == null || instance.products.isEmpty()){
            instance.getJson();
        }
        return instance;
    }

    public void addProduct(Integer idProduct, Integer count){
        products.put(idProduct,count);
        numTotal = numTotal + count;

        saveJson();
    }

    public void remove(Integer idProduct){
        try {
            Integer numProduct = products.get(idProduct);
            if(numProduct != null && numProduct.intValue() >= 0) {
                numTotal = numTotal - numProduct;
                products.remove(idProduct);
            }

            saveJson();
        }catch (Exception e){

        }
    }

    public void addProductElement(Integer idProduct){
        Integer num = products.get(idProduct);
        if(num == null){
            num = 0;
        }

        products.put(idProduct,++num);
        numTotal++;

        saveJson();

    }

    public void removeProductElement(Integer idProduct){
        Integer num = products.get(idProduct);
        if(num == null || num.intValue() == 0){
            num = 0;
        } else {
            products.put(idProduct,--num);
            --numTotal;
        }
        saveJson();
    }

    public void removeProductElementAll(Integer idProduct){
        Integer num = products.get(idProduct);
        products.put(idProduct,0);
        numTotal-=num;
        saveJson();
    }

    public Integer countProduct(Integer idProduct){
        Integer num = products.get(idProduct);
        if(num == null){
            num = 0;
        }

        return num;

    }

    public void clear(){
        products.clear();
        products = new HashMap<Integer,Integer>();
        numTotal = 0;

        saveJson();
    }

    public Map<Integer, Integer> getProducts() {
        return products;
    }


    public void saveJson(){
        if(context != null && products != null && !products.isEmpty()){
            ConfigRepository db = new ConfigRepository(context);
            Gson gson = new Gson();
            JSONOrderProductStorage json = new JSONOrderProductStorage(products,numTotal);
            String jsonProducts = gson.toJson(json);
            Log.d("JSON PRODUCT", "saveJson: "+ jsonProducts);
            db.updateRegister(ConfigRepository.ORDER_PRODUCT, jsonProducts);
        } else if(context != null){
            ConfigRepository db = new ConfigRepository(context);
            db.updateRegister(ConfigRepository.ORDER_PRODUCT, "");
        }

    }

    public void getJson(){
        if(context != null){
            ConfigRepository db = new ConfigRepository(context);
            String json = null;
            json = db.getValueConfig(ConfigRepository.ORDER_PRODUCT);

            if(json != null && !json.isEmpty() && !json.equals("0")){
                try{
                    Gson gson = new Gson();
                    JSONOrderProductStorage jsonProductStorage = gson.fromJson(json, JSONOrderProductStorage.class);
                    products = jsonProductStorage.getProducts();
                    numTotal = jsonProductStorage.getNumTotal();
                    cleanProducts();

                    if(products == null){
                        products = new HashMap<Integer,Integer>();
                    }
                }catch (Exception e){
                    Log.e("ProductStroage", "Error al recoger de bd", e);
                }
            }
        }
    }

    private void cleanProducts(){
        int total = 0;
        List<Integer> keyRemove= new ArrayList<Integer>();
        if(products != null && products.size() > 0){
            for(Integer key: products.keySet() ){
                if(products.get(key) != null && products.get(key).intValue() > 0) {
                    total = total + products.get(key);
                } else{
                    keyRemove.add(key);
                }
            }
        }

        if(keyRemove != null && keyRemove.size() > 0){
            for(Integer key: keyRemove ){
                products.remove(key);
            }
        }

        numTotal = total;
    }



    private class JSONOrderProductStorage{

        private Map<Integer,Integer> products;

        private Integer numTotal = 0;

        public Map<Integer, Integer> getProducts() {
            return products;
        }

        public void setProducts(Map<Integer, Integer> products) {
            this.products = products;
        }

        public Integer getNumTotal() {
            return numTotal;
        }

        public void setNumTotal(Integer numTotal) {
            this.numTotal = numTotal;
        }

        public JSONOrderProductStorage() {
        }

        public JSONOrderProductStorage(Map<Integer, Integer> products, Integer numTotal) {
            this.products = products;
            this.numTotal = numTotal;
        }
    }
}
