package es.coiasoft.mrjeff.domain.storage;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import es.coiasoft.mrjeff.domain.ResultCoupon;
import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;

/**
 * Created by linovm on 21/10/15.
 */
public class OrderStorage {

    private Order order;
    private Boolean error = Boolean.FALSE;
    private es.coiasoft.mrjeff.domain.orders.send.Order orderSend;
    private static OrderStorage instance = null;
    private Boolean isAddressHome = Boolean.TRUE;
    private Integer productDelete = null;
    private Integer productMinOrder = null;
    private ResultCoupon coupon = null;
    private Context context;
    private Boolean removeOk = Boolean.FALSE;


    private OrderStorage(){

    }

    public static OrderStorage getInstance(Context context) {
        if(instance == null){
            instance = new OrderStorage();
            if(context != null) {
                instance.context = context;
            }

        }

        if(context != null){
            instance.context = context;
        }
       /// instance.getJson();
        return instance;
    }


    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Boolean getRemoveOk() {
        return removeOk;
    }

    public void setRemoveOk(Boolean removeOk) {
        this.removeOk = removeOk;
    }

    public es.coiasoft.mrjeff.domain.orders.send.Order getOrderSend() {
        return orderSend;
    }

    public void setOrderSend(es.coiasoft.mrjeff.domain.orders.send.Order orderSend) {
        this.orderSend = orderSend;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getProductDelete() {
        return productDelete;
    }

    public void setProductDelete(Integer productDelete) {
        this.productDelete = productDelete;
    }

    public Integer getProductMinOrder() {
        return productMinOrder;
    }

    public void setProductMinOrder(Integer productMinOrder) {
        this.productMinOrder = productMinOrder;
    }

    public ResultCoupon getCoupon() {
        return coupon;
    }

    public void setCoupon(ResultCoupon coupon) {
        this.coupon = coupon;
        saveJson();
    }

    public void saveJson(){
        if(context != null){
            ConfigRepository db = new ConfigRepository(context);
            Gson gson = new Gson();
            JSONOrderStorage json = new JSONOrderStorage(order,error,orderSend,isAddressHome,productDelete,productMinOrder,coupon);
            String jsonProducts = gson.toJson(json);
            db.updateRegister(ConfigRepository.ORDER, jsonProducts);
        }
    }

    public void getJson(){
        if(context != null){
            ConfigRepository db = new ConfigRepository(context);
            String json = null;
            json = db.getValueConfig(ConfigRepository.ORDER);

            if(json != null && !json.isEmpty() && !json.equals("0")){
                try{
                    Gson gson = new Gson();
                    JSONOrderStorage jsonOrderStorage = gson.fromJson(json, JSONOrderStorage.class);
                    order = jsonOrderStorage.getOrder();
                    orderSend= jsonOrderStorage.getOrderSend();
                    error = jsonOrderStorage.getError();
                    isAddressHome = jsonOrderStorage.getIsAddressHome();
                    productDelete = jsonOrderStorage.getProductDelete();
                    productMinOrder = jsonOrderStorage.getProductMinOrder();
                    coupon = jsonOrderStorage.getCoupon();

                }catch (Exception e){
                    Log.e("ProductStroage", "Error al recoger de bd", e);
                }
            }
        }
    }



    private class JSONOrderStorage{
        private Order order;
        private Boolean error = Boolean.FALSE;
        private es.coiasoft.mrjeff.domain.orders.send.Order orderSend;
        private Boolean isAddressHome = Boolean.TRUE;
        private Integer productDelete = null;
        private Integer productMinOrder = null;
        private ResultCoupon coupon = null;


        public Order getOrder() {
            return order;
        }

        public void setOrder(Order order) {
            this.order = order;
        }

        public Boolean getError() {
            return error;
        }

        public void setError(Boolean error) {
            this.error = error;
        }

        public es.coiasoft.mrjeff.domain.orders.send.Order getOrderSend() {
            return orderSend;
        }

        public void setOrderSend(es.coiasoft.mrjeff.domain.orders.send.Order orderSend) {
            this.orderSend = orderSend;
        }

        public Boolean getIsAddressHome() {
            return isAddressHome;
        }

        public void setIsAddressHome(Boolean isAddressHome) {
            this.isAddressHome = isAddressHome;
        }

        public Integer getProductDelete() {
            return productDelete;
        }

        public void setProductDelete(Integer productDelete) {
            this.productDelete = productDelete;
        }

        public Integer getProductMinOrder() {
            return productMinOrder;
        }

        public void setProductMinOrder(Integer productMinOrder) {
            this.productMinOrder = productMinOrder;
        }

        public ResultCoupon getCoupon() {
            return coupon;
        }

        public void setCoupon(ResultCoupon coupon) {
            this.coupon = coupon;
        }

        public JSONOrderStorage() {
        }

        public JSONOrderStorage(Order order, Boolean error, es.coiasoft.mrjeff.domain.orders.send.Order orderSend, Boolean isAddressHome, Integer productDelete, Integer productMinOrder,ResultCoupon coupon) {
            this.order = order;
            this.error = error;
            this.orderSend = orderSend;
            this.isAddressHome = isAddressHome;
            this.productDelete = productDelete;
            this.productMinOrder = productMinOrder;
            this.coupon = coupon;
        }
    }
}
