package es.coiasoft.mrjeff.domain.storage;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;

public class OrdersCustomerStorage {

	private List<Orders> orders;
	private Map<Integer,Integer> positionOrder = new HashMap<Integer,Integer>();
	private String iddCustomer;

	private long time= 0;
	private Context context;
	private boolean isSave = false;

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	private static OrdersCustomerStorage instance;

	private OrdersCustomerStorage(){
		orders = new ArrayList<Orders>();
	}
	
	public static OrdersCustomerStorage getInstance(Context context){
		if(instance == null){
			instance = new OrdersCustomerStorage();
			if(context != null) {
				instance.context = context;
			}
			instance.getJson();
		} else if(instance.orders == null || instance.orders.isEmpty()){
			instance.getJson();
		}

		return instance;
	}

	public void addOrders( List<Orders> ordersSave){
		if(!isSave) {
			if (ordersSave != null && !ordersSave.isEmpty()) {
				orders = new ArrayList<>();
				for (int i = 0; i < ordersSave.size(); i++) {
					orders.add(i, ordersSave.get(i));
					positionOrder.put(ordersSave.get(i).getId(), i);
				}
			}
		}
	}

	public Orders getOrder(Integer id){
		Orders result = null;
		if(positionOrder.get(id) != null){
			result = orders.get(positionOrder.get(id));
		}

		return result;
	}

	public void addOrder(Orders order){
		if(!isSave) {
			if (positionOrder.get(order.getId()) != null) {
				orders.add(positionOrder.get(order.getId()), order);
			} else {
				orders.add(order);
				positionOrder.put(order.getId(), orders.size() - 1);
			}
		}
	}
	public  List<Orders> getOrders(){
		return orders;
	}


	public void clear(){
		if(!isSave)
			orders = new ArrayList<Orders>();
	}

	public String getIddCustomer() {
		return iddCustomer;
	}

	public void setIddCustomer(String iddCustomer) {
		this.iddCustomer = iddCustomer;
	}

	public void saveJson(){
		isSave = true;
		if(context != null && orders != null && !orders.isEmpty()){

			List<Orders> aux = new ArrayList<>();
			aux.addAll(orders);
			ConfigRepository db = new ConfigRepository(context);
			GsonBuilder gsonBuilder = new GsonBuilder();
			//gsonBuilder.registerTypeAdapter(EnumStateOrder.class, new AttributeScopeDeserializer());
			Gson gson = gsonBuilder.create();
			JSONOrdersStorage json = new JSONOrdersStorage(aux,iddCustomer);
			String jsonProducts = gson.toJson(json);
			db.updateRegister(ConfigRepository.ORDERS, jsonProducts);
		} else if(context != null){
			ConfigRepository db = new ConfigRepository(context);
			db.updateRegister(ConfigRepository.ORDERS, "0");
		}
		isSave = false;
	}

	public void getJson(){
		isSave = true;
		if(context != null){
			ConfigRepository db = new ConfigRepository(context);
			String json = null;
			json = db.getValueConfig(ConfigRepository.ORDERS);

			if(json != null && !json.isEmpty() && !json.equals("0")){
				try{
					Gson gson = new Gson();
					JSONOrdersStorage jsonOrdersStorage = gson.fromJson(json, JSONOrdersStorage.class);
					addOrders(jsonOrdersStorage.getOrders());
					iddCustomer = jsonOrdersStorage.getIdCustomer();
				}catch (Exception e){
					Log.e("ProductStroage", "Error al recoger de bd", e);
				}
			}
		}

		isSave = false;
	}


	private class JSONOrdersStorage implements Serializable{

		private List<Orders> orders;

		private String idCustomer;

		public List<Orders> getOrders() {
			return orders;
		}

		public void setOrders(List<Orders> orders) {
			this.orders = orders;
		}

		public String getIdCustomer() {
			return idCustomer;
		}

		public void setIdCustomer(String idCustomer) {
			this.idCustomer = idCustomer;
		}

		public JSONOrdersStorage(List<Orders> orders) {
			this.orders = orders;
		}

		public JSONOrdersStorage(List<Orders> orders, String idCustomer) {
			this.orders = orders;
			this.idCustomer = idCustomer;
		}
	}


	class AttributeScopeDeserializer implements JsonDeserializer<EnumStateOrder>
	{
		@Override
		public EnumStateOrder deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
		{
			EnumStateOrder[] states = EnumStateOrder.values();
			for (EnumStateOrder state : states)
			{
				if (state.getState() == json.getAsInt())
					return state;
			}
			return null;
		}

	}


	
}
