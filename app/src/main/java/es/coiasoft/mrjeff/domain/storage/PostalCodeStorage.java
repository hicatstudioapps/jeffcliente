package es.coiasoft.mrjeff.domain.storage;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;

/**
 * Created by linovm on 7/10/15.
 */
public class PostalCodeStorage {


    private Map<String,Set<String>> postalCodes;
    private Map<String,String> city;

    private static PostalCodeStorage instance;

    private Boolean load = Boolean.FALSE;

    private Context context;

    private PostalCodeStorage(){
        this.postalCodes = new HashMap<String,Set<String>>();
        this.city = new HashMap<String,String>();
    }

    public static PostalCodeStorage getInstance(Context context) {
        if(instance == null){
            instance = new PostalCodeStorage();
            if(context != null) {
                instance.context = context;
            }
            instance.getJson();
        } else if(instance.postalCodes == null || instance.postalCodes.isEmpty()){
            instance.getJson();
        }

        if(context != null){
            instance.context = context;
        }

        return instance;
    }


    public void addPostaCode (String country, String cp, String city){
        Set<String> cps = postalCodes.get(country);
        if(cps == null){
            cps = new HashSet<String>();
        }

        cps.add(cp);
        postalCodes.put(country, cps);
        this.city.put(cp,city);
    }

    public boolean find(String country, String cp){
        Boolean result = Boolean.FALSE;

        Boolean haveCountry = postalCodes.get(country) !=null &&  !postalCodes.get(country).isEmpty();
        if(haveCountry){
            result = postalCodes.get(country).contains(cp);
        }

        return result;

    }

    public String getCity(String cp){
        return city.get(cp);

    }

    public int count(){
       return postalCodes.size();
    }

    public Boolean getLoad() {
        return load;
    }

    public void setLoad(Boolean load) {
        this.load = load;
    }


    public void saveJson(){
        if(context != null){
            ConfigRepository db = new ConfigRepository(context);
            Gson gson = new Gson();
            JSONPostalCodeStorage json = new JSONPostalCodeStorage(postalCodes,city,load);
            String jsonProducts = gson.toJson(json);
            db.updateRegister(ConfigRepository.POSTAL_CODE, jsonProducts);
            Log.d("POSTALCODE", "Guardado");
        }

    }

    public void getJson(){
        if(context != null){
            ConfigRepository db = new ConfigRepository(context);
            String json = null;
            json = db.getValueConfig(ConfigRepository.POSTAL_CODE);

            if(json != null && !json.isEmpty() && !json.equals("0")){
                try{
                    Gson gson = new Gson();
                    JSONPostalCodeStorage jsonOrderStorage = gson.fromJson(json, JSONPostalCodeStorage.class);
                    postalCodes = jsonOrderStorage.getPostalCodes();
                    city= jsonOrderStorage.getCity();
                    load = jsonOrderStorage.getLoad();

                    Log.d("POSTALCODE","Se recoge de la BD");
                }catch (Exception e){
                    Log.e("POSTALCODE", "Error al recoger de bd", e);
                }
            }
        }
    }



    private class JSONPostalCodeStorage{
        private Map<String,Set<String>> postalCodes;
        private Map<String,String> city;
        private Boolean load = Boolean.FALSE;

        public Map<String, Set<String>> getPostalCodes() {
            return postalCodes;
        }

        public void setPostalCodes(Map<String, Set<String>> postalCodes) {
            this.postalCodes = postalCodes;
        }

        public Map<String, String> getCity() {
            return city;
        }

        public void setCity(Map<String, String> city) {
            this.city = city;
        }

        public Boolean getLoad() {
            return load;
        }

        public void setLoad(Boolean load) {
            this.load = load;
        }

        public JSONPostalCodeStorage() {
        }

        public JSONPostalCodeStorage(Map<String, Set<String>> postalCodes, Map<String, String> city, Boolean load) {
            this.postalCodes = postalCodes;
            this.city = city;
            this.load = load;
        }
    }

}
