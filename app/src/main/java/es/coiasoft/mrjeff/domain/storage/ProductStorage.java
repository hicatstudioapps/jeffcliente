package es.coiasoft.mrjeff.domain.storage;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import es.coiasoft.mrjeff.Utils.LanguageUtil;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.orders.send.Product;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;
import es.coiasoft.mrjeff.manager.model.comparator.ProductComparator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class ProductStorage {
	
	private Map<String,Map<Integer,Products>> products;
	private long time= 0;
	private Context context;

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	private static ProductStorage instance;
	
	private ProductStorage(){
		products = new HashMap<String, Map<Integer,Products>>();
	}
	
	public static ProductStorage getInstance(Context context){
		if(instance == null){
			instance = new ProductStorage();
			if(context != null) {
				instance.context = context;
			}
			instance.getJson();
		} else if(instance.products == null || instance.products.isEmpty()){
			instance.getJson();
		}

		return instance;
	}

	public void addProduct(Products product){
		if(product != null && product.getTags() != null && !product.getTags().isEmpty()){
			for(String keyProduct: product.getTags()) {
				Map<Integer,Products> productsKey = products.get(keyProduct);
				if(productsKey == null){
					productsKey = new HashMap<Integer, Products>();
				}
				productsKey.put(product.getId(), product);
				products.put(keyProduct, productsKey);
			}
		}
	}
	
	public Products findProduct(String key, Integer idProduct){
		Products result = null;
		Map<Integer,Products> productsKey = products.get(key);
		if(productsKey != null && !productsKey.isEmpty()){
			result = productsKey.get(idProduct);
		}
		return result;
	}

	public Products getProduct(Integer idProduct){
		Products result = null;
		Set<String> keys = products.keySet();
		for(String key: keys){
			result = findProduct(key,idProduct);
			if(result != null){
				break;
			}
		}
		return result;
	}
	public ArrayList<Products> getCategory(String key){
		ArrayList<Products> result = null;
		Map<Integer,Products> productsKey = products.get(key);
		if(productsKey != null && !productsKey.isEmpty()){
			result = new ArrayList<Products>(productsKey.values());
		}
        if(result != null && !result.isEmpty()){
            Collections.sort(result, new ProductComparator());
        }
		return result;
	}


	public Integer findProductByUrl(String url){
		Integer result = null;
		Set<String> keys = products.keySet();
		ArrayList<Products> productsResult = new ArrayList<Products>();
		if(keys != null && !keys.isEmpty()){
			for(String key: keys){
				ArrayList<Products> categoryProducts = getCategory(key);
				if(categoryProducts != null && !categoryProducts.isEmpty()){
					for(Products product: categoryProducts){
						if(product.getPermalink() != null &&
								prepareUrl(product.getPermalink().trim()).equalsIgnoreCase(prepareUrl(url.trim()))){
							result = product.getId();
							break;
						}
					}
				}
			}
		}

		return result;
	}

	private String prepareUrl(String url){
		String result = url.replace("http://","").replace("https://","");
		if(result.contains("?")){
			result = result.substring(0,result.indexOf("?"));
		}

		return result;
	}


	public ArrayList<Products> getAllProduct(){
		Set<String> keys = products.keySet();
		ArrayList<Products> productsResult = new ArrayList<Products>();
		if(keys != null && !keys.isEmpty()){
			for(String key: keys){
                productsResult.addAll(getCategory(key));
            }
		}

        return productsResult;
	}


	public void clear(){
		products = new HashMap<String, Map<Integer,Products>>();
	}


	public void saveJson(){
		if(context != null && products != null && !products.isEmpty()){
			ConfigRepository db = new ConfigRepository(context);
			Gson gson = new Gson();
			JSONProductStorage json = new JSONProductStorage(products);
			String jsonProducts = gson.toJson(json);
			printLog("UPDATEPRODUCT",jsonProducts);
            if(LanguageUtil.isSpanish()) {
				db.updateRegister(ConfigRepository.PRODUCT_ES, jsonProducts);
			} else if(LanguageUtil.isCatala()) {
				db.updateRegister(ConfigRepository.PRODUCT_CA, jsonProducts);
			} else {
				db.updateRegister(ConfigRepository.PRODUCT_EN, jsonProducts);
			}
		} else if(context != null){
			ConfigRepository db = new ConfigRepository(context);
			if(LanguageUtil.isSpanish()) {
				db.updateRegister(ConfigRepository.PRODUCT_ES, "");
			} else if(LanguageUtil.isCatala()) {
				db.updateRegister(ConfigRepository.PRODUCT_CA, "");
			} else {
				db.updateRegister(ConfigRepository.PRODUCT_EN, "");
			}
		}

	}

	public void getJson(){
		if(context != null){
			ConfigRepository db = new ConfigRepository(context);
			String json = null;
			if(LanguageUtil.isSpanish()) {
				json = db.getValueConfig(ConfigRepository.PRODUCT_ES);
			} else if(LanguageUtil.isCatala()) {
				json = db.getValueConfig(ConfigRepository.PRODUCT_CA);
			} else {
                json = db.getValueConfig(ConfigRepository.PRODUCT_EN);
			}

			if(json != null && !json.isEmpty() && !json.equals("0")){
				try{
					Gson gson = new Gson();
					JSONProductStorage jsonProductStorage = gson.fromJson(json, JSONProductStorage.class);
					products = jsonProductStorage.getProducts();
				}catch (Exception e){
					Log.e("ProductStroage", "Error al recoger de bd", e);
				}
			}
		}
	}

    private void printLog(String TAG, String sb){
        if (sb.length() > 4000) {
            Log.d(TAG, "sb.length = " + sb.length());
            int chunkCount = sb.length() / 4000;     // integer division
            for (int i = 0; i <= chunkCount; i++) {
                int max = 4000 * (i + 1);
                if (max >= sb.length()) {
                    Log.v(TAG, sb.substring(4000 * i));
                } else {
                    Log.v(TAG, sb.substring(4000 * i, max));
                }
            }
        } else {
            Log.v(TAG, sb.toString());
        }
    }


	private class JSONProductStorage implements Serializable{

		private Map<String,Map<Integer,Products>> products;

		public Map<String, Map<Integer, Products>> getProducts() {
			return products;
		}

		public void setProducts(Map<String, Map<Integer, Products>> products) {
			this.products = products;
		}

		public JSONProductStorage(Map<String, Map<Integer, Products>> products) {
			this.products = products;
		}
	}



	
}
