package es.coiasoft.mrjeff.domain.storage;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Calendar;

import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.SuscriptionInfo;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;

public class SuscriptionCustomerStorage {

	private SuscriptionInfo suscriptionInfo;
	private SuscriptionInfo suscriptionInfoSend;
	private String iddCustomer;
	private long timeLoad = 0;

	private long time= 0;
	private Context context;

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	private static SuscriptionCustomerStorage instance;

	private SuscriptionCustomerStorage(){
	}
	
	public static SuscriptionCustomerStorage getInstance(Context context){
		if(instance == null){
			instance = new SuscriptionCustomerStorage();
			if(context != null) {
				instance.context = context;
			}
			instance.getJson();
		} else if(instance.suscriptionInfo == null){
			instance.getJson();
		}

		return instance;
	}

	public void addInfo(SuscriptionInfo info){
		this.suscriptionInfo = info;
	}

	public SuscriptionInfo getOrder(){
		return suscriptionInfo;
	}

	public SuscriptionInfo getSuscriptionInfo() {
		return suscriptionInfo;
	}

	public void setSuscriptionInfo(SuscriptionInfo suscriptionInfo) {
		this.suscriptionInfo = suscriptionInfo;
	}

	public SuscriptionInfo getSuscriptionInfoSend() {
		return suscriptionInfoSend;
	}

	public void setSuscriptionInfoSend(SuscriptionInfo suscriptionInfoSend) {
		this.suscriptionInfoSend = suscriptionInfoSend;
	}

	public boolean isSuscription(){
		boolean result = true;
		result = CustomerStorage.getInstance(context).isLogged() && suscriptionInfo != null && iddCustomer != null;
		if(result){
			result = CustomerStorage.getInstance(context).getCustomer().getId().toString().equals(iddCustomer) &&
					suscriptionInfo != null && suscriptionInfo.getEstado() != null && suscriptionInfo != null && suscriptionInfo.getEstado().trim().equals("1");
		}

		return result;
	}
	

	public void clear(){
		suscriptionInfo = null;
	}

	public String getIddCustomer() {
		return iddCustomer;
	}

	public void setIddCustomer(String iddCustomer) {
		this.iddCustomer = iddCustomer;
	}

	public void saveJson(){
		if(context != null && suscriptionInfo != null){
			timeLoad = Calendar.getInstance().getTimeInMillis();
			ConfigRepository db = new ConfigRepository(context);
			GsonBuilder gsonBuilder = new GsonBuilder();
			//gsonBuilder.registerTypeAdapter(EnumStateOrder.class, new AttributeScopeDeserializer());
			Gson gson = gsonBuilder.create();
			JSONSuscriptionStorage json = new JSONSuscriptionStorage(suscriptionInfo,iddCustomer,suscriptionInfoSend,timeLoad);
			String jsonProducts = gson.toJson(json);
			db.updateRegister(ConfigRepository.SUSCRIPTION, jsonProducts);
		} else if(context != null){
			ConfigRepository db = new ConfigRepository(context);
			db.updateRegister(ConfigRepository.SUSCRIPTION, "0");
		}

	}

	public void getJson(){
		if(context != null){
			ConfigRepository db = new ConfigRepository(context);
			String json = null;
			json = db.getValueConfig(ConfigRepository.SUSCRIPTION);

			if(json != null && !json.isEmpty() && !json.equals("0")){
				try{
					Gson gson = new Gson();
					JSONSuscriptionStorage jsonOrdersStorage = gson.fromJson(json, JSONSuscriptionStorage.class);
					suscriptionInfo = jsonOrdersStorage.suscriptionInfo;
					iddCustomer = jsonOrdersStorage.getIdCustomer();
					suscriptionInfoSend = jsonOrdersStorage.getSuscriptionInfoSend();
					timeLoad = jsonOrdersStorage.timeLoad;
				}catch (Exception e){
					Log.e("ProductStroage", "Error al recoger de bd", e);
				}
			}
		}
	}

	public boolean isNeccesaryLoad(){
		long today = Calendar.getInstance().getTimeInMillis();
		return Long.valueOf(today - timeLoad).compareTo(Long.valueOf(60000l)) > 0;
	}


	private class JSONSuscriptionStorage implements Serializable{


		private SuscriptionInfo suscriptionInfo;

		private SuscriptionInfo suscriptionInfoSend;

		private String idCustomer;

		private long timeLoad = 0;


		public SuscriptionInfo getSuscriptionInfo() {
			return suscriptionInfo;
		}

		public void setSuscriptionInfo(SuscriptionInfo suscriptionInfo) {
			this.suscriptionInfo = suscriptionInfo;
		}

		public String getIdCustomer() {
			return idCustomer;
		}

		public void setIdCustomer(String idCustomer) {
			this.idCustomer = idCustomer;
		}

		public SuscriptionInfo getSuscriptionInfoSend() {
			return suscriptionInfoSend;
		}

		public void setSuscriptionInfoSend(SuscriptionInfo suscriptionInfoSend) {
			this.suscriptionInfoSend = suscriptionInfoSend;
		}

		public JSONSuscriptionStorage(SuscriptionInfo suscriptionInfo, String idCustomer, SuscriptionInfo suscriptionInfoSend, Long timeLoad) {
			this.suscriptionInfo = suscriptionInfo;
			this.idCustomer = idCustomer;
			this.suscriptionInfoSend = suscriptionInfoSend;
			this.timeLoad = timeLoad;
		}
	}


	class AttributeScopeDeserializer implements JsonDeserializer<EnumStateOrder>
	{
		@Override
		public EnumStateOrder deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
		{
			EnumStateOrder[] states = EnumStateOrder.values();
			for (EnumStateOrder state : states)
			{
				if (state.getState() == json.getAsInt())
					return state;
			}
			return null;
		}

	}


	
}
