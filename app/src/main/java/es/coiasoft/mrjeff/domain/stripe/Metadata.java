package es.coiasoft.mrjeff.domain.stripe;

import java.util.HashMap;

/**
 * Created by linovm on 6/11/15.
 */
public class Metadata {

    private HashMap<String,String> metada;

    public HashMap<String, String> getMetada() {
        return metada;
    }

    public void setMetada(HashMap<String, String> metada) {
        this.metada = metada;
    }
}
