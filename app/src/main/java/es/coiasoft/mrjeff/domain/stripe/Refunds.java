package es.coiasoft.mrjeff.domain.stripe;

import java.util.ArrayList;

/**
 * Created by linovm on 6/11/15.
 */
public class Refunds {

    private ArrayList<String> data;
    private Boolean has_more;
    private String object;
    private Integer total_count;
    private String url;

    public ArrayList<String> getData() {
        return data;
    }

    public void setData(ArrayList<String> data) {
        this.data = data;
    }

    public Boolean getHas_more() {
        return has_more;
    }

    public void setHas_more(Boolean has_more) {
        this.has_more = has_more;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public Integer getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Integer total_count) {
        this.total_count = total_count;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
