package es.coiasoft.mrjeff.gcm;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.localytics.android.PushReceiver;

import es.coiasoft.mrjeff.DeepLinkingHTTP;
import es.coiasoft.mrjeff.DeepLinkingMJeffSchema;

/**
 * Created by Paulino on 25/05/2016.
 */
public class MrJeffBroadCastReceiver extends PushReceiver {

    public MrJeffBroadCastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("GCM-MRJEFF","Entra");
        try {
            String link = intent.getStringExtra("deeplink");
            if (link != null && !link.isEmpty() && !link.trim().equalsIgnoreCase("null")) {
                Log.d("GCM-MRJEFF","Deeplink - " + link);
                intent.setData(Uri.parse(link));
                if (link.startsWith("http")) {
                    intent.setComponent(new ComponentName(context, DeepLinkingHTTP.class));
                    Intent gotoOffersIntent = new Intent(context,DeepLinkingHTTP.class);
                    intent.setData(Uri.parse(link));
                    context.startActivity(gotoOffersIntent);
                } else {
                    intent.setComponent(new ComponentName(context, DeepLinkingMJeffSchema.class));
                    Intent gotoOffersIntent = new Intent(context,DeepLinkingMJeffSchema.class);
                    intent.setData(Uri.parse(link));
                    context.startActivity(gotoOffersIntent);
                }
            } else {
                Log.d("GCM-MRJEFF","No encuentra deeplink");
            }
        } catch (Exception e) {

        }

        super.onReceive(context, intent);

    }

}
