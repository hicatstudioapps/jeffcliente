package es.coiasoft.mrjeff.manager;

import java.util.ArrayList;
import java.util.Observable;
import java.util.StringJoiner;

import es.coiasoft.mrjeff.domain.ResultDatetimeMrJeff;
import es.coiasoft.mrjeff.domain.SuscriptionInfo;
import es.coiasoft.mrjeff.domain.UpdateResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by admin on 09/03/2017.
 */

public interface JeffApi {

    @FormUrlEncoded
    @POST
    public rx.Observable<ArrayList<UpdateResponse>> sendUpdate(@Url String url, @Field("order_id") String id_pedio,
                                                              @Field("payment") String payment);
    //http://192.168.101.1:8081/jeff/time.json
    //http://ec2-52-51-106-207.eu-west-1.compute.amazonaws.com/timetablev2.php
    @GET("http://ec2-52-51-106-207.eu-west-1.compute.amazonaws.com/timetablev2.php")
    public Call<ResultDatetimeMrJeff> getTimeTable(@Query("postalCode")String postalCode);

    @POST("http://ec2-52-51-106-207.eu-west-1.compute.amazonaws.com/clientsuscriptionv2.php")
    @FormUrlEncoded
    Call<SuscriptionInfo> getSuscriptionInfo(@Field("clientId") String clientId);

    @POST("http://ec2-52-51-106-207.eu-west-1.compute.amazonaws.com/deleteappdata.php")
    @FormUrlEncoded
    rx.Observable<ResponseBody> deleteProductOrOrder(@Field("entity_id") String entity_id, @Field("entity_type") String entity_type );
}
