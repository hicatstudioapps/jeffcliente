package es.coiasoft.mrjeff.manager.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import es.coiasoft.mrjeff.manager.model.connect.MrJeffDaraSource;
import es.coiasoft.mrjeff.manager.model.util.ConstantsDB;

/**
 * Created by linovm on 18/10/15.
 */
public class ConfigRepository extends MrJeffDaraSource {

    public static final String NEW_USER = "NEW_USER";
    public static final String POSTALCODE = "POSTALCODE";
    public static final String CITY = "CITY";
    public static final String ADDRESS_CITY = "ADDCITY";
    public static final String ADDRESS_CP = "ADDCP";
    public static final String ADDRESS_PHONE = "ADDPHONE";
    public static final String ADDRESS_NAME = "ADDNAME";
    public static final String ADDRESS_EMAIL = "ADDEMAIL";
    public static final String IDORDER = "IDORDER";
    public static final String ONRESUME = "ONRESUME";
    public static final String PRODUCT_ES = "PRODUCT_ES";
    public static final String PRODUCT_EN = "PRODUCT_EN";
    public static final String PRODUCT_CA = "PRODUCT_CA";
    public static final String ORDER = "ORDER";
    public static final String CUSTOMER = "CUSTOMER";
    public static final String DATE_INVALID_STORAGE = "DATE_INVALID_STORAGE";
    public static final String POSTAL_CODE = "POSTAL_CODE";
    public static final String ORDER_PRODUCT = "ORDER_PRODUCT";
    public static final String RATE = "RATE";
    public static final String CONTACT = "CONTAC";
    public static final String CACHE_UPDATE_PRODUCTS = "CACHE_UPDATE_PRODUCTS";
    public static final String CACHE_UPDATE_FAQS = "CACHE_UPDATE_FAQS";
    public static final String FAQS = "FAQS";
    public static final String ORDERS = "ORDERS";
    public static final String CACHE_UPDATE_ORDERS = "CACHE_UPDATE_ORDERS";
    public static final String SUSCRIPTION = "SUSCRIPTION";
    public static final String BLOCK = "BLOCK";


    public ConfigRepository(Context context) {
        super(context);
    }


    public String getValueConfig(String name) {

        String result = null;
        Cursor c = null;
        try {
            String[] args = {name};
            String[] columns = {ConstantsDB.COLUMN_CONFIG.VALUE.getName()};
            c = databaseReader.query(ConstantsDB.TABLE_CONFIG_NAME,
                    columns,
                    ConstantsDB.COLUMN_CONFIG.NAME + " = ?",
                    args,
                    null, null, null);

            Boolean haveResult = c != null && c.getCount() > 0;
            if (haveResult) {
                c.moveToNext();
                result = c.getString(0);
            }
        } catch (Exception e) {

        } finally {
            if (c != null) {
                c.close();
            }
            try {
                databaseReader.close();
                databaseWriter.close();
            } catch (Exception e) {

            }
        }

        return result;
    }


    public void updateRegister(String name, String value) {
        try {
            ContentValues values = new ContentValues();
            values.put(ConstantsDB.COLUMN_CONFIG.VALUE.getName(), value);

            String selection = ConstantsDB.COLUMN_CONFIG.NAME.getName() + " = ?";
            String[] selectionArgs = {name};

            databaseWriter.update(ConstantsDB.TABLE_CONFIG_NAME, values, selection, selectionArgs);
        } catch (Exception e) {
        } finally {
            try {
                databaseReader.close();
                databaseWriter.close();
            } catch (Exception e) {

            }
        }
    }


}
