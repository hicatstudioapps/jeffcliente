package es.coiasoft.mrjeff.manager.model.comparator;

import java.util.Comparator;

import es.coiasoft.mrjeff.domain.GroupStateOrder;

/**
 * Created by Paulino on 21/01/2016.
 */
public class GroupStateOrderComparator implements Comparator<GroupStateOrder> {

    @Override
    public int compare(GroupStateOrder lhs, GroupStateOrder rhs) {
        return lhs.compareTo(rhs);
    }
}
