package es.coiasoft.mrjeff.manager.model.comparator;

import java.util.Comparator;

import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.orders.response.Orders;

/**
 * Created by Paulino on 21/01/2016.
 */
public class OrdersComparator implements Comparator<Orders> {

    @Override
    public int compare(Orders lhs, Orders rhs) {
        return lhs.compareTo(rhs);
    }
}
