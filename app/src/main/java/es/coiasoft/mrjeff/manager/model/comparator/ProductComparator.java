package es.coiasoft.mrjeff.manager.model.comparator;

import java.util.Comparator;

import es.coiasoft.mrjeff.domain.Products;

/**
 * Created by Paulino on 21/01/2016.
 */
public class ProductComparator implements Comparator<Products> {
    @Override
    public int compare(Products lhs, Products rhs) {
        return lhs.compareTo(rhs);
    }
}
