package es.coiasoft.mrjeff.manager.model.connect;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by linovm on 18/10/15.
 */
public class MrJeffDaraSource {

    protected MrJeffReaderDbHelper openHelper;
    protected SQLiteDatabase databaseWriter;
    protected SQLiteDatabase databaseReader;

    public MrJeffDaraSource(Context context) {
        //Creando una instancia hacia la base de datos
        openHelper = new MrJeffReaderDbHelper(context);
        databaseWriter = openHelper.getWritableDatabase();
        databaseReader = openHelper.getReadableDatabase();
    }
}
