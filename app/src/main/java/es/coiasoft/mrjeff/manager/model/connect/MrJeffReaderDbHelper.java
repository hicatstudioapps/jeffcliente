package es.coiasoft.mrjeff.manager.model.connect;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.manager.model.util.ConstantsDB;

/**
 * Created by linovm on 18/10/15.
 */
public class MrJeffReaderDbHelper extends SQLiteOpenHelper {

    protected Context context;

    public MrJeffReaderDbHelper(Context context) {
        super(context,
                ConstantsDB.DB_NAME,//String name
                null,//factory
                ConstantsDB.DB_VERSION//int version
        );

        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Log.v("BD_INIT", "Se inicia la creacion de la BD");
            insertFromFile(db, R.raw.mrjeff_new);
        } catch (Exception e) {
            Log.e("BD_INIT", "ERROR AL CREAR LA BD",e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            if (oldVersion == 1) {
                Log.v("BD_INIT", "Se inicia la actualizacion de la BD v1");
                insertFromFile(db, R.raw.mrjeff_update_v2);
            } if(oldVersion <= 7){
                insertFromFile(db, R.raw.mrjeff_update_v7);
            } else {
                Log.v("BD_INIT", "Se inicia la actualizacion de la BD v2 o mas");
                insertFromFile(db, R.raw.mrjeff_update_products);

            }
        } catch (Exception e) {
            Log.e("BD_INIT", "ERROR AL ACTUALIZAR LA BD");
        }
    }

    public int insertFromFile(SQLiteDatabase db, int resourceId) throws IOException {
        int result = 0;
        InputStream insertsStream = context.getResources().openRawResource(resourceId);
        BufferedReader insertReader = new BufferedReader(new InputStreamReader(insertsStream));
        int i = 0;
        while (insertReader.ready()) {
            Log.v("BD_INIT", "Se lee sentencia " + i);
            String insertStmt = insertReader.readLine();
            Log.v("BD_INIT", "Se lanza sentencia " +i);
            db.execSQL(insertStmt);
            Log.v("BD_INIT", "Se ejecuto sentencia " + i);
            result++;
            i++;
        }
        insertReader.close();
        return result;
    }
}
