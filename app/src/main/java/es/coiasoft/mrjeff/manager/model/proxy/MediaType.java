package es.coiasoft.mrjeff.manager.model.proxy;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Paulino on 30/08/2016.
 */
public enum MediaType {

    @SerializedName("TV")
    TV("TV"),
    @SerializedName("FACEBOOK")
    FACEBOOK("FACEBOOK"),
    @SerializedName("INSTAGRAM")
    INSTAGRAM("INSTAGRAM"),
    @SerializedName("GOOGLE")
    GOOGLE("GOOGLE"),
    @SerializedName("OTHERS")
    OTHERS("OTHERS"),
    @SerializedName("FRIENDS")
    FRIENDS("FRIENDS");

    private String code;

    private MediaType(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }


}
