package es.coiasoft.mrjeff.manager.model.proxy;

import java.io.Serializable;

/**
 * Created by Paulino on 30/08/2016.
 */
public class OrderProxy implements Serializable {

    private static final long serialVersionUID = -6073587774078590461L;

    private Long idOrder;
    private String note;
    private String idBill;
    private String idDeliveryNote;
    private Integer bidBag;
    private Integer smallbag;
    private Integer hanger;
    private Boolean active;
    private String metNote;
    private String valueNote;
    private Boolean deleted;
    private String idWoocommerce;
    private String idOpenBravo;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Long idOrder) {
        this.idOrder = idOrder;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getIdBill() {
        return idBill;
    }

    public void setIdBill(String idBill) {
        this.idBill = idBill;
    }

    public String getIdDeliveryNote() {
        return idDeliveryNote;
    }

    public void setIdDeliveryNote(String idDeliveryNote) {
        this.idDeliveryNote = idDeliveryNote;
    }

    public Integer getBidBag() {
        return bidBag;
    }

    public void setBidBag(Integer bidBag) {
        this.bidBag = bidBag;
    }

    public Integer getSmallbag() {
        return smallbag;
    }

    public void setSmallbag(Integer smallbag) {
        this.smallbag = smallbag;
    }

    public Integer getHanger() {
        return hanger;
    }

    public void setHanger(Integer hanger) {
        this.hanger = hanger;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getMetNote() {
        return metNote;
    }

    public void setMetNote(String metNote) {
        this.metNote = metNote;
    }

    public String getValueNote() {
        return valueNote;
    }

    public void setValueNote(String valueNote) {
        this.valueNote = valueNote;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getIdWoocommerce() {
        return idWoocommerce;
    }

    public void setIdWoocommerce(String idWoocommerce) {
        this.idWoocommerce = idWoocommerce;
    }

    public String getIdOpenBravo() {
        return idOpenBravo;
    }

    public void setIdOpenBravo(String idOpenBravo) {
        this.idOpenBravo = idOpenBravo;
    }
}