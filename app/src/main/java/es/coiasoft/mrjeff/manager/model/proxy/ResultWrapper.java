package es.coiasoft.mrjeff.manager.model.proxy;

/**
 * Created by Paulino on 30/08/2016.
 */
public class ResultWrapper<T> {

    public static final int RESULT_OK = 1;
    public static final int RESULT_ERROR_FORMAT = -1;
    public static final int RESULT_ERROR_NOT_VALID = -2;
    public static final int RESULT_ERROR_SERVER = -3;
    public static final int RESULT_ERROR_COMPARE = 0;


    private Boolean haveError;
    private String messageError;
    private int codeResult;
    private Boolean isEmpty;
    private T data;


    public Boolean getHaveError() {
        return haveError;
    }
    public String getMessageError() {
        return messageError;
    }
    public int getCodeResult() {
        return codeResult;
    }
    public Boolean getIsEmpty() {
        return isEmpty;
    }
    public T getData() {
        return data;
    }
    public void setHaveError(Boolean haveError) {
        this.haveError = haveError;
    }
    public void setMessageError(String messageError) {
        this.messageError = messageError;
    }
    public void setCodeResult(int codeResult) {
        this.codeResult = codeResult;
    }
    public void setIsEmpty(Boolean isEmpty) {
        this.isEmpty = isEmpty;
    }
    public void setData(T data) {
        this.data = data;
    }




}

