package es.coiasoft.mrjeff.manager.model.proxy;

import java.io.Serializable;

/**
 * Created by Paulino on 30/08/2016.
 */
public class Valoration implements Serializable {

    private static final long serialVersionUID = -282427720398950961L;
    private Long id;
    private OrderProxy order;
    private MediaType media;
    private Integer rateJeff;
    private Integer rateClothes;
    private Boolean recomended;
    private Boolean usedAgain;
    private String notes;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrderProxy getOrder() {
        return order;
    }

    public void setOrder(OrderProxy order) {
        this.order = order;
    }

    public MediaType getMedia() {
        return media;
    }

    public void setMedia(MediaType media) {
        this.media = media;
    }

    public Integer getRateJeff() {
        return rateJeff;
    }

    public void setRateJeff(Integer rateJeff) {
        this.rateJeff = rateJeff;
    }

    public Integer getRateClothes() {
        return rateClothes;
    }

    public void setRateClothes(Integer rateClothes) {
        this.rateClothes = rateClothes;
    }

    public Boolean getRecomended() {
        return recomended;
    }

    public void setRecomended(Boolean recomended) {
        this.recomended = recomended;
    }

    public Boolean getUsedAgain() {
        return usedAgain;
    }

    public void setUsedAgain(Boolean usedAgain) {
        this.usedAgain = usedAgain;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
