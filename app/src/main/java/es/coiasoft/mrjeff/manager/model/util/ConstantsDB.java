package es.coiasoft.mrjeff.manager.model.util;

import android.provider.BaseColumns;

import es.coiasoft.mrjeff.manager.model.ConfigRepository;

/**
 * Created by linovm on 18/10/15.
 */
public class ConstantsDB {

    public static final String DB_NAME="mrjeff";
    public static final Integer DB_VERSION= 8;


    public static final String TABLE_CONFIG_NAME="config";
    public static enum COLUMN_CONFIG{
        ID(BaseColumns._ID), NAME("name"),VALUE("value"),VERSION("VERSION");

        private String name;
        private COLUMN_CONFIG(String name){
            this.name = name;
        }

        public String getName(){
            return name;
        }
    }
}
