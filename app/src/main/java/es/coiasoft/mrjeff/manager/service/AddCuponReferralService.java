
package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import es.coiasoft.mrjeff.ResultServiceCustomice;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 7/10/15.
 */
public class AddCuponReferralService {


    public ResultServiceCustomice getReferral(String idUser,String codeCoupon){
        ResultServiceCustomice result = null;
        try {

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_POST_GET_REFERRAL_USER_PARAM_ID, idUser));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_POST_GET_REFERRAL_USER_PARAM_COUPON, codeCoupon));
            HttpEntity entity = ConnectionUtil.getHttpEntityUrlEncode(pairs);

            HttpResponse response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_POST_ADD_REFERRAL_USER, false, true, entity);

            if (ConnectionUtil.isValidRespose(response)) {
                String responseString = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                result = gson.fromJson(responseString, ResultServiceCustomice.class);
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }
}
