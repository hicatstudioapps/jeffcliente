package es.coiasoft.mrjeff.manager.service;


import android.util.Log;

import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 21/10/15.
 */
public class DeleteOrderService {

    public void deleteCart(String idOrder)  throws Exception{
        HttpResponse response = ConnectionUtil.executeDelete(getUrlDelete(idOrder), true);
        if (ConnectionUtil.isValidRespose(response)) {
            String result = ConnectionUtil.readReponse(response);
        } else {
            Log.d("JSON", "Failed to download file");
        }
    }

    private String getUrlDelete(String idOrder){
        String url = MrJeffConstant.URL.URL_DELETE_ORDER;
        url = url.replace(MrJeffConstant.URL.ARG_ORDER,idOrder);

        return url;
    }

}
