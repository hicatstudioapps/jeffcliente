package es.coiasoft.mrjeff.manager.service;



import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 21/10/15.
 */
public class DeleteProductService {

    public Boolean deleteProduct(Integer id){
        Boolean result = Boolean.FALSE;
        HttpResponse response = ConnectionUtil.executeDelete(getUrlDelete(id), true);
        result = ConnectionUtil.isValidRespose(response);
        return result;
    }

    private String getUrlDelete(Integer id){
        String url = MrJeffConstant.URL.URL_DELETE_PRODUCT;
        url = url.replace(MrJeffConstant.URL.ARG_PRODUCT,id.toString());

        return url;
    }




}
