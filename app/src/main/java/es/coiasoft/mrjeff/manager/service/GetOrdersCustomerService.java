package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Catalog;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.ResultCpsDateInvalid;
import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.orders.response.ResultOrders;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 7/10/15.
 */
public class GetOrdersCustomerService {
    

    private String idCustomer;

    public List<Orders> getOrdersCustomer(String idCustomer,int orderId){
        this.idCustomer = idCustomer;
        //this.idCustomer="1394";
        //this.idCustomer="183";
        String json = connectService(orderId);
        Gson gson = new Gson();
        List<Orders> result = new ArrayList<>();

        try {if(orderId==-1){
            ResultOrders resultJson = gson.fromJson(json, ResultOrders.class);
            result = resultJson.getOrders();}
            else {
            result.add(gson.fromJson(json,Example.class).getOrder());
        }
        }catch (Exception e){
            Log.e("GetOrdersCSer","Error parseando ordenes del usuario",e);
        }
        return result != null && !result.isEmpty() ? result:null;
    }


    private String connectService(int orderId){
        String result = "";
        String url="";
        try {
            if(orderId==-1) {
                url = MrJeffConstant.URL.URL_GET_ORDER_BY_USERS.replace(MrJeffConstant.URL.ARG_ID, idCustomer);
                Log.d("URL orders woocommerce", url);
//                url = "http://192.168.101.1:8081/jeff/coladas.txt";
            }else {
                url="https://mrjeffapp.com/wc-api/v2/orders/"+orderId+"?filter[meta]=true&force=true&consumer_key=ck_04ef92d3a3ff36ccd8427d24f289c47c&consumer_secret=cs_0b609d5cc4cd3d199cd3ba927b4b5ce7";
//                url = "http://192.168.101.1:8081/jeff/coladas.txt";
            }
            HttpResponse response = ConnectionUtil.executeGet(url, true, true);
            if (ConnectionUtil.isValidRespose(response)) {
                result = ConnectionUtil.readReponse(response);
            } else {
                Log.d("JSON", "Failed to download file");
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }

    class Example {

        @SerializedName("order")
        @Expose
        private Orders order;

        public Orders getOrder() {
            return order;
        }

        public void setOrder(Orders order) {
            this.order = order;
        }

    }
}
