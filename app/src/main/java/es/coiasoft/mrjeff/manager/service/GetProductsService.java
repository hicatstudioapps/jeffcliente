package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;

import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.LanguageUtil;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Catalog;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

import java.util.List;
import java.util.Locale;

/**
 * Created by linovm on 7/10/15.
 */
public class GetProductsService {
    

    private String key;

    public List<Products> getProducts(String key){
        this.key = key;
        String json = connectService();
        Gson gson = new Gson();
        Catalog catalog  = gson.fromJson(json,Catalog.class);

        return catalog != null ? catalog.getProducts():null;
    }


    private String connectService(){
        String result = "";
        try {
            HttpResponse response = ConnectionUtil.executeGet(getURL(), true, true);
            if (ConnectionUtil.isValidRespose(response)) {
                result = ConnectionUtil.readReponse(response);
            } else {
                Log.d("JSON", "Failed to download file");
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }

    protected String getURL(){
        String url = MrJeffConstant.URL.URL_GET_PRODUCT;
        if(!LanguageUtil.isSpanish() && !LanguageUtil.isCatala()) {
            url = url.replace("?","?lang=en&");
        } else if(LanguageUtil.isCatala()){
            url = url.replace("?","?lang=ca&");
        }

        Log.d("PRODUCTS",url);
        return url;
    }
}
