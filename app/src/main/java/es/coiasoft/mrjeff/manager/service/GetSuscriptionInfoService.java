package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.InfoResidencia;
import es.coiasoft.mrjeff.domain.SuscriptionInfo;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by Paulino on 20/08/2016.
 */
public class GetSuscriptionInfoService {

    public SuscriptionInfo postSuscription(String idClient){
        SuscriptionInfo info = null;
        try {

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_HAVE_SUSCRIPTION_ATTR_CLIENT, idClient));
            HttpEntity entity = ConnectionUtil.getHttpEntityUrlEncode(pairs);

            HttpResponse response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_HAVE_SUSCRIPTION, false, true, entity);
//            HttpResponse response = ConnectionUtil.executePost("http://192.168.101.1:8081/jeff/sus.txt", false, true, entity);

            if (ConnectionUtil.isValidRespose(response)) {
                String json = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                info = gson.fromJson(json, SuscriptionInfo.class);
            }


        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return info;
    }
}
