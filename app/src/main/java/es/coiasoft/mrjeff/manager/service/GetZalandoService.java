package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.SuscriptionInfo;
import es.coiasoft.mrjeff.domain.ZalandoCoupon;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by Paulino on 20/08/2016.
 */
public class GetZalandoService {

    public static final int RESULT_OK = 1;
    public static final int RESULT_INACTIVO = -1;
    public static final int RESULT_NOTVALID = -2;
    public static final int RESULT_USES = -3;

    public ZalandoCoupon postCode(String coupon, String idClient){
        ZalandoCoupon result = null;
        try {

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_ZALANDO_CODE_ATTR_COUPON, coupon));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_ZALANDO_CODE_ATTR_USER, idClient));
            HttpEntity entity = ConnectionUtil.getHttpEntityUrlEncode(pairs);

            HttpResponse response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_ZALANDO_CODE, false, true, entity);

            if (ConnectionUtil.isValidRespose(response)) {
                String json = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                try {
                    Type listType = new TypeToken<List<ZalandoCoupon>>() {}.getType();
                    List<ZalandoCoupon> resultList = gson.fromJson(json, listType);
                    if(resultList == null || resultList.isEmpty()){
                        result = new ZalandoCoupon();
                        result.setCodeError(RESULT_NOTVALID);
                    } else {
                        result = resultList.get(0);
                    }

                    if(result.getCodigo().equalsIgnoreCase("correcto")){
                        result.setCodeError(RESULT_OK);
                    } else if(result.getMensaje() != null && result.getMensaje().contains("Desactivado")){
                        result.setCodeError(RESULT_INACTIVO);
                    } else if(result.getMensaje() != null && result.getMensaje().contains("No existe")){
                        result.setCodeError(RESULT_NOTVALID);
                    }  else if(result.getMensaje() != null && result.getMensaje().contains("Lo siento este")){
                        result.setCodeError(RESULT_USES);
                    }else{
                        result.setCodeError(RESULT_NOTVALID);
                    }
                }catch (Exception e){
                    result = new ZalandoCoupon();
                    result.setCodeError(RESULT_NOTVALID);
                }
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }
}
