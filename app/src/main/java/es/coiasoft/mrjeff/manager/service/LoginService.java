package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.ResultLogin;
import es.coiasoft.mrjeff.domain.orders.send.ResultCreateProduct;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 7/10/15.
 */
public class LoginService {


    public ResultLogin loginUser(String user,String password){
        ResultLogin result = null;
        try {

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("username", user));
            pairs.add(new BasicNameValuePair("password", password));
            HttpEntity entity = ConnectionUtil.getHttpEntityUrlEncode(pairs);

            HttpResponse response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_POST_LOGIN, false, true, entity);

            if (ConnectionUtil.isValidRespose(response)) {
                String responseString = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                result = gson.fromJson(responseString, ResultLogin.class);
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }
}
