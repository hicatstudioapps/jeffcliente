package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.manager.model.proxy.ResultWrapper;
import es.coiasoft.mrjeff.manager.model.proxy.Valoration;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 7/10/15.
 */
public class PostRateService {


    public boolean postRate(Valoration valoration){
        boolean result = false;
        try {

            HttpEntity entity = ConnectionUtil.getHttpEntityJSON(valoration, Valoration.class);
            HttpResponse response = ConnectionUtil.executePostProxyBackend(MrJeffConstant.URL.URL_POST_RATE, true, true,entity);

            if (ConnectionUtil.isValidRespose(response)) {
                String json = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                ResultWrapper<Valoration> resultService = gson.fromJson(json, new TypeToken<ResultWrapper<Valoration>>() {}.getType());
                if(resultService != null && resultService.getHaveError() != null && !resultService.getHaveError() &&
                        resultService.getData() != null){
                   result = true;
                }
            } else {
                String json = ConnectionUtil.readReponse(response);
                Log.d("readJSONFeed", json);
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }

}
