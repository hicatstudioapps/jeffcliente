package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.InfoResidencia;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by Paulino on 20/08/2016.
 */
public class PostSuscription {

    public List<InfoResidencia> postSuscription(String order, String user, String type, String coupon,String day,String hour,String address,String cp){
        List<InfoResidencia> info = null;
        try {

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_CREATE_SUSCRIPTION_ATTR_CLIENT, user));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_CREATE_SUSCRIPTION_ATTR_CODECOUPON, coupon));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_CREATE_SUSCRIPTION_ATTR_CODEORDER, order));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_CREATE_SUSCRIPTION_ATTR_TYPESUS, type));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_CREATE_SUSCRIPTION_ATTR_DAY, day));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_CREATE_SUSCRIPTION_ATTR_HOUR, hour));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_CREATE_SUSCRIPTION_ATTR_ADDRESS, address));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_CREATE_SUSCRIPTION_ATTR_CP, cp));
            HttpEntity entity = ConnectionUtil.getHttpEntityUrlEncode(pairs);

            HttpResponse response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_CREATE_SUSCRIPTION, false, true, entity);

            if (ConnectionUtil.isValidRespose(response)) {
                String json = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                info = gson.fromJson(json, new TypeToken<List<InfoResidencia>>(){}.getType());
            }


        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return info;
    }
}
