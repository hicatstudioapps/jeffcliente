package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.InfoResidencia;
import es.coiasoft.mrjeff.domain.SuscriptionInfo;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by Paulino on 20/08/2016.
 */
public class PostUpdateSuscriptionService {

    public boolean updateSuscription(SuscriptionInfo info){
        boolean result = false;
        try {

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_UPDATE_ATTR_ID_SUSCRIPTION, info.getIdSuscription()));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_UPDATE_ATTR_DEFAUlT_DAY, info.getDayComplete()));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_UPDATE_ATTR_DEFAUlT_HOUR, info.getDefaultHour()));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_UPDATE_ATTR_DEFAUlT_ADDRESS, info.getDefaultAddress()));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_UPDATE_ATTR_DEFAUlT_POSTAL_CODE, info.getDefaultPostalCode()));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_UPDATE_ATTR_ONLY_DELIVERY, info.getOnlyDelivery()));
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.URL_UPDATE_ATTR_ORDER_ID, info.getOrderId()));
            HttpEntity entity = ConnectionUtil.getHttpEntityUrlEncode(pairs);

            HttpResponse response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_UPDATE_SUSCRIPTION, false, true, entity);

            if (ConnectionUtil.isValidRespose(response)) {
                String json = ConnectionUtil.readReponse(response);
                result = json.contains("1");
            }


        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }
}
