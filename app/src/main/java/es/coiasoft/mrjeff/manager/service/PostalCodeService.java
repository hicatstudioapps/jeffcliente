package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.PostalCode;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 7/10/15.
 */
public class PostalCodeService {


    public List<PostalCode> getCPs(){

        String json = connectService();
        Gson gson = new Gson();
        List<PostalCode> cps =  new ArrayList<>();
        try {
            cps = gson.fromJson(json, new TypeToken<List<PostalCode>>() {}.getType());
        }catch (Exception e){
            Log.e("JSON", "Error parseando PostalCode");
        }
        return cps;
    }


    private String connectService(){
        String result = "";
        try {
            HttpResponse response = ConnectionUtil.executeGet(MrJeffConstant.URL.URL_GET_POSTAL_CODE,true,true);
            if (ConnectionUtil.isValidRespose(response)) {
                result = ConnectionUtil.readReponse(response);
            } else {
                Log.d("JSON", "Failed to download file");
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }
}
