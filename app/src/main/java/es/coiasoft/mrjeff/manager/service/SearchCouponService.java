package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;


import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.orders.ResultSearchCoupon;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 21/10/15.
 */
public class SearchCouponService {



    public ResultSearchCoupon getCoupon(String coupon){
        ResultSearchCoupon result = null;

        try {
            String resultString = "";
            HttpResponse response = ConnectionUtil.executeGet(getUrl(coupon), true, true);
            if (ConnectionUtil.isValidRespose(response)) {
                resultString = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                result = gson.fromJson(resultString, ResultSearchCoupon.class);
            } else {
                Log.d("JSON", "Failed to download file");
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }

        return result;
    }

    private String getUrl(String coupon){
        String url = MrJeffConstant.URL.URL_SEARCH_COUPON;
        url = url.replace(MrJeffConstant.URL.ARG_COUPON,coupon);

        return url;
    }



}
