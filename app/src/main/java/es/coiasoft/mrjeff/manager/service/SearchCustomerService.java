package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;

import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.domain.MessageCustomer;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 21/10/15.
 */
public class SearchCustomerService {

    public Customer searchCustomer(String email){
        Customer result = null;

        try {

            HttpResponse response = ConnectionUtil.executeGet(getUrl(email), false, true);
            if(ConnectionUtil.isValidRespose(response)){
                String bodyResponse = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                result = gson.fromJson(bodyResponse, MessageCustomer.class).getCustomer();
            }

        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }

    private String getUrl(String email){
        String urlFinal = MrJeffConstant.URL.URL_GET_CUSTOMER;
        urlFinal = urlFinal.replace(MrJeffConstant.URL.ARG_CUSTOMER,email);
        return urlFinal;
    }
}
