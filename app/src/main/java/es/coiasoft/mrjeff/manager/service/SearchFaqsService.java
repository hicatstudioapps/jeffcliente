package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Faq;
import es.coiasoft.mrjeff.domain.PostalCode;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 7/10/15.
 */
public class SearchFaqsService {


    public List<Faq> searchFaqsByProduct(String idProduct){
        List<Faq> faqs = null;
        try {

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair(MrJeffConstant.URL.ARG_ID_PRODUCT, idProduct));
            HttpEntity entity = ConnectionUtil.getHttpEntityUrlEncode(pairs);

            HttpResponse response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_POST_SEARCH_FAQS, false, true, entity);

            if (ConnectionUtil.isValidRespose(response)) {
                String json = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                faqs = gson.fromJson(json, new TypeToken<List<Faq>>(){}.getType());
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return faqs;
    }
}
