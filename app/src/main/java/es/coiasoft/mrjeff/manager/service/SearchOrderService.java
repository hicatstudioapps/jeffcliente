package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Faq;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.orders.response.ResultOrders;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 7/10/15.
 */
public class SearchOrderService {


    public Orders searchOrder(Integer idOrder){
        String json = connectService(idOrder);
        Gson gson = new Gson();
        Orders result = null;

        try {
            OrderSearch resultJson = gson.fromJson(json, OrderSearch.class);
            result = resultJson.getOrder();
        }catch (Exception e){
            Log.e("GetOrdersCSer","Error parseando ordenes del usuario",e);
        }
        return result != null? result:null;
    }

    private String connectService(Integer idOrder){
        String result = "";
        try {
            String url = MrJeffConstant.URL.URL_GET_ORDER.replace(MrJeffConstant.URL.ARG_ORDER,idOrder.toString());
//            url="http://192.168.101.1:8081/jeff/orderSus.txt";
            Log.d("url search order Sus", url);
            HttpResponse response = ConnectionUtil.executeGet(url, true, true);
            if (ConnectionUtil.isValidRespose(response)) {
                result = ConnectionUtil.readReponse(response);
            } else {
                Log.d("JSON", "Failed to download file");
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }


    class OrderSearch {
        private Orders order;

        public Orders getOrder() {
            return order;
        }

        public void setOrders(Orders orders) {
            this.order = orders;
        }
    }
}
