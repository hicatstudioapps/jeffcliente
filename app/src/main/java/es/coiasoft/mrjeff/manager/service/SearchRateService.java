package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.StatusOrder;
import es.coiasoft.mrjeff.manager.model.proxy.ResultWrapper;
import es.coiasoft.mrjeff.manager.model.proxy.Valoration;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 7/10/15.
 */
public class SearchRateService {


    public List<String> searchRates(String ids){
        List<String> result = null;
        try {

            HttpResponse response = ConnectionUtil.executeGetProxyBackend(getURL(ids.replaceAll(";",",")), true, true);

            if (ConnectionUtil.isValidRespose(response)) {
                String json = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                ResultWrapper<List<Valoration>> resultService = gson.fromJson(json, new TypeToken<ResultWrapper<List<Valoration>>>() {}.getType());
                if(resultService != null && (resultService.getHaveError() == null || !resultService.getHaveError()) &&
                        resultService.getData() != null && !resultService.getData().isEmpty()){
                    result = new ArrayList<String>();
                    for(Valoration rate : resultService.getData()){
                        if(rate != null && rate.getOrder() != null &&
                                rate.getOrder().getIdWoocommerce() != null){
                            result.add(rate.getOrder().getIdWoocommerce());
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }

    private String getURL(String ids){
        return MrJeffConstant.URL.URL_GET_RATED.replace(MrJeffConstant.URL.URL_GET_RATED_PARAM,ids);
    }
}
