package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Faq;
import es.coiasoft.mrjeff.domain.StatusOrder;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 7/10/15.
 */
public class SearchStatusService {


    public Map<String,StatusOrder> searchStatus(String ids){
        Map<String,StatusOrder> result = null;
        try {
            String url= MrJeffConstant.URL.URL_GET_STATUS_ORDER+ids.trim();
            Log.d("URL status", url);
//            url="http://192.168.101.1:8081/jeff/status.txt";
            HttpResponse response = ConnectionUtil.executeGet(url, true, true);

            if (ConnectionUtil.isValidRespose(response)) {
                String json = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                List<StatusOrder> statusOrders = gson.fromJson(json, new TypeToken<List<StatusOrder>>(){}.getType());
                if(statusOrders != null && !statusOrders.isEmpty()){
                    result = new HashMap<String,StatusOrder>();
                    for(StatusOrder status : statusOrders){
                        result.put(status.getId(),status);
                    }
                }
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }
}
