package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.DatetimeMrJeff;
import es.coiasoft.mrjeff.domain.ResultCpsDateInvalid;
import es.coiasoft.mrjeff.domain.ResultDatetimeMrJeff;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 21/10/15.
 */
public class SearchTimeTableCP {

    public ResultDatetimeMrJeff createProduct(String cp){
        ResultDatetimeMrJeff result = null;
        try {

            HttpResponse response = ConnectionUtil.executeGet(getURL(cp), true, true);

            if (ConnectionUtil.isValidRespose(response)) {
                String responseString = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                result = gson.fromJson(responseString, ResultDatetimeMrJeff.class);
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
       return result;
    }


    public String getURL(String cp){
        Log.d("URL HORARIOS", MrJeffConstant.URL.URL_POST_TIMETABLE + cp);
        return MrJeffConstant.URL.URL_POST_TIMETABLE + cp;
//        return "http://192.168.101.1:8081/jeff/time.json";
    }
}
