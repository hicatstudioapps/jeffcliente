package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.MessageCustomer;
import es.coiasoft.mrjeff.domain.ResultCoupon;
import es.coiasoft.mrjeff.domain.orders.ResultSearchCoupon;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 21/10/15.
 */
public class SendCouponService {

    public ResultSearchCoupon createCoupon(ResultSearchCoupon coupon){
        ResultSearchCoupon result = null;

        try {
            HttpEntity entity = ConnectionUtil.getHttpEntityJSON(coupon, ResultSearchCoupon.class);
            HttpResponse response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_POST_CREATE_COUPON, true, true, entity);
            if(ConnectionUtil.isValidRespose(response)){
                String bodyResponse = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                result = gson.fromJson(bodyResponse, ResultSearchCoupon.class);
            }

        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }
}
