package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.orders.response.ResponseOrder;
import es.coiasoft.mrjeff.domain.orders.send.Line_items;
import es.coiasoft.mrjeff.domain.orders.send.Order;
import es.coiasoft.mrjeff.domain.orders.send.SendOrder;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 21/10/15.
 */
public class SendOrderNoLavoService {

    private static final String URL = "https://mrjeffapp.com/wc-api/v2/orders";
    private static final String URL_ARGS = "?consumer_key=ck_7c88e569d5a24fdf549df164388c9c6e&consumer_secret=cs_c98790791a4118cdc875a68aba71ee65";

    public es.coiasoft.mrjeff.domain.orders.response.Order sendOrder(Order order){
        es.coiasoft.mrjeff.domain.orders.response.Order result = null;

        try {
            preparedRequest(order);
            order.convertDateToBackend();
            HttpEntity entity = ConnectionUtil.getHttpEntityJSON(new SendOrder(order), SendOrder.class);

            HttpResponse response = null;

            response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_POST_ORDER,true,true,entity);

            if(ConnectionUtil.isValidRespose(response)){
                String bodyResponse = ConnectionUtil.readReponse(response);Gson gson = new Gson();
                ResponseOrder responseOrder = gson.fromJson(bodyResponse, ResponseOrder.class);
                result = responseOrder.getOrder();
                if(result != null){
                    result.convertDateFromBackend();
                }
            } else {
                String bodyResponse = ConnectionUtil.readReponse(response);
            }

        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }finally {
            if(order != null){
                order.convertDateFromBackend();
            }
        }
        return result;
    }

    private void preparedRequest(Order order){
        if(CustomerStorage.getInstance(null).isLogged()){
            order.setCustomer_id(CustomerStorage.getInstance(null).getCustomer().getId());
        }
    }

}
