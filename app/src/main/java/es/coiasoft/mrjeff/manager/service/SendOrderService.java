package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.MrJeffApplication;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.orders.send.Line_items;
import es.coiasoft.mrjeff.domain.orders.send.Order;

import es.coiasoft.mrjeff.domain.orders.response.ResponseOrder;
import es.coiasoft.mrjeff.domain.orders.send.Product;
import es.coiasoft.mrjeff.domain.orders.send.SendOrder;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 21/10/15.
 */
public class SendOrderService {

    private static final String URL = "https://mrjeffapp.com/wc-api/v2/orders";
    private static final String URL_ARGS = "?consumer_key=ck_7c88e569d5a24fdf549df164388c9c6e&consumer_secret=cs_c98790791a4118cdc875a68aba71ee65";

    public es.coiasoft.mrjeff.domain.orders.response.Order sendOrder(Order order){
        return sendOrder(order,Boolean.FALSE);
    }
    public es.coiasoft.mrjeff.domain.orders.response.Order sendOrder(Order order,Boolean updateAddress){
        return sendOrder(order,updateAddress,null);
    }
    public es.coiasoft.mrjeff.domain.orders.response.Order sendOrder(Order order,Boolean updateAddress, Integer id){
        es.coiasoft.mrjeff.domain.orders.response.Order result = null;

        try {
            if(!updateAddress) {
                deleteCart(order);
            }
            preparedRequest(order);
            order.convertDateToBackend();
            removeAddress2(order);
            HttpEntity entity = ConnectionUtil.getHttpEntityJSON(new SendOrder(order), SendOrder.class);

            HttpResponse response = null;

            if(isUpdate() || id != null){
                response = ConnectionUtil.executePut(getUrlUpdate(id!= null? id:OrderStorage.getInstance(null).getOrder().getId()), true, true, entity);
            } else {
                response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_POST_ORDER,true,true,entity);
            }

            if(ConnectionUtil.isValidRespose(response)){
                String bodyResponse = ConnectionUtil.readReponse(response);Gson gson = new Gson();
                ResponseOrder responseOrder = gson.fromJson(bodyResponse, ResponseOrder.class);
                result = responseOrder.getOrder();
                if(result != null){
                    result.convertDateFromBackend();
                }
            } else {
                String bodyResponse = ConnectionUtil.readReponse(response);
                int i = 0;
            } ConnectionUtil.readReponse(response);

        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }finally {
            if(order != null){
                order.convertDateFromBackend();
            }
        }
        return result;
    }

    private boolean isUpdate(){
        boolean haveId= OrderStorage.getInstance(null).getOrder() != null
                && OrderStorage.getInstance(null).getOrder().getId() != null;
        return  haveId;
    }

    public void deleteCart(Order order)  throws Exception{
        if(isDiferent(order)) {
            deleteCouponproducts();
            //HttpResponse response = ConnectionUtil.executeDelete(getUrlDelete(OrderStorage.getInstance(null).getOrder().getId().toString()), true);
            OrderStorage.getInstance(null).setOrder(new es.coiasoft.mrjeff.domain.orders.response.Order());
        }
    }


    private String getUrlDelete(String idOrder){
        String url = MrJeffConstant.URL.URL_DELETE_ORDER;
        url = url.replace(MrJeffConstant.URL.ARG_ORDER,idOrder);
        return url;
    }

    private String getUrlUpdate(Integer id){
        String url = MrJeffConstant.URL.URL_UPDATE_ORDER;
        url = url.replace(MrJeffConstant.URL.ARG_ORDER,id.toString());
        Log.d("URL", "getUrlUpdate: "+url);
        return url;
    }

    public void deleteCouponproducts(){
        ArrayList<es.coiasoft.mrjeff.domain.orders.response.Line_items> itemsOrder = OrderStorage.getInstance(null).getOrder().getLine_items();
        for(es.coiasoft.mrjeff.domain.orders.response.Line_items itemorder: itemsOrder){
            Float price = Float.valueOf(itemorder.getPrice());
            if((new Float(0f)).compareTo(price)> 0){
                DeleteProductService service = new DeleteProductService();
                service.deleteProduct(itemorder.getProduct_id());
            }
        }
    }

    public Boolean isDiferent(Order order){
        Boolean result = Boolean.FALSE;
        boolean haveId= OrderStorage.getInstance(null).getOrder() != null
                && OrderStorage.getInstance(null).getOrder().getId() != null;
        if(haveId) {
            if(order.getLine_items().size() != OrderStorage.getInstance(null).getOrder().getLine_items().size()){
                result = Boolean.TRUE;
            } else {
                for(Line_items item:order.getLine_items()){
                    Boolean find = Boolean.FALSE;
                    for(es.coiasoft.mrjeff.domain.orders.response.Line_items aux: OrderStorage.getInstance(null).getOrder().getLine_items()){
                        if(aux.getProduct_id().intValue() == item.getProduct_id().intValue()){
                            find = Boolean.TRUE;
                        }
                        result = result || !find;
                    }
                }

                if(!result) {
                    for(es.coiasoft.mrjeff.domain.orders.response.Line_items aux: OrderStorage.getInstance(null).getOrder().getLine_items()){
                        Boolean find = Boolean.FALSE;
                        for(Line_items item:order.getLine_items()){
                            if(aux.getProduct_id().intValue() == item.getProduct_id().intValue()){
                                find = Boolean.TRUE;
                            }
                            result = result || !find;
                        }
                    }
                }
            }
        }

        return result;
    }

    private void preparedRequest(Order order){

            boolean haveId = OrderStorage.getInstance(null).getOrder() != null
                    && OrderStorage.getInstance(null).getOrder().getId() != null;
            if (haveId) {
                boolean haveElements = order.getLine_items() != null
                        && !order.getLine_items().isEmpty();

                if (haveElements) {
                    for (Line_items item : order.getLine_items()) {
                        Boolean find = Boolean.FALSE;
                        for (es.coiasoft.mrjeff.domain.orders.response.Line_items aux : OrderStorage.getInstance(null).getOrder().getLine_items()) {
                            if (aux.getProduct_id().intValue() == item.getProduct_id().intValue()) {
                                item.setId(aux.getId());
                                find = Boolean.TRUE;
                            }
                        }
                    }
                }
        }
        if(CustomerStorage.getInstance(null).isLogged()){
            order.setCustomer_id(CustomerStorage.getInstance(null).getCustomer().getId());
        }

        boolean haveElements = order.getLine_items() != null
                && !order.getLine_items().isEmpty();

    }

    private void removeAddress2(Order order){
        if(order != null && order.getBilling_address() != null){
            order.getBilling_address().setAddress_2(null);
        }

        if(order != null && order.getShipping_address() != null){
            order.getShipping_address().setAddress_2(null);
        }
    }



}
