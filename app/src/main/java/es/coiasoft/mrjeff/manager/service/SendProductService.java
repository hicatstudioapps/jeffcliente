package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.orders.send.ResultCreateProduct;
import es.coiasoft.mrjeff.domain.orders.send.SendProducts;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 21/10/15.
 */
public class SendProductService {

    public ResultCreateProduct createProduct(SendProducts sendProducts){
        ResultCreateProduct result = null;

        try {

            HttpEntity entity = ConnectionUtil.getHttpEntityJSON(sendProducts, SendProducts.class);
            HttpResponse response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_POST_PRODUCT, true, true, entity);
            if(ConnectionUtil.isValidRespose(response)){
                String bodyResponse = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                result = gson.fromJson(bodyResponse, ResultCreateProduct.class);
            }

        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }
}
