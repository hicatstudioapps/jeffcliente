package es.coiasoft.mrjeff.manager.service;


import android.util.Log;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.ChargeStripe;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;


/**
 * Created by linovm on 6/11/15.
 */
public class StripeService {


    public Boolean sendOrderStripe(String amount, String token, String currency, String description,String email, String coupon, String discount,int haveSuscription, int typeSuscription, String idSuscription){
        Boolean result = Boolean.FALSE;

        ChargeStripe stripe = new ChargeStripe();
        stripe.setStripeAmount(amount);
        stripe.setStripeCurrency(currency);
        stripe.setStripeToken(token);
        stripe.setStripeDescription(description);
        stripe.setEmail(email);
        stripe.setDiscount(discount);
        stripe.setCoupon(coupon);
        stripe.setHaveSuscription(haveSuscription);
        stripe.setTypeSuscription(typeSuscription);
        stripe.setIdSuscription(idSuscription);

        try {
            HttpEntity entity = ConnectionUtil.getHttpEntityJSON(stripe, ChargeStripe.class);

            HttpResponse response = null;
            response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_STRIPE_PAYMENT_SUBSCRIPTION,true,true,entity);

            if (ConnectionUtil.isValidRespose(response)) {
                String bodyResponse = ConnectionUtil.readReponse(response);

                if(bodyResponse.toLowerCase().trim().contains(MrJeffConstant.STRIPE.RESPONSE_SUCCESS.toLowerCase().trim())){
                    result = Boolean.TRUE;
                }else if(bodyResponse.toLowerCase().trim().contains(MrJeffConstant.STRIPE.RESPONSE_SUCCESS_SUBSCRIPTION1.toLowerCase().trim()) &&
                        bodyResponse.toLowerCase().trim().contains(MrJeffConstant.STRIPE.RESPONSE_SUCCESS_SUBSCRIPTION2.toLowerCase().trim())){
                    result = Boolean.TRUE;
                }else {
                    result = Boolean.FALSE;
                }
            }else {
                result = Boolean.FALSE;
            }
        } catch (Exception e) {
            Log.e("ERROR","PAGO CON STRIPE",e);
            result = Boolean.FALSE;
        }

        return result;
    }
}
