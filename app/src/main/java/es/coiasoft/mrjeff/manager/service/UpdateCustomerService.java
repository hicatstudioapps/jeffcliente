package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.MessageCustomer;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 21/10/15.
 */
public class UpdateCustomerService {

    public Customer updateCustomer(Customer customer){
        Customer result = null;

        try {
            customer.setUsername(null);
            MessageCustomer message =new MessageCustomer();
            message.setCustomer(customer);
            HttpEntity entity = ConnectionUtil.getHttpEntityJSON(message, MessageCustomer.class);
            HttpResponse response = ConnectionUtil.executePut(MrJeffConstant.URL.URL_PUT_UPDATE_CUSTOMER.replace(MrJeffConstant.URL.ARG_ID,customer.getId().toString()), true, true, entity);
            if(ConnectionUtil.isValidRespose(response)){
                String bodyResponse = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                result = gson.fromJson(bodyResponse, MessageCustomer.class).getCustomer();
            }

        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }
}
