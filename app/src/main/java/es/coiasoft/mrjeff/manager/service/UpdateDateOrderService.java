package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.orders.send.Order;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 7/10/15.
 */
public class UpdateDateOrderService {


    public Boolean updateDateOrder(Order order,Integer idOrder){
        Boolean result = Boolean.FALSE;
        try {

            if(order != null) {
                order.convertDateFromBackend();
                if(order.getOrder_meta() != null &&
                        order.getOrder_meta().getMyfield2() != null &&
                        order.getOrder_meta().getMyfield3() != null &&
                        order.getOrder_meta().getMyfield4() != null) {
                    List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                    pairs.add(new BasicNameValuePair("id", idOrder.toString()));
                    pairs.add(new BasicNameValuePair("myfield2", order.getOrder_meta().getMyfield2()));
                    pairs.add(new BasicNameValuePair("myfield3", order.getOrder_meta().getMyfield3()));
                    pairs.add(new BasicNameValuePair("myfield4", order.getOrder_meta().getMyfield4()));
                    pairs.add(new BasicNameValuePair("myfield5", order.getOrder_meta().getMyfield5()));
                    HttpEntity entity = ConnectionUtil.getHttpEntityUrlEncode(pairs);

                    HttpResponse response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_UPDATE_DATE_ORDER, false, true, entity);

                    if (ConnectionUtil.isValidRespose(response)) {
                        result = Boolean.TRUE;
                    }
                }
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }
}
