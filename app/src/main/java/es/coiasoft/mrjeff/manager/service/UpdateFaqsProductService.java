package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.ResultDatetimeMrJeff;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 21/10/15.
 */
public class UpdateFaqsProductService {

    public List<String> updateFaqs(String date){
        List<String> result = null;
        try {

            HttpResponse response = ConnectionUtil.executeGet(getURL(date), true, getURL(date).contains("https"));

            if (ConnectionUtil.isValidRespose(response)) {
                String responseString = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                result = gson.fromJson(responseString,new TypeToken<List<String>>(){}.getType());
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
       return result;
    }


    public String getURL(String date){
        return MrJeffConstant.URL.URL_UPDATE_CACHE_FAQS + date;
    }
}
