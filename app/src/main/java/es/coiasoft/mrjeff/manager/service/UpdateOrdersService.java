package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.CacheOrder;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 21/10/15.
 */
public class UpdateOrdersService {

    public List<CacheOrder> updateOrders(String id, String date){
        List<CacheOrder> result = null;
        try {

            HttpResponse response = ConnectionUtil.executeGet(getURL(id,date), true, true);

            if (ConnectionUtil.isValidRespose(response)) {
                String responseString = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                result = gson.fromJson(responseString,new TypeToken<List<CacheOrder>>(){}.getType());
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
       return result;
    }


    public String getURL(String id,String date){
        return MrJeffConstant.URL.URL_UPDATE_CACHE_ORDERS.replaceAll("##ID##",id).replaceAll("##DAY##",date);
    }
}
