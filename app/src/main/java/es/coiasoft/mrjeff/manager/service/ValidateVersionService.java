package es.coiasoft.mrjeff.manager.service;

import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.ResultValidate;
import es.coiasoft.mrjeff.manager.service.util.ConnectionUtil;

/**
 * Created by linovm on 7/10/15.
 */
public class ValidateVersionService {

    public static final int SUCCESS = 1;
    public static final int ERROR = -1;
    public static final int NOTVALID = 0;

    public int validateVersion(String label,String version){
        int result = ERROR;
        try {
            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            pairs.add(new BasicNameValuePair("so", label));
            pairs.add(new BasicNameValuePair("version", version));
            HttpEntity entity = ConnectionUtil.getHttpEntityUrlEncode(pairs);

            HttpResponse response = ConnectionUtil.executePost(MrJeffConstant.URL.URL_POST_VALIDATE_VERSION, false, true, entity);

            if (ConnectionUtil.isValidRespose(response)) {
                String bodyResponse = ConnectionUtil.readReponse(response);
                Gson gson = new Gson();
                ResultValidate resultValidate = gson.fromJson(bodyResponse, ResultValidate.class);
                result = ResultValidate.STATE_OK.equals(resultValidate.getEstado())?SUCCESS:NOTVALID;
            }

        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }
}
