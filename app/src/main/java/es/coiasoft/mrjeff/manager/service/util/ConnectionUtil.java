package es.coiasoft.mrjeff.manager.service.util;

import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.net.ssl.SSLContext;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpDelete;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.client.methods.HttpPut;
import cz.msebera.android.httpclient.client.methods.HttpRequestBase;
import cz.msebera.android.httpclient.config.Registry;
import cz.msebera.android.httpclient.config.RegistryBuilder;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;
import cz.msebera.android.httpclient.conn.HttpClientConnectionManager;
import cz.msebera.android.httpclient.conn.socket.ConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.SSLConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.SSLContexts;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.CloseableHttpClient;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.impl.conn.BasicHttpClientConnectionManager;
import cz.msebera.android.httpclient.util.EntityUtils;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;

/**
 * Created by linovm on 2/12/15.
 */
public class ConnectionUtil {


    public static CloseableHttpClient getHttpRequestGET(Boolean isSSL){
        CloseableHttpClient result = null;

        HttpClientBuilder builder = HttpClientBuilder.create();

        if(isSSL){
            SSLContext context = SSLContexts.createDefault();
            SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(context,SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            builder.setSSLSocketFactory(sslConnectionSocketFactory);

            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("https", sslConnectionSocketFactory)
                    .build();

            HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);

            builder.setConnectionManager(ccm);
        }

        result = builder.build();

        return result;
    }

    public static HttpResponse executeWithTimeoutExceptionControl(CloseableHttpClient request, HttpRequestBase client, int iteration) throws IOException {
        HttpResponse result = null;
        try{
            result = request.execute(client);
        }catch (ConnectTimeoutException e1){
            if(iteration < 3){
                result = executeWithTimeoutExceptionControl(request,client,++iteration);
            } else{
                throw e1;
            }
        }
        return result;
    }



    public static HttpGet getHttpGetComplete(String url, Boolean json){
        HttpGet client = new HttpGet(url);
        if(json){
            client.addHeader("Accept", "application/json");
        }

        return client;
    }

    public static HttpPost getHttpPostComplete(String url, Boolean json, HttpEntity entity){
        HttpPost client = new HttpPost(url);
        if(json){
            client.addHeader("Content-Type", "application/json; charset=UTF-8");
            client.addHeader("Accept", "application/json");
        }

        if(entity!= null) {
            client.setEntity(entity);
        }

        return client;
    }

    public static HttpPut getHttpPutComplete(String url, Boolean json, HttpEntity entity){
        HttpPut client = new HttpPut(url);
        if(json){
            client.addHeader("Content-Type", "application/json; charset=UTF-8");
            client.addHeader("Accept", "application/json");
        }
        try {
            Log.d("Entity", "getHttpPutComplete: "+ EntityUtils.toString(entity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(entity!= null) {
            client.setEntity(entity);
        }

        return client;
    }

    public static HttpDelete getHttpDeleteComplete(String url){
        HttpDelete client = new HttpDelete(url);
        return client;
    }



    public static HttpResponse executeGet(String url, Boolean json, Boolean isSSL){
        HttpResponse result = null;
        try {
            CloseableHttpClient request = getHttpRequestGET(url.contains("https"));
            result = executeWithTimeoutExceptionControl(request,getHttpGetComplete(url, json),0);
        } catch (Exception e){
            Log.e("ERROR", "Error de conexión " + url.substring(0,url.indexOf("?") > 0? url.indexOf("?"): url.length()),e);
        }

        return result;
    }

    public static HttpResponse executeGetProxyBackend(String url, Boolean json, Boolean isSSL){
        HttpResponse result = null;
        try {
            CloseableHttpClient request = getHttpRequestGET(url.contains("https"));
            HttpGet client = getHttpGetComplete(url, json);
            client.addHeader("authorization", MrJeffConstant.URL.URL_KEYS_PROXY);
            result = executeWithTimeoutExceptionControl(request,client,0);
        } catch (Exception e){
            Log.e("ERROR", "Error de conexión " + url.substring(0,url.indexOf("?") > 0? url.indexOf("?"): url.length()),e);
        }

        return result;
    }

    public static HttpResponse executePost(String url, Boolean json, Boolean isSSL, HttpEntity entity){
        HttpResponse result = null;
        try {
            CloseableHttpClient request = getHttpRequestGET(url.contains("https"));
            result = executeWithTimeoutExceptionControl(request,getHttpPostComplete(url, json,entity),0);
        } catch (Exception e){
            Log.e("ERROR", "Error de conexión " + url.substring(0,url.indexOf("?") > 0? url.indexOf("?"): url.length()),e);
        }

        return result;
    }

    public static HttpResponse executePostProxyBackend(String url, Boolean json, Boolean isSSL, HttpEntity entity){
        HttpResponse result = null;
        try {
            CloseableHttpClient request = getHttpRequestGET(url.contains("https"));
            HttpPost client = getHttpPostComplete(url, json, entity);
            client.addHeader("authorization", MrJeffConstant.URL.URL_KEYS_PROXY);
            result = executeWithTimeoutExceptionControl(request,client,0);
        } catch (Exception e){
            Log.e("ERROR", "Error de conexión " + url.substring(0,url.indexOf("?") > 0? url.indexOf("?"): url.length()),e);
        }

        return result;
    }

    public static HttpResponse executePut(String url, Boolean json, Boolean isSSL, HttpEntity entity){
        HttpResponse result = null;
        try {
            CloseableHttpClient request = getHttpRequestGET(url.contains("https"));
            result = executeWithTimeoutExceptionControl(request,getHttpPutComplete(url, json,entity),0);
        } catch (Exception e){
            Log.e("ERROR", "Error de conexión " + url.substring(0,url.indexOf("?") > 0? url.indexOf("?"): url.length()),e);
        }

        return result;
    }

    public static HttpResponse executeDelete(String url, Boolean isSSL){
        HttpResponse result = null;
        try {
            CloseableHttpClient request = getHttpRequestGET(url.contains("https"));
            result = executeWithTimeoutExceptionControl(request,getHttpDeleteComplete(url),0);
        } catch (Exception e){
            Log.e("ERROR", "Error de conexión " + url.substring(0,url.indexOf("?") > 0? url.indexOf("?"): url.length()),e);
        }

        return result;
    }

    public static HttpEntity getHttpEntityJSON(Object source, Class sourceclass){
        Gson gson = new Gson();
        String jsonSend = gson.toJson(source, sourceclass);
        StringEntity result = new StringEntity(jsonSend,"UTF-8");
        return  result;
    }

    public static HttpEntity getHttpEntityUrlEncode(List<NameValuePair> parameters) throws UnsupportedEncodingException {
        UrlEncodedFormEntity result =new UrlEncodedFormEntity(parameters,"UTF-8");
        return result;
    }

    public static Boolean isValidRespose(HttpResponse response){
        boolean result = false;

        if(response != null &&
                (response.getStatusLine().getStatusCode() == 200 || response.getStatusLine().getStatusCode() == 201)){
            result = true;
        }

        return result;

    }

    public static String readReponse(HttpResponse response){
        StringBuilder stringBuilder = new StringBuilder();
        try {
            HttpEntity entity = response.getEntity();
            InputStream inputStream = entity.getContent();
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            inputStream.close();
        }catch (Exception e){
            Log.e("ERROR", "Error leyendo respuesta ");
        }
        return  stringBuilder.toString();
    }




}
