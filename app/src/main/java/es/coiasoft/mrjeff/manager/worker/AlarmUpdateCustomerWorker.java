package es.coiasoft.mrjeff.manager.worker;

import android.content.Context;
import android.util.Log;

import java.util.Observable;

import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.manager.service.SearchCustomerService;
import es.coiasoft.mrjeff.manager.service.SendCustomerService;

/**
 * Created by linovm on 7/10/15.
 */
public class AlarmUpdateCustomerWorker extends Observable implements Runnable {

    public static final int RESULT_ERROR= -1;
    public static final int RESULT_SUCCESS = 1;

    private Integer resultCode = RESULT_ERROR;
    private Customer result;
    private Context context;


    public AlarmUpdateCustomerWorker(Context context) {
        this.context = context;
    }

    @Override
    public void run() {
        Customer customer = CustomerStorage.getInstance(context).getCustomer();
        if(customer != null && customer.getId() != null
                && customer.getEmail() != null){
            SearchCustomerService searchCustomerService = new SearchCustomerService();
            result = searchCustomerService.searchCustomer(customer.getEmail());
            if(result != null){
                result.setAvatar_url(customer.getAvatar_url());
                CustomerStorage.getInstance(context).setCustomer(customer);
                Log.d("UPDATE", "Se actualiza la info del usuario");
            } else {
                Log.d("UPDATE", "No se actualiza la info del usuario");
            }
        } else {
            Log.d("UPDATE", "No es necesario actualizar el usuario");
        }

    }

}
