package es.coiasoft.mrjeff.manager.worker;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;

import es.coiasoft.mrjeff.ResultServiceCountCoupon;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.ResultCoupon;
import es.coiasoft.mrjeff.domain.orders.ResultSearchCoupon;
import es.coiasoft.mrjeff.domain.orders.send.Coupon_lines;
import es.coiasoft.mrjeff.domain.orders.send.Line_items;
import es.coiasoft.mrjeff.domain.orders.send.Product;
import es.coiasoft.mrjeff.domain.orders.send.ResultCreateProduct;
import es.coiasoft.mrjeff.domain.orders.send.SendProducts;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.manager.service.SearchCouponService;
import es.coiasoft.mrjeff.manager.service.SearchCuponCountUsage;
import es.coiasoft.mrjeff.manager.service.SendOrderService;
import es.coiasoft.mrjeff.manager.service.SendProductService;

/**
 * Created by linovm on 7/10/15.
 */
public class CouponWorker extends Observable implements Runnable {

    public static final int RESULT_ERROR_COUPON_NOT_FOUND = -1;
    public static final int RESULT_INIT = 0;
    public static final int RESULT_ERROR_RECOVERY_ORDER = -2;
    public static final int RESULT_ERROR_APPLY_DISCOUNT = -3;
    public static final int RESULT_ERROR_EXPIRED = -4;
    public static final int RESULT_ERROR_AMOUNT= -5;
    public static final int RESULT_ERROR_USAGE_LIMIT= -6;
    public static final int RESULT_ERROR_USAGE_LIMIT_USER= -7;
    public static final int RESULT_ERROR_USER_NOT_VALID= -8;
    public static final int RESULT_ERROR_PRODUCT_NOT_VALID= -9;
    public static final int RESULT_ERROR_REFERRAL_NOT_VALID= -10;
    public static final int RESULT_SUCCESS = 1;

    private String coupon;
    private Integer resultCode = RESULT_INIT;
    private es.coiasoft.mrjeff.domain.orders.response.Order result;
    private ResultCoupon resultCopupon = null;

    public CouponWorker(String coupon) {
        this.coupon = coupon;
        resultCopupon = new ResultCoupon(RESULT_SUCCESS,null);
        resultCopupon.setCode(this.coupon);
    }

    @Override
    public void run() {
        SearchCouponService serviceCouponService = new SearchCouponService();
        ResultSearchCoupon coupon = serviceCouponService.getCoupon(this.coupon);
        if(coupon == null) {
            resultCode = RESULT_ERROR_COUPON_NOT_FOUND;
        } else {
            resultCopupon.setAmount(coupon.getCoupon().getMinimum_amount());

            if(!isCouponValidDate(coupon)){
                resultCode = RESULT_ERROR_EXPIRED;
            }else if(!isAmountValid(coupon)){
                resultCode = RESULT_ERROR_AMOUNT;
            } else if(coupon.getCoupon().getUsage_count() != null &&
                    coupon.getCoupon().getUsage_limit() != null &&
                    coupon.getCoupon().getUsage_count().intValue() >= coupon.getCoupon().getUsage_limit().intValue()) {
                resultCode = RESULT_ERROR_USAGE_LIMIT;
            }else if(!isUserCountValid(coupon)) {
                resultCode = RESULT_ERROR_USAGE_LIMIT_USER;
            }else if(!isUserValid(coupon)) {
                resultCode = RESULT_ERROR_USER_NOT_VALID;
            }else if(!isProductValid(coupon)) {
                resultCode = RESULT_ERROR_PRODUCT_NOT_VALID;
            }else if(!isUserReferralValid(coupon)) {
                resultCode = RESULT_ERROR_REFERRAL_NOT_VALID;
            } else {
                Boolean isDiscount = setDiscount(coupon);
                if(!isDiscount){
                    resultCode = RESULT_ERROR_APPLY_DISCOUNT;
                }else{
                    SendOrderService service = new SendOrderService();
                    result = service.sendOrder(OrderStorage.getInstance(null).getOrderSend(),Boolean.FALSE);
                    if(result != null) {
                        OrderStorage.getInstance(null).setOrder(result);
                        OrderStorage.getInstance(null).setError(Boolean.FALSE);
                        resultCode = RESULT_SUCCESS;
                    } else {
                        OrderStorage.getInstance(null).setError(Boolean.TRUE);
                        resultCode = RESULT_ERROR_RECOVERY_ORDER;
                    }

                    OrderStorage.getInstance(null).saveJson();
                }
            }
        }
        resultCopupon.setCodeResponse(resultCode);
        setChanged();
        notifyObservers(resultCopupon);

    }

    private Boolean isCouponValidDate(ResultSearchCoupon coupon){
        Boolean result = Boolean.TRUE;
        try {
            if (coupon.getCoupon().getExpiry_date() != null) {
                String date = coupon.getCoupon().getExpiry_date().substring(0, coupon.getCoupon().getExpiry_date().indexOf("T"));
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date expired = format.parse(date);
                if (expired.compareTo(new Date()) < 0) {
                    result = Boolean.FALSE;
                }
            }
        }catch (Exception e){

        }

        return result;
    }

    private Boolean isAmountValid(ResultSearchCoupon coupon){
        Boolean result = Boolean.TRUE;

        if(coupon.getCoupon().getMinimum_amount() != null){
            Float amount = Float.valueOf(coupon.getCoupon().getMinimum_amount());
            Float total = Float.valueOf(OrderStorage.getInstance(null).getOrder().getTotal());
            if(total.compareTo(amount)<0){
                result = Boolean.FALSE;
            }
        }

        return result;
    }

    private Boolean isUserCountValid(ResultSearchCoupon coupon){
        Boolean result = Boolean.TRUE;
        if(coupon.getCoupon().getUsage_limit_per_user() != null &&
                coupon.getCoupon().getUsage_limit_per_user().intValue() != 0 &&
                CustomerStorage.getInstance(null).getCustomer() != null &&
                CustomerStorage.getInstance(null).getCustomer().getId() != null ){
            SearchCuponCountUsage serviceUsage = new SearchCuponCountUsage();
            ResultServiceCountCoupon countResult = serviceUsage.getReferral(CustomerStorage.getInstance(null).getCustomer().getId().toString(), coupon.getCoupon().getCode());
                    if(countResult != null &&
                            countResult.getCouponusage() != null &&
                            countResult.getCouponusage().intValue() >= coupon.getCoupon().getUsage_limit_per_user().intValue()){
                        result = Boolean.FALSE;
                    }
        }

        return result;
    }

    private Boolean isUserValid(ResultSearchCoupon coupon){
        Boolean result = Boolean.TRUE;
        if(coupon.getCoupon().getCustomer_emails() != null &&
                coupon.getCoupon().getCustomer_emails().length > 0){
            String emailUser = CustomerStorage.getInstance(null).getCustomer().getEmail();
            if(emailUser != null){
                Boolean find = Boolean.FALSE;
                for(String customerValid: coupon.getCoupon().getCustomer_emails()){
                    if(customerValid != null && customerValid.trim().toLowerCase().equals(emailUser.trim().toLowerCase())){
                        find = Boolean.TRUE;
                        break;
                    }
                }

                result = find;
            }
        }

        return result;
    }

    private Boolean isProductValid(ResultSearchCoupon coupon){
        Boolean result = Boolean.TRUE;
        if(coupon.getCoupon().getProduct_ids() != null &&
                coupon.getCoupon().getProduct_ids().length > 0){
            if(OrderStorage.getInstance(null).getOrder() != null &&
                    OrderStorage.getInstance(null).getOrder().getLine_items() != null &&
                    !OrderStorage.getInstance(null).getOrder().getLine_items().isEmpty()){
                Boolean find = Boolean.FALSE;
                for(es.coiasoft.mrjeff.domain.orders.response.Line_items item: OrderStorage.getInstance(null).getOrder().getLine_items()){
                    if(productValid(item,coupon.getCoupon().getProduct_ids())){
                        find = Boolean.TRUE;
                        break;
                    }
                }
                result = find;
            }
        }

        return result;
    }

    private Boolean productValid(es.coiasoft.mrjeff.domain.orders.response.Line_items items, String[] idProducts){
        Boolean result = Boolean.FALSE;

        if(items != null && items.getProduct_id() != null && idProducts != null && idProducts.length > 0){
            Boolean find = Boolean.FALSE;
            for(String idProduct: idProducts) {
                if(idProduct!=null && idProduct.trim().equalsIgnoreCase(items.getProduct_id().toString().trim())){
                    find = Boolean.TRUE;
                    break;
                }
            }
            result = find;
        }

        return result;
    }

    private Boolean isUserReferralValid(ResultSearchCoupon coupon){
        Boolean result = Boolean.TRUE;
        if(coupon.getCoupon() != null &&
                coupon.getCoupon().getCode() != null &&
                    coupon.getCoupon().getCode().toLowerCase().indexOf(MrJeffConstant.ORDER.COUPON_REFERRAL.toLowerCase()) >=0){
            result = CustomerStorage.getInstance(null).getCustomer() != null &&
                    CustomerStorage.getInstance(null).getCustomer().getOrders_count() != null &&
                    (CustomerStorage.getInstance(null).getCustomer().getOrders_count().intValue() == 0 ||
                            (CustomerStorage.getInstance(null).getCustomer().getOrders_count().intValue() == 1 &&
                              CustomerStorage.getInstance(null).getCustomer().getLast_order_id() != null &&
                              OrderStorage.getInstance(null).getOrder() != null &&
                              OrderStorage.getInstance(null).getOrder().getId() != null &&
                              OrderStorage.getInstance(null).getOrder().getId().equals(CustomerStorage.getInstance(null).getCustomer().getLast_order_id())));
        }

        return result;
    }



    private Boolean setDiscount(ResultSearchCoupon coupon){

        Float totalDiscount = null;

        if(coupon !=null && coupon.getCoupon() != null &&
                coupon.getCoupon().getProduct_ids() != null &&
                coupon.getCoupon().getProduct_ids().length > 0){
            totalDiscount = discountProductList(coupon);
        } else {
            totalDiscount = discountTotal(coupon);
        }

        resultCopupon.setDiscount(totalDiscount);

        DecimalFormat format = new DecimalFormat("#.00");

        return createProduct(coupon.getCoupon().getCode(), format.format(totalDiscount));
    }

    private Float discountTotal(ResultSearchCoupon coupon){
        Float discount = Float.valueOf(coupon.getCoupon().getAmount());
        Float totalDiscount = discount;
        Float total = Float.valueOf(OrderStorage.getInstance(null).getOrder().getTotal());
        if(coupon.getCoupon().getType().equals("percent")) {
            Float totalTax = Float.valueOf(OrderStorage.getInstance(null).getOrder().getTotal_tax());
            totalDiscount = (total * (discount / 100));
        }

        if(totalDiscount.compareTo(total) > 0){
            totalDiscount = total;
        }

        if(totalDiscount.compareTo(0f)>0 ){
            totalDiscount = totalDiscount * (-1f);
        }

        return totalDiscount;
    }

    private Float discountProductList(ResultSearchCoupon coupon){
        Float totalDiscount = 0f;
        if(coupon.getCoupon().getProduct_ids() != null &&
                coupon.getCoupon().getProduct_ids().length > 0){
            if(OrderStorage.getInstance(null).getOrder() != null &&
                    OrderStorage.getInstance(null).getOrder().getLine_items() != null &&
                    !OrderStorage.getInstance(null).getOrder().getLine_items().isEmpty()){
                Boolean find = Boolean.FALSE;
                for(es.coiasoft.mrjeff.domain.orders.response.Line_items item: OrderStorage.getInstance(null).getOrder().getLine_items()){
                    totalDiscount += productDiscount(coupon,item,coupon.getCoupon().getProduct_ids());
                }
            }
        }

        return totalDiscount;
    }

    private Float productDiscount(ResultSearchCoupon coupon,es.coiasoft.mrjeff.domain.orders.response.Line_items items, String[] idProducts){
        Float totalDiscount = 0f;

        if(items != null && items.getProduct_id() != null && idProducts != null && idProducts.length > 0){
            for(String idProduct: idProducts) {
                if(idProduct!=null && idProduct.trim().equalsIgnoreCase(items.getProduct_id().toString().trim())){
                    Float discount = Float.valueOf(coupon.getCoupon().getAmount());
                    totalDiscount = discount;
                    Float total = Float.valueOf(items.getTotal()) + Float.valueOf(items.getTotal_tax());
                    if(coupon.getCoupon().getType().equals("percent") ||
                            coupon.getCoupon().getType().equals("percent_product")) {
                        totalDiscount = (total * (discount / 100));
                    }

                    if(totalDiscount.compareTo(total) > 0){
                        totalDiscount = total;
                    }

                    if(totalDiscount.compareTo(0f)>0 ) {
                        totalDiscount = totalDiscount * (-1f);
                    }

                    break;
                }
            }
        }

        return totalDiscount;
    }


    private Boolean createProduct(String title, String price){
        Boolean result = Boolean.FALSE;
        Product product = new Product();
        product.setTitle(title);
        product.setRegular_price(price);
        SendProducts send= new SendProducts();
        send.setProduct(product);

        SendProductService service = new SendProductService();
        ResultCreateProduct productresponse = service.createProduct(send);
        if(productresponse != null
                && productresponse.getProduct() != null
                && productresponse.getProduct().getId() != null) {
            OrderStorage.getInstance(null).setProductDelete(productresponse.getProduct().getId());
            Line_items item = new Line_items();
            item.setProduct_id(productresponse.getProduct().getId());
            item.setQuantity(1);
            OrderStorage.getInstance(null).getOrderSend().getLine_items().add(item);
            result = Boolean.TRUE;

            OrderStorage.getInstance(null).getOrderSend().setCoupon_lines(new ArrayList<Coupon_lines>());
            Coupon_lines cLines= new Coupon_lines();
            cLines.setCode(title);
            OrderStorage.getInstance(null).getOrderSend().getCoupon_lines().add(cLines);
        }

        return result;
    }

}
