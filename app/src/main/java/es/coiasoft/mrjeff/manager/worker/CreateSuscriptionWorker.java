package es.coiasoft.mrjeff.manager.worker;

import android.content.Context;

import java.util.List;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.InfoResidencia;
import es.coiasoft.mrjeff.manager.service.PostCouponResidencia;
import es.coiasoft.mrjeff.manager.service.PostSuscription;

/**
 * Created by linovm on 7/10/15.
 */
public class CreateSuscriptionWorker extends Observable implements Runnable {

    private String order;
    private String user;
    private String type;
    private String coupon;
    private List<InfoResidencia> result;
    private Context context;
    private String day;
    private String hour;
    private String address;
    private String cp;


    public CreateSuscriptionWorker(String order, String user,String type, String coupon,String day,String hour,String address,String cp, Context context) {
        this.order = order;
        this.user = user;
        this.type = type;
        this.coupon = coupon;
        this.day = day;
        this.hour = hour;
        this.address = address;
        this.cp = cp;
        this.context = context;
    }

    @Override
    public void run() {
        PostSuscription service = new PostSuscription();
        service.postSuscription(order,user,type,coupon,day,hour,address,cp);
        setChanged();
        notifyObservers(result);
    }
}
