package es.coiasoft.mrjeff.manager.worker;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.ResultCoupon;
import es.coiasoft.mrjeff.domain.orders.ResultSearchCoupon;
import es.coiasoft.mrjeff.domain.orders.send.Coupon_lines;
import es.coiasoft.mrjeff.domain.orders.send.Line_items;
import es.coiasoft.mrjeff.domain.orders.send.Product;
import es.coiasoft.mrjeff.domain.orders.send.ResultCreateProduct;
import es.coiasoft.mrjeff.domain.orders.send.SendProducts;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.manager.service.SearchCouponService;
import es.coiasoft.mrjeff.manager.service.SendCustomerService;
import es.coiasoft.mrjeff.manager.service.SendOrderService;
import es.coiasoft.mrjeff.manager.service.SendProductService;

/**
 * Created by linovm on 7/10/15.
 */
public class CustomerWorker extends Observable implements Runnable {

    public static final int RESULT_ERROR= -1;
    public static final int RESULT_SUCCESS = 1;

    private Integer resultCode = RESULT_ERROR;
    private Customer result;
    private Customer customer;

    public CustomerWorker(Customer customer) {
        this.customer = customer;
    }

    @Override
    public void run() {
        SendCustomerService service = new SendCustomerService();
        result = service.createCustomer(customer);
        if(result != null) {
            resultCode = RESULT_SUCCESS;
        }
        setChanged();
        notifyObservers(resultCode);

    }

}
