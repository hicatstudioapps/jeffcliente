package es.coiasoft.mrjeff.manager.worker;

import java.util.ArrayList;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.orders.send.Coupon_lines;
import es.coiasoft.mrjeff.domain.orders.send.Line_items;
import es.coiasoft.mrjeff.domain.orders.send.Order;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.manager.service.SendOrderService;

/**
 * Created by linovm on 7/10/15.
 */
public class DeleteCouponWorker extends Observable implements Runnable {

    public static final int RESULT_ERROR = -2;
    public static final int RESULT_SUCCESS = 1;


    private es.coiasoft.mrjeff.domain.orders.response.Order result;

    public DeleteCouponWorker() {
    }

    @Override
    public void run() {

        removeCoupon();
        SendOrderService service = new SendOrderService();
        //agregue
        Order order=OrderStorage.getInstance(null).getOrderSend();
        order.setCoupon_lines(new ArrayList<Coupon_lines>());
        //
        result = service.sendOrder(order,Boolean.FALSE);
        Integer resultCode = -1;
        if(result != null) {
            OrderStorage.getInstance(null).setOrder(result);
            OrderStorage.getInstance(null).setError(Boolean.FALSE);
            resultCode = RESULT_SUCCESS;
        } else {
            OrderStorage.getInstance(null).setError(Boolean.TRUE);
            resultCode = RESULT_ERROR;
        }

        OrderStorage.getInstance(null).saveJson();

        setChanged();
        notifyObservers(resultCode);

    }

    public void removeCoupon(){
        ArrayList<Line_items> items = OrderStorage.getInstance(null).getOrderSend().getLine_items();
        if(items != null && !items.isEmpty()){
            int idProductDelete=-1;
            int aux = 0;
            for(Line_items item: items){
                Integer idProduct = item.getProduct_id();
                ArrayList<es.coiasoft.mrjeff.domain.orders.response.Line_items> itemsOrder = OrderStorage.getInstance(null).getOrder().getLine_items();
                for(es.coiasoft.mrjeff.domain.orders.response.Line_items itemorder: itemsOrder){
                    if(idProduct.equals(itemorder.getProduct_id())){
                        Float price = Float.valueOf(itemorder.getPrice());
                        if((new Float(0f)).compareTo(price)>=0){
                            idProductDelete = aux;
                        }
                        break;
                    }
                }
                aux++;
            }

            if(idProductDelete != -1) {
                items.remove(idProductDelete);
            }

        }
    }
}
