package es.coiasoft.mrjeff.manager.worker;

import java.util.Observable;

import es.coiasoft.mrjeff.manager.service.DeleteOrderService;

/**
 * Created by linovm on 7/10/15.
 */
public class DeleteOrderWorker extends Observable implements Runnable {

    private String idOrder;


    public DeleteOrderWorker(String idOrder) {
        this.idOrder = idOrder;
    }

    @Override
    public void run() {
        try {
            DeleteOrderService service = new DeleteOrderService();
            service.deleteCart(idOrder);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
