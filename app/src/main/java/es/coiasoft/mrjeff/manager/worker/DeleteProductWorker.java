package es.coiasoft.mrjeff.manager.worker;

import java.util.Observable;

import es.coiasoft.mrjeff.manager.service.DeleteProductService;

/**
 * Created by linovm on 7/10/15.
 */
public class DeleteProductWorker extends Observable implements Runnable {

    private Integer id;
    private Boolean result;


    public DeleteProductWorker(Integer id) {

      this.id = id;
    }




    @Override
    public void run() {
        DeleteProductService service = new DeleteProductService();
        result = service.deleteProduct(id);
        setChanged();
        notifyObservers();

    }
}
