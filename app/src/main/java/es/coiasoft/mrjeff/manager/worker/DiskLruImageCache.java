package es.coiasoft.mrjeff.manager.worker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import com.jakewharton.disklrucache.DiskLruCache;
import es.coiasoft.mrjeff.Utils.Utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;

import es.coiasoft.mrjeff.BuildConfig;

public class DiskLruImageCache {

    public static DiskLruCache mDiskCache;
    private Bitmap.CompressFormat mCompressFormat = Bitmap.CompressFormat.JPEG;
    private int mCompressQuality = 70;
    private static final int APP_VERSION = 1;
    private static final int VALUE_COUNT = 1;
    private static final String TAG = "DiskLruImageCache";
    private static final String uniqueName = "mrfejjimages";
    private static final int diskCacheSize = 1024 * 1024 * 200;
    private static final Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.PNG;
    private static final int quality = 30;
    public static DiskLruImageCache instance;

    public static DiskLruImageCache getInstance(Context context) {
        if(instance == null){
            instance = new DiskLruImageCache(context);
        }
        return instance;
    }


    private DiskLruImageCache(Context context) {
        try {
            final File diskCacheDir = getDiskCacheDir(context, uniqueName );
            mDiskCache = DiskLruCache.open( diskCacheDir, APP_VERSION, VALUE_COUNT, diskCacheSize );
            mCompressFormat = compressFormat;
            mCompressQuality = quality;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean writeBitmapToFile( Bitmap bitmap, DiskLruCache.Editor editor )
            throws IOException, FileNotFoundException {
        OutputStream out = null;
        try {
            out = new BufferedOutputStream( editor.newOutputStream( 0 ), Utils.IO_BUFFER_SIZE );
            return bitmap.compress( mCompressFormat, mCompressQuality, out );
        } finally {
            if ( out != null ) {
                out.close();
            }
        }
    }

    private File getDiskCacheDir(Context context, String uniqueName) {

        // Check if media is mounted or storage is built-in, if so, try and use external cache dir
        // otherwise use internal cache dir
        final String cachePath =
                Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                        !Utils.isExternalStorageRemovable() ?
                        Utils.getExternalCacheDir(context).getPath() :
                        context.getCacheDir().getPath();

        return new File(cachePath + File.separator + uniqueName);
    }

    public void put( String key, Bitmap data ) {

            DiskLruCache.Editor editor = null;
            try {
                if (mDiskCache.size() < (mDiskCache.getMaxSize() - (1024 * 1024))) {

                    editor = mDiskCache.edit(toHex(key));
                    if (editor == null) {
                        return;
                    }

                    if (writeBitmapToFile(data, editor)) {
                        mDiskCache.flush();
                        editor.commit();
                        if (BuildConfig.DEBUG) {
                            Log.d("cache_test_DISK_", "image put on disk cache " + key);
                        }

                    } else {
                        editor.abort();
                        if (BuildConfig.DEBUG) {
                            Log.d("cache_test_DISK_", "ERROR on: image put on disk cache " + key);
                        }
                    }
                }
            } catch (IOException e) {
                if (BuildConfig.DEBUG) {
                    Log.d("cache_test_DISK_", "ERROR on: image put on disk cache " + key);
                }
                try {
                    if (editor != null) {
                        editor.abort();
                    }
                } catch (IOException ignored) {
                }
            }

    }

    public Bitmap getBitmap( String key ) {

        Bitmap bitmap = null;
        DiskLruCache.Snapshot snapshot = null;
        try {

            snapshot = mDiskCache.get(toHex(key));
            if (snapshot == null) {
                return null;
            }
            final InputStream in = snapshot.getInputStream(0);
            if (in != null) {
                final BufferedInputStream buffIn =
                        new BufferedInputStream(in, Utils.IO_BUFFER_SIZE);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inDither=false;                     //Disable Dithering mode
                options.inPurgeable=true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
                options.inInputShareable=true;              //Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future
                options.inTempStorage=new byte[32 * 1024];
                bitmap = BitmapFactory.decodeStream(buffIn, null, options);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (snapshot != null) {
                snapshot.close();
            }
        }

        if (BuildConfig.DEBUG) {
            Log.d("cache_test_DISK_", bitmap == null ? "" : "image read from disk " + key);
        }


        return bitmap;

    }


    public int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 2;
        if (height >= reqHeight || width >= reqWidth) {
            inSampleSize *= 2;
        }
        return inSampleSize;
    }

    public Bitmap decodeSampledBitmapFromResource(String file,
                                                         int reqWidth, int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(file, options);
    }


    public boolean containsKey( String key ) {

        boolean contained = false;
        DiskLruCache.Snapshot snapshot = null;
        try {
            snapshot = mDiskCache.get( toHex(key) );
            contained = snapshot != null;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if ( snapshot != null ) {
                snapshot.close();
            }
        }

        return contained;

    }

    public void clearCache() {
        if ( BuildConfig.DEBUG ) {
            Log.d( "cache_test_DISK_", "disk cache CLEARED");
        }
        try {
            mDiskCache.delete();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    public File getCacheFolder() {
        return mDiskCache.getDirectory();
    }

    private String toHex(String arg) {
        String result = String.format("%040x", new BigInteger(1, arg.getBytes(/*YOUR_CHARSET?*/)));
        int pos = result.length()- 60 > 0 ? result.length()- 60: 0;
        return result.substring(pos);
    }

}