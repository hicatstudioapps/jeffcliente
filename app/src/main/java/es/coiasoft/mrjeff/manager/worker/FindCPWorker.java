package es.coiasoft.mrjeff.manager.worker;

import android.util.Log;

import es.coiasoft.mrjeff.domain.storage.PostalCodeStorage;
import es.coiasoft.mrjeff.manager.service.PostalCodeService;

import java.util.List;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.PostalCode;

/**
 * Created by linovm on 7/10/15.
 */
public class FindCPWorker extends Observable implements Runnable {



    @Override
    public void run() {
        PostalCodeService postalCodeService = new PostalCodeService();
        List<PostalCode> cps = postalCodeService.getCPs();

        if(cps != null && !cps.isEmpty()){
            for(PostalCode cp:cps){
                PostalCodeStorage.getInstance(null).addPostaCode(cp.getPais(),cp.getCodigo(),cp.getCiudad());
            }
        }


        if(cps != null && !cps.isEmpty()){
            Log.d("UPDATE", "Se actualizan los codigos postales");
            PostalCodeStorage.getInstance(null).saveJson();
        } else {
            Log.d("UPDATE", "No se actualizan los codigos postales");
        }

        setChanged();
        notifyObservers();
    }
}
