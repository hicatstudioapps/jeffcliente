package es.coiasoft.mrjeff.manager.worker;

import java.util.Observable;

import es.coiasoft.mrjeff.domain.orders.ResultSearchCoupon;
import es.coiasoft.mrjeff.manager.service.SearchCouponService;

/**
 * Created by Paulino on 20/01/2016.
 */
public class FindCouponWorker extends Observable implements Runnable{

    private String coupon;

    public FindCouponWorker(String coupon) {
        this.coupon = coupon;
    }

    @Override
    public void run() {
        SearchCouponService serviceCouponService = new SearchCouponService();
        ResultSearchCoupon coupon = serviceCouponService.getCoupon(this.coupon);
        setChanged();
        notifyObservers(coupon != null?Boolean.TRUE:Boolean.FALSE);
    }
}
