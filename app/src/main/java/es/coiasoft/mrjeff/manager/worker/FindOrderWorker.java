package es.coiasoft.mrjeff.manager.worker;

import java.util.Collections;
import java.util.List;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.manager.model.comparator.OrdersComparator;
import es.coiasoft.mrjeff.manager.service.GetOrdersCustomerService;
import es.coiasoft.mrjeff.manager.service.SearchOrderService;

/**
 * Created by linovm on 7/10/15.
 */
public class FindOrderWorker extends Observable implements Runnable {


    private Integer idOrder;

    public FindOrderWorker( Integer idOrder){
        this.idOrder = idOrder;
    }


    @Override
    public void run() {
        Orders result = null;
        if(idOrder != null){
            SearchOrderService service = new SearchOrderService();
            result = service.searchOrder(idOrder);
        }

        setChanged();
        notifyObservers(result);
    }
}
