package es.coiasoft.mrjeff.manager.worker;

import android.content.Context;
import android.util.Log;

import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.StatusOrder;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.storage.OrdersCustomerStorage;
import es.coiasoft.mrjeff.manager.model.comparator.OrdersComparator;
import es.coiasoft.mrjeff.manager.service.GetOrdersCustomerService;
import es.coiasoft.mrjeff.manager.service.SearchRateService;
import es.coiasoft.mrjeff.manager.service.SearchStatusService;

import static com.paypal.android.sdk.da.i;

/**
 * Created by linovm on 7/10/15.
 */
public class FindOrdersCustomerWorker extends Observable implements Runnable {


    private int orderId;
    private String idCustomer;
    private  boolean updateCache;
    private Context context;

    public FindOrdersCustomerWorker(String idCustomer,Context context,int orderId){
        this(idCustomer,false,context,orderId);
    }

    public FindOrdersCustomerWorker(String idCustomer, boolean updateCacge,Context context, int orderId){
        this.idCustomer = idCustomer;
        this.updateCache = updateCacge;
        this.context = context;
        this.orderId=orderId;
    }



    @Override
    public void run() {
        List<Orders> result = null;
        boolean isNeccesaryUpdate = true;
        if(idCustomer != null && !idCustomer.isEmpty()){

//            if(updateCache){
//                isNeccesaryUpdate = true;
//            } else{
//               String idCustomerCache =  OrdersCustomerStorage.getInstance(context).getIddCustomer();
//                if(idCustomerCache == null || !idCustomerCache.equalsIgnoreCase(idCustomer)){
//                    isNeccesaryUpdate = true;
//                }
//
//                result = OrdersCustomerStorage.getInstance(context).getOrders();
//                if(result == null || result.isEmpty()){
//                    isNeccesaryUpdate = true;
//                }
//            }

            if(isNeccesaryUpdate) {
                List<Orders> resultToSave = new ArrayList<Orders>();
                GetOrdersCustomerService service = new GetOrdersCustomerService();
                result = service.getOrdersCustomer(idCustomer,orderId);
                if(result != null && !result.isEmpty()){
                    Collections.sort(result, new OrdersComparator());
                    final String[] ids = {""};
                    final int[] pos = {0};
                    Stream.of(result).filter(value ->
                        (value != null && value.getId() != null &&
                                !"pending".equalsIgnoreCase(value.getStatus()) &&
                                !"cancelled".equalsIgnoreCase(value.getStatus())
                    )).forEach(orders -> {
                        Log.d("estado ", orders.getStatus()+"-"+orders.getId());
                        ids[0] = ids[0] + (pos[0] > 0 ? ";" : "") + orders.getId();
                        resultToSave.add(orders);
                        pos[0]++;
                    });
//                    for(int i = 0; i < result.size(); i++){
//                        Log.d("estado ", result.get(i).getStatus());
//                        if(result.get(i) != null && result.get(i).getId() != null &&  !"pending".equalsIgnoreCase(result.get(i).getStatus()) &&
//                                !"cancelled".equalsIgnoreCase(result.get(i).getStatus())) {
//                            ids[0] = ids[0] + (pos[0] > 0 ? ";" : "") + result.get(i).getId();
//                            resultToSave.add(result.get(i));
//                            pos[0]++;
//                        }
//                    }


                    if(ids[0] != null && !ids[0].isEmpty()) {
                        SearchStatusService searchStatusService = new SearchStatusService();
                        Map<String, StatusOrder> status = searchStatusService.searchStatus(ids[0]);

                        if (status != null && !status.isEmpty()) {
                            for (int i = 0; i < resultToSave.size(); i++) {
                                if (resultToSave.get(i) != null && resultToSave.get(i).getId() != null) {
                                    resultToSave.get(i).setStateEnum(getState(status.get(resultToSave.get(i).getId().toString())));
                                }
                            }
                        }
                    }

//                    OrdersCustomerStorage.getInstance(context).addOrders(resultToSave);
//                    OrdersCustomerStorage.getInstance(context).setIddCustomer(idCustomer);
//                    OrdersCustomerStorage.getInstance(context).saveJson();
                }
            } else {
//                List<Orders> resultToSave = OrdersCustomerStorage.getInstance(context).getOrders();
//                if(resultToSave != null && !resultToSave.isEmpty()) {
//                    SearchRateService searchRateService = new SearchRateService();
//
//                    String ids = "";
//                    int pos = 0;
//                    for(int i = 0; i < result.size(); i++){
//                        if(result.get(i) != null && result.get(i).getId() != null &&  !"pending".equalsIgnoreCase(result.get(i).getStatus()) &&
//                                !"cancelled".equalsIgnoreCase(result.get(i).getStatus())) {
//                            ids = ids + (pos > 0 ? ";" : "") + result.get(i).getId();
//                            pos++;
//                        }
//                    }
//                    if(ids != null && !ids.isEmpty()) {
//                        List<String> rates = searchRateService.searchRates(ids);
//
//                        if (rates != null && !rates.isEmpty()) {
//                            for (int i = 0; i < resultToSave.size(); i++) {
//                                if (resultToSave.get(i) != null && resultToSave.get(i).getId() != null &&
//                                        rates.contains(resultToSave.get(i).getId().toString())) {
//                                    resultToSave.get(i).setHaveRate(true);
//                                } else {
//                                    resultToSave.get(i).setHaveRate(false);
//                                }
//
//                            }
//                        }
//                    }
//
//                    OrdersCustomerStorage.getInstance(context).addOrders(resultToSave);
//                    OrdersCustomerStorage.getInstance(context).setIddCustomer(idCustomer);
//                    OrdersCustomerStorage.getInstance(context).saveJson();
//                }
            }
        }

        setChanged();
        notifyObservers(result);
    }

    private EnumStateOrder getState(StatusOrder statusOrder){
        EnumStateOrder result = null;

        if(statusOrder != null && statusOrder.getIdstatus() >= 0){
            switch (statusOrder.getIdstatus()){
                case 0:
                    result = EnumStateOrder.CONFIRM;
                    break;
                case 1:
                    result = EnumStateOrder.PICKUP;
                    break;
                case 2:
                    result = EnumStateOrder.WASHING;
                    break;
                case 3:
                    result = EnumStateOrder.DELIVERY;
                    break;
                case 4:
                    result = EnumStateOrder.FINALLY;
                    break;
                default:
                    result = EnumStateOrder.CONFIRM;
                    break;
            }
        } else {
            result = EnumStateOrder.CONFIRM;
        }
        return result;
    }
}
