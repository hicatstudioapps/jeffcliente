package es.coiasoft.mrjeff.manager.worker;

import android.content.Context;

import java.util.Date;
import java.util.List;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.service.GetProductsService;

/**
 * Created by linovm on 7/10/15.
 */
public class FindProductsWorker extends Observable implements Runnable {

    private final String key;
    private Boolean load = Boolean.FALSE;
    private Boolean start = Boolean.FALSE;
    private Context context;

    private static FindProductsWorker instance;

    public Boolean getLoad() {
        return load;
    }

    public Boolean getStart() {
        return start;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public static FindProductsWorker getInstance(Context context) {
        if(instance == null){
            instance = new FindProductsWorker("Ofertas");
        }

        if(context != null && instance.context == null){
            instance.context = context;
        }
        return instance;
    }

    private FindProductsWorker(String key) {
        this.key = key;
    }

    @Override
    public synchronized void run() {
        start = Boolean.TRUE;
        GetProductsService getProductsService = new GetProductsService();
        List<Products> products = getProductsService.getProducts(key);

        ProductStorage instance = ProductStorage.getInstance(context);
        if(products != null && !products.isEmpty()){
            instance.clear();
            for(Products product:products){
                instance.addProduct(product);
            }
        }

        if(products != null && !products.isEmpty()) {
            instance.saveJson();
        }

        load = Boolean.TRUE;
        start = Boolean.FALSE;
        instance.setTime((new Date()).getTime());
        setChanged();
        notifyObservers();

    }


}
