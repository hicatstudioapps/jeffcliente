package es.coiasoft.mrjeff.manager.worker;

import android.content.Context;

import java.util.ArrayList;
import java.util.Map;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.StatusOrder;
import es.coiasoft.mrjeff.domain.SuscriptionInfo;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.storage.OrdersCustomerStorage;
import es.coiasoft.mrjeff.domain.storage.SuscriptionCustomerStorage;
import es.coiasoft.mrjeff.manager.service.GetSuscriptionInfoService;
import es.coiasoft.mrjeff.manager.service.SearchOrderService;
import es.coiasoft.mrjeff.manager.service.SearchStatusService;

/**
 * Created by linovm on 7/10/15.
 */
public class GetSuscriptionWorker extends Observable implements Runnable {

    private String idClient;
    private Context context;


    public GetSuscriptionWorker(String idClient, Context context) {
        this.idClient = idClient;
        this.context = context;
    }

    @Override
    public void run() {
        GetSuscriptionInfoService service = new GetSuscriptionInfoService();
        SuscriptionInfo info = service.postSuscription(idClient);
        SuscriptionCustomerStorage.getInstance(context).setIddCustomer(idClient);
        SuscriptionCustomerStorage.getInstance(context).addInfo(info);
        SuscriptionCustomerStorage.getInstance(context).saveJson();

        try {
            if(info != null &&
                    info.getOrderId() != null) {
                SearchOrderService serviceOrder = new SearchOrderService();
                Orders orders = serviceOrder.searchOrder(Integer.valueOf(info.getOrderId()));

                if(orders != null){
                    SearchStatusService searchStatusService = new SearchStatusService();
                    Map<String, StatusOrder> status = searchStatusService.searchStatus(info.getOrderId().toString());

                    if (status != null && !status.isEmpty()) {
                        orders.setStateEnum(getState(status.get(orders.getId().toString())));
                    }
                    OrdersCustomerStorage.getInstance(context).addOrder(orders);
                }
            }
        }catch (Exception e){

        }
        setChanged();
        notifyObservers(info);
    }

    private EnumStateOrder getState(StatusOrder statusOrder){
        EnumStateOrder result = null;

        if(statusOrder != null && statusOrder.getIdstatus() >= 0){
            switch (statusOrder.getIdstatus()){
                case 0:
                    result = EnumStateOrder.CONFIRM;
                    break;
                case 1:
                    result = EnumStateOrder.PICKUP;
                    break;
                case 2:
                    result = EnumStateOrder.WASHING;
                    break;
                case 3:
                    result = EnumStateOrder.DELIVERY;
                    break;
                case 4:
                    result = EnumStateOrder.FINALLY;
                    break;
                default:
                    result = EnumStateOrder.CONFIRM;
                    break;
            }
        } else {
            result = EnumStateOrder.CONFIRM;
        }
        return result;
    }
}
