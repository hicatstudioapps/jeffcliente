package es.coiasoft.mrjeff.manager.worker;

import android.content.Context;

import java.util.Map;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.StatusOrder;
import es.coiasoft.mrjeff.domain.SuscriptionInfo;
import es.coiasoft.mrjeff.domain.ZalandoCoupon;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.storage.OrdersCustomerStorage;
import es.coiasoft.mrjeff.domain.storage.SuscriptionCustomerStorage;
import es.coiasoft.mrjeff.manager.service.GetSuscriptionInfoService;
import es.coiasoft.mrjeff.manager.service.GetZalandoService;
import es.coiasoft.mrjeff.manager.service.SearchOrderService;
import es.coiasoft.mrjeff.manager.service.SearchStatusService;

/**
 * Created by linovm on 7/10/15.
 */
public class GetZalandoCouponWorker extends Observable implements Runnable {

    private String coupon;
    private String idClient;
    private Context context;


    public GetZalandoCouponWorker(String coupon,String idClient, Context context) {
        this.idClient = idClient;
        this.coupon = coupon;
        this.context = context;
    }

    @Override
    public void run() {
        GetZalandoService service = new GetZalandoService();
        ZalandoCoupon data = service.postCode(coupon,idClient);

        setChanged();
        notifyObservers(data);
    }

}
