package es.coiasoft.mrjeff.manager.worker;

import android.os.Bundle;

import java.util.Observable;

import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.ResultLogin;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.manager.service.LoginService;
import es.coiasoft.mrjeff.manager.service.SearchCustomerService;
import es.coiasoft.mrjeff.manager.service.SendCustomerService;

/**
 * Created by linovm on 7/10/15.
 */
public class LoginFacebookWorker extends Observable implements Runnable {

    public static final int RESULT_ERROR= -1;
    public static final int RESULT_SUCCESS = 1;

    private Integer resultCode = RESULT_ERROR;
    private Customer result;
    private Bundle bundle;

    public LoginFacebookWorker(Bundle bundle) {
        this.bundle = bundle;
    }

    @Override
    public void run() {
        SearchCustomerService searchCustomerService = new SearchCustomerService();
        result = searchCustomerService.searchCustomer(bundle.getString("email"));
        if(result != null) {
            result.setAvatar_url(bundle.getString("profile_pic"));
            CustomerStorage.getInstance(null).setCustomer(result);
            resultCode = RESULT_SUCCESS;
        } else {
            Customer customer = new Customer();
            customer.setUsername(bundle.getString("first_name").trim());
            customer.setFirst_name(bundle.getString("first_name"));
            customer.setLast_name(bundle.getString("last_name"));
            customer.setEmail(bundle.getString("email"));
            customer.setPassword(bundle.getString("idFacebook"));

            SendCustomerService service = new SendCustomerService();
            result = service.createCustomer(customer);
            if(result != null) {
                result.setAvatar_url(bundle.getString("profile_pic"));
                CustomerStorage.getInstance(null).setCustomer(result);
                resultCode = RESULT_SUCCESS;
            }
        }

        setChanged();
        notifyObservers(resultCode);

    }

}
