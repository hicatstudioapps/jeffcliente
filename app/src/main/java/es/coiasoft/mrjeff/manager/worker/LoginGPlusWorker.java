package es.coiasoft.mrjeff.manager.worker;

import android.os.Bundle;

import java.util.Observable;

import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.manager.service.SearchCustomerService;
import es.coiasoft.mrjeff.manager.service.SendCustomerService;

/**
 * Created by linovm on 7/10/15.
 */
public class LoginGPlusWorker extends Observable implements Runnable {

    public static final int RESULT_ERROR= -1;
    public static final int RESULT_SUCCESS = 1;

    private Integer resultCode = RESULT_ERROR;
    private Customer result;
    private Bundle bundle;

    public LoginGPlusWorker(Bundle bundle) {
        this.bundle = bundle;
    }

    @Override
    public void run() {
        SearchCustomerService searchCustomerService = new SearchCustomerService();
        result = searchCustomerService.searchCustomer(bundle.getString("email"));
        if(result != null) {
            result.setAvatar_url(bundle.getString("profile_pic"));
            CustomerStorage.getInstance(null).setCustomer(result);
            resultCode = RESULT_SUCCESS;
        } else {
            Customer customer = new Customer();
            if(bundle != null && bundle.containsKey("first_name") && bundle.getString("first_name") != null){
                customer.setUsername(bundle.getString("first_name").trim().substring(0,4));
            }
            if(bundle != null && bundle.containsKey("first_name") && bundle.getString("first_name") != null){
                customer.setFirst_name(bundle.getString("first_name"));
            }
            if(bundle != null && bundle.containsKey("last_name") && bundle.getString("last_name") != null){
                customer.setLast_name(bundle.getString("last_name"));
            }
            if(bundle != null && bundle.containsKey("email") && bundle.getString("email") != null){
                customer.setEmail(bundle.getString("email"));
            }
            if(bundle != null && bundle.containsKey("id") && bundle.getString("id") != null){
                customer.setPassword(bundle.getString("id"));
            }

            SendCustomerService service = new SendCustomerService();
            result = service.createCustomer(customer);
            if(result != null) {
                result.setAvatar_url(bundle.getString("profile_pic"));
                CustomerStorage.getInstance(null).setCustomer(result);
                resultCode = RESULT_SUCCESS;
            }
        }

        setChanged();
        notifyObservers(resultCode);

    }

}
