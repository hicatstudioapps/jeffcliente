package es.coiasoft.mrjeff.manager.worker;

import java.util.Observable;

import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.ResultLogin;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.manager.service.LoginService;
import es.coiasoft.mrjeff.manager.service.SearchCustomerService;
import es.coiasoft.mrjeff.manager.service.SendCustomerService;

/**
 * Created by linovm on 7/10/15.
 */
public class LoginWorker extends Observable implements Runnable {

    public static final int RESULT_ERROR= -1;
    public static final int RESULT_SUCCESS = 1;

    private Integer resultCode = RESULT_ERROR;
    private Customer result;
    private String user;
    private String password;

    public LoginWorker(String user, String password) {
        this.user = user;
        this.password = password;
    }

    @Override
    public void run() {
        LoginService service = new LoginService();
        ResultLogin resultLogin = service.loginUser(user,password);
        if(resultLogin != null && resultLogin.getEstado().equals("1")) {
            SearchCustomerService searchCustomerService = new SearchCustomerService();
            result = searchCustomerService.searchCustomer(user);
            if(result != null) {
                CustomerStorage.getInstance(null).setCustomer(result);
                resultCode = RESULT_SUCCESS;
            }
        }
        setChanged();
        notifyObservers(resultCode);

    }

}
