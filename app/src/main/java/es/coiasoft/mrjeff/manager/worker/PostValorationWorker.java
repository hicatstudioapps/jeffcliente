package es.coiasoft.mrjeff.manager.worker;

import java.util.Observable;

import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.ResultLogin;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.manager.model.proxy.Valoration;
import es.coiasoft.mrjeff.manager.service.LoginService;
import es.coiasoft.mrjeff.manager.service.PostRateService;
import es.coiasoft.mrjeff.manager.service.SearchCustomerService;

/**
 * Created by linovm on 7/10/15.
 */
public class PostValorationWorker extends Observable implements Runnable {

    public static final int RESULT_ERROR= -1;
    public static final int RESULT_SUCCESS = 1;

    private Integer resultCode = RESULT_ERROR;
    private Valoration valoration;

    public PostValorationWorker(Valoration valoration) {
        this.valoration = valoration;
    }

    @Override
    public void run() {
        PostRateService service = new PostRateService();
        boolean result = service.postRate(valoration);

        setChanged();
        notifyObservers(result);

    }

}
