package es.coiasoft.mrjeff.manager.worker;

import android.content.Context;

import java.util.Observable;

import es.coiasoft.mrjeff.domain.ZalandoCoupon;
import es.coiasoft.mrjeff.manager.service.GetZalandoService;
import es.coiasoft.mrjeff.manager.service.PutZalandoService;

/**
 * Created by linovm on 7/10/15.
 */
public class PutZalandoUserWorker extends Observable implements Runnable {

    private String idClient;
    private Context context;


    public PutZalandoUserWorker(String idClient, Context context) {
        this.idClient = idClient;
        this.context = context;
    }

    @Override
    public void run() {
        PutZalandoService service = new PutZalandoService();
        Boolean data = service.postUser(idClient);

        setChanged();
        notifyObservers(data);
    }

}
