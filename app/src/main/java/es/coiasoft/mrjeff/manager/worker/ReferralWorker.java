package es.coiasoft.mrjeff.manager.worker;

import android.util.Log;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;

import es.coiasoft.mrjeff.ResultServiceCustomice;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.domain.ResultCoupon;
import es.coiasoft.mrjeff.domain.orders.Coupon;
import es.coiasoft.mrjeff.domain.orders.ResultSearchCoupon;
import es.coiasoft.mrjeff.domain.orders.send.Coupon_lines;
import es.coiasoft.mrjeff.domain.orders.send.Line_items;
import es.coiasoft.mrjeff.domain.orders.send.Product;
import es.coiasoft.mrjeff.domain.orders.send.ResultCreateProduct;
import es.coiasoft.mrjeff.domain.orders.send.SendProducts;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.manager.service.AddCuponReferralService;
import es.coiasoft.mrjeff.manager.service.SearchCouponService;
import es.coiasoft.mrjeff.manager.service.SearchCuponReferralService;
import es.coiasoft.mrjeff.manager.service.SendCouponService;
import es.coiasoft.mrjeff.manager.service.SendOrderService;
import es.coiasoft.mrjeff.manager.service.SendProductService;

/**
 * Created by linovm on 7/10/15.
 */
public class ReferralWorker extends Observable implements Runnable {


    public static final int RESULT_SUCCESS = 1;

    private String coupon;
    private Coupon result=null;

    public ReferralWorker() {

    }

    @Override
    public void run() {
        coupon = MrJeffConstant.ORDER.COUPON_REFERRAL + (CustomerStorage.getInstance(null).getCustomer() != null
                                                            && CustomerStorage.getInstance(null).getCustomer().getId() != null?
                                                            CustomerStorage.getInstance(null).getCustomer().getId().toString() : "001");

        Log.d("REFERRALL", "Cupon por defecto -> " + coupon);
        //Comprobamos si tiene referral
        if(CustomerStorage.getInstance(null).getCustomer() != null &&
                CustomerStorage.getInstance(null).getCustomer().getId()!=null ) {
            Log.d("REFERRALL", "Se busca cupon para el usuario -> " + CustomerStorage.getInstance(null).getCustomer().getId());
            SearchCuponReferralService serviceSearch = new SearchCuponReferralService();
            ResultServiceCustomice resultSearch = serviceSearch.getReferral(CustomerStorage.getInstance(null).getCustomer().getId().toString());
            if (resultSearch != null && resultSearch.getEstado() != null
                    && !resultSearch.getEstado().trim().equals("0")) {
                coupon = resultSearch.getCupon().trim();
                Log.d("REFERRALL", "Se encuentra el cupon -> " + coupon);
            } else {
                //Recogemos el cupon de ejemplo
                SearchCouponService serviceCouponService = new SearchCouponService();
                ResultSearchCoupon couponAux = serviceCouponService.getCoupon(MrJeffConstant.ORDER.COUPON_REFERRAL_SAMPLE);
                if (couponAux != null && couponAux.getCoupon() != null) {
                    //Creamos el cupon
                    Log.d("REFERRALL", "Se recoge el coupon de ejemplo");
                    coupon = createCoupon(couponAux);

                    //Registramos el cupon al usuario
                    AddCuponReferralService serviceAddReg = new AddCuponReferralService();
                    serviceAddReg.getReferral(CustomerStorage.getInstance(null).getCustomer().getId().toString(), coupon);
                    Log.d("REFERRALL", "Se encuentra asigna al usuario");
                }
            }
        }

        if(coupon != null) {
            SearchCouponService serviceCouponService = new SearchCouponService();
            ResultSearchCoupon couponAuxResult = serviceCouponService.getCoupon(coupon);
            if(couponAuxResult != null){
                result = couponAuxResult.getCoupon();
            }
        }
        setChanged();
        Log.d("REFERRALL", "Se notifica el cupón -> " + coupon);
        notifyObservers(result);

    }

    private String createCoupon(ResultSearchCoupon coupon){
        coupon.getCoupon().setCreated_at(null);
        coupon.getCoupon().setUpdated_at(null);
        coupon.getCoupon().setId(null);
        coupon.getCoupon().setUsage_count(null);
        coupon.getCoupon().setExpiry_date(null);
        coupon.getCoupon().setCode(MrJeffConstant.ORDER.COUPON_REFERRAL + CustomerStorage.getInstance(null).getCustomer().getId().toString());

        String result = coupon.getCoupon().getCode();

        SendCouponService service = new SendCouponService();
        ResultSearchCoupon couponResult = service.createCoupon(coupon);
        if(couponResult != null &&
                couponResult.getCoupon() != null &&
                couponResult.getCoupon().getCode() != null){
            Log.d("REFERRALL", "Se creo el cupón correctamente");
            result = couponResult.getCoupon().getCode();
        }

        return result;

    }



}
