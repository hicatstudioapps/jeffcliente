package es.coiasoft.mrjeff.manager.worker;

import es.coiasoft.mrjeff.manager.service.RegisterCodeService;

import java.util.Observable;

/**
 * Created by linovm on 7/10/15.
 */
public class RegisterCPandEmailWorker extends Observable implements Runnable {

    private String email;
    private String cp;
    private Boolean result;


    public RegisterCPandEmailWorker(String cp, String email) {
        this.cp = cp;
        this.email = email;
    }

    @Override
    public void run() {
        RegisterCodeService registerCodeService = new RegisterCodeService();
        result = registerCodeService.setCPandEmail(cp,email);
        setChanged();
        notifyObservers();

    }
}
