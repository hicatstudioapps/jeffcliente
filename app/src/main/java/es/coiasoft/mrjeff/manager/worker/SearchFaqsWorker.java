package es.coiasoft.mrjeff.manager.worker;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.Faq;
import es.coiasoft.mrjeff.domain.storage.FaqsStorage;
import es.coiasoft.mrjeff.manager.service.SearchFaqsService;
import es.coiasoft.mrjeff.manager.service.ValidateVersionService;

/**
 * Created by linovm on 7/10/15.
 */
public class SearchFaqsWorker extends Observable implements Runnable {

    private String idProduct;
    private List<String> ids;
    private List<Faq> result;
    private Context context;


    public SearchFaqsWorker(String idProduct,Context context) {
        this.idProduct = idProduct;
        this.context = context;
    }

    public SearchFaqsWorker(List<String> ids,Context context) {
        this.ids = ids;
        this.context = context;
    }


    @Override
    public void run() {
        if(ids  == null){
            ids = new ArrayList<String>();
        }

        if(idProduct != null && !idProduct.isEmpty()){
            ids.add(idProduct);
        }

        for(String id: ids) {
            SearchFaqsService service = new SearchFaqsService();
            result = service.searchFaqsByProduct(id);
            FaqsStorage.getInstance(context).addProduct(id,result);
        }

        FaqsStorage.getInstance(context).saveJson();

        setChanged();
        notifyObservers(result);
    }
}
