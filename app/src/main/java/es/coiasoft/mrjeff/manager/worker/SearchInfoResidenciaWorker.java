package es.coiasoft.mrjeff.manager.worker;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.Faq;
import es.coiasoft.mrjeff.domain.InfoResidencia;
import es.coiasoft.mrjeff.domain.storage.FaqsStorage;
import es.coiasoft.mrjeff.manager.service.PostCouponResidencia;
import es.coiasoft.mrjeff.manager.service.SearchFaqsService;

/**
 * Created by linovm on 7/10/15.
 */
public class SearchInfoResidenciaWorker extends Observable implements Runnable {

    private String code;
    private String user;
    private List<InfoResidencia> result;
    private Context context;


    public SearchInfoResidenciaWorker(String code,String user, Context context) {
        this.code = code;
        this.user = user;
        this.context = context;
    }

    @Override
    public void run() {
        PostCouponResidencia service = new PostCouponResidencia();
        result = service.searchCoupon(code,user);
        setChanged();
        notifyObservers(result);
    }
}
