package es.coiasoft.mrjeff.manager.worker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.DatetimeMrJeff;
import es.coiasoft.mrjeff.domain.ResultDatetimeMrJeff;
import es.coiasoft.mrjeff.domain.storage.DateTimeTableStorage;
import es.coiasoft.mrjeff.manager.service.SearchTimeTableCP;

/**
 * Created by linovm on 7/10/15.
 */
public class SearchTimeTableWorker extends Observable implements Runnable {

    public static final int RESULT_ERROR= -1;
    public static final int RESULT_SUCCESS = 1;

    private Integer resultCode = RESULT_ERROR;
    private String cp;
    private String password;

    public SearchTimeTableWorker(String cp) {
        this.cp = cp;
    }

    @Override
    public void run() {
        SearchTimeTableCP service = new SearchTimeTableCP();
        ResultDatetimeMrJeff result =  service.createProduct(cp);
        if(result != null && result.getTimetable() != null && !result.getTimetable().isEmpty()){
            resultCode = RESULT_SUCCESS;
            for(DatetimeMrJeff day: result.getTimetable()){
                if(day != null){
                    DateTimeTableStorage.getInstance().addDate(cp, day.getDay(), day);
                }
            }
        }

        if(result != null && result.getDate() != null){
            calculateDiference(result.getDate());
        }

        setChanged();
        notifyObservers(resultCode);

    }


    private void calculateDiference(String date){
        long diff = 0l;

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar calendarSystem = Calendar.getInstance();
        try {
            calendarSystem.setTime(simpleDateFormat.parse(date));
            diff = calendar.getTimeInMillis() -  calendarSystem.getTimeInMillis();
            DateTimeTableStorage.getInstance().setDifference(diff * -1l);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
