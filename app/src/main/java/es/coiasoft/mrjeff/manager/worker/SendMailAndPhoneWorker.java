package es.coiasoft.mrjeff.manager.worker;

import java.util.Observable;

import es.coiasoft.mrjeff.manager.service.SendMailAndPhoneService;

/**
 * Created by linovm on 7/10/15.
 */
public class SendMailAndPhoneWorker extends Observable implements Runnable {

    private String email;
    private String phone;
    private Boolean result;


    public SendMailAndPhoneWorker(String phone, String email) {
        this.phone = phone;
        this.email = email;
    }

    @Override
    public void run() {
        SendMailAndPhoneService service = new SendMailAndPhoneService();
        result = service.setCPandEmail(phone,email);
        setChanged();
        notifyObservers();

    }
}
