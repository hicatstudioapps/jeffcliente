package es.coiasoft.mrjeff.manager.worker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Observable;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.orders.send.Line_items;
import es.coiasoft.mrjeff.domain.orders.send.Order;
import es.coiasoft.mrjeff.domain.orders.send.OrderMeta;
import es.coiasoft.mrjeff.domain.storage.DateTimeTableStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.manager.service.SendIdsNoLavoService;
import es.coiasoft.mrjeff.manager.service.SendOrderNoLavoService;
import es.coiasoft.mrjeff.manager.service.SendOrderService;

/**
 * Created by linovm on 7/10/15.
 */
public class SendOrderNoLavoWorker extends Observable implements Runnable {

    private Order order;
    private es.coiasoft.mrjeff.domain.orders.response.Order result;

    public SendOrderNoLavoWorker(Order order) {
        this.order = order;
    }

    @Override
    public void run() {
        OrderStorage.getInstance(null).setRemoveOk(false);
        List<Integer> idsChild = new ArrayList<Integer>();
        try {
            Order orderChild1 = generateChild(order,"2/4");
            Integer idChild1 = sendChild(orderChild1);
            if(idChild1 != null){
                idsChild.add(idChild1);
            }

            Order orderChild2 = generateChild(orderChild1,"3/4");
            Integer idChild2 = sendChild(orderChild2);
            if(idChild2 != null){
                idsChild.add(idChild2);
            }

            Order orderChild3 = generateChild(orderChild2,"4/4");
            Integer idChild3 = sendChild(orderChild3);
            if(idChild3 != null){
                idsChild.add(idChild3);
            }
        }catch (Exception e){

        }

        completeOrderParent(idsChild);
        finallyCart();
        sendIds(idsChild);
    }

    private void sendIds(List<Integer> ids){
        try {
            String idsSend = result.getId().toString();
            if(ids != null && !ids.isEmpty()){
                for(Integer id: ids){
                    idsSend = idsSend + ";" +id.toString();
                }
            }
            SendIdsNoLavoService sendIdsNoLavoService = new SendIdsNoLavoService();
            sendIdsNoLavoService.sendIds(idsSend);
        }catch (Exception e){

        }
    }

    private void completeOrderParent(List<Integer> ids){
        OrderMeta orderMeta = new OrderMeta();
        orderMeta.setSubscription_number("1/4");
        if(ids != null && !ids.isEmpty()){
            String idsChilds = "";
            int i = 1;
            for(Integer id: ids){
                idsChilds = idsChilds +id.toString() + (i < ids.size() ?";":"");
                i++;
            }
            orderMeta.setSubscription_children(idsChilds);
        }
        order.setOrder_meta(orderMeta);
    }

    private Integer sendChild(Order target){
        Integer result = null;
        SendOrderNoLavoService service = new SendOrderNoLavoService();

        target.setStatus(MrJeffConstant.ORDER.UPDATE_STATE_FINALLY);
        es.coiasoft.mrjeff.domain.orders.response.Order orderChild = service.sendOrder(target);

        if(orderChild != null){
            result = orderChild.getId();
        }

        return result;
    }

    private Order generateChild(Order source, String number){
        Order target= null;
        try {
            target = (Order)source.clone();
            OrderMeta orderMeta = new OrderMeta();
            orderMeta.setSubscription_son("true");
            orderMeta.setSubscription_number(number);
            target.setOrder_meta(orderMeta);
            removeTotalProduct(target);
            nextWeek(target);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return target;
    }

    private void removeTotalProduct(Order source){
        if(source != null &&
                source.getLine_items() != null &&
                !source.getLine_items().isEmpty()){
            for(Line_items item: source.getLine_items()){
                item.setTotal(0f);
                item.setSubtotal(0f);
            }
        }
    }

    public void nextWeek(Order target) {
        String addressBill = target.getBilling_address().getAddress_2();

        String[] parts = addressBill.split(" ");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(parts[0]);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.setTimeInMillis(calendar.getTimeInMillis() + (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY*7));
            String firstDate = DateTimeTableStorage.getInstance().getSecondDateHour(order.getBilling_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT,calendar);
            String addressShipp = target.getShipping_address().getAddress_2();
            String[] parts2 = addressShipp.split(" ");
            date = format.parse(parts2[0]);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(date);
            calendar2.setTimeInMillis(calendar2.getTimeInMillis() + (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY*7));
            String secondDate = DateTimeTableStorage.getInstance().getSecondDateHour(order.getBilling_address().getPostcode(), MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND,calendar2);


            target.getBilling_address().setAddress_2(firstDate);
            target.getShipping_address().setAddress_2(secondDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    private void finallyCart(){
        SendOrderService service = new SendOrderService();
        Order order = new Order();
        order.setOrder_meta(this.order.getOrder_meta());
        result = service.sendOrder(order,Boolean.TRUE);
        order.setStatus(MrJeffConstant.ORDER.UPDATE_STATE_FINALLY);
        result = service.sendOrder(order,Boolean.TRUE);

        if(result != null && result.getId() != null) {
            OrderStorage.getInstance(null).setOrder(result);
            OrderStorage.getInstance(null).setError(Boolean.FALSE);
        } else {
            OrderStorage.getInstance(null).setError(Boolean.TRUE);
        }

        if(OrderStorage.getInstance(null).getRemoveOk()) {
            OrderStorage.getInstance(null).setOrder(new es.coiasoft.mrjeff.domain.orders.response.Order());
            OrderStorage.getInstance(null).setOrderSend(new es.coiasoft.mrjeff.domain.orders.send.Order());
            OrderStorage.getInstance(null).saveJson();
        } else {
            OrderStorage.getInstance(null).setRemoveOk(true);
        }

    }
}
