package es.coiasoft.mrjeff.manager.worker;

import android.util.Log;

import es.coiasoft.mrjeff.Utils.ApiServiceGenerator;
import es.coiasoft.mrjeff.domain.orders.send.Order;
import es.coiasoft.mrjeff.manager.JeffApi;
import es.coiasoft.mrjeff.manager.service.SendOrderService;

import java.io.IOException;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import okhttp3.ResponseBody;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by linovm on 7/10/15.
 */
public class SendOrderWorker extends Observable implements Runnable {

    private Order order;
    private es.coiasoft.mrjeff.domain.orders.response.Order result;
    private Boolean updateAddress;
    private Boolean init;
    private Integer id;

    public SendOrderWorker(Order order) {
        this(order,Boolean.FALSE);
    }


    public SendOrderWorker(Order order, Boolean updateAddress) {
       this(order,updateAddress,Boolean.TRUE);
    }

    public SendOrderWorker(Order order, Boolean updateAddress,Boolean init) {
        this(order,updateAddress,init,null);
    }

    public SendOrderWorker(Order order, Boolean updateAddress,Boolean init,Integer id) {
        this.order = order;
        this.updateAddress = updateAddress;
        this.init = init;
        this.id = id;
    }

    @Override
    public void run() {
        OrderStorage.getInstance(null).setRemoveOk(false);
        SendOrderService service = new SendOrderService();

        result = service.sendOrder(order,updateAddress,id);
        if(result != null && result.getId() != null) {
            if(OrderStorage.getInstance(null).getOrder()!= null){
                Log.d("Last ORDER", String.valueOf(OrderStorage.getInstance(null).getOrder().getId()));
                ApiServiceGenerator.createService(JeffApi.class).deleteProductOrOrder(String.valueOf(OrderStorage.getInstance(null).getOrder().getId()),
                        "order")
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Action1<ResponseBody>() {
                            @Override
                            public void call(ResponseBody responseBody) {
                                try {
                                    Log.d("RESULT", responseBody.string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
            }
            OrderStorage.getInstance(null).setOrder(result);
            OrderStorage.getInstance(null).setError(Boolean.FALSE);
        } else {
            OrderStorage.getInstance(null).setError(Boolean.TRUE);
        }

        OrderStorage.getInstance(null).saveJson();

        if(OrderStorage.getInstance(null).getRemoveOk()) {
            OrderStorage.getInstance(null).setOrder(new es.coiasoft.mrjeff.domain.orders.response.Order());
            OrderStorage.getInstance(null).setOrderSend(new es.coiasoft.mrjeff.domain.orders.send.Order());
            OrderStorage.getInstance(null).saveJson();
        } else {
            OrderStorage.getInstance(null).setRemoveOk(true);
        }
        setChanged();
        notifyObservers(init);

    }
}
