package es.coiasoft.mrjeff.manager.worker;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.orders.send.Coupon_lines;
import es.coiasoft.mrjeff.domain.orders.send.Line_items;
import es.coiasoft.mrjeff.domain.orders.send.Order;
import es.coiasoft.mrjeff.domain.orders.send.Product;
import es.coiasoft.mrjeff.domain.orders.send.ResultCreateProduct;
import es.coiasoft.mrjeff.domain.orders.send.SendProducts;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.manager.service.SendOrderService;
import es.coiasoft.mrjeff.manager.service.SendProductService;

/**
 * Created by linovm on 7/10/15.
 */
public class SendOrderWorkerMin extends Observable implements Runnable {

    private Order order;
    private es.coiasoft.mrjeff.domain.orders.response.Order result;
    private Boolean updateAddress;
    private Boolean init;
    private Float taxMinOrder;

    public SendOrderWorkerMin(Order order, Float taxMinOrder) {
        this(order,taxMinOrder,Boolean.FALSE);
    }


    public SendOrderWorkerMin(Order order, Float taxMinOrder, Boolean updateAddress) {
       this(order,taxMinOrder,updateAddress,Boolean.TRUE);
    }

    public SendOrderWorkerMin(Order order, Float taxMinOrder, Boolean updateAddress, Boolean init) {
        this.order = order;
        this.updateAddress = updateAddress;
        this.init = init;
        this.taxMinOrder = taxMinOrder;
    }


    @Override
    public void run() {
        DecimalFormat format = new DecimalFormat("#.00");
        Boolean resultcreateproduct = createProduct(format.format(taxMinOrder));
        if(resultcreateproduct) {
            SendOrderService service = new SendOrderService();
            result = service.sendOrder(order, updateAddress);
            if (result != null) {
                OrderStorage.getInstance(null).setOrder(result);
                OrderStorage.getInstance(null).setError(Boolean.FALSE);
            } else {
                OrderStorage.getInstance(null).setError(Boolean.TRUE);
            }
        }else {
            OrderStorage.getInstance(null).setError(Boolean.TRUE);
        }
        OrderStorage.getInstance(null).saveJson();
        setChanged();
        notifyObservers(init);

    }


    private Boolean createProduct(String price){
        Boolean result = Boolean.FALSE;
        Product product = new Product();
        product.setTitle("Pedido mínimo ");
        product.setRegular_price(price);
        SendProducts send= new SendProducts();
        send.setProduct(product);

        SendProductService service = new SendProductService();
        ResultCreateProduct productresponse = service.createProduct(send);
        if(productresponse != null
                && productresponse.getProduct() != null
                && productresponse.getProduct().getId() != null) {
            OrderStorage.getInstance(null).setProductMinOrder(productresponse.getProduct().getId());
            Line_items item = new Line_items();
            item.setProduct_id(productresponse.getProduct().getId());
            item.setQuantity(1);
            OrderStorage.getInstance(null).getOrderSend().getLine_items().add(item);
            result = Boolean.TRUE;
        }

        return result;
    }
}
