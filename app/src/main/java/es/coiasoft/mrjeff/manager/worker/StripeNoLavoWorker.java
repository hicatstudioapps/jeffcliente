package es.coiasoft.mrjeff.manager.worker;

import java.util.Observable;

import es.coiasoft.mrjeff.manager.service.StripeNoLavoService;
import es.coiasoft.mrjeff.manager.service.StripeService;

/**
 * Created by linovm on 7/10/15.
 */
public class StripeNoLavoWorker extends Observable implements Runnable {


    private String amount;
    private String token;
    private String currency;
    private String description;
    private String email;
    private Boolean result;


    public StripeNoLavoWorker(String amount, String token, String email, String description, String currency) {
        this.amount = amount;
        this.email = email;
        this.token = token;
        this.description = description;
        this.currency = currency;
    }


    @Override
    public void run() {
        StripeNoLavoService stripeService = new StripeNoLavoService();
        result = stripeService.sendOrderStripe(amount,token,currency,description,email);
        setChanged();
        notifyObservers(result);

    }
}
