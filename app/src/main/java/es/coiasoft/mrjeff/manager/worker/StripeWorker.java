package es.coiasoft.mrjeff.manager.worker;

import es.coiasoft.mrjeff.manager.service.StripeService;

import java.util.Observable;

/**
 * Created by linovm on 7/10/15.
 */
public class StripeWorker extends Observable implements Runnable {


    private String amount;
    private String token;
    private String currency;
    private String description;
    private String email;
    private Boolean result;
    private String coupon;
    private String discount;
    private int haveSuscription;
    private int typeSuscription;
    private String idSuscription;



    public StripeWorker(String amount,String token, String email, String description, String coupon, String discount, String currency,int haveSuscription, int typeSuscription, String idSuscription) {
        this.amount = amount;
        this.email = email;
        this.token = token;
        this.description = description;
        this.coupon = coupon;
        this.discount = discount;
        this.currency = currency;
        this.haveSuscription = haveSuscription;
        this.typeSuscription = typeSuscription;
        this.idSuscription = idSuscription;
    }


    @Override
    public void run() {
        StripeService stripeService = new StripeService();
        result = stripeService.sendOrderStripe(amount,token,currency,description,email,coupon,discount,haveSuscription,typeSuscription,idSuscription);
        setChanged();
        notifyObservers(result);

    }
}
