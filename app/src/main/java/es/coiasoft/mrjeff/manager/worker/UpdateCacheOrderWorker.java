package es.coiasoft.mrjeff.manager.worker;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.domain.CacheOrder;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;
import es.coiasoft.mrjeff.manager.service.UpdateCacheProductService;
import es.coiasoft.mrjeff.manager.service.UpdateFaqsProductService;
import es.coiasoft.mrjeff.manager.service.UpdateOrdersService;

/**
 * Created by linovm on 7/10/15.
 */
public class UpdateCacheOrderWorker extends Observable implements Runnable, Observer {

    private Context context;
    private ConfigRepository dataSource;
    private int calls = 0;
    private int callsrecived=0;

    public UpdateCacheOrderWorker(Context context) {
        this.context = context;
        dataSource = new ConfigRepository(context);
    }

    @Override
    public void run() {
        calls = 0;
        callsrecived=0;
        try {
            if(CustomerStorage.getInstance(context).isLogged()) {
                String date = dataSource.getValueConfig(ConfigRepository.CACHE_UPDATE_ORDERS);
                String id = CustomerStorage.getInstance(context).getCustomer().getId().toString();
                UpdateOrdersService updateProducts = new UpdateOrdersService();
                List<CacheOrder> ids = updateProducts.updateOrders(id, date);

                if (ids != null && !ids.isEmpty()) {
                    calls++;
//                    FindOrdersCustomerWorker worker = new FindOrdersCustomerWorker(CustomerStorage.getInstance(context).getCustomer().getId().toString(), true, context);
//                    worker.addObserver(this);
//                    Thread threadProduct = new Thread(worker);
//                    threadProduct.start();
                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    dataSource.updateRegister(ConfigRepository.CACHE_UPDATE_ORDERS, format.format(today));
                }

                calls++;
                GetSuscriptionWorker workerSus = new GetSuscriptionWorker(CustomerStorage.getInstance(context).getCustomer().getId().toString(), context);
                workerSus.addObserver(this);
                Thread threadSus = new Thread(workerSus);
                threadSus.start();
            }
        }catch (Exception e){

        }

    }

    @Override
    public void update(Observable observable, Object data) {
        callsrecived++;

        if(callsrecived == calls){
            setChanged();
            notifyObservers();
        }
    }
}
