package es.coiasoft.mrjeff.manager.worker;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;
import es.coiasoft.mrjeff.manager.service.UpdateCacheProductService;
import es.coiasoft.mrjeff.manager.service.UpdateFaqsProductService;

/**
 * Created by linovm on 7/10/15.
 */
public class UpdateCacheproductWorker extends Observable implements Runnable {

    private Context context;
    private ConfigRepository dataSource;

    public UpdateCacheproductWorker(Context context) {
        this.context = context;
        dataSource = new ConfigRepository(context);
    }

    @Override
    public void run() {

        try {
            String date = dataSource.getValueConfig(ConfigRepository.CACHE_UPDATE_PRODUCTS);
            UpdateCacheProductService updateProducts = new UpdateCacheProductService();
            if(date == null){
                date = "2016-04-01";
            }
            List<String> ids = updateProducts.getUpdateCache(date);
            if(ids != null && !ids.isEmpty()) {
                Thread threadProduct = new Thread(FindProductsWorker.getInstance(context));
                threadProduct.start();
                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                dataSource.updateRegister(ConfigRepository.CACHE_UPDATE_PRODUCTS,format.format(today));
            }
        }catch (Exception e){

        }

        try {
            String date = dataSource.getValueConfig(ConfigRepository.CACHE_UPDATE_FAQS);
            UpdateFaqsProductService updateFaqs = new UpdateFaqsProductService();
            List<String> ids = updateFaqs.updateFaqs(date);
            if(ids != null && !ids.isEmpty()) {
                SearchFaqsWorker worker = new SearchFaqsWorker(ids,context);
                Thread threadProduct = new Thread(worker);
                threadProduct.start();
                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                dataSource.updateRegister(ConfigRepository.CACHE_UPDATE_FAQS,format.format(today));
            }
        }catch (Exception e){

        }

    }
}
