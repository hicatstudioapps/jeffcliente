package es.coiasoft.mrjeff.manager.worker;

import java.util.Observable;

import es.coiasoft.mrjeff.domain.Customer;
import es.coiasoft.mrjeff.manager.service.SendCustomerService;
import es.coiasoft.mrjeff.manager.service.UpdateCustomerService;

/**
 * Created by linovm on 7/10/15.
 */
public class UpdateCustomerWorker extends Observable implements Runnable {

    public static final int RESULT_ERROR= -1;
    public static final int RESULT_SUCCESS = 1;

    private Integer resultCode = RESULT_ERROR;
    private Customer result;
    private Customer customer;

    public UpdateCustomerWorker(Customer customer) {
        this.customer = customer;
        resultCode = RESULT_ERROR;
    }

    @Override
    public void run() {
        UpdateCustomerService service = new UpdateCustomerService();
        result = service.updateCustomer(customer);
        if(result != null) {
            resultCode = RESULT_SUCCESS;
        }
        setChanged();
        notifyObservers(resultCode);

    }

}
