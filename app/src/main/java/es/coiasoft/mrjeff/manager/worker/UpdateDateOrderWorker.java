package es.coiasoft.mrjeff.manager.worker;

import java.util.Observable;

import es.coiasoft.mrjeff.domain.orders.send.Order;
import es.coiasoft.mrjeff.manager.service.RegisterCodeService;
import es.coiasoft.mrjeff.manager.service.UpdateDateOrderService;

/**
 * Created by linovm on 7/10/15.
 */
public class UpdateDateOrderWorker extends Observable implements Runnable {

    private Integer id;
    private Order order;
    private Boolean result;


    public UpdateDateOrderWorker(Order order, Integer id) {
        this.order = order;
        this.id = id;
    }

    @Override
    public void run() {
        UpdateDateOrderService service = new UpdateDateOrderService();
        result = service.updateDateOrder(order,id);
        //setChanged();
        //notifyObservers();

    }
}
