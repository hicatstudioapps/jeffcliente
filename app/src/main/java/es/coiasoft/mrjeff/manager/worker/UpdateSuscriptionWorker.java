package es.coiasoft.mrjeff.manager.worker;

import android.content.Context;

import java.util.List;
import java.util.Map;
import java.util.Observable;

import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.InfoResidencia;
import es.coiasoft.mrjeff.domain.StatusOrder;
import es.coiasoft.mrjeff.domain.SuscriptionInfo;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrdersCustomerStorage;
import es.coiasoft.mrjeff.domain.storage.SuscriptionCustomerStorage;
import es.coiasoft.mrjeff.manager.service.GetSuscriptionInfoService;
import es.coiasoft.mrjeff.manager.service.PostSuscription;
import es.coiasoft.mrjeff.manager.service.PostUpdateSuscriptionService;
import es.coiasoft.mrjeff.manager.service.SearchOrderService;
import es.coiasoft.mrjeff.manager.service.SearchStatusService;

/**
 * Created by linovm on 7/10/15.
 */
public class UpdateSuscriptionWorker extends Observable implements Runnable {

    private SuscriptionInfo info;
    private Boolean result;
    private Context context;


    public UpdateSuscriptionWorker(SuscriptionInfo info, Context context) {
        this.context = context;
        this.info = info;
    }

    @Override
    public void run() {
        PostUpdateSuscriptionService service = new PostUpdateSuscriptionService();
        String [] hours= info.getDefaultHour().split("-");
        info.setDefaultHour(hours[0]+" - "+hours[1]);
        result = service.updateSuscription(info);
        if(result){
            GetSuscriptionInfoService serviceGet = new GetSuscriptionInfoService();
            SuscriptionInfo infoResult = serviceGet.postSuscription(CustomerStorage.getInstance(context).getCustomer().getId().toString());
            try {
                if(infoResult != null &&
                        infoResult.getOrderId() != null) {
                    SearchOrderService serviceOrder = new SearchOrderService();
                    Orders orders = serviceOrder.searchOrder(Integer.valueOf(info.getOrderId()));

                    if(orders != null){
                        SearchStatusService searchStatusService = new SearchStatusService();
                        Map<String, StatusOrder> status = searchStatusService.searchStatus(infoResult.getOrderId().toString());

                        if (status != null && !status.isEmpty()) {
                            orders.setStateEnum(getState(status.get(orders.getId().toString())));
                        }
                        OrdersCustomerStorage.getInstance(context).addOrder(orders);
                    }
                }
            }catch (Exception e){

            }
            SuscriptionCustomerStorage.getInstance(context).setIddCustomer(CustomerStorage.getInstance(context).getCustomer().getId().toString());
            SuscriptionCustomerStorage.getInstance(context).addInfo(infoResult);
            SuscriptionCustomerStorage.getInstance(context).saveJson();
        }

        setChanged();
        notifyObservers(result);
    }

    private EnumStateOrder getState(StatusOrder statusOrder){
        EnumStateOrder result = null;

        if(statusOrder != null && statusOrder.getIdstatus() >= 0){
            switch (statusOrder.getIdstatus()){
                case 0:
                    result = EnumStateOrder.CONFIRM;
                    break;
                case 1:
                    result = EnumStateOrder.PICKUP;
                    break;
                case 2:
                    result = EnumStateOrder.WASHING;
                    break;
                case 3:
                    result = EnumStateOrder.DELIVERY;
                    break;
                case 4:
                    result = EnumStateOrder.FINALLY;
                    break;
                default:
                    result = EnumStateOrder.CONFIRM;
                    break;
            }
        } else {
            result = EnumStateOrder.CONFIRM;
        }
        return result;
    }
}
