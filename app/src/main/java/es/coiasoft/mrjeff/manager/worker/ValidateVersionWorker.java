package es.coiasoft.mrjeff.manager.worker;

import es.coiasoft.mrjeff.manager.service.ValidateVersionService;

import java.util.Calendar;
import java.util.Observable;

/**
 * Created by linovm on 7/10/15.
 */
public class ValidateVersionWorker extends Observable implements Runnable {

    private String version;
    private String label;
    private Boolean result;
    private long time=0;
    private long MAX_WAIT = 600000;
    private static ValidateVersionWorker instance;

    public static ValidateVersionWorker getInstance(String label, String version){
        if(instance == null){
            instance = new ValidateVersionWorker(label,version);
        }

        return instance;
    }


    private ValidateVersionWorker(String label, String version) {
        this.label = label;
        this.version = version;
    }

    @Override
    public void run() {
        long timeNow = Calendar.getInstance().getTimeInMillis();
        if(time == 0 || (timeNow - time) > MAX_WAIT) {
            time = timeNow;
            ValidateVersionService service = new ValidateVersionService();
            int resultCode = service.validateVersion(label, version);
            result = resultCode != ValidateVersionService.NOTVALID;
        } else {
            result = true;
        }
        setChanged();
        notifyObservers(result);
    }
}
