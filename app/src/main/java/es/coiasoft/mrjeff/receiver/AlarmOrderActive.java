package es.coiasoft.mrjeff.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import es.coiasoft.mrjeff.MrJeffApplication;
import es.coiasoft.mrjeff.receiver.service.OrderActiveService;
import es.coiasoft.mrjeff.receiver.service.UpdateCacheService;

/**
 * Created by Paulino on 03/02/2016.
 */
public class AlarmOrderActive extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.d("ALARM", "ACTIVE - " + intent.getAction());
        if(!MrJeffApplication.isActivityVisible()) {
            Intent service = new Intent(context, OrderActiveService.class);
            context.startService(service);
        }
    }

}
