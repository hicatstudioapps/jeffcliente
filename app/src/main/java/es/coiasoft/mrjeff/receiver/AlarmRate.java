package es.coiasoft.mrjeff.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import es.coiasoft.mrjeff.receiver.service.RateService;
import es.coiasoft.mrjeff.receiver.service.UpdateCacheService;

/**
 * Created by Paulino on 03/02/2016.
 */
public class AlarmRate extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.d("ALARM", "RATE - " + intent.getAction());
        Intent service = new Intent(context, RateService.class);
        context.startService(service);
    }

}
