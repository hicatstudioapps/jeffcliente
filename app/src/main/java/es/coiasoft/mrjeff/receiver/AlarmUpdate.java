package es.coiasoft.mrjeff.receiver;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import es.coiasoft.mrjeff.receiver.service.UpdateCacheService;

/**
 * Created by Paulino on 03/02/2016.
 */
public class AlarmUpdate extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Intent service = new Intent(context, UpdateCacheService.class);
        context.startService(service);
    }

}
