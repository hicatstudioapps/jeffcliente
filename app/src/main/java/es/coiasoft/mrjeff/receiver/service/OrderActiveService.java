package es.coiasoft.mrjeff.receiver.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.HomeActivity;
import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.Utils.CreateNotificationUtil;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.orders.response.Line_items;
import es.coiasoft.mrjeff.domain.orders.response.Order;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;
import es.coiasoft.mrjeff.manager.worker.FindOrdersCustomerWorker;
import es.coiasoft.mrjeff.manager.worker.UpdateCacheOrderWorker;

/**
 * Created by Paulino on 04/02/2016.
 */
public class OrderActiveService extends Service{

    private ConfigRepository dataSource;

    private Handler handler;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler = new Handler();
        dataSource = new ConfigRepository(getApplicationContext());
        if(CustomerStorage.getInstance(getApplicationContext()).isLogged()) {
            UpdateCacheOrderWorker worker = new UpdateCacheOrderWorker(getApplicationContext());
            Thread thread = new Thread(worker);
            thread.start();

        }




        return super.onStartCommand(intent,flags,startId);
    }
}
