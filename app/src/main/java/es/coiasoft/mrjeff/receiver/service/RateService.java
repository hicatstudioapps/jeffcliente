package es.coiasoft.mrjeff.receiver.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Calendar;

import es.coiasoft.mrjeff.DialogRateActivity;
import es.coiasoft.mrjeff.MrJeffApplication;
import es.coiasoft.mrjeff.Utils.DialogAssessment;
import es.coiasoft.mrjeff.domain.storage.PostalCodeStorage;
import es.coiasoft.mrjeff.manager.model.ConfigRepository;
import es.coiasoft.mrjeff.manager.worker.AlarmUpdateCustomerWorker;
import es.coiasoft.mrjeff.manager.worker.FindCPWorker;
import es.coiasoft.mrjeff.manager.worker.FindProductsWorker;

/**
 * Created by Paulino on 04/02/2016.
 */
public class RateService extends Service {


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(isOk()){
            try {
                    Intent intentActivity = new Intent(getApplicationContext(), DialogRateActivity.class);
                    intentActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentActivity);
            }catch (Exception e){
                Log.e("ERROR","Sleep RateService", e);
            }
        }
        return super.onStartCommand(intent,flags,startId);
    }

    private boolean isOk(){
        return isOpen() && getRateBd();
    }

    private boolean isOpen(){
        return MrJeffApplication.isActivityVisible();
    }

    private boolean getRateBd(){
        boolean result = false;
        ConfigRepository db = new ConfigRepository(getApplicationContext());
        String value = db.getValueConfig(ConfigRepository.RATE);
        if(value != null && value.trim().equals("0")){
            result = true;
        } else if(value != null && !value.trim().equals("-1")){
            long time = Long.valueOf(value).longValue();
            long now = Calendar.getInstance().getTimeInMillis();
            long diffMin = 86400000;
            long diff = now - time;
            result = diff > diffMin;
        }

        return result;
    }



}
