package es.coiasoft.mrjeff.receiver.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import es.coiasoft.mrjeff.domain.storage.PostalCodeStorage;
import es.coiasoft.mrjeff.manager.worker.AlarmUpdateCustomerWorker;
import es.coiasoft.mrjeff.manager.worker.FindCPWorker;
import es.coiasoft.mrjeff.manager.worker.UpdateCacheproductWorker;

/**
 * Created by Paulino on 04/02/2016.
 */
public class UpdateCacheService extends Service {


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Context context = getApplicationContext();

        PostalCodeStorage.getInstance(context);
        FindCPWorker workerZIPCode = new FindCPWorker();
        Thread threadCPWorker = new Thread(workerZIPCode);
        threadCPWorker.start();


        UpdateCacheproductWorker updateProduct = new UpdateCacheproductWorker(context);
        Thread threadProducts = new Thread(updateProduct);
        threadProducts.start();

        Thread threadCustomer = new Thread(new AlarmUpdateCustomerWorker(context));
        threadCustomer.start();


        return super.onStartCommand(intent,flags,startId);
    }
}
