package es.coiasoft.mrjeff.view;

import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Paulino on 16/02/2016.
 */
public class AnimationExpandCollapseDecorator<T extends View> {

    private T v;
    private Boolean isExpand;
    private int minHeigt = 0;
    private TextView text;
    private View button;


    public AnimationExpandCollapseDecorator(T view, TextView text,View button){
            this(view, Boolean.FALSE, 70,text,button);
    }

    public AnimationExpandCollapseDecorator(T view,Boolean initialExpand, int minHeigt, TextView text,View button){
        this.v = view;
        this.isExpand = initialExpand;
        if(!isExpand){
            this.minHeigt =  v.getLayoutParams().height;
        } else {
            this.minHeigt = minHeigt;
        }
        this.text = text;
        this.button = button;
        if(isNeccesaryExpand()) {
            v.setOnClickListener(initEventClick());
        } else {
            button.setVisibility(View.GONE);
        }
    }

    private boolean isNeccesaryExpand(){
        return text != null && text.getText() != null && text.getText().length() > 45;
    }

    private View.OnClickListener initEventClick(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    isExpand = !isExpand;
                    if(!isExpand){
                        collapse();
                    } else {
                        expand();
                    }
            }
        };
    }


    private void expand() {
        if(text != null){
            text.setSingleLine(false);
        }
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = minHeigt;
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        :  ((int)(targetHeight * interpolatedTime) > minHeigt ?(int)(targetHeight * interpolatedTime) : minHeigt );
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        int time = (int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density);
        a.setDuration(time);
        v.startAnimation(a);
        rotationButton(time);
    }

    private void collapse() {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                Log.d("COLLAPSE", "Init: " + v.getLayoutParams().height + "   Min: " + minHeigt + " Interpolated:" + interpolatedTime);

                if(LinearLayout.LayoutParams.WRAP_CONTENT == v.getLayoutParams().height || (v.getLayoutParams().height- (int)(initialHeight * interpolatedTime)) > minHeigt) {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                } else  if(LinearLayout.LayoutParams.WRAP_CONTENT != v.getLayoutParams().height || (v.getLayoutParams().height- (int)(initialHeight * interpolatedTime)) < minHeigt){
                    v.getLayoutParams().height = minHeigt;
                    v.requestLayout();
                }

                if(interpolatedTime == 1){
                    v.getLayoutParams().height = minHeigt;
                    v.requestLayout();
                    if(text != null){
                        text.setSingleLine(true);
                    }
                }

                if(interpolatedTime == 0.0){
                    //Start
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        Log.d("COLLAPSE", "Init: " + initialHeight + "   Min: " + minHeigt + " Densitivy: " + v.getContext().getResources().getDisplayMetrics().density);
        int time = (int)((initialHeight-minHeigt) / v.getContext().getResources().getDisplayMetrics().density);
        a.setDuration(time);
        v.startAnimation(a);
        rotationButton(time);
    }


    private void rotationButton(int time){
        if(button != null) {
            RotateAnimation rotate = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(time);
            rotate.setInterpolator(new LinearInterpolator());
            rotate.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    /*Matrix matrix = new Matrix();
                    ((ImageView)button).setScaleType(ImageView.ScaleType.MATRIX);   //required
                    matrix.setScale(0.5f,0.5f);
                    matrix.postRotate((float) 180, ((ImageView)button).getDrawable().getBounds().width()/2, ((ImageView)button).getDrawable().getBounds().height()/2);
                    ((ImageView)button).setImageMatrix(matrix);*/
                    if (isExpand) {
                        ((ImageView) button).setRotation(180f);
                    } else {
                        ((ImageView) button).setRotation(360f);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            button.startAnimation(rotate);
        }
    }
}
