package es.coiasoft.mrjeff.view.adapter;

import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Paulino on 11/05/2016.
 */
public class HolderDate {

    private TextView day;
    private TextView month;
    private LinearLayout parent;
    private String date;

    public TextView getDay() {
        return day;
    }

    public void setDay(TextView day) {
        this.day = day;
    }

    public TextView getMonth() {
        return month;
    }

    public void setMonth(TextView month) {
        this.month = month;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public LinearLayout getParent() {
        return parent;
    }

    public void setParent(LinearLayout parent) {
        this.parent = parent;
    }
}
