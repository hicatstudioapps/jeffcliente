package es.coiasoft.mrjeff.view.adapter;

import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Paulino on 11/05/2016.
 */
public class HolderHour {

    private TextView hour;
    private String hourTag;
    private LinearLayout parent;

    public TextView getHour() {
        return hour;
    }

    public void setHour(TextView hour) {
        this.hour = hour;
    }

    public String getHourTag() {
        return hourTag;
    }

    public void setHourTag(String hourTag) {
        this.hourTag = hourTag;
    }

    public LinearLayout getParent() {
        return parent;
    }

    public void setParent(LinearLayout parent) {
        this.parent = parent;
    }
}
