package es.coiasoft.mrjeff.view.adapter;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import es.coiasoft.mrjeff.view.components.MrJeffTextBoldView;

/**
 * Created by linovm on 9/10/15.
 */
public class HolderProductOrder {


    private TextView text_name;
    private TextView text_price;
    private TextView product_price_sus;
    private TextView product_desc;
    private TextView text_count;
    private LinearLayout main;
    private LinearLayout desc;
    private Integer id;
    private TextView btnPlus;
    private TextView btnMinus;
    private Float taxUnit =0f;
    private Float priceUnit = 0f;
    private ImageView product_trash;
    private RelativeLayout panelsuscription;
    private RelativeLayout panelnotsuscription;
    private boolean isSuscription;

    public MrJeffTextBoldView getPlus() {
        return plus;
    }

    public void setPlus(MrJeffTextBoldView plus) {
        this.plus = plus;
    }

    private MrJeffTextBoldView plus;

    public MrJeffTextBoldView getMinus() {

        return minus;
    }

    public void setMinus(MrJeffTextBoldView minus) {
        this.minus = minus;
    }

    private MrJeffTextBoldView minus;

    public HolderProductOrder(boolean isSuscription) {
        this.isSuscription = isSuscription;
    }

    public TextView getText_name() {
        return text_name;
    }

    public void setText_name(TextView text_name) {
        this.text_name = text_name;
    }

    public TextView getText_price() {
        return text_price;
    }

    public void setText_price(TextView text_price) {
        this.text_price = text_price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TextView getText_count() {
        return text_count;
    }

    public void setText_count(TextView text_count) {
        this.text_count = text_count;
    }

    public LinearLayout getMain() {
        return main;
    }

    public void setMain(LinearLayout main) {
        this.main = main;
    }

    public LinearLayout getDesc() {
        return desc;
    }

    public void setDesc(LinearLayout desc) {
        this.desc = desc;
    }

    public TextView getBtnPlus() {
        return btnPlus;
    }

    public void setBtnPlus(TextView btnPlus) {
        this.btnPlus = btnPlus;
    }

    public TextView getBtnMinus() {
        return btnMinus;
    }

    public void setBtnMinus(TextView btnMinus) {
        this.btnMinus = btnMinus;
    }

    public Float getTaxUnit() {
        return taxUnit;
    }

    public void setTaxUnit(Float taxUnit) {
        this.taxUnit = taxUnit;
    }

    public Float getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(Float priceUnit) {
        this.priceUnit = priceUnit;
    }

    public ImageView getProduct_trash() {
        return product_trash;
    }

    public void setProduct_trash(ImageView product_trash) {
        this.product_trash = product_trash;
    }

    public RelativeLayout getPanelsuscription() {
        return panelsuscription;
    }

    public void setPanelsuscription(RelativeLayout panelsuscription) {
        this.panelsuscription = panelsuscription;
    }

    public RelativeLayout getPanelnotsuscription() {
        return panelnotsuscription;
    }

    public void setPanelnotsuscription(RelativeLayout panelnotsuscription) {
        this.panelnotsuscription = panelnotsuscription;
    }

    public boolean isSuscription() {
        return isSuscription;
    }

    public void setSuscription(boolean suscription) {
        isSuscription = suscription;
    }

    public TextView getProduct_price_sus() {
        return product_price_sus;
    }

    public void setProduct_price_sus(TextView product_price_sus) {
        this.product_price_sus = product_price_sus;
    }

    public TextView getProduct_desc() {
        return product_desc;
    }

    public void setProduct_desc(TextView product_desc) {
        this.product_desc = product_desc;
    }
}

