package es.coiasoft.mrjeff.view.adapter;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ropasgo on 5/7/16.
 */
public abstract class HolderRecycleViewMrJEff<T> extends RecyclerView.ViewHolder {


    protected RecyclerView.Adapter adapter;
    protected int position = -1;

    public HolderRecycleViewMrJEff(View itemView) {
        super(itemView);
        initComponent();
    }

    public abstract  void initComponent();

    public abstract void setText(T object);

    public abstract T getData();

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
    }

}
