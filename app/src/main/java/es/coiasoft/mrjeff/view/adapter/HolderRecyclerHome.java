package es.coiasoft.mrjeff.view.adapter;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import es.coiasoft.mrjeff.OneProductActivity;
import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.view.listener.ProductAddRemoveListener;
import es.coiasoft.mrjeff.view.util.LoadImage;

/**
 * Created by Paulino on 13/09/2016.
 */
public class HolderRecyclerHome extends HolderRecycleViewMrJEff<Products>{


    //Info product
    private Integer idProduct;
    //Components
    private SimpleDraweeView imageProduct;
    private ImageView buttonPlus;
    private ImageView buttonMinus;
    private TextView price;
    private TextView title;
    private TextView count;
    private RelativeLayout panelproduct;

    private int posParent;

    private ProductAddRemoveListener listener;
    private Products data;

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public ImageView getImageProduct() {
        return imageProduct;
    }

    public void setImageProduct(SimpleDraweeView imageProduct) {
        this.imageProduct = imageProduct;
    }

    public ImageView getButtonPlus() {
        return buttonPlus;
    }

    public void setButtonPlus(ImageView buttonPlus) {
        this.buttonPlus = buttonPlus;
    }

    public ImageView getButtonMinus() {
        return buttonMinus;
    }

    public void setButtonMinus(ImageView buttonMinus) {
        this.buttonMinus = buttonMinus;
    }

    public TextView getPrice() {
        return price;
    }

    public void setPrice(TextView price) {
        this.price = price;
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }

    public TextView getCount() {
        return count;
    }

    public void setCount(TextView count) {
        this.count = count;
    }

    public RelativeLayout getPanelproduct() {
        return panelproduct;
    }

    public void setPanelproduct(RelativeLayout panelproduct) {
        this.panelproduct = panelproduct;
    }

    public int getPosParent() {
        return posParent;
    }

    public void setPosParent(int posParent) {
        this.posParent = posParent;
    }

    public void setData(Products data) {
        this.data = data;
    }

    public HolderRecyclerHome(View itemView, ProductAddRemoveListener listener) {
        super(itemView);
        this.listener = listener;
    }


    @Override
    public void initComponent() {
        imageProduct  = (SimpleDraweeView) itemView.findViewById(R.id.imageProduct);
        buttonPlus  = (ImageView) itemView.findViewById(R.id.buttonplus);
        buttonMinus  = (ImageView) itemView.findViewById(R.id.buttominus);
        price  = (TextView) itemView.findViewById(R.id.price);
        title  = (TextView) itemView.findViewById(R.id.title);
        count  = (TextView) itemView.findViewById(R.id.number);
        panelproduct = (RelativeLayout) itemView.findViewById(R.id.panelproduct);

        initEvent();
    }

    @Override
    public void setText(Products object) {
        this.data = object;
        Integer num = OrderProductsStorage.getInstance(itemView.getContext()).countProduct(data.getId());
        if (num.compareTo(Integer.valueOf(0)) > 0) {
            count.setText(num.toString());
            buttonMinus.setVisibility(View.VISIBLE);
        } else {
            count.setText("0");
            buttonMinus.setVisibility(View.GONE);
        }
        price.setText(data.getPrice() + "€");
        title.setText(data.getTitle());
        String urlImage = data.getUrlImage();
        if (urlImage != null && !urlImage.equals("null")) {
            new LoadImage(urlImage,imageProduct,itemView.getContext());
        }
    }

    @Override
    public Products getData() {
        return data;
    }

    public void initEvent(){
        imageProduct.setOnClickListener(getOnClickListenerDetails());
        buttonPlus.setOnClickListener(getOnClickListenerPlus());
        buttonMinus.setOnClickListener(getOnClickListenerMinus());
    }


    private View.OnClickListener getOnClickListenerDetails(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LocalyticsEventActionMrJeff(itemView.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.HOMESUSCRIPCION, Boolean.FALSE, data.getId() != null ? data.getId().toString() : "");
                Intent show = new Intent(itemView.getContext(), OneProductActivity.class);
                show.putExtra(MrJeffConstant.INTENT_ATTR.IDPRODUCT, data.getId());
                show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, posParent);
                itemView.getContext().startActivity(show);
            }
        };
    }

    private View.OnClickListener getOnClickListenerPlus(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valueNumber = count.getText().toString();
                Integer num = 0;
                Integer numCart = 0;
                try {
                    num = Integer.valueOf(valueNumber) + 1;
                } catch (Exception e) {
                    num = 0;
                }

                count.setText(num.toString());
                buttonMinus.setVisibility(View.VISIBLE);
                if(listener != null){
                    listener.addProduct(data.getId());
                }
            }
        };

    }

    private View.OnClickListener getOnClickListenerMinus(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valueNumber = count.getText().toString();
                Integer num = 0;
                try {
                    num = Integer.valueOf(valueNumber) - 1;
                } catch (Exception e) {
                    num = 0;
                }
                num = num.compareTo(Integer.valueOf(0)) <= 0 ? 0 : num;
                count.setText(num.toString());
                if (num.intValue() <= 0) {
                    buttonMinus.setVisibility(View.INVISIBLE);
                }
                if(listener != null){
                    listener.removeProduct(data.getId());
                }
            }
        };

    }

    public ProductAddRemoveListener getListener() {
        return listener;
    }

    public void setListener(ProductAddRemoveListener listener) {
        this.listener = listener;
    }
}
