package es.coiasoft.mrjeff.view.adapter;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import es.coiasoft.mrjeff.NoLavoActivity;
import es.coiasoft.mrjeff.OneProductActivity;
import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.view.adapter.HolderRecycleViewMrJEff;
import es.coiasoft.mrjeff.view.listener.ProductAddRemoveListener;
import es.coiasoft.mrjeff.view.util.LoadImage;

/**
 * Created by Paulino on 13/09/2016.
 */
public class HolderRecyclerHomeSuscription extends HolderRecycleViewMrJEff<Products>{


    //Components
    private SimpleDraweeView imageProduct;
    private TextView title;
    private RelativeLayout panelproduct;

    private int posParent;

    private Products data;

    public SimpleDraweeView getImageProduct() {
        return imageProduct;
    }

    public void setImageProduct(SimpleDraweeView imageProduct) {
        this.imageProduct = imageProduct;
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }

    public RelativeLayout getPanelproduct() {
        return panelproduct;
    }

    public void setPanelproduct(RelativeLayout panelproduct) {
        this.panelproduct = panelproduct;
    }

    public int getPosParent() {
        return posParent;
    }

    public void setPosParent(int posParent) {
        this.posParent = posParent;
    }

    public void setData(Products data) {
        this.data = data;
    }

    public HolderRecyclerHomeSuscription(View itemView) {
        super(itemView);
    }


    @Override
    public void initComponent() {
        imageProduct  = (SimpleDraweeView) itemView.findViewById(R.id.imageproduct);
        title  = (TextView) itemView.findViewById(R.id.title);
        panelproduct = (RelativeLayout) itemView.findViewById(R.id.panelproduct);

        initEvent();
    }

    @Override
    public void setText(Products object) {
        this.data = object;
        title.setText(data.getTitle());
        String urlImage = data.getUrlImage();
        if (urlImage != null && !urlImage.equals("null")) {
            new LoadImage(urlImage,imageProduct,itemView.getContext());
        }
    }

    @Override
    public Products getData() {
        return data;
    }

    public void initEvent(){
        imageProduct.setOnClickListener(getOnClickListenerDetails());
    }


    private View.OnClickListener getOnClickListenerDetails(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LocalyticsEventActionMrJeff(itemView.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.HOMESUSCRIPCION, Boolean.FALSE, data.getId() != null ? data.getId().toString() : "");
                Intent show = new Intent(itemView.getContext(), NoLavoActivity.class);
                show.putExtra(MrJeffConstant.INTENT_ATTR.IDPRODUCT, data.getId());
                show.putExtra(MrJeffConstant.INTENT_ATTR.POSFRAGMENT, posParent);
                itemView.getContext().startActivity(show);
            }
        };
    }

}
