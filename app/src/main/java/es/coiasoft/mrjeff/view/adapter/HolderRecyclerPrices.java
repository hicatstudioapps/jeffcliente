package es.coiasoft.mrjeff.view.adapter;

import android.view.View;
import android.widget.TextView;

import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.orders.send.Product;

/**
 * Created by Paulino on 13/09/2016.
 */
public class HolderRecyclerPrices extends HolderRecycleViewMrJEff<Products>{

    public TextView name;
    public TextView price;
    private Products data;

    public HolderRecyclerPrices(View itemView) {
        super(itemView);
    }

    @Override
    public void initComponent() {
        name = (TextView) itemView.findViewById(R.id.product_list_name);
        price = (TextView) itemView.findViewById(R.id.product_list_price);
    }

    @Override
    public void setText(Products object) {
        this.data = object;
        name.setText(data.getTitle());
        price.setText(data.getPrice() + "€");
    }

    @Override
    public Products getData() {
        return data;
    }
}
