package es.coiasoft.mrjeff.view.adapter;

import android.view.View;
import android.widget.LinearLayout;

import es.coiasoft.mrjeff.MyWashActivity;
import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.domain.GroupStateOrder;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;

/**
 * Created by Paulino on 13/09/2016.
 */
public class HolderRecylerStateOrder extends HolderRecycleViewMrJEff<GroupStateOrder> {

    private MrJeffTextView titlemontorders;
    private LinearLayout listOrders;
    private GroupStateOrder data;
    private MyWashActivity activity;

    public HolderRecylerStateOrder(View itemView, MyWashActivity activity) {
        super(itemView);
        this.activity = activity;
    }

    @Override
    public void initComponent() {
        titlemontorders = (MrJeffTextView) itemView.findViewById(R.id.titlemontorders);
        listOrders = (LinearLayout) itemView.findViewById(R.id.listOrders);
        listOrders.setScrollContainer(false);
    }

    @Override
    public void setText(GroupStateOrder object) {
        this.data = object;

        titlemontorders.setText(itemView.getContext().getResources().getString(data.getState().getTitle()));

        if(data.getOrders() != null && !data.getOrders().isEmpty()){
            for(Orders order:data.getOrders()){
                HolderOneOrderMyListMonth holder = new HolderOneOrderMyListMonth(itemView.getContext(),listOrders,order,data);
                holder.setActivity(activity);
            }
        }
    }

    @Override
    public GroupStateOrder getData() {
        return data;
    }
}
