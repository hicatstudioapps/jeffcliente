package es.coiasoft.mrjeff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import es.coiasoft.mrjeff.ConnectionActivity;
import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.domain.storage.MonthStorage;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;

/**
 * Created by linovm on 9/10/15.
 */
public class ListAdapterDay extends ArrayAdapter<String> {



    private final ListView listView;
    private LinearLayout open = null;
    private ConnectionActivity connectionActivity;
    private final int layout = R.layout.date;
    private int positionSelected = 0;


    public ListAdapterDay(Context contexto, ArrayList<String> entradas, ListView list) {
        super(contexto, R.layout.date, entradas);
        this.listView = list;
        this.connectionActivity = connectionActivity;
    }

    @Override
    public View getView(int posicion, View currentView, ViewGroup pariente) {
        View view =  currentView;


        if (view == null){
            view = newView(posicion);
        }

        final HolderDate h = (HolderDate) view.getTag();

        if(posicion == positionSelected){
            changeColor(h);
        } else {
            changeColorNotselected(h);
        }

        onEntrada(getItem(posicion), h);


        return view;
    }

    private void changeColor(HolderDate h){
        h.getParent().setBackgroundResource(R.color.colorSecondary);
        h.getMonth().setTextColor(getContext().getResources().getColor(android.R.color.white));
        h.getDay().setTextColor(getContext().getResources().getColor(android.R.color.white));
    }

    private void changeColorNotselected(HolderDate h){
        h.getParent().setBackgroundResource(android.R.color.transparent);
        h.getMonth().setTextColor(getContext().getResources().getColor(R.color.colorAccent));
        h.getDay().setTextColor(getContext().getResources().getColor(R.color.colorAccent));
    }

    public int getPositionSelected() {
        return positionSelected;
    }

    public void setPositionSelected(int positionSelected) {
        this.positionSelected = positionSelected;
    }

    private View newView(final int posicion) {
        LayoutInflater inf = LayoutInflater.from(getContext());
        View view = inf.inflate(this.layout, null);
        final HolderDate h = new HolderDate();
        h.setDay((MrJeffTextView) view.findViewById(R.id.day));
        h.setMonth((MrJeffTextView) view.findViewById(R.id.month));
        h.setParent((LinearLayout)view.findViewById(R.id.parent));
        view.setTag(h);

        return view;
    }

    public void onEntrada (Object entrada, final HolderDate h){

        h.setDate((String)entrada);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(format.parse((String) entrada));
            setDay(calendar,h.getDay());
            h.getMonth().setText(getDate(calendar));
        }catch (Exception e){

        }


    }


    private String getDate(Calendar calendar){
        StringBuilder builderMonth = new StringBuilder();
        builderMonth.append(calendar.get(Calendar.DAY_OF_MONTH));
        builderMonth.append(" ");
        builderMonth.append(getContext().getResources().getString(R.string.of));
        builderMonth.append(" ");
        builderMonth.append(getContext().getResources().getString(MonthStorage.months.get(calendar.get(Calendar.MONTH) + 1)));

        return builderMonth.toString();
    }


    private void setDay(Calendar calendar, TextView textView){
        switch (calendar.get(Calendar.DAY_OF_WEEK)){
            case Calendar.MONDAY:
                textView.setText(getContext().getResources().getString(R.string.monday));
                break;
            case Calendar.TUESDAY:
                textView.setText(getContext().getResources().getString(R.string.tuesday));
                break;
            case Calendar.WEDNESDAY:
                textView.setText(getContext().getResources().getString(R.string.wednesday));
                break;
            case Calendar.THURSDAY:
                textView.setText(getContext().getResources().getString(R.string.thursday));
                break;
            case Calendar.FRIDAY:
                textView.setText(getContext().getResources().getString(R.string.friday));
                break;
            case Calendar.SATURDAY:
                textView.setText(getContext().getResources().getString(R.string.saturday));
                break;
            case Calendar.SUNDAY:
                textView.setText(getContext().getResources().getString(R.string.sunday));
                break;
        }
    }

}
