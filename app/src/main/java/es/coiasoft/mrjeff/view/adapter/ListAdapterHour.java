package es.coiasoft.mrjeff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import es.coiasoft.mrjeff.ConnectionActivity;
import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.domain.storage.MonthStorage;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;

/**
 * Created by linovm on 9/10/15.
 */
public class ListAdapterHour extends ArrayAdapter<String> {



    private final ListView listView;
    private LinearLayout open = null;
    private ConnectionActivity connectionActivity;
    private final int layout = R.layout.hour;
    private int positionSelected = 0;


    public ListAdapterHour(Context contexto, ArrayList<String> entradas, ListView list) {
        super(contexto, R.layout.hour, entradas);
        this.listView = list;
        this.connectionActivity = connectionActivity;
    }

    @Override
    public View getView(int posicion, View currentView, ViewGroup pariente) {
        View view =  currentView;

        if (view == null){
            view = newView(posicion);
        }

        final HolderHour h = (HolderHour) view.getTag();

        if(posicion == positionSelected){
            changeColor(h);
        } else {
            changeColorNotselected(h);
        }

        onEntrada(getItem(posicion), h);


        return view;
    }

    private void changeColor(HolderHour h){
        h.getParent().setBackgroundResource(R.color.colorSecondary);
        h.getHour().setTextColor(getContext().getResources().getColor(android.R.color.white));
    }

    private void changeColorNotselected(HolderHour h){
        h.getParent().setBackgroundResource(android.R.color.transparent);
        h.getHour().setTextColor(getContext().getResources().getColor(R.color.colorAccent));
    }

    public int getPositionSelected() {
        return positionSelected;
    }

    public void setPositionSelected(int positionSelected) {
        this.positionSelected = positionSelected;
    }


    private View newView(final int posicion) {
        LayoutInflater inf = LayoutInflater.from(getContext());
        View view = inf.inflate(this.layout, null);
        final HolderHour h = new HolderHour();
        h.setHour((MrJeffTextView) view.findViewById(R.id.hour));
        h.setParent((LinearLayout)view.findViewById(R.id.parent));
        view.setTag(h);

        return view;
    }

    public void onEntrada (Object entrada, final HolderHour h){
        h.setHourTag((String)entrada);
        h.getHour().setText((String)entrada);
    }


}
