package es.coiasoft.mrjeff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.orders.response.Line_items;

/**
 * Created by linovm on 9/10/15.
 */
public class ListAdapterProductMyOrder extends ArrayAdapter<Line_items> {



    private final ListView listView;
    private LinearLayout open = null;
    private int layout;


    public ListAdapterProductMyOrder(Context contexto, int R_layout_IdView, ArrayList<Line_items> entradas, ListView list) {
        super(contexto, R_layout_IdView, entradas);
        this.listView = list;
        this.layout = R_layout_IdView;
    }

    @Override
    public View getView(int posicion, View currentView, ViewGroup pariente) {
        View view =  currentView;

        if (view != null){
            HolderProductMyOrder h = (HolderProductMyOrder) view.getTag();
        }

        view = newView(posicion);


        final HolderProductMyOrder h = (HolderProductMyOrder) view.getTag();
        onEntrada(getItem(posicion), h);


        return view;
    }

    private View newView(final int posicion) {
        LayoutInflater inf = LayoutInflater.from(getContext());
        View view = inf.inflate(this.layout, null);
        final HolderProductMyOrder h = new HolderProductMyOrder();
        h.name = (TextView) view.findViewById(R.id.product_order_name);
        h.price = (TextView) view.findViewById(R.id.product_order_price);
        h.count = (TextView) view.findViewById(R.id.product_order_count);

        view.setTag(h);



        return view;
    }

    public void onEntrada (Object entrada, final HolderProductMyOrder h){
        h.name.setText(((Line_items) entrada).getName());
        final NumberFormat format = new DecimalFormat("#.00");
        Float totalTax = Float.valueOf(((es.coiasoft.mrjeff.domain.orders.response.Line_items) entrada).getTotal_tax());
        Float totalPrice = Float.valueOf(((es.coiasoft.mrjeff.domain.orders.response.Line_items) entrada).getTotal());
        totalPrice = totalPrice + totalTax;
        h.price.setText(format.format(totalPrice) + "€");
        h.count.setText(((Line_items) entrada).getQuantity().toString());
    }




}
