package es.coiasoft.mrjeff.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.view.components.MrJeffTextView;

/**
 * Created by linovm on 9/10/15.
 */
public class ListAdapterString extends ArrayAdapter<String> {



    private LinearLayout open = null;
    private Context context;


    public ListAdapterString(Context contexto, ArrayList<String> entradas) {
        super(contexto, R.layout.address_list, entradas);
        this.context = contexto;
    }

    @Override
    public View getView(int posicion, View currentView, ViewGroup pariente) {
        View view =  currentView;

        if (view == null){
            LayoutInflater inf = LayoutInflater.from(getContext());
             view = inf.inflate(R.layout.address_list, null);
        }

        String text = getItem(posicion);

        MrJeffTextView textView = (MrJeffTextView)view.findViewById(R.id.address_name);
        textView.setText(text);

        return view;
    }






}
