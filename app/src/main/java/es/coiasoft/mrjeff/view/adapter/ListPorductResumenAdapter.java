package es.coiasoft.mrjeff.view.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import es.coiasoft.mrjeff.HomeActivity;
import es.coiasoft.mrjeff.PaidCartActivity;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.domain.orders.response.Line_items;

import java.util.ArrayList;

import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.domain.storage.OrderProductsStorage;
import es.coiasoft.mrjeff.manager.worker.DeleteCouponWorker;
import es.coiasoft.mrjeff.view.components.MrJeffTextBoldView;
import es.coiasoft.mrjeff.view.components.ProgressDialogMrJeff;

/**
 * Created by linovm on 9/10/15.
 */
public abstract class ListPorductResumenAdapter extends ArrayAdapter<Line_items> {

    private final Context context;
    private ListView listView;
    private LinearLayout open = null;
    private boolean isSuscription = false;



    public ListPorductResumenAdapter(Context contexto, int R_layout_IdView, ArrayList<Line_items> entradas, ListView listView, boolean isSuscription) {
        super(contexto, R_layout_IdView, entradas);
        this.listView = listView;
        this.isSuscription = isSuscription;
        this.context= contexto;
    }

    @Override
    public View getView(final int posicion, View currentView, ViewGroup pariente) {
        View view = newView(posicion);
        final Line_items l= getItem(posicion);
        final HolderProductOrder h = (HolderProductOrder) view.getTag();
        h.getProduct_trash().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.Theme_AppCompat_Light)).setTitle("Eliminar producto").setMessage("Está seguro que desea eliminar este producto de su pedido?").setNeutralButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        OrderProductsStorage.getInstance(context.getApplicationContext()).remove(getItem(posicion).getProduct_id());
                        remove(getItem(posicion));
                        ((PaidCartActivity)context).setHasChange(true);
                        checkEmpty();
                    }

                }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }
        });
//        h.getPlus().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                addProduct(getItem(posicion).getProduct_id(),h,posicion,l);
//                ((PaidCartActivity)context).setHasChange(true);
//            }
//        });
//        h.getMinus().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                removeProduct(getItem(posicion).getProduct_id(),h,posicion,l);
//                ((PaidCartActivity)context).setHasChange(true);
//            }
//        });
        onEntrada(getItem(posicion), h);

        return view;
    }

    private void checkEmpty() {
        if(OrderProductsStorage.getInstance(context.getApplicationContext()).getNumTotal()==0){
            new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.Theme_AppCompat_Light)).setTitle("Vacio").setMessage("Se ha quedado sin productos en el carrito, debe ir a la pantalla principal para adicionar estos.").setNeutralButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent myIntent = new Intent(context, HomeActivity.class);
                    context.startActivity(myIntent);
                }

            }).setCancelable(false).show();
        }
    }

    void deleteAll(Integer idProduct, HolderProductOrder holdr, final int position, Line_items l){
        String valueNumberCart = holdr.getText_count().getText().toString();
        Integer numCart = 0;
        try {
            numCart = Integer.valueOf(valueNumberCart) - 1;
        } catch (Exception e) {
            numCart = 0;
        }
        numCart = numCart.compareTo(Integer.valueOf(0)) <= 0 ? 0 : numCart;
        for (int i=0; i< numCart; i++)
            OrderProductsStorage.getInstance(context.getApplicationContext()).removeProductElement(idProduct);
    }

    public void addProduct(Integer idProduct, HolderProductOrder holdr, int position, Line_items l) {
        String valueNumberCart = holdr.getText_count().getText().toString();
        Log.d("ProductoID", idProduct + "");
        Integer numCart = 0;
        try {
            numCart = Integer.valueOf(valueNumberCart) + 1;
        } catch (Exception e) {
            numCart = 0;
        }
        OrderProductsStorage.getInstance(context.getApplicationContext()).addProductElement(idProduct);
        l.setQuantity(numCart);
        notifyDataSetChanged();
        holdr.getText_count().setText(numCart+"");
    }

    public void removeProduct(Integer idProduct, HolderProductOrder holdr, final int position, Line_items l) {
        String valueNumberCart = holdr.getText_count().getText().toString();
        Integer numCart = 0;
        try {
            numCart = Integer.valueOf(valueNumberCart) - 1;
        } catch (Exception e) {
            numCart = 0;
        }
        numCart = numCart.compareTo(Integer.valueOf(0)) <= 0 ? 0 : numCart;
        if(numCart == 0){
            new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.Theme_AppCompat_Light)).setTitle("Eliminar producto").setMessage("Está seguro que desea eliminar este producto de su pedido?").setNeutralButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    OrderProductsStorage.getInstance(context.getApplicationContext()).remove(getItem(position).getProduct_id());
                    remove(getItem(position));
                }

            }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).show();
        }else{
            OrderProductsStorage.getInstance(context.getApplicationContext()).removeProductElement(idProduct);
            holdr.getText_count().setText(numCart+"");
            l.setQuantity(numCart);
            notifyDataSetChanged();
        }
    }

    private View newView(final int posicion) {
        LayoutInflater inf = LayoutInflater.from(getContext());
        final View view = inf.inflate(R.layout.product_resume, listView,false);
        final HolderProductOrder h = new HolderProductOrder(isSuscription);
        h.setText_name((TextView) view.findViewById(R.id.product_name));
        h.setText_price((TextView) view.findViewById(R.id.product_price));
        h.setText_count((TextView) view.findViewById(R.id.product_count));
        h.setMain((LinearLayout) view.findViewById(R.id.panelPrinListOrder));
        h.setProduct_trash((ImageView) view.findViewById(R.id.product_trash));
        h.setMain((LinearLayout) view.findViewById(R.id.panelPrinListOrder));
        h.setProduct_trash((ImageView) view.findViewById(R.id.product_trash));
        h.setPanelnotsuscription((RelativeLayout)view.findViewById(R.id.panelnotsuscription));
        h.setPanelsuscription((RelativeLayout)view.findViewById(R.id.panelsuscription));
        h.setProduct_price_sus((TextView) view.findViewById(R.id.product_price_sus));
        h.setProduct_desc((TextView) view.findViewById(R.id.product_desc));

        if(isSuscription){
            h.getPanelnotsuscription().setVisibility(View.GONE);
            h.getPanelsuscription().setVisibility(View.VISIBLE);
        } else {
            h.getPanelnotsuscription().setVisibility(View.VISIBLE);
            h.getPanelsuscription().setVisibility(View.GONE);
        }

        view.setTag(h);
        return view;
    }

    public abstract void onEntrada (Object entrada, final HolderProductOrder h);




}
