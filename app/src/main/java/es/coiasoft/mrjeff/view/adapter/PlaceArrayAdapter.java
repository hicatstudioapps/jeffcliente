package es.coiasoft.mrjeff.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.view.adapter.domain.PlaceAutoComplete;

/**
 * Created by Paulino on 10/03/2016.
 */
public class PlaceArrayAdapter extends ArrayAdapter<PlaceAutoComplete> implements Filterable {
        private static final String TAG = "ADAPTER_PLACE";
        private GoogleApiClient mGoogleApiClient;
        private AutocompleteFilter placeFilter;
        private LatLngBounds location;
        private ArrayList<PlaceAutoComplete> mResultList;


    public PlaceArrayAdapter(Context context, int resource, LatLngBounds location,
            AutocompleteFilter filter) {
        super(context, resource);
        this.location = location;
        this.placeFilter = filter;
    }

    public void setGoogleApiClient(GoogleApiClient googleApiClient) {
        if (googleApiClient == null || !googleApiClient.isConnected()) {
            mGoogleApiClient = null;
        } else {
            mGoogleApiClient = googleApiClient;
        }
    }

    @Override
    public View getView(int posicion, View currentView, ViewGroup pariente) {

        LayoutInflater inf = LayoutInflater.from(getContext());
        View view = inf.inflate(R.layout.item_text, null);
        TextView title  = (TextView) view.findViewById(R.id.text_place);
        PlaceAutoComplete place = getItem(posicion);
        if(place != null && place.getDescription() != null) {
            title.setText(place.getDescription());
            view.setTag(place);
        }

        return view;
    }

    @Override
    public int getCount() {
        if(mResultList!= null)
        return mResultList.size();
        else
            return 0;
    }

    @Override
    public PlaceAutoComplete getItem(int position) {
        return mResultList == null || mResultList.isEmpty()?null : (mResultList.get(position > mResultList.size()-1 ? mResultList.size()-1:position));
    }

    private ArrayList<PlaceAutoComplete> getPredictions(CharSequence constraint) {
        if (mGoogleApiClient != null) {
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi
                            .getAutocompletePredictions(mGoogleApiClient, constraint.toString(),
                                    location, placeFilter);
            // Wait for predictions, set the timeout.
            AutocompletePredictionBuffer autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS);
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                Toast.makeText(getContext(), "Error: " + status.toString(),
                        Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Error getting place predictions: " + status
                        .toString());
                autocompletePredictions.release();
                return null;
            }

            Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
            ArrayList resultList = new ArrayList<>(autocompletePredictions.getCount());
            while (iterator.hasNext()) {
                AutocompletePrediction prediction = iterator.next();
                resultList.add(new PlaceAutoComplete(prediction.getPlaceId(),prediction.getFullText(null).toString()));
            }
            autocompletePredictions.release();
            return resultList;
        }
        Log.e(TAG, "Google API client is not connected.");
        return null;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected android.widget.Filter.FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint != null) {
                    mResultList = getPredictions(constraint);
                    if (mResultList != null) {
                        // Results
                        results.values = mResultList;
                        results.count = mResultList.size();
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

}
