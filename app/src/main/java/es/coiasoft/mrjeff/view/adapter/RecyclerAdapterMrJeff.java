package es.coiasoft.mrjeff.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paulino on 02/09/2016.
 */
public abstract class RecyclerAdapterMrJeff<E extends HolderRecycleViewMrJEff<T>, T> extends RecyclerView.Adapter<E> {

    private List<T> items;
    private Context context;
    private int layout;

    private int position = 0;

    public RecyclerAdapterMrJeff(Context context, List<T> items, int layout) {
        super();
        this.context = context;
        this.items = items;
        this.layout = layout;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.layout;
    }

    @Override
    public void onBindViewHolder(E itemsViewHolder, int i) {
        itemsViewHolder.setAdapter(this);
        itemsViewHolder.setText(items.get(i));
    }

    @Override
    public E onCreateViewHolder(ViewGroup viewGroup, int layout) {
        View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(layout, viewGroup, false);

        E holder = getInstanceHolder(itemView);


        return holder;
    }

    public abstract E getInstanceHolder(View v);

    public void addItem(T item) {
        items.add(item);
    }

    public void addAllItem(ArrayList<T> item) {
        items.addAll(item);
    }

    public int getPosition() {
        return position;
    }

}
