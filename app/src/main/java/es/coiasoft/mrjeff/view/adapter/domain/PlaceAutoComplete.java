package es.coiasoft.mrjeff.view.adapter.domain;

/**
 * Created by Paulino on 10/03/2016.
 */
public class PlaceAutoComplete {

    private CharSequence placeId;
    private CharSequence description;

    public PlaceAutoComplete(CharSequence placeId, CharSequence description) {
        this.placeId = placeId;
        this.description = description;
    }

    public CharSequence getPlaceId() {
        return placeId;
    }

    public void setPlaceId(CharSequence placeId) {
        this.placeId = placeId;
    }

    public CharSequence getDescription() {
        return description;
    }

    public void setDescription(CharSequence description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description.toString();
    }
}
