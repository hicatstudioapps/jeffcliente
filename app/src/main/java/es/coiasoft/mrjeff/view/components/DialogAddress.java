package es.coiasoft.mrjeff.view.components;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.List;

import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.view.adapter.ListAdapterString;

/**
 * Created by linovm on 26/10/15.
 */
public class DialogAddress extends Dialog{

    private ViewFlipper vf;
    private ListView listView;
    private TextView btnYes;
    private TextView btnNo;
    private EditText textAddress;
    private List<String> address;
    private int positionIndex = -1;

    public DialogAddress(Context context, final EditText textAddress,List<String> address) {
        super(context, R.style.datePickerMrJeff);
        setContentView(R.layout.select_address);
        this.textAddress = textAddress;
        this.address = address;
        this.btnNo = (TextView) findViewById(R.id.btnCancelAddress);
        this.btnYes = (TextView) findViewById(R.id.btnAcceptAddress);
        this.listView = (ListView) findViewById(R.id.btnAcceptAddress);

        this.listView.setAdapter(new ListAdapterString(context,new ArrayList<String>(address)));

        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = null;
                try {
                    textView = (TextView) listView.getChildAt(positionIndex).findViewById(R.id.address_name);
                    if (textView != null) {
                        textView.setTextColor(getContext().getResources().getColor(R.color.grey80));
                    }
                }catch (Exception e){

                }
                positionIndex = position;
                try {
                    textView = (TextView) listView.getChildAt(positionIndex).findViewById(R.id.address_name);
                    if (textView != null) {
                        textView.setTextColor(getContext().getResources().getColor(R.color.grey20));
                    }
                }catch (Exception e){

                }
            }
        });

        this.btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textAddress != null){
                    TextView textView = null;
                    try {
                        textView = (TextView) listView.getChildAt(positionIndex).findViewById(R.id.address_name);
                        if (textView != null) {
                            String text = textView.getText().toString();
                            textAddress.setText(text);
                        }
                    }catch (Exception e){

                    }
                }
                close();
            }
        });

        this.btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close();
            }
        });
    }

    public void close(){
        hide();
    }


}
