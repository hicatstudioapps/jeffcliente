package es.coiasoft.mrjeff.view.components;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.localytics.android.Localytics;

import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.HomeActivity;
import es.coiasoft.mrjeff.LoginNewAccountActivity;
import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.storage.CustomerStorage;
import es.coiasoft.mrjeff.manager.worker.LoginFacebookWorker;
import es.coiasoft.mrjeff.manager.worker.LoginGPlusWorker;
import es.coiasoft.mrjeff.manager.worker.LoginWorker;

/**
 * Created by linovm on 14/12/15.
 */
public class LoginDialog extends Dialog implements Observer, GoogleApiClient.OnConnectionFailedListener{


    protected LinearLayout btnWDLogin;
    protected LinearLayout btnWDNewUser;
    protected LinearLayout buttonlogingoogle;
    protected LoginButton loginFaceButton;
    protected CallbackManager callbackManager;
    protected Bundle bFacebookData;
    protected FragmentActivity parent;
    protected LoginDialog  instance;
    protected ImageView closedialog;
    protected Handler handlerDialog = new Handler();
    protected GoogleApiClient mGoogleApiClient;
    protected GoogleSignInOptions gso;
    protected SignInButton sign_in_button;

    public LoginDialog(FragmentActivity activity,GoogleSignInOptions gso){
        super(activity, R.style.datePickerMrJeff);
        this.parent = activity;
        this.gso= gso;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        callbackManager = CallbackManager.Factory.create();
        mGoogleApiClient = new GoogleApiClient.Builder(parent)
                .enableAutoManage(parent, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        setContentView(R.layout.optionslogin_dialog);
        instance = this;
        getElementWindowOptions();

    }


    private void getElementWindowOptions(){
        btnWDLogin = (LinearLayout)findViewById(R.id.buttonlogin);
        btnWDLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent show = new Intent(v.getContext(), LoginNewAccountActivity.class);
                show.putExtra(MrJeffConstant.LOGIN_EXTRA.LOGIN_ATTR, MrJeffConstant.LOGIN_EXTRA.LOGIN_BUTTON);
                getContext().startActivity(show);

            }
        });
        btnWDNewUser = (LinearLayout)findViewById(R.id.buttonnewuser);
        btnWDNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent show = new Intent(v.getContext(), LoginNewAccountActivity.class);
                show.putExtra(MrJeffConstant.LOGIN_EXTRA.LOGIN_ATTR,MrJeffConstant.LOGIN_EXTRA.REGIS_BUTTON);
                getContext().startActivity(show);
            }
        });

        closedialog = (ImageView)findViewById(R.id.closedialog);
        closedialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instance.dismiss();
            }
        });



        loginFaceButton = (LoginButton) findViewById(R.id.login_button_facebook);
        float fbIconScale = 0.25F;
        Drawable drawable = getContext().getResources().getDrawable(R.drawable.facebook);
        drawable.setBounds(0, 0, (int) (drawable.getIntrinsicWidth() * fbIconScale), (int) (drawable.getIntrinsicHeight() * fbIconScale));
        loginFaceButton.setCompoundDrawables(drawable, null, null, null);
        loginFaceButton.setReadPermissions("user_friends", "email", "user_about_me");

        loginFaceButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Procesando datos...");
                progressDialog.show();
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        // Get facebook data from login
                        bFacebookData = getFacebookData(object);
                        if (bFacebookData != null) {
                            LoginFacebookWorker worker = new LoginFacebookWorker(bFacebookData);
                            worker.addObserver(instance);
                            Thread thread = new Thread(worker);
                            thread.start();
                        } else {
                            new AlertDialog.Builder(new ContextThemeWrapper(parent, R.style.Theme_AppCompat_Light)).setTitle(R.string.modalfacebookerror).setMessage(R.string.modalfacebookerrortext).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            }).show();
                        }

                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email,gender, location"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                new AlertDialog.Builder(new ContextThemeWrapper(parent, R.style.Theme_AppCompat_Light)).setTitle(R.string.modalfacebookcancel).setMessage(R.string.modalfacebookcanceltext).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
            }

            @Override
            public void onError(FacebookException exception) {
                new AlertDialog.Builder(new ContextThemeWrapper(parent, R.style.Theme_AppCompat_Light)).setTitle(R.string.modalfacebookerror).setMessage(R.string.modalfacebookerrortext).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
            }
        });

        sign_in_button =(SignInButton)findViewById(R.id.sign_in_button);
        sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        buttonlogingoogle= (LinearLayout) findViewById(R.id.buttonlogingoogle);
        buttonlogingoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
    }

    private Bundle getFacebookData(JSONObject object) {

        try {
            Bundle bundle = new Bundle();
            String id = object.getString("id");

            URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
            Log.i("profile_pic", profile_pic + "");
            bundle.putString("profile_pic", profile_pic.toString());

            bundle.putString("idFacebook", id);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));
            if (object.has("gender"))
                bundle.putString("gender", object.getString("gender"));
            if (object.has("birthday"))
                bundle.putString("birthday", object.getString("birthday"));
            if (object.has("location"))
                bundle.putString("location", object.getJSONObject("location").getString("name"));

            return bundle;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    public void initLoginFacebook(int requestCode, int resultCode, Intent data){
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void initLoginGoogle(int requestCode, int resultCode, Intent data){
        if (requestCode == MrJeffConstant.ACTIVITY_RESULT.GOOGLE_SIG_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            final Bundle bundle = new Bundle();

            if(acct.getPhotoUrl() != null){
                bundle.putString("profile_pic",acct.getPhotoUrl().toString());
            }


            bundle.putString("id", acct.getId());
            if (acct.getDisplayName() != null)
                bundle.putString("first_name", acct.getDisplayName());
            bundle.putString("last_name", "");
            if (acct.getEmail() != null)
                bundle.putString("email", acct.getEmail());


            ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Procesando datos...");
            progressDialog.show();
            LoginGPlusWorker worker = new LoginGPlusWorker(bundle);
            worker.addObserver(instance);
            Thread thread = new Thread(worker);
            thread.start();

        } else {
            new AlertDialog.Builder(new ContextThemeWrapper(parent, R.style.Theme_AppCompat_Light)).setTitle(R.string.modalgplusrror).setMessage(R.string.modalgpluserrortext).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            }).show();
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        parent.startActivityForResult(signInIntent, MrJeffConstant.ACTIVITY_RESULT.GOOGLE_SIG_IN);
    }

    @Override
    public void update(final Observable observable, Object data) {
        if(data instanceof Integer &&
                ((Integer)data).intValue() == LoginWorker.RESULT_SUCCESS){
            Thread threadUpdate = new Thread(new Runnable() {
                @Override
                public void run() {
                    handlerDialog.post(new Runnable() {
                        @Override
                        public void run() {
                            Localytics.setCustomerFirstName(CustomerStorage.getInstance(instance.getContext()).getCustomer().getFirst_name());
                            Localytics.setCustomerLastName(CustomerStorage.getInstance(instance.getContext()).getCustomer().getLast_name());
                            Localytics.setCustomerEmail(CustomerStorage.getInstance(instance.getContext()).getCustomer().getEmail());

                            Map<String, String> values = new HashMap<String, String>();
                            values.put("Email", CustomerStorage.getInstance(instance.getContext()).getCustomer().getEmail());
                            if(observable instanceof LoginGPlusWorker) {
                                Localytics.tagEvent("Se ha autenticado con facebook", values);
                            } else {
                                Localytics.tagEvent("Se ha autenticado con google plus", values);
                            }
                            Intent show = new Intent(getContext(), HomeActivity.class);
                            getContext().startActivity(show);
                        }
                    });
                }
            });
            threadUpdate.start();


        }else {
            Thread threadUpdate = new Thread(new Runnable() {
                @Override
                public void run() {
                    handlerDialog.post(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(new ContextThemeWrapper(instance.getContext(), R.style.Theme_AppCompat_Light)).setTitle(R.string.login_modal_error).setMessage(R.string.login_modal_error_text).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            }).show();
                        }
                    });
                }
            });
            threadUpdate.start();
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
