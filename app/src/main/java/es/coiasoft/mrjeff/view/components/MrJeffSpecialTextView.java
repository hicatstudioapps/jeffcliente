package es.coiasoft.mrjeff.view.components;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Paulino on 19/01/2016.
 */
public class MrJeffSpecialTextView extends TextView {

    public MrJeffSpecialTextView(Context context) {
        super(context);
        setFont(context);
    }

    public MrJeffSpecialTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public MrJeffSpecialTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }


    private void setFont(Context context){
        Typeface fontRegular = Typeface.createFromAsset(context.getAssets(), "fonts/special.ttf");
        setTypeface(fontRegular, Typeface.NORMAL);
    }
}
