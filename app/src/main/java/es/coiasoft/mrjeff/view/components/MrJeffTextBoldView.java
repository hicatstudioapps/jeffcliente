package es.coiasoft.mrjeff.view.components;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Paulino on 19/01/2016.
 */
public class MrJeffTextBoldView extends MrJeffTextView {

    public MrJeffTextBoldView(Context context) {
        super(context);
        setFont(context);
    }

    public MrJeffTextBoldView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public MrJeffTextBoldView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }


    private void setFont(Context context){
        Typeface fontBold = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueMedium.ttf");
        setTypeface(fontBold, Typeface.BOLD);
    }
}
