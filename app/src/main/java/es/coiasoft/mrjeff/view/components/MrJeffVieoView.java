package es.coiasoft.mrjeff.view.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * Created by Paulino on 20/01/2016.
 */
public class MrJeffVieoView extends VideoView {

    public MrJeffVieoView(Context context) {
        super(context);
    }

    public MrJeffVieoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MrJeffVieoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }

}
