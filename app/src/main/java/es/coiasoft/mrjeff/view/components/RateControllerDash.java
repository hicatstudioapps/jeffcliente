package es.coiasoft.mrjeff.view.components;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.HomeActivity;
import es.coiasoft.mrjeff.MyWashActivity;
import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.manager.model.proxy.MediaType;
import es.coiasoft.mrjeff.manager.model.proxy.OrderProxy;
import es.coiasoft.mrjeff.manager.model.proxy.Valoration;
import es.coiasoft.mrjeff.manager.worker.PostValorationWorker;

/**
 * Created by Paulino on 30/08/2016.
 */
public class RateControllerDash implements Animation.AnimationListener, Observer {

    private Valoration valoration = new Valoration();
    private View view = null;
    private HomeActivity.FragmentSuscription context = null;
    private RelativeLayout mediatv;
    private RelativeLayout mediaface;
    private RelativeLayout mediainst;
    private RelativeLayout mediafri;
    private RelativeLayout mediagoo;
    private RelativeLayout mediaoth;
    private ImageView ratejeff1;
    private ImageView ratejeff2;
    private ImageView ratejeff3;
    private ImageView ratejeff4;
    private ImageView ratejeff5;
    private ImageView rateclo1;
    private ImageView rateclo2;
    private ImageView rateclo3;
    private ImageView rateclo4;
    private ImageView rateclo5;
    private RelativeLayout friendyes;
    private RelativeLayout friendno;
    private RelativeLayout returnyes;
    private RelativeLayout returnno;
    private RelativeLayout parent;
    private EditText note;
    private Animation bottomUp;
    private Animation bottomDown;
    private ImageView rateclose;
    private MrJeffTextView btnsend;
    protected ProgressDialog progressDialog;
    protected Handler handler = new Handler();
    protected boolean isShow = true;


    public RateControllerDash(View view, HomeActivity.FragmentSuscription context) {
        this.valoration.setOrder(new OrderProxy());
        this.view = view;
        this.context = context;
        initComponent();
        initEventMedia();
        initEventRecomended();
        initEventAgain();
        initEventRateJeff();
        initEventRateClo();
        initEventNote();
        initEventClose();
        initEventSend();
    }

    private void initComponent() {
        mediatv = (RelativeLayout) view.findViewById(R.id.mediatv);
        mediaface = (RelativeLayout) view.findViewById(R.id.mediaface);
        mediainst = (RelativeLayout) view.findViewById(R.id.mediainst);
        mediafri = (RelativeLayout) view.findViewById(R.id.mediafri);
        mediagoo = (RelativeLayout) view.findViewById(R.id.mediagoo);
        mediaoth = (RelativeLayout) view.findViewById(R.id.mediaoth);
        ratejeff1 = (ImageView) view.findViewById(R.id.ratejeff1);
        ratejeff2 = (ImageView) view.findViewById(R.id.ratejeff2);
        ratejeff3 = (ImageView) view.findViewById(R.id.ratejeff3);
        ratejeff4 = (ImageView) view.findViewById(R.id.ratejeff4);
        ratejeff5 = (ImageView) view.findViewById(R.id.ratejeff5);
        rateclo1 = (ImageView) view.findViewById(R.id.rateclo1);
        rateclo2 = (ImageView) view.findViewById(R.id.rateclo2);
        rateclo3 = (ImageView) view.findViewById(R.id.rateclo3);
        rateclo4 = (ImageView) view.findViewById(R.id.rateclo4);
        rateclo5 = (ImageView) view.findViewById(R.id.rateclo5);
        friendyes = (RelativeLayout) view.findViewById(R.id.friendyes);
        friendno = (RelativeLayout) view.findViewById(R.id.friendno);
        returnyes = (RelativeLayout) view.findViewById(R.id.returnyes);
        returnno = (RelativeLayout) view.findViewById(R.id.returnno);
        note = (EditText) view.findViewById(R.id.note);
        parent = (RelativeLayout) view.findViewById(R.id.panelRate);
        bottomUp = AnimationUtils.loadAnimation(context.getActivity(), R.anim.bottom_up);
        bottomUp.setAnimationListener(this);
        bottomDown = AnimationUtils.loadAnimation(context.getActivity(), R.anim.bottom_down);
        bottomDown.setAnimationListener(this);
        rateclose = (ImageView) view.findViewById(R.id.rateclose);
        btnsend = (MrJeffTextView) view.findViewById(R.id.btnsend);
    }

    private void initEventSend() {
        btnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    if (progressDialog == null) {
                        progressDialog = ProgressDialogMrJeff.createAndStart(context.getActivity(), R.string.ratesend);
                    } else {
                        progressDialog.show();
                    }
                    PostValorationWorker worker = new PostValorationWorker(valoration);
                    worker.addObserver(RateControllerDash.this);
                    Thread thread = new Thread(worker);
                    thread.start();
                } else {
                    new AlertDialog.Builder(new ContextThemeWrapper(context.getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.empty).setMessage(context.getResources().getString(R.string.rateerror)).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    }).show();
                }
            }
        });

    }

    private boolean isValid() {
        boolean result = valoration != null && valoration.getOrder() != null && valoration.getOrder().getIdWoocommerce() != null;
        result = result && valoration != null && valoration.getMedia() != null;
        result = result && valoration != null && valoration.getUsedAgain() != null;
        result = result && valoration != null && valoration.getRateClothes() != null;
        result = result && valoration != null && valoration.getRecomended() != null;
        result = result && valoration != null && valoration.getRateJeff() != null;
        return result;
    }

    private void initEventClose() {
        rateclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hiddenPanel();
            }
        });
    }


    private void initEventNote() {
        note.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                valoration.setNotes(note.getText().toString());
            }
        });
    }

    private void initEventMedia() {
        mediatv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkMedia(mediatv, MediaType.TV);
            }
        });
        mediaface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkMedia(mediaface, MediaType.FACEBOOK);
            }
        });
        mediainst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkMedia(mediainst, MediaType.INSTAGRAM);
            }
        });
        mediafri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkMedia(mediafri, MediaType.FRIENDS);
            }
        });
        mediagoo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkMedia(mediagoo, MediaType.GOOGLE);
            }
        });
        mediaoth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkMedia(mediagoo, MediaType.OTHERS);
            }
        });


    }

    private void initEventRateClo() {
        rateclo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valoration.setRateClothes(1);
                rateclo1.setImageResource(R.drawable.starpink);
                rateclo2.setImageResource(R.drawable.starblue);
                rateclo3.setImageResource(R.drawable.starblue);
                rateclo4.setImageResource(R.drawable.starblue);
                rateclo5.setImageResource(R.drawable.starblue);
            }
        });
        rateclo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valoration.setRateClothes(2);
                rateclo1.setImageResource(R.drawable.starpink);
                rateclo2.setImageResource(R.drawable.starpink);
                rateclo3.setImageResource(R.drawable.starblue);
                rateclo4.setImageResource(R.drawable.starblue);
                rateclo5.setImageResource(R.drawable.starblue);
            }
        });
        rateclo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valoration.setRateClothes(3);
                rateclo1.setImageResource(R.drawable.starpink);
                rateclo2.setImageResource(R.drawable.starpink);
                rateclo3.setImageResource(R.drawable.starpink);
                rateclo4.setImageResource(R.drawable.starblue);
                rateclo5.setImageResource(R.drawable.starblue);
            }
        });
        rateclo4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valoration.setRateClothes(4);
                rateclo1.setImageResource(R.drawable.starpink);
                rateclo2.setImageResource(R.drawable.starpink);
                rateclo3.setImageResource(R.drawable.starpink);
                rateclo4.setImageResource(R.drawable.starpink);
                rateclo5.setImageResource(R.drawable.starblue);
            }
        });
        rateclo5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valoration.setRateClothes(5);
                rateclo1.setImageResource(R.drawable.starpink);
                rateclo2.setImageResource(R.drawable.starpink);
                rateclo3.setImageResource(R.drawable.starpink);
                rateclo4.setImageResource(R.drawable.starpink);
                rateclo5.setImageResource(R.drawable.starpink);
            }
        });

    }

    private void initEventRateJeff() {
        ratejeff1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valoration.setRateJeff(1);
                ratejeff1.setImageResource(R.drawable.starpink);
                ratejeff2.setImageResource(R.drawable.starblue);
                ratejeff3.setImageResource(R.drawable.starblue);
                ratejeff4.setImageResource(R.drawable.starblue);
                ratejeff5.setImageResource(R.drawable.starblue);
            }
        });

        ratejeff2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valoration.setRateJeff(2);
                ratejeff1.setImageResource(R.drawable.starpink);
                ratejeff2.setImageResource(R.drawable.starpink);
                ratejeff3.setImageResource(R.drawable.starblue);
                ratejeff4.setImageResource(R.drawable.starblue);
                ratejeff5.setImageResource(R.drawable.starblue);
            }
        });

        ratejeff3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valoration.setRateJeff(3);
                ratejeff1.setImageResource(R.drawable.starpink);
                ratejeff2.setImageResource(R.drawable.starpink);
                ratejeff3.setImageResource(R.drawable.starpink);
                ratejeff4.setImageResource(R.drawable.starblue);
                ratejeff5.setImageResource(R.drawable.starblue);
            }
        });

        ratejeff4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valoration.setRateJeff(4);
                ratejeff1.setImageResource(R.drawable.starpink);
                ratejeff2.setImageResource(R.drawable.starpink);
                ratejeff3.setImageResource(R.drawable.starpink);
                ratejeff4.setImageResource(R.drawable.starpink);
                ratejeff5.setImageResource(R.drawable.starblue);
            }
        });

        ratejeff5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valoration.setRateJeff(5);
                ratejeff1.setImageResource(R.drawable.starpink);
                ratejeff2.setImageResource(R.drawable.starpink);
                ratejeff3.setImageResource(R.drawable.starpink);
                ratejeff4.setImageResource(R.drawable.starpink);
                ratejeff5.setImageResource(R.drawable.starpink);
            }
        });
    }

    private void checkMedia(RelativeLayout panel, MediaType mediaType) {
        valoration.setMedia(mediaType);
        diselectedAllMedia();
        selected(panel);
    }


    private void diselectedAllMedia() {
        diselected(mediatv);
        diselected(mediaface);
        diselected(mediainst);
        diselected(mediafri);
        diselected(mediagoo);
        diselected(mediaoth);
    }

    private void initEventAgain() {
        returnyes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAgain(returnyes, Boolean.TRUE);
            }
        });


        returnno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAgain(returnno, Boolean.FALSE);
            }
        });

    }

    private void checkAgain(RelativeLayout panel, Boolean again) {
        valoration.setUsedAgain(again);
        diselectedAllAgain();
        selected(panel);
    }

    private void diselectedAllAgain() {
        diselected(returnyes);
        diselected(returnno);
    }

    private void initEventRecomended() {
        friendyes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkFriend(friendyes, Boolean.TRUE);
            }
        });


        friendno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkFriend(friendno, Boolean.FALSE);
            }
        });

    }

    private void checkFriend(RelativeLayout panel, Boolean recommended) {
        valoration.setRecomended(recommended);
        diselectedAllRecomended();
        selected(panel);
    }

    private void diselectedAllRecomended() {
        diselected(friendyes);
        diselected(friendno);
    }


    private void selected(RelativeLayout panel) {
        if (panel != null && panel.getChildCount() > 0) {
            for (int i = 0; i < panel.getChildCount(); i++) {
                if (panel.getChildAt(i) instanceof ImageView) {
                    ((ImageView) panel.getChildAt(i)).setImageResource(R.drawable.checkblue);
                }
            }

        }
    }

    private void diselected(RelativeLayout panel) {
        if (panel != null && panel.getChildCount() > 0) {
            for (int i = 0; i < panel.getChildCount(); i++) {
                if (panel.getChildAt(i) instanceof ImageView) {
                    ((ImageView) panel.getChildAt(i)).setImageResource(R.drawable.circleblue);
                }
            }

        }
    }

    public void showPanel(String idWoocommerce) {
        isShow = true;
        valoration.getOrder().setIdWoocommerce(idWoocommerce);
        valoration.setRateClothes(null);
        valoration.setRateJeff(null);
        valoration.setUsedAgain(null);
        valoration.setMedia(null);
        valoration.setRecomended(null);
        valoration.setNotes(null);
        diselectedAllMedia();
        diselectedAllAgain();
        diselectedAllRecomended();
        ratejeff1.setImageResource(R.drawable.starblue);
        ratejeff2.setImageResource(R.drawable.starblue);
        ratejeff3.setImageResource(R.drawable.starblue);
        ratejeff4.setImageResource(R.drawable.starblue);
        ratejeff5.setImageResource(R.drawable.starblue);
        rateclo1.setImageResource(R.drawable.starblue);
        rateclo2.setImageResource(R.drawable.starblue);
        rateclo3.setImageResource(R.drawable.starblue);
        rateclo4.setImageResource(R.drawable.starblue);
        rateclo5.setImageResource(R.drawable.starblue);
        note.setText(null);

        parent.startAnimation(bottomUp);
        parent.setVisibility(View.VISIBLE);
    }


    public void hiddenPanel() {
        isShow = false;
        parent.startAnimation(bottomDown);
        parent.setVisibility(View.GONE);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        if(!isShow){
            parent.setBackgroundResource(android.R.color.transparent);
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if(isShow){
            parent.setBackgroundResource(R.color.colorSecondaryTrans);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

    @Override
    public void update(Observable observable, Object data) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        Thread threadNotiy = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        new AlertDialog.Builder(new ContextThemeWrapper(context.getActivity(), R.style.Theme_AppCompat_Light)).setTitle(R.string.empty).setMessage(context.getResources().getString(R.string.ratethanks)).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                hiddenPanel();
                                context.setValoration();
                            }
                        }).show();
                    }
                });
            }
        });

        threadNotiy.start();
    }
}
