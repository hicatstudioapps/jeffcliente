package es.coiasoft.mrjeff.view.components;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import es.coiasoft.mrjeff.view.listener.ScrollViewListenerMrJeff;

/**
 * Created by Paulino on 06/09/2016.
 */
public class RecyclerViewMrJeff extends RecyclerView {

    private ScrollViewListenerMrJeff listener;

    public RecyclerViewMrJeff(Context context) {
        super(context);
    }

    public RecyclerViewMrJeff(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RecyclerViewMrJeff(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setListener(ScrollViewListenerMrJeff listener){
        this.listener = listener;
    }

    @Override
    protected void onScrollChanged(int x, int y, int oldx, int oldy) {
        super.onScrollChanged(x, y, oldx, oldy);

        if(listener != null){
            listener.onScrollChangedCoiasoft(x,y,oldx,oldy);

        }

    }
}
