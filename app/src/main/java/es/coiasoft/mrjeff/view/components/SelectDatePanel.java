package es.coiasoft.mrjeff.view.components;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.MrJeffApplication;
import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;
import es.coiasoft.mrjeff.domain.storage.DateTimeTableStorage;
import es.coiasoft.mrjeff.manager.worker.SearchTimeTableWorker;
import es.coiasoft.mrjeff.view.adapter.ListAdapterDay;
import es.coiasoft.mrjeff.view.adapter.ListAdapterHour;
import es.coiasoft.mrjeff.view.components.util.SelectPanelListener;

/**
 * Created by Paulino on 11/05/2016.
 */
public class SelectDatePanel implements Animation.AnimationListener, Observer{

    private ListView month;
    private ListView hours;
    private TextView title;
    private TextView button;
    private RelativeLayout parent;
    private View activity;
    private Animation bottomUp;
    private Animation bottomDown;
    private boolean show = false;
    private String cp;
    private int type;
    private SelectPanelListener listener;
    private String dateReference;
    private String date;
    private String hour;
    private String titlePanelText;
    private boolean load = false;
    private final Handler handler = new Handler();
    private ProgressDialogMrJeff progressDialogMrJeff;
    private boolean change = false;
    private boolean isChange = false;
    private Integer minDays = 0;


    public ListView getMonth() {
        return month;
    }

    public void setMonth(ListView month) {
        this.month = month;
    }

    public ListView getHours() {
        return hours;
    }

    public void setHours(ListView hours) {
        this.hours = hours;
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }

    public TextView getButton() {
        return button;
    }

    public void setButton(TextView button) {
        this.button = button;
    }

    public RelativeLayout getParent() {
        return parent;
    }

    public void setParent(RelativeLayout parent) {
        this.parent = parent;
    }

    public View getActivity() {
        return activity;
    }

    public void setActivity(View activity) {
        this.activity = activity;
    }

    public SelectDatePanel(View activity,SelectPanelListener listener) {
        this(activity,listener,false);
    }

    public SelectDatePanel(View activity,SelectPanelListener listener,boolean isChange) {
        this.activity = activity;
        this.listener = listener;
        this.isChange = isChange;
        initComponents();
        initEvent();
    }


    private void initComponents(){
        month = (ListView)activity.findViewById(R.id.listDates);
        hours = (ListView)activity.findViewById(R.id.listHours);
        title = (TextView)activity.findViewById(R.id.titlePanelDates);
        button = (TextView)activity.findViewById(R.id.btnPanelDates);
        parent = (RelativeLayout)activity.findViewById(R.id.panelSelectDate);
        bottomUp = AnimationUtils.loadAnimation(activity.getContext(), R.anim.bottom_up);
        bottomUp.setAnimationListener(this);
        bottomDown= AnimationUtils.loadAnimation(activity.getContext(), R.anim.bottom_down);
        bottomDown.setAnimationListener(this);
    }


    private void initEvent(){
        parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hiddenPanel();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(change){
                    listener.notifyChanges();
                }
                hiddenPanel();
                putHours();
            }
        });

        month.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String dateSelect = (String)month.getAdapter().getItem(position);
                eventSelectionDate(position,dateSelect,null);
                change = true;
            }
        });

        hours.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((ListAdapterHour)hours.getAdapter()).setPositionSelected(position);
                ((ListAdapterHour)hours.getAdapter()).notifyDataSetChanged();
                change = true;
            }
        });
    }

    private void eventSelectionDate(final int position,String dateSelection,String hour){
        month.setSelection(position);
        month.post(new Runnable() {
            @Override
            public void run() {
                month.smoothScrollToPosition(position);
            }
        });


        String[] hoursValid = DateTimeTableStorage.getInstance().getValidHour(cp,type,dateSelection);
        if(hoursValid != null && hoursValid.length > 0) {
            List<String> hoursArray = Arrays.asList(hoursValid);
            ((ListAdapterDay) month.getAdapter()).setPositionSelected(position);
            ((ListAdapterDay) month.getAdapter()).notifyDataSetChanged();

            hours.setAdapter(new ListAdapterHour(activity.getContext(), new ArrayList<String>(hoursArray), hours));
            int pos = 0;
            if (hour != null) {
                pos = hoursArray .indexOf(hour);
                pos = pos < 0 ? 0 : pos;
            }
            ((ListAdapterHour) hours.getAdapter()).setPositionSelected(pos);
            ((ListAdapterHour) hours.getAdapter()).notifyDataSetChanged();
            final int positionHour = pos;
            hours.setSelection(position);
            hours.post(new Runnable() {
                @Override
                public void run() {
                    hours.smoothScrollToPosition(positionHour);
                }
            });
        }
    }

    private void  showPanel(){
        show = true;
        parent.startAnimation(bottomUp);
        parent.setVisibility(View.VISIBLE);
    }


    private void putHours(){
        if(month != null && month.getAdapter() != null) {
            String dateSelected = (String) month.getAdapter().getItem(((ListAdapterDay) month.getAdapter()).getPositionSelected());
            String selected = dateSelected + " " + (String) hours.getAdapter().getItem(((ListAdapterHour) hours.getAdapter()).getPositionSelected());
            try {
                if (type == MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT) {
                    MrJeffApplication.entregaReference="";
                    selectPickUp(selected, dateSelected);
                } else {
                    selectDelivery(selected);
                }
            } catch (ParseException e) {
                if (type == MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT) {
                    listener.putHoursPickUp();
                } else {
                    listener.putHoursDelivery(Calendar.getInstance(), false);
                }
            }
        } else {
            if (type == MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT) {
                listener.putHoursPickUp();
            } else {
                listener.putHoursDelivery(Calendar.getInstance(), false);
            }
        }
    }
    private void  hiddenPanel(){
        show = false;
        parent.startAnimation(bottomDown);
        parent.setVisibility(View.GONE);
    }

    private void selectPickUp(String selected, String date) throws ParseException{
        listener.paintHourDatePickUp(selected);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(format.parse(date));
        listener.putHoursDelivery(calendar,true);
        if(!isChange) {
            new LocalyticsEventActionMrJeff(activity.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.DATEBILLING, false, selected);
        } else {
            new LocalyticsEventActionMrJeff(activity.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.UPDATE_PICKUP_ORDER,Boolean.FALSE,"Correcto",selected);
        }
    }


    private void selectDelivery(String selected) throws ParseException {
        listener.paintHourDateDelivery(selected);
        if(!isChange) {
            new LocalyticsEventActionMrJeff(activity.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.DATESHIPPING, false, selected);
        } else {
            new LocalyticsEventActionMrJeff(activity.getContext(), MrJeffConstant.LOCALYTICS_EVENTS.ACTION.UPDATE_DELIVERY_ORDER,Boolean.FALSE,"Correcto",selected);
        }
    }

    public void showPanelSelectDate(String title, String date, String hour,String cp, int type, String dateReference,Integer minDays){
        this.cp = cp;
        this.type = type;
        this.dateReference = dateReference;
        this.hour = hour;
        this.date = date;
        this.titlePanelText = title;
        this.minDays = minDays;
        if (!DateTimeTableStorage.getInstance().isLoadCp(cp) && !load) {
            load = true;
            progressDialogMrJeff = ProgressDialogMrJeff.createAndStart(activity.getContext(), R.string.loadtimetable);
            SearchTimeTableWorker worker = new SearchTimeTableWorker(cp);
            worker.addObserver(this);
            Thread thread = new Thread(worker);
            thread.start();
        } else {
            ArrayList<String> days = DateTimeTableStorage.getInstance().getDateType(cp, type, dateReference,minDays);
            if (days != null && !days.isEmpty()) {
                removeFirstDay(days);
                if(type==1){
                    int pos= days.indexOf(MrJeffApplication.entregaReference);
                    if(pos==-1)
                        pos=0;
                    List data= days.subList(pos, days.size());
                    days= new ArrayList<>();
                    days.addAll(data);
                }
                month.setAdapter(new ListAdapterDay(activity.getContext(), days, month));
                if (date != null) {
                    int pos = days.indexOf(date);
                    pos = pos < 0 ? 0 : pos;
                    eventSelectionDate(pos, days.get(pos), hour);
                } else {
                    eventSelectionDate(0, days.get(0), hour);
                }
            } else {
            }


            if (title != null) {
                this.title.setText(title);
            }
            showPanel();
        }
    }

    public void showPanelSelectDateDrop(String title, String date, String hour,String cp, int type, String dateReference,Integer minDays){
        this.cp = cp;
        this.type = type;
        this.dateReference = dateReference;
        this.hour = hour;
        this.date = date;
        this.titlePanelText = title;
        this.minDays = minDays;
        if (!DateTimeTableStorage.getInstance().isLoadCp(cp) && !load) {
            load = true;
            progressDialogMrJeff = ProgressDialogMrJeff.createAndStart(activity.getContext(), R.string.loadtimetable);
            SearchTimeTableWorker worker = new SearchTimeTableWorker(cp);
            worker.addObserver(this);
            Thread thread = new Thread(worker);
            thread.start();
        } else {
            ArrayList<String> days = DateTimeTableStorage.getInstance().getDateType(cp, type, dateReference,minDays);
            if (days != null && !days.isEmpty()) {
                removeFirstDay(days);
                if(type==1){
                    int pos= days.indexOf(date);
                    for (int i=0; i <= pos; i++)
                        days.remove(i);
                }
                month.setAdapter(new ListAdapterDay(activity.getContext(), days, month));
                if (date != null) {
                    int pos = days.indexOf(date);
                    pos = pos < 0 ? 0 : pos;
                    eventSelectionDate(pos, days.get(pos), hour);
                } else {
                    eventSelectionDate(0, days.get(0), hour);
                }
            } else {
            }


            if (title != null) {
                this.title.setText(title);
            }
            showPanel();
        }
    }

    private void removeFirstDay(ArrayList<String> days){
        if(days != null && !days.isEmpty()) {
            try {
                Calendar today = Calendar.getInstance();
                String date = days.get(0);
                String[] hoursValid = DateTimeTableStorage.getInstance().getValidHour(cp,type,date);
                if(hoursValid == null && hoursValid.length <= 0) {
                    days.remove(date);
                }
            }catch (Exception e){

            }
        }


    }


    @Override
    public void onAnimationStart(Animation animation) {
        if(!show){
            parent.setBackgroundResource(android.R.color.transparent);
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if(show){
            parent.setBackgroundResource(R.color.backmodal);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void update(Observable observable, Object data) {

        if(progressDialogMrJeff != null && progressDialogMrJeff.isShowing()){
            progressDialogMrJeff.dismiss();
        }

        if(observable instanceof SearchTimeTableWorker){
            Thread threadNotiy = new Thread(new Runnable() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            showPanelSelectDate(titlePanelText, date, hour, cp, type, dateReference,minDays);
                        }
                    });
                }
            });

            threadNotiy.start();
        }

    }
}
