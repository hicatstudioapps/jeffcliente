package es.coiasoft.mrjeff.view.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Paulino on 02/11/2016.
 */
public class ViewPagerMrJeff extends ViewPager {

    private enum SwipeDirection {
        ALL,NONE,RIGHT,LEFT;
    }

    private SwipeDirection direction = SwipeDirection.ALL;
    private float initialXValue;
    private int maxPage = 4;
    private int position = 0;

    public ViewPagerMrJeff(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public int getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }

    public SwipeDirection getDirection() {
        return direction;
    }

    public void setDirection(SwipeDirection direction) {
        this.direction = direction;
    }

    public float getInitialXValue() {
        return initialXValue;
    }

    public void setInitialXValue(float initialXValue) {
        this.initialXValue = initialXValue;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.IsSwipeAllowed(event)) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    private boolean IsSwipeAllowed(MotionEvent event) {

        if(direction == SwipeDirection.NONE )//disable any swipe
            return false;

        if(event.getAction()==MotionEvent.ACTION_DOWN) {
            initialXValue = event.getX();
            return true;
        }

        if(event.getAction()==MotionEvent.ACTION_MOVE) {
            try {
                float diffX = event.getX() - initialXValue;
                if (diffX > 0) {
                    if(direction == SwipeDirection.LEFT || direction == SwipeDirection.ALL ){
                        if(getPosition() == 0){
                            return false;
                        } else{
                            return true;
                        }
                    } else {
                        return false;
                    }
                }else if (diffX < 0) {
                    if(direction == SwipeDirection.RIGHT || direction == SwipeDirection.ALL ){
                        if(getPosition() == maxPage -1){
                            return false;
                        } else{
                            return true;
                        }
                    } else {
                        return false;
                    }
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        return true;
    }

}
