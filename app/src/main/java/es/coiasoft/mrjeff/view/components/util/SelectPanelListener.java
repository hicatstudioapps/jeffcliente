package es.coiasoft.mrjeff.view.components.util;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by Paulino on 16/05/2016.
 */
public interface SelectPanelListener {

    Calendar putHoursPickUp();

    void putHoursDelivery(Calendar calendar, boolean reload);

    Calendar paintHourDatePickUp(String date)  throws ParseException;

    Calendar paintHourDateDelivery(String date) throws ParseException;

    void notifyChanges();

}
