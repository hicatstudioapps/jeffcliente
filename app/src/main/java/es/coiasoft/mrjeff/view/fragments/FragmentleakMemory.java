package es.coiasoft.mrjeff.view.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * Created by Paulino on 12/05/2016.
 */
@SuppressLint("ValidFragment")
public abstract class FragmentleakMemory extends Fragment {

    protected View v;

    public FragmentleakMemory() {
        super();
        setRetainInstance(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            v.destroyDrawingCache();
            v = null;
            Runtime.getRuntime().gc();
            System.gc();
        }catch (Exception e){

        }
    }

    @Override
    public void onPause () {
        if(v != null) {
            Glide.clear(v);
            unbindDrawables(v);
        }
        super.onPause();
        Runtime.getRuntime().gc();
        System.gc();
    }


    public void unbindDrawables(View view) {//pass your parent view here
        try {
            if (view.getBackground() != null)
                view.getBackground().setCallback(null);

            if (view instanceof ImageView) {
                ImageView imageView = (ImageView) view;
                imageView.setImageBitmap(null);
                imageView.destroyDrawingCache();
            } else if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    unbindDrawables(viewGroup.getChildAt(i));

                if (!(view instanceof AdapterView))
                    viewGroup.removeAllViews();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
