package es.coiasoft.mrjeff.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.SupportMapFragment;

import es.coiasoft.mrjeff.OrderAddressActivity;
import es.coiasoft.mrjeff.view.listener.ListenerMap;

/**
 * Created by Paulino on 04/04/2016.
 */
public class SupporMapFragmentDragable extends SupportMapFragment {


    private ListenerMap listenerMap;

    public GoogleMap map;

    public void setMap(GoogleMap map) {
        this.map = map;
    }

    public static SupporMapFragmentDragable newInstanceDrawable(GoogleMapOptions options) {
        SupporMapFragmentDragable var1 = new SupporMapFragmentDragable();
        Bundle var2 = new Bundle();
        var2.putParcelable("MapOptions", options);
        var1.setArguments(var2);
        return var1;
    }

    public ListenerMap getListenerMap() {
        return listenerMap;
    }

    public void setListenerMap(ListenerMap listenerMap) {
        this.listenerMap = listenerMap;
    }

    public SupporMapFragmentDragable(){
        super();
    }

    public View mOriginalContentView;
    public TouchableWrapper mTouchView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        mOriginalContentView = super.onCreateView(inflater, parent, savedInstanceState);
        mTouchView = new TouchableWrapper(getActivity(),this);
        mTouchView.addView(mOriginalContentView);
        return mTouchView;
    }

    @Override
    public View getView() {
        return mOriginalContentView;
    }

    public void setOnDragStarted() {
        if (listenerMap != null)   {
            listenerMap.setOnDragStarted();
        }
    }

    public void setOnDragEnd(GoogleMap gMap){
        if (listenerMap != null)   {
            listenerMap.setOnDragEnd(gMap);
        }
    }


    public  class TouchableWrapper extends FrameLayout {

        private long lastTouched = 0;
        private SupporMapFragmentDragable dragableFragment;

        public TouchableWrapper(Context context,SupporMapFragmentDragable dragableFragment ) {
            super(context);
            this.dragableFragment = dragableFragment;
        }


        @Override
        public boolean dispatchTouchEvent(MotionEvent ev) {
            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    this.dragableFragment.setOnDragStarted();
                    break;
                case MotionEvent.ACTION_UP:
                    this.dragableFragment.setOnDragEnd(map);
                    break;
            }
            return super.dispatchTouchEvent(ev);
        }

    }

}
