package es.coiasoft.mrjeff.view.listener;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import java.util.regex.Pattern;

import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.Utils.event.LocalyticsEventActionMrJeff;

/**
 * Created by linovm on 25/10/15.
 */
public class FocusChangeEditTextListener implements View.OnFocusChangeListener {

    private static final Pattern PATTERN_PHONE_SPAIN9 = Pattern.compile("^9[0-9]{8}$");
    private static final Pattern PATTERN_PHONE_SPAIN8 = Pattern.compile("^8[0-9]{8}$");
    private static final Pattern PATTERN_PHONE_MOBILE = Pattern.compile("^6[0-9]{8}$");
    private final EditText editText;
    private Boolean delivery;
    private Boolean profile;
    private Boolean isSuscription;

    public FocusChangeEditTextListener(EditText editText,Boolean delivery,Boolean isSuscription){
        this(editText,delivery,Boolean.FALSE,isSuscription);
    }

    public FocusChangeEditTextListener(EditText editText,Boolean delivery,Boolean profile,Boolean isSuscription){
        this.editText = editText;
        this.delivery = delivery;
        this.isSuscription = isSuscription;
        this.profile = profile;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        String text = ((EditText)v).getText().toString();
        if(!hasFocus){
            switch (editText.getId()){
                case R.id.billing_address_name:
                    if(!validRequired(text)){
                        new LocalyticsEventActionMrJeff(editText.getContext(),profile?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NAMESPROFILE :delivery?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NAMESHIPPING:MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NAMEBILLING,isSuscription,text,"No valido");
                        editText.setError("Debe cubrir el nombre y debe tener al menos 4 caracteres");
                    } else {
                        editText.setError(null);
                        new LocalyticsEventActionMrJeff(editText.getContext(),profile?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NAMESPROFILE:delivery?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NAMESHIPPING:MrJeffConstant.LOCALYTICS_EVENTS.ACTION.NAMEBILLING,isSuscription,text,"Valido");
                    }
                    break;
                case R.id.billing_address_address:
                    if(!validRequired(text)){
                        new LocalyticsEventActionMrJeff(editText.getContext(),profile?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.ADDRESSPROFILE:delivery?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.ADDRESSSHIPPING:MrJeffConstant.LOCALYTICS_EVENTS.ACTION.ADDRESSBILING,isSuscription,text,"No valido");
                        editText.setError("Debe cubrir la dirección y debe tener al menos 4 caracteres");
                    } else {
                        editText.setError(null);
                        new LocalyticsEventActionMrJeff(editText.getContext(),profile?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.ADDRESSPROFILE:delivery?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.ADDRESSSHIPPING:MrJeffConstant.LOCALYTICS_EVENTS.ACTION.ADDRESSBILING,isSuscription,text,"Valido");
                    }
                    break;
                case R.id.billing_address_phone:
                    if(!validPhone(text)){
                        new LocalyticsEventActionMrJeff(editText.getContext(),profile?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.PHONEPROFILE :delivery?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.PHONEPSHIPPING:MrJeffConstant.LOCALYTICS_EVENTS.ACTION.PHONEBILLING,isSuscription,text,"No valido");
                        editText.setError("El número de teléfono no tiene un formato valido");
                    } else {
                        new LocalyticsEventActionMrJeff(editText.getContext(),profile?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.PHONEPROFILE :delivery?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.PHONEPSHIPPING:MrJeffConstant.LOCALYTICS_EVENTS.ACTION.PHONEBILLING,isSuscription,text,"Valido");
                        editText.setError(null);
                    }
                    break;
                case R.id.billing_address_cp:
                    if (!validRequiredMin(5, text)){
                        new LocalyticsEventActionMrJeff(editText.getContext(),profile?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CPPROFILE:delivery?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CPSHIPPING:MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CPBILLING,isSuscription,text,"No Valido");
                        editText.setError("El código postal no tiene un formato valido");
                    } else {
                        new LocalyticsEventActionMrJeff(editText.getContext(),profile?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CPPROFILE:delivery?MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CPSHIPPING:MrJeffConstant.LOCALYTICS_EVENTS.ACTION.CPBILLING,isSuscription,text,"Valido");
                        editText.setError(null);
                    }
                    break;
            }
        }
    }


    public static  boolean validRequired(String text){
        boolean result = true;
        if( text == null || text.isEmpty() || text.length() < 4){
           result = false;
        }

        return result;
    }


    public static  boolean validRequiredMin(int lengthMin, String text){
        boolean result = true;
        if(!validRequired(text) || text.length() < lengthMin){
            result = false;
        }
        return result;
    }

    public static boolean validPhone(String text){
        boolean result = false;
        if (TextUtils.isEmpty(text)) {
            return false;
        } else if(PATTERN_PHONE_SPAIN9.matcher(text).matches() ||
                PATTERN_PHONE_SPAIN8.matcher(text).matches() ||
                PATTERN_PHONE_MOBILE.matcher(text).matches()){
            result = true;
        }

        return result;

    }


    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

}
