package es.coiasoft.mrjeff.view.listener;

import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Pattern;

import es.coiasoft.mrjeff.R;
import es.coiasoft.mrjeff.Utils.MrJeffConstant;

/**
 * Created by linovm on 25/10/15.
 */
public class FocusChangeNewUserEditTextListener implements View.OnFocusChangeListener {

    private final EditText editText;
    private final EditText editTextAux;

    public FocusChangeNewUserEditTextListener(EditText editText,EditText editTextAux){
        this.editText = editText;
        this.editTextAux = editTextAux;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        String text = ((EditText)v).getText().toString();
        if(!hasFocus){
            switch (editText.getId()){
                case R.id.user_email:
                    if(!isValidEmail(text)){
                        editText.setError(getHtmlError(R.string.errorformatmail));
                    } else {
                        editText.setError(null);
                    }
                    break;
                case R.id.user_firstname:
                    if(!validRequired(text)){
                        editText.setError(getHtmlError(R.string.errorformatname));
                    } else {
                        editText.setError(null);
                    }
                    break;
                case R.id.user_pass:
                    if(!validRequiredMin(6, text)) {
                        editText.setError(getHtmlError(R.string.errorformatpassword));
                    } else {
                        editText.setError(null);
                    }
                    break;
                case R.id.user_repass:
                    if(!isEqual(text, editTextAux.getText().toString())) {
                        editText.setError(getHtmlError(R.string.errorformatrepass));
                    } else {
                        editText.setError(null);
                    }
                    break;
            }
        }
    }

    private Spanned getHtmlError(int resource){
        String textError = editText.getContext().getResources().getString(resource);
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(editText.getContext().getResources().getColor(R.color.redmrjeff));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(textError);
        ssbuilder.setSpan(fgcspan, 0, textError.length(), 0);

        return ssbuilder;

    }


    public static boolean isEqual(String source, String target){
        boolean result = false;
        if(source != null && target != null
                && source.equals(target)){
            result = true;
        }

        return result;
    }

    public static  boolean validRequired(String text){
        boolean result = true;
        if( text == null || text.isEmpty() || text.length() < 2){
           result = false;
        }

        return result;
    }


    public static  boolean validRequiredMin(int lengthMin, String text){
        boolean result = true;
        if(!validRequired(text) || text.length() < lengthMin){
            result = false;
        }

        return result;
    }



    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

}
