package es.coiasoft.mrjeff.view.listener;

import android.view.View;

import com.google.android.gms.maps.GoogleMap;

/**
 * Created by Paulino on 05/11/2016.
 */

public interface ListenerMap {

    void setOnDragStarted();

    void setOnDragEnd(GoogleMap gMap);
}
