package es.coiasoft.mrjeff.view.listener;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import es.coiasoft.mrjeff.AddressActivity;
import es.coiasoft.mrjeff.domain.storage.LocationStorage;

/**
 * Created by Paulino on 07/03/2016.
 */
public class LocationAddressListener implements LocationListener{


    @Override
    public void onLocationChanged(Location loc) {
        loc.getLatitude();
        loc.getLongitude();
        LocationStorage.getInstance().setLastLocation(loc);
    }

    @Override
    public void onProviderDisabled(String provider) {
        LocationStorage.getInstance().setLocationActive(Boolean.FALSE);
    }

    @Override
    public void onProviderEnabled(String provider) {
        LocationStorage.getInstance().setLocationActive(Boolean.TRUE);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

}
