package es.coiasoft.mrjeff.view.listener;

/**
 * Created by Paulino on 10/03/2016.
 */
public interface OrderAdressFragmentListener {

    void onClickSearchAddressBilling();

    void onClickSearchAddressShipping();

    void onClickOkAddress();

    void onClickCancelChangeAddress();


    void onClickGoTohour();

    void setTitlePageOne();

    void setTitlePageTwo();


}
