package es.coiasoft.mrjeff.view.listener;

/**
 * Created by Paulino on 13/09/2016.
 */
public interface ProductAddRemoveListener {

    void addProduct(Integer idProduct);
    void removeProduct(Integer idProduct);
}
