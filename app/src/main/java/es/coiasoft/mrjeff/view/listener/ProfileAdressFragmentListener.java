package es.coiasoft.mrjeff.view.listener;

/**
 * Created by Paulino on 10/03/2016.
 */
public interface ProfileAdressFragmentListener {

    void onClickSearchAddress();

    void onClickOkAddress();

    void onClickGoToEditAddress();

    void onClickCancelChangeAddress();

    void onClickOkEdit();

    void setTitlePageOne();

    void setTitlePageTwo();


}
