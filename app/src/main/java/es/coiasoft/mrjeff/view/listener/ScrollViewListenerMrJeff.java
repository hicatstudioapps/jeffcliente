package es.coiasoft.mrjeff.view.listener;

/**
 * Created by ropasgo on 31/8/16.
 */
public interface ScrollViewListenerMrJeff {

    void onScrollChangedCoiasoft(int x, int y, int oldx, int oldy);
}
