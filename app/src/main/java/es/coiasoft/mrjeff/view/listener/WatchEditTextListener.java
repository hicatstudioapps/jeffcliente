package es.coiasoft.mrjeff.view.listener;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Created by linovm on 25/10/15.
 */
public class WatchEditTextListener implements TextWatcher {

    private final Integer type;
    private final EditText editText;

    public WatchEditTextListener(Integer type, EditText editText){
        this.type = type;
        this.editText = editText;
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            editText.setError(null);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        String text = s.toString();

        if(text == null || text.isEmpty() || text.length() < 4){
            editText.setError("Debe cubrir este campo");
        }

    }


}
