package es.coiasoft.mrjeff.view.util;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.EnumStateOrder;
import es.coiasoft.mrjeff.domain.orders.response.Orders;
import es.coiasoft.mrjeff.domain.storage.DateTimeTableStorage;

/**
 * Created by Paulino on 16/05/2016.
 */
public class FindStateOrder {

    public static EnumStateOrder getStateOrder(Orders order) {
        EnumStateOrder result = null;
        try {
            Calendar today = Calendar.getInstance();
            today.setTimeInMillis(today.getTimeInMillis() + DateTimeTableStorage.getInstance().getDifference());

            if (order.getOrder_meta() != null &&
                    order.getOrder_meta().getMyfield2() != null &&
                    order.getOrder_meta().getMyfield3() != null &&
                    order.getOrder_meta().getMyfield4() != null &&
                    order.getOrder_meta().getMyfield5() != null) {
                result = compareWithMetaData(today, order);
            } else if(order.getBilling_address().getAddress_2() != null &&
                    order.getBilling_address().getAddress_2().contains("/") &&
                    order.getShipping_address().getAddress_2() != null &&
                    order.getShipping_address().getAddress_2().contains("/")){
                result = compareWithAddress2Android(today,order);
            } else if(order.getBilling_address().getAddress_2() != null &&
                    order.getBilling_address().getAddress_2().contains("-") &&
                    order.getShipping_address().getAddress_2() != null &&
                    order.getShipping_address().getAddress_2().contains("-")){
                result = compareWithAddress2(today,order);
            }
        }catch (Exception e){
            Log.e("ERROR", "Error calculando el estado");
        }

        return result;
    }


    private static EnumStateOrder compareWithMetaData(Calendar today, Orders order) throws Exception{
        String datePickUp = order.getOrder_meta().getMyfield2() +" "+ order.getOrder_meta().getMyfield3().split(" - ")[0];
        String dateDelivery = order.getOrder_meta().getMyfield4() +" "+ order.getOrder_meta().getMyfield5().split(" - ")[0];
        return compareDate(today,datePickUp,dateDelivery);
    }


    private static EnumStateOrder compareWithAddress2Android(Calendar today, Orders order) throws Exception{
        String datePickUp = convertAddress2(order.getBilling_address().getAddress_2());
        String dateDelivery = convertAddress2(order.getShipping_address().getAddress_2());
        return compareDate(today,datePickUp,dateDelivery);
    }

    private static EnumStateOrder compareWithAddress2(Calendar today, Orders order) throws Exception{
        SimpleDateFormat formatSource = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        SimpleDateFormat formatTarget = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        String datePickUp = order.getBilling_address().getAddress_2().replace(" - ","-");
        String dateDelivery = order.getShipping_address().getAddress_2().replace(" - ","-");
        try {
            datePickUp = formatTarget.format(formatSource.parse(datePickUp));
            dateDelivery = formatTarget.format(formatSource.parse(dateDelivery));
        }catch (ParseException e){
            datePickUp = order.getBilling_address().getAddress_2().replace(" - ","-");
            dateDelivery = order.getShipping_address().getAddress_2().replace(" - ","-");
        }
        return compareDate(today,datePickUp,dateDelivery);
    }

    private static String convertAddress2(String address2){
        String result = "";
        String [] splitDate = address2.split(" ");
        result = splitDate[0].replace("/","-") + " " + splitDate[1];
        return result;
    }


    public static EnumStateOrder compareDate(Calendar today, String datePickup, String dateDelivery) throws ParseException {
        EnumStateOrder result = null;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm");

        Calendar pickUp = Calendar.getInstance();
        pickUp.setTime(format.parse(datePickup));

        Calendar delivery = Calendar.getInstance();
        delivery.setTime(format.parse(dateDelivery));


        if( (today.getTimeInMillis() - delivery.getTimeInMillis())  >  MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY  ){
            result = EnumStateOrder.FINALLY;
        } else if((delivery.getTimeInMillis() - today.getTimeInMillis()) <  MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY){
            result = EnumStateOrder.DELIVERY;
        } else if((delivery.getTimeInMillis() - today.getTimeInMillis()) > MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY    &&
                (today.getTimeInMillis() - pickUp.getTimeInMillis())  >  MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_TWO_HOUR){
            result = EnumStateOrder.WASHING;
        }  else if((today.getTimeInMillis() - pickUp.getTimeInMillis()) < MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_TWO_HOUR    &&
                (pickUp.getTimeInMillis() - today.getTimeInMillis())  <  MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY){
            result = EnumStateOrder.PICKUP;
        } else {
            result = EnumStateOrder.CONFIRM;
        }

        return result;


    }

}
