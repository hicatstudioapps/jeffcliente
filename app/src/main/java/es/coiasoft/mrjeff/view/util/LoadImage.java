package es.coiasoft.mrjeff.view.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.common.util.UriUtil;
import com.facebook.drawee.view.SimpleDraweeView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;
import java.math.BigInteger;

import es.coiasoft.mrjeff.domain.storage.MrJeffImageStorage;
import es.coiasoft.mrjeff.manager.worker.DiskLruImageCache;

/**
 * Created by Paulino on 10/05/2016.
 */
public class LoadImage {

    private String imageUrl;
    public SimpleDraweeView image;
    public FileAsyncHttpResponseHandler fileHandler;
    public Context context;

    public LoadImage(String imageUrl, SimpleDraweeView image, Context context) {
        this.imageUrl = imageUrl;
        this.image = image;
        this.context = context;
        setImage();
    }

    public void setImage() {
        if(MrJeffImageStorage.images.get(toHex(this.imageUrl)) != null){
            Glide.with(context)
                    .load(MrJeffImageStorage.images.get(toHex(this.imageUrl)).intValue())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(image);
            Uri uri = new Uri.Builder()
                    .scheme(UriUtil.LOCAL_RESOURCE_SCHEME) // "res"
                    .path(String.valueOf((MrJeffImageStorage.images.get(toHex(this.imageUrl)).intValue())))
                    .build();
            image.setImageURI(uri);
        } else {
            image.setImageURI(imageUrl);
            Log.d("IMAGE", "Donwload " + imageUrl);
        }
    }

    private String toHex(String arg) {
        String result = String.format("%040x", new BigInteger(1, arg.getBytes(/*YOUR_CHARSET?*/)));
        int pos = result.length()- 60 > 0 ? result.length()- 60: 0;
        result = result.substring(pos);
        Log.d("IAMGE",result + ";" + arg);
        return  result;
    }
}
