package es.coiasoft.mrjeff.view.validator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;

/**
 * Created by Paulino on 10/03/2016.
 */
public class DateTimeMrJeff {

    public static final int ENABLED_TYPE_ADDRESS_HOME = 1;
    public static final int ENABLED_TYPE_ADDRESS_OFFICE = 2;
    public static final int ENABLED_TYPE_ADDRESS_BOTH = 3;
    public static final int ENABLED_TYPE_ANY = -1;

    public static final int ENABLED_SERVICE_ANY = -1;

    private int dayOfWeek;
    private int enabledType = -1;
    private int enabledService = -1;
    private List<String> hoursHome;
    private List<String> hoursOffice;
    private String hourCloseOrders;

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public int getEnabledType() {
        return enabledType;
    }

    public void setEnabledType(int enabledType) {
        this.enabledType = enabledType;
    }

    public int getEnabledService() {
        return enabledService;
    }

    public void setEnabledService(int enabledService) {
        this.enabledService = enabledService;
    }

    public List<String> getHoursHome() {
        return hoursHome;
    }

    public void setHoursHome(List<String> hoursHome) {
        this.hoursHome = hoursHome;
    }

    public List<String> getHoursOffice() {
        return hoursOffice;
    }

    public void setHoursOffice(List<String> hoursOffice) {
        this.hoursOffice = hoursOffice;
    }

    public String getHourCloseOrders() {
        return hourCloseOrders;
    }

    public void setHourCloseOrders(String hourCloseOrders) {
        this.hourCloseOrders = hourCloseOrders;
    }



    public static class Builder{

        public Builder(){

        }


        public Map<Integer,DateTimeMrJeff> generateMap(){
            Map<Integer,DateTimeMrJeff> days = new HashMap<Integer,DateTimeMrJeff>();
            days.put(Calendar.MONDAY, generateDayWeek(Calendar.MONDAY));
            days.put(Calendar.TUESDAY, generateDayWeek(Calendar.TUESDAY));
            days.put(Calendar.WEDNESDAY, generateDayWeek(Calendar.WEDNESDAY));
            days.put(Calendar.THURSDAY, generateDayWeek(Calendar.THURSDAY));
            days.put(Calendar.FRIDAY, generateDayFriday());
            days.put(Calendar.SATURDAY, generateDaySaturday());
            days.put(Calendar.SUNDAY, generateDaySunday());
            return days;
        }

        private DateTimeMrJeff generateDayWeek(int day){
            DateTimeMrJeff result = new DateTimeMrJeff();
            result.dayOfWeek= day;
            result.enabledService = MrJeffConstant.RESULT_SERVICE_DATE_INVALID.BOTH;
            result.enabledType = ENABLED_TYPE_ADDRESS_BOTH;
            result.hourCloseOrders = "17:00";
            result.hoursHome = hoursOfDayHome();
            result.hoursOffice = hoursOfDayOffice();

            return result;
        }

        private DateTimeMrJeff generateDaySaturday(){
            DateTimeMrJeff result = new DateTimeMrJeff();
            result.dayOfWeek= Calendar.SATURDAY;
            result.enabledService = MrJeffConstant.RESULT_SERVICE_DATE_INVALID.BOTH;
            result.enabledType = ENABLED_TYPE_ADDRESS_HOME;
            result.hourCloseOrders = "00:00";
            result.hoursHome = hoursOfSaturdayHome();
            result.hoursOffice = hoursOfDayOffice();

            return result;
        }

        private DateTimeMrJeff generateDaySunday(){
            DateTimeMrJeff result = new DateTimeMrJeff();
            result.dayOfWeek= Calendar.SUNDAY;
            result.enabledService = MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT;
            result.enabledType = ENABLED_TYPE_ADDRESS_HOME;
            result.hourCloseOrders = "17:00";
            result.hoursHome = hoursOfDayHome();
            result.hoursOffice = hoursOfDayOffice();

            return result;
        }

        private DateTimeMrJeff generateDayFriday(){
            DateTimeMrJeff result = new DateTimeMrJeff();
            result.dayOfWeek= Calendar.FRIDAY;
            result.enabledService = MrJeffConstant.RESULT_SERVICE_DATE_INVALID.BOTH;
            result.enabledType = ENABLED_TYPE_ADDRESS_BOTH;
            result.hourCloseOrders = "17:00";
            result.hoursHome = hoursOfDayHome();
            result.hoursOffice = hoursOfDayOffice();

            return result;
        }

        private List<String> hoursOfDayHome(){
            List<String> hours= new ArrayList<String>();
            hours.add("20:00");
            hours.add("21:00");

            return hours;
        }

        private List<String> hoursOfDayOffice(){
            List<String> hours= new ArrayList<String>();
            hours.add("14:00");
            hours.add("15:00");

            return hours;
        }

        private List<String> hoursOfSaturdayHome(){
            List<String> hours= new ArrayList<String>();
            hours.add("10:00");
            hours.add("11:00");

            return hours;
        }



    }
}
