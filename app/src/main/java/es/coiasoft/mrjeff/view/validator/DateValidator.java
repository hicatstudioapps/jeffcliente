package es.coiasoft.mrjeff.view.validator;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import es.coiasoft.mrjeff.Utils.MrJeffConstant;
import es.coiasoft.mrjeff.domain.Attributes;
import es.coiasoft.mrjeff.domain.Products;
import es.coiasoft.mrjeff.domain.storage.OrderStorage;
import es.coiasoft.mrjeff.domain.storage.ProductStorage;

/**
 * Created by Paulino on 10/03/2016.
 */
public class DateValidator extends Observable implements Runnable {


    private String date;
    private int type;
    private String cp;
    private String dateAux;
    private int typeDate;

    @Override
    public void run() {
        boolean result = isValid();
        setChanged();
        notifyObservers(result);
    }

    private boolean isValid(){
        boolean result = false;
        try {
            String pattern = "dd/MM/yyyy HH:mm";
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateFormat.parse(date));

            result = isNotLate(calendar);
            if(result) {
                if (type == MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT) {
                    result = isValidPickUp(calendar);
                } else {
                    result = isValidDelivery(calendar);
                }
            }

        }catch (Exception e){
            Log.e("VALID_DATE","Error validando la fecha");
        }

        return result;
    }

    private boolean isValidDelivery(Calendar calendar){
        boolean result = false;
            try {
                String pattern = "dd/MM/yyyy HH:mm";
                SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
                Calendar dateAuxCalendar = Calendar.getInstance();
                dateAuxCalendar.setTime(dateFormat.parse(dateAux));

                long timeMin = dateAuxCalendar.getTimeInMillis() + (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * (getMinDays()+1));

                calendar.set(Calendar.HOUR_OF_DAY,23);
                calendar.set(Calendar.HOUR_OF_DAY,59);

                result = timeMin <= calendar.getTimeInMillis();

                if (result) {
                    result = isValidServiceToDay(calendar);
                }

                if (result) {
                    result = isValidSTypeToDay(calendar);
                }

                if(result) {
                   // result = DateInvalidStorage.getInstance().isValidDate(cp, type, calendar.getTimeInMillis());
                }


            }catch (Exception e){

            }

        return  result;
    }

    private Boolean isNotLate(Calendar calendar){

        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY,0);
        today.set(Calendar.MINUTE,0);

        return calendar.getTimeInMillis() >= today.getTimeInMillis();


    }

    private Integer getMinDays(){
        Integer result = MrJeffConstant.ORDER_DAYS_SEND.MIN_DAYS;

        try {
            if(OrderStorage.getInstance(null).getOrder() != null && OrderStorage.getInstance(null).getOrder().getLine_items() != null){
                for(es.coiasoft.mrjeff.domain.orders.response.Line_items item: OrderStorage.getInstance(null).getOrder().getLine_items()){
                    Integer idProduct = item.getProduct_id();
                    Products product = ProductStorage.getInstance(null).getProduct(idProduct);
                    if(product.getAttributes()!= null){
                        for(Attributes attr: product.getAttributes()){
                            Integer days = 0;
                            if(attr.getName().trim().toLowerCase().equals(MrJeffConstant.ORDER_DAYS_SEND.ATTR_DAYS.trim().toLowerCase())){
                                days = Integer.valueOf(attr.getOptions().get(0));
                                if(result.compareTo(days) < 0 ){
                                    result = days;
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }catch (Exception e) {

        }


        return result;
    }

    private boolean isValidPickUp(Calendar calendar){
        boolean result = false;

        Calendar today = Calendar.getInstance();

        if(calendar.get(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH) &&
                calendar.get(Calendar.MONTH) == today.get(Calendar.MONTH) &&
                calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR)){
            if(type == DateTimeMrJeff.ENABLED_TYPE_ADDRESS_OFFICE){
                result = false;
            } else {
                result = isValidToday();
            }
        } else {
            result = true;
        }


        if(result) {
            result = isValidServiceToDay(calendar);
        }

        if(result){
            result = isValidSTypeToDay(calendar);
        }

        if(result)  {
           // result = DateInvalidStorage.getInstance().isValidDate(cp, type, calendar.getTimeInMillis());
        }

        return  result;
    }

    public boolean isValidServiceToDay(Calendar calendar){
        boolean result = false;
        Map<Integer,DateTimeMrJeff> times = new DateTimeMrJeff.Builder().generateMap();

        DateTimeMrJeff dateTime = times.get(calendar.get(Calendar.DAY_OF_WEEK));
        if(dateTime != null){
            result = dateTime.getEnabledService() == MrJeffConstant.RESULT_SERVICE_DATE_INVALID.BOTH ||  dateTime.getEnabledService() == type;
        }
        return result;
    }

    public boolean isValidSTypeToDay( Calendar calendar){
        boolean result = false;
        Map<Integer,DateTimeMrJeff> times = new DateTimeMrJeff.Builder().generateMap();

        DateTimeMrJeff dateTime = times.get(calendar.get(Calendar.DAY_OF_WEEK));
        if(dateTime != null){
            result = dateTime.getEnabledType() == DateTimeMrJeff.ENABLED_TYPE_ADDRESS_BOTH ||  dateTime.getEnabledService() == typeDate;
        }
        return result;
    }

    public boolean isValidToday(){
        boolean result = false;
        Map<Integer,DateTimeMrJeff> times = new DateTimeMrJeff.Builder().generateMap();

        DateTimeMrJeff dateTime = times.get(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
        if(dateTime != null){
            String hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + ":" + Calendar.getInstance().get(Calendar.MINUTE);
            result = hour.compareTo(dateTime.getHourCloseOrders()) < 0;
        }
        return result;
    }

    public void isValidDate(Observer observer,String date, int type, String cp, int typeDate, String dateAux){
        this.date = date;
        this.type = type;
        this.cp = cp;
        this.typeDate = typeDate;
        this.dateAux = dateAux;

        addObserver(observer);

        Thread thread = new Thread(this);
        thread.start();
    }

    public boolean isValidDateSync(String date, int type, String cp, int typeDate, String dateAux){
        this.date = date;
        this.type = type;
        this.cp = cp;
        this.typeDate = typeDate;
        this.dateAux = dateAux;

        return isValid();
    }


    public String minDatePickUp(String cp){
        Calendar today = Calendar.getInstance();
        String pattern = "dd/MM/yyyy HH:mm";
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

        date = dateFormat.format(today.getTime());
        this.type =  MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_COLLECT ;
        this.cp = cp;
        this.typeDate = DateTimeMrJeff.ENABLED_TYPE_ADDRESS_HOME;
        boolean valid = false;
        do {
            valid = isValid();
            if(!valid) {
                long time = today.getTimeInMillis() + MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY;
                today.setTimeInMillis(time);
                today.set(Calendar.HOUR_OF_DAY, 20);
                today.set(Calendar.MINUTE, 0);
                date = dateFormat.format(today.getTime());
            }
        }while (!valid);

        DateTimeMrJeff dateTime = new DateTimeMrJeff.Builder().generateMap().get(today.get(Calendar.DAY_OF_WEEK));
        String timeDay = dateTime.getHoursHome().get(0);
        today.set(Calendar.HOUR_OF_DAY, Integer.valueOf(timeDay.split(":")[0]));
        today.set(Calendar.MINUTE,Integer.valueOf(timeDay.split(":")[1]));

        return dateFormat.format(today.getTime());
    }

    public String minDateDelivery(String cp,String dateAux){
        Calendar today = Calendar.getInstance();
        long time = today.getTimeInMillis() + (MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY * getMinDays());
        today.setTimeInMillis(time);
        String pattern = "dd/MM/yyyy HH:mm";
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

        date = dateFormat.format(today.getTime());
        this.type =  MrJeffConstant.RESULT_SERVICE_DATE_INVALID.ONLY_SEND ;
        this.cp = cp;
        this.typeDate = DateTimeMrJeff.ENABLED_TYPE_ADDRESS_HOME;
        this.dateAux = dateAux;
        boolean valid = false;
        do {
            valid = isValid();
            if(!valid) {
                time = today.getTimeInMillis() + MrJeffConstant.ORDER_DAYS_SEND.MILSECONDS_DAY;
                today.setTimeInMillis(time);
                today.set(Calendar.HOUR_OF_DAY, 20);
                today.set(Calendar.MINUTE, 0);
                date = dateFormat.format(today.getTime());
            }
        }while (!valid);
        DateTimeMrJeff dateTime = new DateTimeMrJeff.Builder().generateMap().get(today.get(Calendar.DAY_OF_WEEK));
        String timeDay = dateTime.getHoursHome().get(0);
        today.set(Calendar.HOUR_OF_DAY, Integer.valueOf(timeDay.split(":")[0]));
        today.set(Calendar.MINUTE,Integer.valueOf(timeDay.split(":")[1]));

        return dateFormat.format(today.getTime());
    }

    public static boolean formatDateCorrect(String date){
        boolean result = false;
        try {
            String pattern = "dd/MM/yyyy HH:mm";
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
            Date dateTIme = dateFormat.parse(date);
            result = true;
        }catch (Exception e){

        }

        return result;
    }

}
